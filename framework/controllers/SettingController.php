<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\SettingsForm;
use app\models\PasswordForm;


class SettingController extends Controller
{

	function init()
	{
		if (\Yii::$app->user->isGuest)
		{
			return Yii::$app->getResponse()->redirect('/account/login');
		}
	}


	public function actionIndex($command='',$id=NULL)
	{
		$model_SettingsForm = new SettingsForm();
		if ($model_SettingsForm->load(Yii::$app->request->post()))
		{
			$model_SettingsForm->saveSettings();
		}
		else
		{
			$model_SettingsForm->username = Yii::$app->user->getIdentity()->username;
			$model_SettingsForm->phone = Yii::$app->user->getIdentity()->phone;
			$model_SettingsForm->skype = Yii::$app->user->getIdentity()->skype;
		}
		return $this->render('view__index', ['model' => $model_SettingsForm]);
	}





	public function actionPassword($command='',$id=NULL)
	{
		$model_PasswordForm = new PasswordForm();
		if ($model_PasswordForm->load(Yii::$app->request->post()))
		{
			$model_PasswordForm->changePassword();
			$data = \Yii::$app->request->post('PasswordForm', []);
			return $this->render('/blocks/view__change_password_confirm',$data);
		}
		return $this->render('view__password', ['model' => $model_PasswordForm]);
	}


}
