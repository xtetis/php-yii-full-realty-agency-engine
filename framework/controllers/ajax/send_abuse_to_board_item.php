<?
use app\models\LibCommon;
use app\models\LibSql;
use app\models\RegisterForm;
use app\models\LoginForm;
use app\models\gii\xta_user_message;
use app\models\gii\xta_obj_abuse;
use app\models\gii\xta_user;
use app\models\gii\xta_cache;
use app\models\Email;










$add_message = '';

if (!Yii::$app->user->id)
{
	if (
	      isset($_POST['mes']) &&
	      isset($_POST['email']) &&
	      isset($_POST['phone']) &&
	      isset($_POST['username'])
	   )
	{
	
		$pass  = LibCommon::fn__get_random_hash(8);
		$email = trim($_POST['email']);
		$phone = trim($_POST['phone']);
		$username = trim($_POST['username']);
		
		$accesstoken = md5(md5($email).md5($pass));
		
		
		
		
		
		$model_register_form = new RegisterForm();
		$model_register_form->email = $email;
		$model_register_form->pass  = $pass;
		$model_register_form->pass_repeat = $pass;
		$model_register_form->username = $username;
		$model_register_form->phone = $phone;
		
		if ($model_register_form->validate())
		{
			$model_xta_user = new xta_user();
	
			$model_xta_user->email = $email;
			$model_xta_user->pass = md5(md5($pass));
			$model_xta_user->username = $username;
			$model_xta_user->phone = $phone;
			$model_xta_user->id_user_role = 2;
			$model_xta_user->skype = '';
			$model_xta_user->accesstoken = $accesstoken;
			
			if ($model_xta_user->validate())
			{
				$model_xta_user->save();
				$model_LoginForm = new LoginForm();
				$model_LoginForm->email = $email;
				$model_LoginForm->pass = $pass;
				$model_LoginForm->rememberMe = true;
				$model_LoginForm->login();
				Email::fn__send_register_notification_to_user(Yii::$app->user->id,$pass);
				$add_message = 'Зарегистрирован пользователь <b>'.$email.'</b> c паролем <b>'.$pass.'</b>. Уведомление о регистрации отправлено на почту. ';
			}
			else
			{
				if (count($model_xta_user->getErrors()))
				{
					foreach ($model_xta_user->getErrors() as  $error)
					{
						$ret = array(
							'result'=>'error',
							'message'=>$error[0],
						);
						echo json_encode($ret);	exit;
					}
				}
				$ret = array(
					'result'=>'error',
					'message'=>'Обнаружена ошибка при добавлении пользователя',
				);
				echo json_encode($ret);	exit;
			}
		}
		else
		{
			if (count($model_register_form->getErrors()))
			{
				foreach ($model_register_form->getErrors() as  $error)
				{
					$ret = array(
						'result'=>'error',
						'message'=>$error[0],
					);
					echo json_encode($ret);	exit;
				}
			}
			$ret = array(
				'result'=>'error',
				'message'=>'Обнаружена ошибка при проверке пользователя',
			);
			echo json_encode($ret);	exit;
		}
	}
}










if (Yii::$app->user->id)
{
	if (isset($_POST['mes']))
	{
		$message = $_POST['mes'];
		
		if (mb_strlen($message,'utf-8')<6)
		{
			$ret = array(
				'result'=>'error',
				'message'=>$add_message.'Cообщение не может быть короче 6 символов',
			);
			echo json_encode($ret);	exit;
		}
		
		$id_board_item = intval($_POST['id_board_item']);
		if (!$id_board_item)
		{
			$ret = array(
				'result'=>'error',
				'message'=>$add_message.'Такого объявления не существует',
			);
			echo json_encode($ret);	exit;
		}
		
		$model_xta_obj_abuse = new xta_obj_abuse();
		$model_xta_obj_abuse->id_obj = $id_board_item;
		$model_xta_obj_abuse->id_user = Yii::$app->user->id;
		$model_xta_obj_abuse->message = $message;
		$model_xta_obj_abuse->save();
		setcookie("clearcache", 1, time()+3600, "/");
		
		$ret = array(
			'result'=>'ok',
			'message'=>$add_message.'Ваша жалоба отправлена',
		);
		echo json_encode($ret); exit;
	}
	else
	{
		$ret = array(
			'result'=>'error',
			'message'=>$add_message.'Жалоба не отправлена',
		);
		echo json_encode($ret);	exit;
	}
}



