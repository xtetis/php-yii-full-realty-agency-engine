<?
use app\models\LibCommon;
use app\models\LibSql;
use app\models\mgii\mgii_xta_album;
use app\models\mgii\mgii_xta_image;
use app\models\mgii\mgii_xta_love_album;
use app\models\mgii\mgii_xta_love_anketa;


//echo $command;
//print_r($_POST);

//==============================================================================
if (
     (
       ($command == 'create') ||
       ($command == 'edit')
     ) &&
     (isset($_POST['name'])) &&
     (isset($_POST['hidden'])) &&
     (isset($_POST['id_album']))
   )
{
	//echo 'zzzzzz';
	$id_album = intval($_POST['id_album']);
	$hidden = intval($_POST['hidden']);
	$name = $_POST['name'];
	
	if ($id_album)
	{
		//$model__xta_love_album = mgii_xta_love_album::find()->where(["id_album" => $id_album])->one();
		$model__xta_love_album = mgii_xta_love_album::findOne($id_album);
		$model__xta_album = mgii_xta_album::findOne($model__xta_love_album->id_album);
		//$model__xta_album = mgii_xta_album::findOne($id_album);
		
		if ($model__xta_love_album && $model__xta_album)
		{
			$model__xta_love_album->name = $name;
			$model__xta_love_album->hidden = $hidden;
			if ($model__xta_love_album->validate())
			{
				$model__xta_love_album->save();
				$ret = array(
					'result'=>'ok',
					'message'=>'Изменения сохранены',
				);
				echo json_encode($ret);	exit;
			}
			else
			{
				foreach ($model__xta_love_album->errors as $field => $errors) {
					$ret = array(
						'result'=>'error',
						'message'=>$errors[0],
					);
					echo json_encode($ret);	exit;
				}
			}
		}
	}
	else
	{

		
		$model__xta_love_album = new mgii_xta_love_album();
		$model__xta_love_album->name = $name;
		$model__xta_love_album->hidden = $hidden;
		$model__xta_love_album->id_user = Yii::$app->user->id;
		$model__xta_love_album->validate();
		foreach ($model__xta_love_album->errors as $field => $errors) {
			if ($field!='id_album')
			{
				$ret = array(
					'result'=>'error',
					'message'=>$errors[0],
				);
				echo json_encode($ret);	exit;
			}
		}
		
		$model_xta_album = new mgii_xta_album();
		$model_xta_album->save();
		$model__xta_love_album->id_album = $model_xta_album->id;
		
		if ($model__xta_love_album->validate())
		{
			$model__xta_love_album->save();
			$ret = array(
				'result'=>'ok',
				'message'=>'Изменения сохранены',
			);
			echo json_encode($ret);	exit;
		}
		else
		{
			foreach ($model__xta_love_album->errors as $field => $errors) {
				$ret = array(
					'result'=>'error',
					'message'=>$errors[0],
				);
				echo json_encode($ret);	exit;
			}
			//print_r($model__xta_love_album->errors);
		}
	}
	
}
//==============================================================================











//==============================================================================
if (
     ($command == 'upload') &&
     (isset($_POST['id_album'])) &&
     (isset($_POST['img']))
   )
{
	$id_album = intval($_POST['id_album']);
	
	$model__xta_love_album = mgii_xta_love_album::findOne($id_album);
	//echo $id_album.'=';
	if (
	     (!$model__xta_love_album) ||
	     ($model__xta_love_album->id_user!=Yii::$app->user->id)
	   )
	{
		$ret = array(
			'result'=>'error',
			'message'=>'Неверный альбом пользователя',
		);
		echo json_encode($ret);	exit;
	}
	
	//echo $model__xta_love_album->id_album.'=';
	$model__xta_album = mgii_xta_album::findOne($model__xta_love_album->id_album);

	if (!$model__xta_album)
	{
		$ret = array(
			'result'=>'error',
			'message'=>'Такого альбома не существует',
		);
		echo json_encode($ret);	exit;
	}
	
	$model_xta_image = new mgii_xta_image();
	$model_xta_image->id_album = $model__xta_album->id;
	$model_xta_image->fn__save_base64_to_img($_POST['img']);
	$model_xta_image->save();
	
	$model__xta_love_anketa = mgii_xta_love_anketa::find()->where(["id_user" =>Yii::$app->user->id])->one();
	if (!$model__xta_love_anketa->id_image)
	{
		$model__xta_love_anketa->id_image = $model_xta_image->id;
		$model__xta_love_anketa->save();
	}
	
	$ret = array(
		'result'=>'ok',
		'message'=>'Изображение добавлено',
	);
	echo json_encode($ret);	exit;
}
//==============================================================================















