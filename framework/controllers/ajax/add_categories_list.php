<?
use app\models\gii\xta_obj_category;

$model_xta_obj_category = xta_obj_category::find()->all();
$categories_all = array();
foreach ($model_xta_obj_category as $item)
{
	$data_xta_obj_category[] = $item->getAttributes(null);
}

foreach ($data_xta_obj_category as $item1)
{
	if ($item1['id_parent']==0)
	{
		$item1['parent_depth'] = 0;
		$item1['has_children'] = xta_obj_category::hasChildrenArray($data_xta_obj_category,$item1);
		$categories_all[]=$item1;
		foreach ($data_xta_obj_category as $item2)
		{
			if ($item2['id_parent']==$item1['id'])
			{
				$item2['parent_depth'] = 1;
				$item2['has_children'] = xta_obj_category::hasChildrenArray($data_xta_obj_category,$item2);
				$categories_all[]=$item2;
				foreach ($data_xta_obj_category as $item3)
				{
					if ($item3['id_parent']==$item2['id'])
					{
						$item3['parent_depth'] = 2;
						$item3['has_children'] = xta_obj_category::hasChildrenArray($data_xta_obj_category,$item3);
						$categories_all[]=$item3;
					}
				}
			}
		}
	}
}

?>
<div class="row board_add_id_categiry_row1">
	<div class="col-md-4 column_head_level_0">
		Рубрика
	</div>
	<div class="col-md-4 column_head_level_1">
	</div>
	<div class="col-md-4 column_head_level_2">
	</div>
</div>
<div class="row board_add_id_categiry_row2">
	<div class="col-md-4">
		<?
			foreach ($categories_all as $item)
			{
				if ($item['parent_depth']==0)
				{?>
<div class="add_categories_list 
            level_<?=$item['parent_depth']?>
            parent_<?=$item['id_parent']?>
            <?=(($item['has_children'])?'':'nochild')?>"
     title="<?=$item['name']?>"
     parent="<?=$item['id_parent']?>"
     idx="<?=$item['id']?>">
  <?=$item['name']?>
  <?if ($item['has_children']){?>
  	<i class="glyphicon glyphicon-arrow-right" style="font-size:11px;"></i>
  <?}else{?>
	  <i class="glyphicon glyphicon-ok" style="font-size:11px;"></i>
  <?}?>
</div>
				<?}
			}
		?>
	</div>
	<div class="col-md-4">
		<?
			foreach ($categories_all as $item)
			{
				if ($item['parent_depth']==1)
				{?>
<div class="add_categories_list 
            level_<?=$item['parent_depth']?>
            parent_<?=$item['id_parent']?>
            <?=(($item['has_children'])?'':'nochild')?>"
     title="<?=$item['name']?>"
     parent="<?=$item['id_parent']?>"
     idx="<?=$item['id']?>">
  <?=$item['name']?>
  <?if ($item['has_children']){?>
  	<i class="glyphicon glyphicon-arrow-right" style="font-size:11px;"></i>
  <?}else{?>
	  <i class="glyphicon glyphicon-ok" style="font-size:11px;"></i>
  <?}?>
</div>
				<?}
			}
		?>
	</div>
	<div class="col-md-4">
		<?
			foreach ($categories_all as $item)
			{
				if ($item['parent_depth']==2)
				{?>
<div class="add_categories_list 
            level_<?=$item['parent_depth']?>
            parent_<?=$item['id_parent']?>
            <?=(($item['has_children'])?'':'nochild')?>"
     title="<?=$item['name']?>"
     parent="<?=$item['id_parent']?>"
     idx="<?=$item['id']?>">
  <?=$item['name']?>
  <?if ($item['has_children']){?>
  	<i class="glyphicon glyphicon-arrow-right" style="font-size:11px;"></i>
  <?}else{?>
	  <i class="glyphicon glyphicon-ok" style="font-size:11px;"></i>
  <?}?>
</div>
				<?}
			}
		?>
	</div>
</div>
