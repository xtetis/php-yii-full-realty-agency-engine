<?
use app\models\LibCommon;
use app\models\LibSql;
use yii\db\Query;
$connection = \Yii::$app->db;


	$id = intval($id);

  $sql="UPDATE `xta_user_message` SET `readed`=1 WHERE 
        (`id_user_to` = ".Yii::$app->user->id." AND `id_user_from`=".$id.")
        AND `readed`=0";
	$model = $connection->createCommand($sql);
	$user_from = $model->query();




// Пагинация
//==============================================================================
$sql = "SELECT 
          count(*) as 'count'
        FROM `xta_user_message`
        WHERE 
          (
           `xta_user_message`.`id_user_to` = ".Yii::$app->user->id." 
             AND 
           `xta_user_message`.`id_user_from`=".$id."
          )
          OR
          (
           `xta_user_message`.`id_user_to` = ".$id." 
             AND 
           `xta_user_message`.`id_user_from`=".Yii::$app->user->id.")";



$model = $connection->createCommand($sql);
$count = $model->queryOne();
$count = intval($count['count']);
$maxpage            = ceil($count/Yii::$app->params['per_pages']);
$page               = LibCommon::fn__get_correct_page($maxpage);
$data['pagination'] = LibCommon::fn__get_html_pagination('/ajax/get_dialog_message_list/'.$id.'?page=', $maxpage, $page);
if ($maxpage==1)
{
	$data['pagination'] = '';
}
//==============================================================================








//==============================================================================
  $sql = "SELECT 
            `xta_user_message`.*,
            t3.username as from_username
          FROM `xta_user_message`
          LEFT JOIN `xta_user` t3 ON
                    t3.id = `xta_user_message`.`id_user_from`
          WHERE 
            (
             `xta_user_message`.`id_user_to` = ".Yii::$app->user->id." 
               AND 
             `xta_user_message`.`id_user_from`=".$id."
            )
            OR
            (
             `xta_user_message`.`id_user_to` = ".$id." 
               AND 
             `xta_user_message`.`id_user_from`=".Yii::$app->user->id.")
          ORDER BY `xta_user_message`.`createdon` DESC
          LIMIT ".(($page-1)*10)." , 10
          ";



$model = $connection->createCommand($sql);
$messages = $model->queryAll();

$data['message_list'] = array();
foreach ($messages as $messages_item)
{
	$data['message_list'][]=$messages_item;
}
$data['messages'] = $messages;
//==============================================================================


echo $this->render('view__get_dialog_message_list', $data);
