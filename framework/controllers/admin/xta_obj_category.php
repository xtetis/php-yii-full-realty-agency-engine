<?
use app\models\gii\xta_obj_category;
use app\models\gii\xta_obj_category__search;
use yii\data\ActiveDataProvider;

$data['command'] = $command;
$data['id'] = $id;
$data['action_name'] = $this->action->id;


if ($command=='select')
{

$categories_all = array();
$categories_root = xta_obj_category::find()->where(['id_parent' => 0])->all();

foreach ($categories_root as $categories_root_item)
{
	$categories_all[]=$categories_root_item;
	$categories_child = xta_obj_category::find()->where(['id_parent' => $categories_root_item->id])->all();
	foreach ($categories_child as $categories_child_item)
	{
		$categories_all[]=$categories_child_item;
		$categories_child_child = xta_obj_category::find()->where(['id_parent'=>$categories_child_item->id])->all();
		foreach ($categories_child_child as $categories_child_child_item)
		{
			$categories_all[]=$categories_child_child_item;
			$categories_child_child_child = xta_obj_category::find()->where(['id_parent' => $categories_child_child_item->id])->all();
		}
	}

}

$data['categories_all'] = $categories_all;

}
elseif($command=='create')
{
	$model = new xta_obj_category();

	if ($model->load(Yii::$app->request->post()) && $model->save()) 
	{
		return Yii::$app->response->redirect(array('/admin/'.$this->action->id.'/update/'.$model->id)); 
	}
	else
	{
		$data['model'] = $model;
	}
}
elseif($command=='update')
{
	$model = xta_obj_category::findOne($id);
	if ($model->load(Yii::$app->request->post()))
	{
		$model_parent = xta_obj_category::findOne($model->id_parent);
		if (($model->id_parent)&&($model_parent))
		{
			if (($model->getChildrenDepth()+$model_parent->getParentDepth())>1)
			{
				$model->addError('id_parent', 'Слишком большая вложенность');
			}
		}

		if ($model->validate(null, false))
		{
			$model->save();
		}
	}
	$data['model'] = $model;
}
elseif($command=='delete')
{
	xta_obj_category::findOne($id)->delete();
	return Yii::$app->response->redirect(array('/admin/'.$this->action->id)); 
}


echo $this->render('view__'.$this->action->id,$data);
?>
