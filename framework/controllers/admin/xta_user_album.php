<?
use app\models\gii\xta_user_album;
use app\models\gii\xta_user_album__search;
use app\models\gii\xta_album;
use app\models\gii\xta_image;
use app\models\UploadForm;
use yii\web\UploadedFile;

$data['command'] = $command;
$data['id'] = $id;
$data['action_name'] = $this->action->id;


if ($command=='select')
{
	$searchModel = new xta_user_album__search();
	$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
	$data['dataProvider'] = $dataProvider;
	$data['searchModel'] = $searchModel;
}
elseif($command=='create')
{
	$model = new xta_user_album();

	if ($model->load(Yii::$app->request->post()) && $model->save()) 
	{
		$model_xta_album = new xta_album();
		$model_xta_album->save();
		$model->id_album = $model_xta_album->id;
		$model->save();
		return Yii::$app->response->redirect(array('/admin/'.$this->action->id.'/update/'.$model->id)); 
	}
	else
	{
		$data['model'] = $model;
	}
}
elseif($command=='update')
{

	if (
	     (isset($_GET['delimage'])) &&
	     (intval($_GET['delimage']))
	   )
	{
		$model__xta_image = xta_image::findOne(intval($_GET['delimage']));
		if ($model__xta_image)
		{
			$model__xta_image->fn__delete_image();
			return Yii::$app->response->redirect(array('/admin/'.$this->action->id.'/update/'.$id)); 
		}
	}

	$model = xta_user_album::findOne($id);
	$model_UploadForm = new UploadForm();
	$data['model_UploadForm'] = $model_UploadForm;
	$model_xta_album = xta_album::findOne($model->id_album);
	$data['model__xta_image_many'] = array();

	


	
	
	
	if (Yii::$app->request->isPost)
	{
		$model_UploadForm->imageFiles = UploadedFile::getInstances($model_UploadForm, 'imageFiles');
		if ($model_UploadForm->validate())
		{
			foreach ($model_UploadForm->imageFiles as $file)
			{
				$file->saveAs($_SERVER['DOCUMENT_ROOT'].'/public/temp/tmp.jpg');
				$filedata = file_get_contents($_SERVER['DOCUMENT_ROOT'].'/public/temp/tmp.jpg');
				$filedata = 'data:image/png;base64,'.base64_encode($filedata);

				$model_xta_image = new xta_image();
				$model_xta_image->id_album = $model_xta_album->id;
				$model_xta_image->fn__save_base64_to_img($filedata);
				$model_xta_image->save();
				
			}
			
		}
	}
	
	
	if ($model_xta_album)
	{
		$data['model__xta_image_many'] = $model_xta_album->fn__get_xta_image_many()->all();
	}
	
	
	
	
	if ($model->load(Yii::$app->request->post()))
	{
		$model->save();
	}
	
	$data['model'] = $model;
	$data['model_xta_album'] = $model_xta_album;
}
elseif($command=='delete')
{
	xta_user_album::findOne($id)->delete();
	return Yii::$app->response->redirect(array('/admin/'.$this->action->id)); 
}


echo $this->render('view__'.$this->action->id,$data);
?>
