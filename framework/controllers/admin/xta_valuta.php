<?
use app\models\gii\xta_valuta;
use app\models\gii\xta_valuta__search;

$data['command'] = $command;
$data['id'] = $id;
$data['action_name'] = $this->action->id;


if ($command=='select')
{
	$searchModel = new xta_valuta__search();
	$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
	$data['dataProvider'] = $dataProvider;
	$data['searchModel'] = $searchModel;
}
elseif($command=='create')
{
	$model = new xta_valuta();

	if ($model->load(Yii::$app->request->post()) && $model->save()) 
	{
		return Yii::$app->response->redirect(array('/admin/'.$this->action->id.'/update/'.$model->id)); 
	}
	else
	{
		$data['model'] = $model;
	}
}
elseif($command=='update')
{
	$model = xta_valuta::findOne($id);
	if ($model->load(Yii::$app->request->post()))
	{
		$model->save();
	}
	$data['model'] = $model;
}
elseif($command=='delete')
{
	xta_valuta::findOne($id)->delete();
	return Yii::$app->response->redirect(array('/admin/'.$this->action->id)); 
}


echo $this->render('view__'.$this->action->id,$data);
?>
