<?
use app\models\gii\xta_user_role;
use app\models\gii\xta_user_role__search;

$data['command'] = $command;
$data['id'] = $id;
$data['action_name'] = $this->action->id;


if ($command=='select')
{
	$searchModel = new xta_user_role__search();
	$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
	$data['dataProvider'] = $dataProvider;
	$data['searchModel'] = $searchModel;
}
elseif($command=='create')
{
	$model = new xta_user_role();

	if ($model->load(Yii::$app->request->post()) && $model->save()) 
	{
		return Yii::$app->response->redirect(array('/admin/'.$this->action->id.'/update/'.$model->id)); 
	}
	else
	{
		$data['model'] = $model;
	}
}
elseif($command=='update')
{
	$model = xta_user_role::findOne($id);
	if ($model->load(Yii::$app->request->post()))
	{
		$model->save();
	}
	$data['model'] = $model;
}
elseif($command=='delete')
{
	xta_user_role::findOne($id)->delete();
	return Yii::$app->response->redirect(array('/admin/'.$this->action->id)); 
}


echo $this->render('view__'.$this->action->id,$data);
?>
