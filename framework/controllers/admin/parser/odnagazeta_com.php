<?

use app\models\LibCommon;
use app\models\LibSql;
use app\models\gii\xta_obj;
use app\models\gii\xta_image;
use app\models\gii\xta_album;
use app\models\gii\xta_site;
use yii\imagine\Image;



$donext=true;
//$only_with_images=false;
$only_with_images=false;
$minimum_length_obj_text=70;
$data['command'] = $command;
require_once $_SERVER['DOCUMENT_ROOT'].'/public/components/php/classes/simple_html_dom.php';
set_time_limit(0);

function fn_repeat_parse($url){
$timeout=5000;
return '
<script>
  var interval='.$timeout.';
  var stro=\'Повторить парсинг ↺ - \';
  var loc=\''.$url.'\';


  var totalseconds=parseInt(interval/1000);
  var counter=totalseconds;
  function fn__timer(){
    if (counter<=0){
    location.href=loc;
    clearInterval(timer);
    }
    $(\'#parse_next\').text(stro+counter);
    counter--;
  }
  var timer = setInterval("fn__timer();", 1000);
</script>

<div style="text-align:center; padding:2px;">
 <div class="btn-group btn-group-sm">
   <a onclick="clearInterval(timer)" href="'.$url.'"
      class="btn btn-primary" id="parse_next"
      style="color:#fff;">Повторить удаление ↺</a>
   <a onclick="clearInterval(timer)"
      class="btn  btn-warning"
      id="parse_next">Пауза</a>
 </div>
</div>';
}





function fn_set_set($name,$value){
  $filename = $_SERVER['DOCUMENT_ROOT'].'/public/temp/odnagazeta_com/settings/settings.ser';
  if (file_exists($filename)){
     $set = file_get_contents($filename);
     $set = unserialize($set);
     }
  $set[$name]=$value;
  $set = serialize($set);
  file_put_contents($filename,$set);
}


function fn_get_set($name){
  $filename = $_SERVER['DOCUMENT_ROOT'].'/public/temp/odnagazeta_com/settings/settings.ser';
  if (!file_exists($filename)) return '';
  $set = file_get_contents($filename);
  $set = unserialize($set);
  return $set[$name];
}












//Удаляем спарсенное
//**************************************************************************************************
if ((isset($_GET['do']))&&($_GET['do']=='clearparse')){
	$console.='<div>Стираем данные парсинга</div>';

  $filename = $_SERVER['DOCUMENT_ROOT'].'/public/temp/odnagazeta_com/data/nav_pages_list.txt';
	if (file_exists($filename))
	unlink($filename);
	$console.='<div>Удалили список постраничной навигации</div>';

  if($handle = opendir($_SERVER['DOCUMENT_ROOT'].'/public/temp/odnagazeta_com/data/nav_pages_list/')){
     while(false !== ($file = readdir($handle)))
       if($file != "." && $file != "..")
         unlink($_SERVER['DOCUMENT_ROOT'].'/public/temp/odnagazeta_com/data/nav_pages_list/'.$file);
       closedir($handle);
    }


  $filename = $_SERVER['DOCUMENT_ROOT'].'/public/temp/odnagazeta_com/data/obj_pages_list.txt';
	if (file_exists($filename))
	unlink($filename);
	$console.='<div>Удалили список адресов объявлений</div>';

  if($handle = opendir($_SERVER['DOCUMENT_ROOT'].'/public/temp/odnagazeta_com/data/obj_pages_list/')){
     while(false !== ($file = readdir($handle)))
       if($file != "." && $file != "..")
         unlink($_SERVER['DOCUMENT_ROOT'].'/public/temp/odnagazeta_com/data/obj_pages_list/'.$file);
       closedir($handle);
    }
	$console.='<div>Удалили скачанные страницы объявлений</div>';


  if($handle = opendir($_SERVER['DOCUMENT_ROOT'].'/public/temp/odnagazeta_com/data/obj_pages_parsed/')){
     while(false !== ($file = readdir($handle)))
       if($file != "." && $file != "..")
         unlink($_SERVER['DOCUMENT_ROOT'].'/public/temp/odnagazeta_com/data/obj_pages_parsed/'.$file);
       closedir($handle);
    }
	$console.='<div>Удалили распарсенные страницы объявлений</div>';


  $filename = $_SERVER['DOCUMENT_ROOT'].'/public/temp/odnagazeta_com/data/sql.txt';
	if (file_exists($filename))
	unlink($filename);
	$console.='<div>Удалили SQL</div>';


  if($handle = opendir($_SERVER['DOCUMENT_ROOT'].'/public/temp/odnagazeta_com/data/images/')){
     while(false !== ($file = readdir($handle)))
       if($file != "." && $file != "..")
         unlink($_SERVER['DOCUMENT_ROOT'].'/public/temp/odnagazeta_com/data/images/'.$file);
       closedir($handle);
    }
	$console.='<div>Удалили картинки объявлений</div>';

}
//**************************************************************************************************








// Сохраняем и читаем настройки
//==============================================================================
if (isset($_POST['settings_path'])){
   $settings_path = trim($_POST['settings_path']);
   $settings_pagecount = intval($_POST['settings_pagecount']);
   $settings_id_category = intval($_POST['settings_id_category']);
   $settings_id_user = intval($_POST['settings_id_user']);
   fn_set_set('settings_path',$settings_path);
   fn_set_set('settings_pagecount',$settings_pagecount);
   fn_set_set('settings_id_category',$settings_id_category);
   fn_set_set('settings_id_user',$settings_id_user);
   }

$data['settings_path']=fn_get_set('settings_path');
$data['settings_pagecount']=fn_get_set('settings_pagecount');
$data['settings_id_category']=fn_get_set('settings_id_category');
$data['settings_id_user']=fn_get_set('settings_id_user');
$console.='<div>Настройки: URI = http://odnagazeta.com'.$data['settings_path'].'</div>';
$console.='<div>Настройки: страниц = '.$data['settings_pagecount'].'</div>';
$console.='<div>Настройки: категория для импорта = '.$data['settings_id_category'].'</div>';
$console.='<div>Настройки: юзер для импорта = '.$data['settings_id_user'].'</div>';
if (
     (!$data['settings_pagecount']) ||
     (!strlen($data['settings_path'])) ||
     (!$data['settings_id_category']) ||
     (!$data['settings_id_user'])
   ){
   $console.='<div>Парсить нельзя, сначала установите настройки</div>';
   $donext=false;
   }
//==============================================================================






//1. Генерируем страницы навигации
//==============================================================================
if (($id==1)&&($donext)){
   $nav_pages_list = array();
   $nav_pages_list[]='http://odnagazeta.com'.$data['settings_path'];
   for ($i = 2; $i <= $data['settings_pagecount']; $i++){
     $nav_pages_list[]='http://odnagazeta.com'.$data['settings_path'].'?pages='.$i;
     }
   $filename = $_SERVER['DOCUMENT_ROOT'].'/public/temp/odnagazeta_com/data/nav_pages_list.txt';
   file_put_contents($filename,serialize($nav_pages_list));
   $console.='<div>Сгенерировано '.$data['settings_pagecount'].' страниц навигации</div>';
}
//==============================================================================



//2. Скачиваем страницы навигации
//==============================================================================
if (($id==2)&&($donext)){
   $filename = $_SERVER['DOCUMENT_ROOT'].'/public/temp/odnagazeta_com/data/nav_pages_list.txt';
   $nav_pages_list = unserialize(file_get_contents($filename));
   $i=0; $j=0;
   foreach ($nav_pages_list as $item){
     $item_filename=$_SERVER['DOCUMENT_ROOT'].'/public/temp/odnagazeta_com/data/nav_pages_list/'.$i.'.txt';
     if (!file_exists($item_filename)){
        file_put_contents($item_filename,@file_get_contents($item));
        sleep(1);
        $j++;
        }
     $i++;
     if ($j>=30){
         $console.='<div>Скачано '.$i.' из '.count($nav_pages_list).' страниц навигации</div>';
         $data["repeat_parse"]=fn_repeat_parse('/admin/parser/'.$command.'/2');
         break;
        }

     }
   $console.='<div>Скачано '.$data['settings_pagecount'].' страниц навигации</div>';
}
//==============================================================================




//3. Парсим страницы навигации
//==============================================================================
if (($id==3)&&($donext)){
   $obj_pages_list = array();
   $filename = $_SERVER['DOCUMENT_ROOT'].'/public/temp/odnagazeta_com/data/nav_pages_list.txt';
   $nav_pages_list = unserialize(file_get_contents($filename));
   $i=0;
   foreach ($nav_pages_list as $item){
     $item_filename=$_SERVER['DOCUMENT_ROOT'].'/public/temp/odnagazeta_com/data/nav_pages_list/'.$i.'.txt';
     $item_content = file_get_contents($item_filename);
     $html = str_get_html($item_content);
     foreach($html->find('a.objText') as $item_link){
        $obj_pages_list[]='http://odnagazeta.com'.$item_link->href;
      }
     $html->clear();
     unset($html);
     $i++;
     }
   $filename = $_SERVER['DOCUMENT_ROOT'].'/public/temp/odnagazeta_com/data/obj_pages_list.txt';
   file_put_contents($filename,serialize($obj_pages_list));
   $console.='<div>Получено '.count($obj_pages_list).' ссылок на страницы с объявлениями</div>';
}
//==============================================================================








//4. Скачиваем страницы объявлений
//==============================================================================
if (($id==4)&&($donext)){
   $filename = $_SERVER['DOCUMENT_ROOT'].'/public/temp/odnagazeta_com/data/obj_pages_list.txt';
   $obj_pages_list = unserialize(file_get_contents($filename));
   $i=0; $j=0;
   foreach ($obj_pages_list as $item){
     $item_filename=$_SERVER['DOCUMENT_ROOT'].'/public/temp/odnagazeta_com/data/obj_pages_list/'.$i.'.txt';
     if (!file_exists($item_filename)){
        file_put_contents($item_filename,file_get_contents($item));
        sleep(1);
        $j++;
        if ($j>=10){
           $console.='<div>Скачано '.$i.' из '.count($obj_pages_list).' объявлений, продолжите парсинг</div>';
           $data["repeat_parse"]=fn_repeat_parse('/admin/parser/'.$command.'/4');
           break;
           }
        }
     $i++;
     }
   if (($i)==count($obj_pages_list)){
      $console.='<div>Скачивание страниц объявлений завершено</div>';
      }
}
//==============================================================================








//5. Парсим страницы объявлений
//==============================================================================
if (($id==5)&&($donext)){
   $filename = $_SERVER['DOCUMENT_ROOT'].'/public/temp/odnagazeta_com/data/obj_pages_list.txt';
   $obj_pages_list = unserialize(file_get_contents($filename));
   $i=0; $j=0;
   foreach ($obj_pages_list as $item){
     $obj_text_item= array();
     $obj_text_item['text']='';
     $obj_text_item['phone']='';
     $obj_text_item['source']=$item;
     $obj_text_item['date']='';
     $obj_text_item['images']=array();
     $item_filename=$_SERVER['DOCUMENT_ROOT'].'/public/temp/odnagazeta_com/data/obj_pages_list/'.$i.'.txt';
     $filename_p = $_SERVER['DOCUMENT_ROOT'].'/public/temp/odnagazeta_com/data/obj_pages_parsed/'.$i.'.txt';
     if (file_exists($filename_p)){$i++; continue;}
     $item_content = file_get_contents($item_filename);
     $item_content = iconv('windows-1251','utf-8',$item_content);
     $html = str_get_html($item_content);
     foreach($html->find('span.objText') as $html_item){
        foreach($html_item->find('span') as $html_item_item){
           if (strpos($html_item_item,'Другой город')!==false){
              $html_item_item->outertext='';
              }
           if (strpos($html_item_item,'Кривой Рог')!==false){
              $html_item_item->outertext='';
              }
           $obj_text_item['text'] = strip_tags($html_item);
           }
        }


     foreach($html->find('div[class="tcenter ptop2 cdarkgray"]') as $html_item){
        $_arr = explode('<span',$html_item);
        $_date =trim(strip_tags($_arr[0]));
        if (strlen($_date)==10){
           $obj_text_item['date']=$_date;
           }
        }


     preg_match_all('/(?<!\w)(?:(?:(?:(?:\+?3)?8\W{0,5})?0\W{0,5})?[34569]\s?\d[^\w,;(\+]{0,5})?\d\W{0,5}\d\W{0,5}\d\W{0,5}\d\W{0,5}\d\W{0,5}\d\W{0,5}\d(?!(\W?\d))/x', $obj_text_item['text'], $pmres);
     $phone_array = $pmres[0];
     foreach ($phone_array as $phone_array_item){
       if (strlen(LibCommon::fn__get_only_numbers($phone_array_item))>=10){
          $obj_text_item['phone'] = LibCommon::fn__get_only_numbers($phone_array_item);
          }
       }

     foreach($html->find('a.image-zoom') as $html_image){
        $obj_text_item['images'][]= $html_image->href;
        }
     $html->clear();
     unset($html);

     //echo '<pre>'; print_r($obj_text_item); echo '</pre><br><br>';
     //if ($j>=5) exit;


     file_put_contents($filename_p,serialize($obj_text_item));

     if ($j>=30){
       $console.='<div>Распарсено '.$i.' из '.count($obj_pages_list).' объявлений, продолжите парсинг</div>';
       $data["repeat_parse"]=fn_repeat_parse('/admin/parser/'.$command.'/5');
       break;
       }

     $i++; $j++;
     }

   if (($i)==count($obj_pages_list)){
      $console.='<div>Парсинг страниц объявлений завершено</div>';
      }
}
//==============================================================================












//6. Скачиваем изображения
//==============================================================================
if (($id==6)&&($donext)){
   $filename = $_SERVER['DOCUMENT_ROOT'].'/public/temp/odnagazeta_com/data/obj_pages_list.txt';
   $obj_pages_list = unserialize(file_get_contents($filename));
   $i=0; $j=0;
   foreach ($obj_pages_list as $item){
     $filename_p = $_SERVER['DOCUMENT_ROOT'].'/public/temp/odnagazeta_com/data/obj_pages_parsed/'.$i.'.txt';
     $dataf = unserialize(file_get_contents($filename_p));
     if ((strlen($dataf['phone']))&&
        (strlen($dataf['date']))&&
        (mb_strlen($dataf['text'],'utf-8')>=$minimum_length_obj_text)&&
        (count($dataf['images']))
        &&(!LibSql::fn__get_count_by_where('xta_obj',"`source` = ".LibSql::sql_valid($item)))
        ){
        $img_counter=0;
        foreach ($dataf['images'] as $image_item){
          $md5 = md5($image_item);
          $filename_img=$_SERVER['DOCUMENT_ROOT'].'/public/temp/odnagazeta_com/data/images/'.$md5.'.jpg';
          if (!file_exists($filename_img)){
             $img_data = @file_get_contents($image_item);
             if (strlen($img_data)){
                $j++;
                file_put_contents($filename_img,$img_data);
                $original = Image::getImagine()->open($filename_img);
                $originalSize = $original->getSize();
                $width = $originalSize->getWidth();
                $height = $originalSize->getHeight();
                Image::crop($filename_img, $width, ($height-15), array(0,15))->save($filename_img, ['quality' => 50]);
                $console.='<div>Скачана картинка '.$image_item.'</div>';
                }
             }
          $img_counter++;
          if ($img_counter>=7){break;}
          }
        }
     $i++;

     if ($j>=30){
       $console.='<div>Скачаны картинки в '.$i.' из '.count($obj_pages_list).' объявлений</div>';
       $data["repeat_parse"]=fn_repeat_parse('/admin/parser/'.$command.'/6');
       break;
       }

     }
}
//==============================================================================












//7. Генерируем SQL
//==============================================================================
if (($id==7)&&($donext)){
   $filename = $_SERVER['DOCUMENT_ROOT'].'/public/temp/odnagazeta_com/data/obj_pages_list.txt';
   $obj_pages_list = unserialize(file_get_contents($filename));
   $i=0; $j=0;
   $total_sql="";
   foreach ($obj_pages_list as $item){
     $item_sql="";
     $filename_p = $_SERVER['DOCUMENT_ROOT'].'/public/temp/odnagazeta_com/data/obj_pages_parsed/'.$i.'.txt';
     $dataf = unserialize(file_get_contents($filename_p));
     $tmp_param = true;
     if (($only_with_images)&&(!count($dataf['images']))){
        $tmp_param = false;
        }

     if ((strlen($dataf['phone']))&&
        (strlen($dataf['date']))&&
        (mb_strlen($dataf['text'],'utf-8')>=$minimum_length_obj_text)&&
        ($tmp_param)&&
        (!LibSql::fn__get_count_by_where('xta_obj',"`source` = ".LibSql::sql_valid($item)))){
         $j++;
         $obj_name = trim($dataf['text']);
         $obj_name = mb_substr($obj_name, 0, mb_strpos($obj_name, ' ', 69,'utf-8'),'utf-8');
         $obj_date = implode('-',array_reverse(explode('.',$dataf['date'])));


					$model_xta_album = new xta_album();
					$model_xta_album->save();
					$console.='<div>Добавлен альбом '.$model_xta_album->id.'</div>';


					$img_counter=0;
					foreach ($dataf['images'] as $image_item)
					{
						$md5 = md5($image_item);
						$localpath = '/public/temp/odnagazeta_com/data/images/'.$md5.'.jpg';
						$console.='<div>Пробуем добавить изображение <a href="'.$localpath.'" target="_blank">img</a></div>';
						$filename_img=$_SERVER['DOCUMENT_ROOT'].$localpath;
						if (file_exists($filename_img))
						{
							$console.='<div>Пробуем добавить изображение</div>';
							$filedata = file_get_contents($filename_img);
							$filedata = 'data:image/png;base64,'.base64_encode($filedata);

							$model_xta_image = new xta_image();
							$model_xta_image->id_album = $model_xta_album->id;
							$model_xta_image->fn__save_base64_to_img($filedata);
							$model_xta_image->save();
							if (!$img_counter)
							{
								$model_xta_album->id_image = $model_xta_image->id;
								$model_xta_album->save();
							}
							$console.='<div>Добавлена картинка '.$model_xta_image->id.'</div>';
							$img_counter++; if ($img_counter>=7){break;}
						}
					}

					$model_xta_site = xta_site::getCurrentSite();
					$model_xta_obj = new xta_obj();

					$model_xta_obj->id_district = 0;
					$model_xta_obj->id_city     = $model_xta_site->id_city;
					$model_xta_obj->id_category = $data['settings_id_category'];
					$model_xta_obj->id_user     = $data['settings_id_user'];
					$model_xta_obj->id_album    = $model_xta_album->id;
					$model_xta_obj->id_valuta   = 1;
					$model_xta_obj->name        = $obj_name;
					$model_xta_obj->about       = $dataf['text'];
					$model_xta_obj->phone       = $dataf['phone'];
					//$model_xta_obj->pub_date    = new \yii\db\Expression('NOW()');
          $model_xta_obj->pub_date    = $obj_date;
          $model_xta_obj->createdon   = $obj_date;
					$model_xta_obj->published   = 1;
					$model_xta_obj->source      = $item;
					$model_xta_obj->save();
//					echo $obj_date.'<br>';
//          echo $dataf['date'].'<br>';
          //print_r($model_xta_obj);

					$console.='<div>Объявление '.$i.' <a href="/board/item/'.$model_xta_obj->id.'" target="_blank">#'.$model_xta_obj->id.'</a> добавлено в базу ('.$item.')</div><br><br>';

          //break;
        }else
        {
					$console.='<br><div>Объявление '.$i.' не соответствует необходимым условиям ('.$item.')</div>';
					$console.='<div>Телефон - '.strlen($dataf['phone']).' символов</div>';
					$console.='<div>Дата - '.strlen($dataf['date']).' символов</div>';
					$console.='<div>Текст - '.mb_strlen($dataf['text'],'utf-8').' символов</div>';
					$console.='<div>Разрешение по картинкам - '.intval($tmp_param).'</div>';
					$console.='<div>Уже находится в базе - '.LibSql::fn__get_count_by_where('xta_obj',"`source` = ".LibSql::sql_valid($item)).'</div><br><br>';


        }
     $i++;

     $total_sql.="\n\n\n".$item_sql;

     }

   //echo '<pre>'.$total_sql.'</pre>'; exit;
   $filename_sql = $_SERVER['DOCUMENT_ROOT'].'/public/temp/odnagazeta_com/data/sql.txt';
   file_put_contents($filename_sql,$total_sql);
   $console.='<div>Сгенерировано '.$j.' объявлений</div>';
}
//==============================================================================





$data['console'] = $console;
echo $this->render('view__parser__'.$command,$data);

?>
