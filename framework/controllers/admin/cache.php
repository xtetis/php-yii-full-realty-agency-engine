<?
use app\models\gii\xta_cache;
use app\models\gii\xta_cache_in_tag;
use app\models\gii\xta_cache_tag;

$data['command'] = $command;
$data['id'] = $id;
$data['action_name'] = $this->action->id;


if($command=='cleartag')
{
	$model__xta_cache_tag = xta_cache_tag::find()->where(['id'=>$id])->one();
	if ($model__xta_cache_tag)
	{
		xta_cache::fn__clear_tag($model__xta_cache_tag->name);
		return Yii::$app->getResponse()->redirect('/admin/cache');
	}
}

if($command=='clearall')
{
	xta_cache::fn__clear_all();
	return Yii::$app->getResponse()->redirect('/admin/cache');
}

if($command=='clearfile')
{
	Yii::$app->cache->flush();
	return Yii::$app->getResponse()->redirect('/admin/cache');
}



$model__xta_cache_tag_all = xta_cache_tag::find()->all();
$data['model__xta_cache_tag_all'] = $model__xta_cache_tag_all;

$counts_cache = array();
foreach ($model__xta_cache_tag_all as $item)
{
	//echo $item->name.'-';
	if (strpos($item->name,'board_item_')!==false)
	{
		continue;
	}
	$counts_cache[$item->id] = xta_cache_in_tag::find()->where(['id_tag'=>$item->id])->count();
}
$data['counts_cache'] = $counts_cache;


echo $this->render('view__'.$this->action->id,$data);
?>
