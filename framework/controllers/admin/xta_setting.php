<?
use app\models\gii\xta_site;
use app\models\gii\xta_setting;
use app\models\gii\xta_setting__search;

$data['command'] = $command;
$data['id'] = $id;
$data['action_name'] = $this->action->id;


if ($command=='select')
{
	$searchModel = new xta_setting__search();
	$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
	$data['dataProvider'] = $dataProvider;
	$data['searchModel'] = $searchModel;
}
elseif($command=='create')
{
	$model = new xta_setting();

	if ($model->load(Yii::$app->request->post()) && $model->save()) 
	{
		return Yii::$app->response->redirect(array('/admin/'.$this->action->id.'/update/'.$model->id)); 
	}
	else
	{
		$data['model'] = $model;
	}
}
elseif($command=='update')
{
	$model = xta_setting::findOne($id);
	if ($model->load(Yii::$app->request->post()))
	{
		$model_original = xta_setting::findOne($id);
		$original_value = unserialize($model_original->value);
		if (!is_array($original_value))
		{
			$original_value = [];
		}
		
		if ($model->allsite)
		{
			$original_value = [];
			$original_value[0] = $model->value;
		}
		else
		{
			if (isset($original_value[0]))
			{
				unset($original_value[0]);
			}
			$original_value[xta_site::getCurrentSiteId()] = $model->value;
		}
		$model->value = serialize($original_value);
		$model->save();
	}
	$model->value = unserialize($model->value);
	$model->allsite = 0;
	if (isset($model->value[xta_site::getCurrentSiteId()]))
	{
		$model->value = $model->value[xta_site::getCurrentSiteId()];
	}
	elseif(isset($model->value[0]))
	{
		$model->value = $model->value[0];
		$model->allsite = 1;
	}
	else
	{
		$model->value='';
	}
	$data['model'] = $model;
}
elseif($command=='delete')
{
	xta_setting::findOne($id)->delete();
	return Yii::$app->response->redirect(array('/admin/'.$this->action->id)); 
}


echo $this->render('view__'.$this->action->id,$data);
?>
