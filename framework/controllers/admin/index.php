<?
use yii\base\Model;
use yii\db\Query;

$data = array();

// Новые объявления
//==============================================================================
$count_days = 	15;
for ($i = 0; $i < $count_days; $i++)
{
	$query = new Query;
	$query->select(["DATE(DATE_ADD(NOW(), INTERVAL -".$i." DAY)) as 'date', COUNT(*) as 'count'"]);
	$query->from('xta_obj');
	$query->where("DATE(`createdon`) = DATE(DATE_ADD(NOW(), INTERVAL -".$i." DAY))");
	$command = $query->createCommand();
	$dataQuery = $command->queryAll();
	$data['count_obj'][$dataQuery[0]['date']] =  $dataQuery[0]['count'];
}

$data['count_obj'] = array_reverse($data['count_obj']);

foreach ($data['count_obj'] as $key => $value) {
$data['count_obj']['info'][0][] = '"'.$key.'"';
$data['count_obj']['info'][1][] = $value;
}
//==============================================================================




// Опубликованные объявления
//==============================================================================
$count_days = 	15;
for ($i = 0; $i < $count_days; $i++)
{
	$query = new Query;
	$query->select(["DATE(DATE_ADD(NOW(), INTERVAL -".$i." DAY)) as 'date', COUNT(*) as 'count'"]);
	$query->from('xta_obj');
	$query->where("DATE(`pub_date`) = DATE(DATE_ADD(NOW(), INTERVAL -".$i." DAY))");
	$command = $query->createCommand();
	$dataQuery = $command->queryAll();
	$data['count_obj_pub'][$dataQuery[0]['date']] =  $dataQuery[0]['count'];
}

$data['count_obj_pub'] = array_reverse($data['count_obj_pub']);

foreach ($data['count_obj_pub'] as $key => $value) {
$data['count_obj_pub']['info'][0][] = '"'.$key.'"';
$data['count_obj_pub']['info'][1][] = $value;
}
//==============================================================================





// Новые сообщения
//==============================================================================
$count_days = 	15;
for ($i = 0; $i < $count_days; $i++)
{
	$query = new Query;
	$query->select(["DATE(DATE_ADD(NOW(), INTERVAL -".$i." DAY)) as 'date', COUNT(*) as 'count'"]);
	$query->from('xta_user_message');
	$query->where("DATE(`createdon`) = DATE(DATE_ADD(NOW(), INTERVAL -".$i." DAY))");
	$command = $query->createCommand();
	$dataQuery = $command->queryAll();
	$data['count_message'][$dataQuery[0]['date']] =  $dataQuery[0]['count'];
}

$data['count_message'] = array_reverse($data['count_message']);

foreach ($data['count_message'] as $key => $value) {
$data['count_message']['info'][0][] = '"'.$key.'"';
$data['count_message']['info'][1][] = $value;
}
//==============================================================================





echo $this->render('view__index',$data);
?>
