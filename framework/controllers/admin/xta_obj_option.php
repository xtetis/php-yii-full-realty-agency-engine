<?
use app\models\gii\xta_obj_option;
use app\models\gii\xta_obj_option__search;
use app\models\gii\xta_obj_category;
use app\models\gii\xta_obj_option_in_category;
use app\models\gii\xta_obj_option_available;



$data['command'] = $command;
$data['id'] = $id;
$data['action_name'] = $this->action->id;


if ($command=='select')
{
	$searchModel = new xta_obj_option__search();
	$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
	$data['dataProvider'] = $dataProvider;
	$data['searchModel'] = $searchModel;
}
elseif($command=='create')
{
	$model = new xta_obj_option();

	if ($model->load(Yii::$app->request->post()) && $model->save()) 
	{
		return Yii::$app->response->redirect(array('/admin/'.$this->action->id.'/update/'.$model->id)); 
	}
	else
	{
		$data['model'] = $model;
	}
}
elseif($command=='update')
{
	$data['active_tab'] = 'option';
	$model = xta_obj_option::findOne($id);
	if (isset($_POST['in_category']))
	{
		$data['active_tab'] = 'categs';
		$in_category = Yii::$app->request->post();
		$in_category = $in_category['in_category'];
		//echo '<pre>'; print_r($in_category); echo '</pre>'; exit;
		Yii::$app->db->createCommand("UPDATE xta_obj_option_in_category 
		                              SET use_setting=0, use_filter=0
		                              WHERE id_obj_option=:id_obj_option")
			->bindValue(':id_obj_option', $id)
			->execute();
		if (is_array($in_category))
		{
			$category_all = xta_obj_category::find()->all();
			
			foreach ($category_all as $category_all_item)
			{
				$count = xta_obj_option_in_category::find()->where(['id_obj_option' => $id,'id_obj_category'=>$category_all_item->id])->count();
				
				if (!$count)
				{
					$xta_obj_option_in_category = new xta_obj_option_in_category();
					$xta_obj_option_in_category->id_obj_option = $id;
					$xta_obj_option_in_category->id_obj_category = $category_all_item->id;
					$xta_obj_option_in_category->use_setting = 0;
					$xta_obj_option_in_category->use_filter = 0;
					$xta_obj_option_in_category->save();
				}
				
				$xta_obj_option_in_category = xta_obj_option_in_category::find()->where(['id_obj_option' => $id,'id_obj_category'=>$category_all_item->id])->one();
				
				if (
				    (isset($in_category['use_filter']))    &&
				    (is_array($in_category['use_filter'])) &&
				    (in_array($category_all_item->id,$in_category['use_filter']))
				   )
				{
					$xta_obj_option_in_category->use_filter = 1;
					$xta_obj_option_in_category->save();
				}


				if (
				    (isset($in_category['use_setting']))    &&
				    (is_array($in_category['use_setting'])) &&
				    (in_array($category_all_item->id,$in_category['use_setting']))
				   )
				{
					$xta_obj_option_in_category->use_setting = 1;
					$xta_obj_option_in_category->save();
				}
				
			}
		}
	}
	elseif($_POST['opt_available'])
	{
		$av_name = $_POST['opt_available'];
		$av_id = intval($_POST['opt_available_id']);
		$av_parent = intval($_POST['opt_parent']);
		if (strlen($av_name))
		{
			if ($av_id)
			{
				$model_xta_obj_option_available = xta_obj_option_available::findOne($av_id);
			}
			else
			{
				$model_xta_obj_option_available = new xta_obj_option_available();
			}
			$model_xta_obj_option_available->id_obj_option = $id;
			$model_xta_obj_option_available->name = $av_name;
			$model_xta_obj_option_available->id_parent = $av_parent;
			$model_xta_obj_option_available->save();
		}
		$data['active_tab'] = 'values';
	}
	else
	{
		
		if ($model->load(Yii::$app->request->post()))
		{
			$model->save();
		}
	}
	$data['model'] = $model;





	// Получаем данные об опции
	$categories_all = array();
	$model_xta_obj_category = xta_obj_category::find()->all();
	foreach ($model_xta_obj_category as $item)
	{
		$data_xta_obj_category[] = $item->getAttributes(null);
	}
	
	foreach ($data_xta_obj_category as $item1)
	{
		if ($item1['id_parent']==0)
		{
			$item1['parent_depth'] = 0;
			$item1['has_children'] = xta_obj_category::hasChildrenArray($data_xta_obj_category,$item1);
			$categories_all[]=$item1;
			foreach ($data_xta_obj_category as $item2)
			{
				if ($item2['id_parent']==$item1['id'])
				{
					$item2['parent_depth'] = 1;
					$item2['has_children'] = xta_obj_category::hasChildrenArray($data_xta_obj_category,$item2);
					$categories_all[]=$item2;
					foreach ($data_xta_obj_category as $item3)
					{
						if ($item3['id_parent']==$item2['id'])
						{
							$item3['parent_depth'] = 2;
							$item3['has_children'] = xta_obj_category::hasChildrenArray($data_xta_obj_category,$item3);
							$categories_all[]=$item3;
						}
					}
				}
			}
		}
	}


	$model_in_category = xta_obj_option_in_category::find()->where(['id_obj_option' => $id])->all();
	$in_category = array();
	foreach ($model_in_category as $item)
	{
		$in_category[$item->id_obj_category] = $item->attributes;
	}
	
	
	$model_obj_option_available = xta_obj_option_available::find()->where(['id_obj_option' => $id])->all();
	$data['model_obj_option_available'] = $model_obj_option_available;
	

	$data['categories_all'] = $categories_all;
	$data['in_category'] = $in_category;

}
elseif($command=='delete')
{
	xta_obj_option::findOne($id)->delete();
	return Yii::$app->response->redirect(array('/admin/'.$this->action->id)); 
}




echo $this->render('view__'.$this->action->id,$data);
?>
