<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;


class MessageController extends Controller
{

	function init()
	{
		if (\Yii::$app->user->isGuest)
		{
			return Yii::$app->getResponse()->redirect('/account/login');
		}
	}


	public function actionIndex($command='',$id=NULL)
	{
		require(__DIR__.'/message/'.$this->action->id.'.php');
	}

	public function actionUnreaded($command='',$id=NULL)
	{
		require(__DIR__.'/message/index.php');
	}


}
