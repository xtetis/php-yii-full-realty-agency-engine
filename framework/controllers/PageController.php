<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\gii\xta_user_album;
use app\models\gii\xta_user_album__search;
use app\models\gii\xta_album;
use app\models\gii\xta_image;




class PageController extends Controller
{

	public function actionIndex($command='select',$id=NULL)
	{
		require(__DIR__.'/page/'.$this->action->id.'.php');
	}
	
	public function actionDesign()
	{
		$model__xta_user_album_all = xta_user_album::find()->all();
		$data['model__xta_user_album_all'] = $model__xta_user_album_all;
		
		foreach ($model__xta_user_album_all as $model__xta_user_album)
		{
			$model__xta_album = xta_album::findOne($model__xta_user_album->id_album);
			if ($model__xta_album)
			{
				$data['images'][$model__xta_user_album->id_album]=$model__xta_album->fn__get_xta_image_many()->all();
			}
		}
		
		echo $this->render('view__'.$this->action->id, $data);
	}
	
	public function actionDesignitem($id=NULL)
	{
		$model__xta_user_album = xta_user_album::findOne($id);
		$model__xta_album = xta_album::findOne($model__xta_user_album->id_album);
		$data['images']=$model__xta_album->fn__get_xta_image_many()->all();
		$data['model__xta_user_album'] = $model__xta_user_album;
		echo $this->render('view__'.$this->action->id, $data);
	}

}
