<?

use app\models\gii\xta_user;
use app\models\gii\xta_cache;
use app\models\gii\xta_site;
use app\models\gii\xta_obj;
use app\models\gii\xta_image;
use app\models\gii\xta_album;
use app\models\gii\xta_obj_option_value;
use app\models\gii\xta_obj_category;
use app\models\gii\xta_obj_option_available;
use app\models\BoardAddForm;
use app\models\RegisterForm;
use app\models\LoginForm;
use app\models\Email;

	$model_xta_user = new xta_user();
	$model_BoardAddForm = new BoardAddForm();


	if (
	     $model_BoardAddForm->load(Yii::$app->request->post()) && 
	     $model_BoardAddForm->validate() &&
	     $model_BoardAddForm->fn__validate_options()
	   )
	{
		if (Yii::$app->user->isGuest)
		{
			$newpass=substr(md5($model_BoardAddForm->email.$model_BoardAddForm->phone),0,6);
			$model_RegisterForm              = new RegisterForm();
			$model_RegisterForm->email       = $model_BoardAddForm->email;
			$model_RegisterForm->pass        = $newpass;
			$model_RegisterForm->pass_repeat = $newpass;
			$model_RegisterForm->username    = $model_BoardAddForm->username;
			$model_RegisterForm->phone       = $model_BoardAddForm->phone;
			$model_RegisterForm->skype       = $model_BoardAddForm->skype;
			if ($model_RegisterForm->register())
			{
				$model_LoginForm = new LoginForm();
				$model_LoginForm->email = $model_BoardAddForm->email;
				$model_LoginForm->pass  = $newpass;
				$model_LoginForm->rememberMe = true;
				$model_LoginForm->login();
				Email::fn__send_register_notification_to_user(Yii::$app->user->id,$newpass);
			}
		}

		$model_xta_album = new xta_album();
		$model_xta_album->save();
		$model_BoardAddForm->id_album = $model_xta_album->id;

		$i=0;
		if (is_array($model_BoardAddForm->images))
		{
			foreach ($model_BoardAddForm->images as $image_data)
			{
				$model_xta_image = new xta_image();
				$model_xta_image->id_album = $model_BoardAddForm->id_album;
				$model_xta_image->fn__save_base64_to_img($image_data);
				$model_xta_image->save();
				if (!$i)
				{
					$model_xta_album->id_image = $model_xta_image->id;
					$model_xta_album->save();
				}
				$i++;
			}
		}
		$model_xta_site = xta_site::getCurrentSite();
		$model_xta_obj = new xta_obj();
		
		$model_xta_obj->id_district = $model_BoardAddForm->id_district;
		$model_xta_obj->id_city     = $model_xta_site->id_city;
		$model_xta_obj->id_category = $model_BoardAddForm->id_category;
		$model_xta_obj->id_user     = Yii::$app->user->id;
		$model_xta_obj->id_album    = $model_BoardAddForm->id_album;
		$model_xta_obj->id_valuta   = $model_BoardAddForm->id_valuta;
		$model_xta_obj->name        = $model_BoardAddForm->name;
		$model_xta_obj->phone       = $model_BoardAddForm->phone;
		$model_xta_obj->about       = $model_BoardAddForm->about;
		$model_xta_obj->price       = intval($model_BoardAddForm->price);
		$model_xta_obj->pub_date  = new \yii\db\Expression('NOW()');
		$model_xta_obj->published   = 1;
		$model_xta_obj->save();
		
		// Сохраняем опции объявления
		$model__xta_obj_category = xta_obj_category::find()->where(["id" =>$model_xta_obj->id_category])->one();
		
		if ($model__xta_obj_category)
		{
			$xta_obj_option_many = $model__xta_obj_category->fn__get_xta_obj_option_many()->all();
			
			foreach ($xta_obj_option_many as $item)
			{
				$value = $_POST['BoardAddForm']['option_'.$item->id];
				
				if (strval(intval($value)) == $value)
				{
					$value = intval($value);
				}
				else
				{
					$value = '';
				}
				if ($item->option_type==1) // Text
				{
					if (strlen($value))
					{
						$model_xta_obj_option_value = new xta_obj_option_value();
						$model_xta_obj_option_value->id_obj = $model_xta_obj->id;
						$model_xta_obj_option_value->id_obj_option = $item->id;
						$model_xta_obj_option_value->value = strval($value);
						$model_xta_obj_option_value->save();
					}
				}
				elseif($item->option_type==0) // Select
				{
					$values = xta_obj_option_available::find()->where("id_obj_option = ".$item->id)->all();
					if ($values)
					{
						foreach ($values as $values_item)
						{
							$arr_value[$values_item->id] = $values_item->name;
						}
					}
					
					if (($values)&&(is_array($values))&&(isset($arr_value[$value])))
					{
						// Добавляем значение опции
						$model_xta_obj_option_value = new xta_obj_option_value();
						$model_xta_obj_option_value->id_obj = $model_xta_obj->id;
						$model_xta_obj_option_value->id_obj_option = $item->id;
						$model_xta_obj_option_value->value = strval($arr_value[$value]);
						$model_xta_obj_option_value->save();
					}
				}
			}
			
		}
		
		//xta_cache::fn__clear_tag('board_index');

		$subject='Вы добавили новое объявление на сайте 
		<a href="+link_autologin_my_obj+">+http_host+</a>';
		$emailmessage='
		<div><b>Заголовок объявления:</b> <a href="'.Email::fn__get_autologin_link_for_user(Yii::$app->user->id,'/board/item/'.$model_xta_obj->id).'">'.$model_xta_obj->name.'</a></div>
		<div><b>Рубрика:</b> '.$model_xta_obj->fn__get_xta_obj_category()->one()->name.'</div>
		<div><b>Дата создания:</b> '.date("d-m-Y H:i:s").'</div>
		';
		Email::sendNotification(Yii::$app->user->id,$subject,$emailmessage);

		setcookie("clearcache", 1, time()+3600, "/");
		return Yii::$app->getResponse()->redirect('/board/item/'.$model_xta_obj->id.'?clearcache');
	}
	else 
	{
		if (!Yii::$app->user->isGuest)
		{
			$model_BoardAddForm->phone=Yii::$app->user->getIdentity()->phone;;
		}
		echo $this->render('view__'.$this->action->id, ['model' => $model_BoardAddForm]);
	}
?>
