<?
use app\models\gii\xta_obj;
use app\models\gii\xta_obj_category;
use app\models\gii\xta_obj_option;
use app\models\gii\xta_cache;
use app\models\gii\xta_padeg;
use app\models\gii\xta_city;
use app\models\gii\xta_district;


// Cache start
//==============================================================================
$id_cache = 'board_item_'.$id;
$cache = xta_cache::fn__get_cache($id_cache);
if ($cache===false):
//==============================================================================
	$data  = array();

	$model__xta_obj = xta_obj::findOne(intval($id));
	
	if (!$model__xta_obj)
	{
		return Yii::$app->getResponse()->redirect('/board');
	}


	if (!$model__xta_obj->published)
	{
		return Yii::$app->getResponse()->redirect('/board');
	}

	if ($model__xta_obj->fn__get_xta_obj_abuse_many()->one())
	{
		return Yii::$app->getResponse()->redirect('/board');
	}
	
	
	$data['is_archive'] = false;
	if (floor((time() - strtotime($model__xta_obj->pub_date))/86400)>Yii::$app->params['count_days_obj_active'])
	{
		$data['is_archive'] = true;
	}
	
	//var_dump($data['is_archive']);

	$data['model__xta_obj']            = $model__xta_obj;
	$data['model__xta_valuta']         = $model__xta_obj->fn__get_xta_valuta()->one();
	$data['model__xta_district']       = $model__xta_obj->fn__get_xta_district()->one();
	$data['model__xta_city']           = $model__xta_obj->fn__get_xta_city()->one();
	$data['model__xta_obj_category']   = $model__xta_obj->fn__get_xta_obj_category()->one();
	$data['model__xta_user']           = $model__xta_obj->fn__get_xta_user()->one();
	$data['model__xta_album']          = $model__xta_obj->fn__get_xta_album()->one();
	$data['model__xta_image_many']     = $data['model__xta_album']->fn__get_xta_image_many()->all();
	$data['model__xta_obj_abuse_many'] = $model__xta_obj->fn__get_xta_obj_abuse_many()->all();
	$data['categs_array']              = $data['model__xta_obj_category']->fn__get_categs_array();
	$data['model__xta_obj_option_value_many'] = $model__xta_obj->fn__get_xta_obj_option_value_many()->all();

	// Получаем опции объявления
	//----------------------------------------------------------------------------
	$array_id_obj_option = array();
	foreach ($data['model__xta_obj_option_value_many'] as $item)
	{
		$array_id_obj_option[] = $item->id_obj_option;
	}
	$array_id_obj_option = implode(',',$array_id_obj_option);
	$where ='1';
	if (strlen($array_id_obj_option))
	{
		$where = 'id IN ('.$array_id_obj_option.')';
	}
	$model__xta_obj_option_many = xta_obj_option::find()->where($where)->all();
	//print_r($model__xta_obj_option_many);
	$data['model__xta_obj_option_many'] = $model__xta_obj_option_many;
	//----------------------------------------------------------------------------
	
	
	$data['current_city_name'] = xta_city::fn__get_current_city()->name;
	$data['current_city_name_p5'] = xta_padeg::fn__get_padeg($data['current_city_name'],5);

	$district_list = xta_district::fn__get_data_list(true);
	if (isset($district_list[$model__xta_obj->id_district]))
	{
		$data['district_name'] = $district_list[$model__xta_obj->id_district];
	}
	else
	{
		$data['district_name'] = 'Все районы';
	}



// Cache end
//==============================================================================
	xta_cache::fn__set_cache($id_cache,$data,array('board_item','board_item_'.$id));
else:
	$data = $cache;
endif;
//==============================================================================



echo $this->render('view__'.$this->action->id, $data);




