<?
use app\models\gii\xta_obj;
use app\models\gii\xta_cache;
use app\models\gii\xta_site;
use app\models\LibCommon;
use  yii\db\Query;







if($command==='unpublish')
{
	$id = intval($id);
	$model = xta_obj::findOne($id);
	if ($model)
	{
		$model->published   = 0;
		$model->save();
	}
	xta_cache::fn__clear_tag('board_index');
	xta_cache::fn__clear_tag('board_item_'.$id);
	return Yii::$app->getResponse()->redirect('/board/myad');
}
elseif($command==='publish')
{
	$id = intval($id);
	$model = xta_obj::findOne($id);
	if ($model)
	{
		$model->pub_date    = new \yii\db\Expression('NOW()');
		$model->published   = 1;
		$model->save();
	}
	xta_cache::fn__clear_tag('board_index');
	xta_cache::fn__clear_tag('board_item_'.$id);
	return Yii::$app->getResponse()->redirect('/board/myad');
}
elseif($command==='updatepub')
{
	$id = intval($id);
	$model = xta_obj::findOne($id);
	if ($model)
	{
		$model->pub_date    = new \yii\db\Expression('NOW()');
		$model->save();
	}
	xta_cache::fn__clear_tag('board_index');
	xta_cache::fn__clear_tag('board_item_'.$id);
	return Yii::$app->getResponse()->redirect('/board/myad');
}
elseif($command==='edit')
{
	require(__DIR__.'/myad_edit.php');
}
else
{
	$model_xta_site = xta_site::getCurrentSite();
	$where = '
	xta_obj.id_city = '.$model_xta_site->id_city.'
	AND
	xta_obj.id_user = '.Yii::$app->user->id.' ';


	if ($command==='unpublished')
	{
		$where.=' 
				AND 
				( 
					published=0
				)
				AND
				( 
					xta_obj.id NOT IN (SELECT id_obj FROM xta_obj_abuse)
				)';
	}
	elseif($command==='archive')
	{
		$where.=' 
				AND 
				( 
					published=1
					
				)	
				AND
				(
					pub_date < NOW() - INTERVAL '.Yii::$app->params['count_days_obj_active'].' DAY 
				)
				AND
				(
					xta_obj.id NOT IN (SELECT id_obj FROM xta_obj_abuse)
				)';
	}
	elseif($command==='blocked')
	{
		$where.='
				AND
				(
					xta_obj.id IN (SELECT id_obj FROM xta_obj_abuse)
				)';
	}
	else
	{
		$where.=' 
				AND 
					published=1
				AND 
					pub_date >= (NOW() - INTERVAL '.Yii::$app->params['count_days_obj_active'].' DAY ) 
				AND 
					xta_obj.id NOT IN (SELECT id_obj FROM xta_obj_abuse)';
	}


	// Пагинация
	//==============================================================================
	$query = new Query;
	$query->select(['count(*) as count']);
	$query->from('xta_obj');
	$query->where($where);
	$dataQuery = $query->createCommand()->queryAll();
	$count_obj = intval($dataQuery['count']);
	$maxpage = ceil(intval($dataQuery[0]['count'])/Yii::$app->params['per_pages']);

	$curpage            = LibCommon::fn__get_correct_page($maxpage);
	$data['pagination'] = LibCommon::fn__get_html_pagination('/account/obj'.(strlen($command)?'/'.$command:'').'?page=', $maxpage, $curpage);
	//==============================================================================




	// Выборка
	//==============================================================================
	$orderBy = array('xta_obj.pub_date' => SORT_DESC);
	$query = new Query;
	$query->select(['xta_obj.*', 
		              'IF(pub_date < NOW() - INTERVAL '.Yii::$app->params['count_days_obj_active'].' DAY,1,0) as pubdatecause',
		              'IF(xta_obj.id IN (SELECT id_obj FROM xta_obj_abuse),1,0) as abusecause',
		              'xta_valuta.shortname as valutaname',  
		              'xta_city.name as cityname',
		              'xta_district.name as districtname',
		              'xta_album.id_image',
		             ]);
	$query->from('xta_obj');
	$query->join('LEFT JOIN', 'xta_valuta', 'xta_valuta.id = xta_obj.id_valuta');
	$query->join('LEFT JOIN', 'xta_city', 'xta_city.id = xta_obj.id_city');
	$query->join('LEFT JOIN', 'xta_district', 'xta_district.id = xta_obj.id_district');
	$query->join('LEFT JOIN', 'xta_album', 'xta_album.id = xta_obj.id_album');
	$query->where($where);
	$query->limit(Yii::$app->params['per_pages']);
	$query->offset(Yii::$app->params['per_pages']*($curpage-1));
	$query->orderBy($orderBy);
	$data['objs'] = $query->createCommand()->queryAll();
	//==============================================================================

	$data['command']      = $command;
	echo $this->render('view__myad', $data);
}
