<?
use app\models\gii\xta_cache;
use app\models\gii\xta_user;
use app\models\gii\xta_site;
use app\models\gii\xta_obj;
use app\models\gii\xta_image;
use app\models\gii\xta_album;
use app\models\gii\xta_obj_option_value;
use app\models\gii\xta_obj_category;
use app\models\gii\xta_obj_option_available;
use app\models\BoardAddForm;
use app\models\RegisterForm;
use app\models\LoginForm;



$model__xta_obj = xta_obj::findOne(intval($id));
if (!$model__xta_obj)
{
	return Yii::$app->getResponse()->redirect('/board/myad');
}


$model__xta_user = xta_user::findOne($model__xta_obj->id_user);
if (!$model__xta_user)
{
	return $this->goHome();
}


if (Yii::$app->user->getIdentity()->id != $model__xta_user->id)
{
	return Yii::$app->getResponse()->redirect('/board/myad');
}

$model_BoardAddForm = new BoardAddForm();
$model_BoardAddForm->id_obj = $model__xta_obj->id;

if ($model_BoardAddForm->load(Yii::$app->request->post()))
{
	if (
		   $model_BoardAddForm->validate() &&
		   $model_BoardAddForm->fn__validate_options()
		 )
	{
		$model__xta_album = xta_album::findOne($model__xta_obj->id_album);
		$model_BoardAddForm->id_album = $model__xta_album->id;
		if ($model__xta_album)
		{
			$model__xta_album->fn__delete_images_from_album();
			$model__xta_album->id_image=0;
			$model__xta_album->save();
			$i=0;
			if (is_array($model_BoardAddForm->images))
			{
				foreach ($model_BoardAddForm->images as $image_data)
				{
					$model_xta_image = new xta_image();
					$model_xta_image->id_album = $model_BoardAddForm->id_album;
					$model_xta_image->fn__save_base64_to_img($image_data);
					$model_xta_image->save();
					
					if (!$i)
					{
						$model__xta_album->id_image = $model_xta_image->id;
						$model__xta_album->save();
					}
					$i++;
				}
			}
		}
		
		$model__xta_obj->id_district = $model_BoardAddForm->id_district;
		$model__xta_obj->id_category = $model_BoardAddForm->id_category;
		$model__xta_obj->id_valuta   = $model_BoardAddForm->id_valuta;
		$model__xta_obj->name        = $model_BoardAddForm->name;
		$model__xta_obj->about       = $model_BoardAddForm->about;
		$model__xta_obj->price       = intval($model_BoardAddForm->price);
		$model__xta_obj->pub_date  = new \yii\db\Expression('NOW()');
		$model__xta_obj->published   = 1;
		$model__xta_obj->save();
		
		
		$model__xta_obj_category = xta_obj_category::find()->where(["id" =>$model__xta_obj->id_category])->one();
		if ($model__xta_obj_category)
		{
			$xta_obj_option_many = $model__xta_obj_category->fn__get_xta_obj_option_many()->all();
			foreach ($xta_obj_option_many as $xta_obj_option_item)
			{
				
				$value = $_POST['BoardAddForm']['option_'.$xta_obj_option_item->id];
				
				if (strval(intval($value)) == $value)
				{
					$value = intval($value);
				}
				else
				{
					$value = '';
				}
				
				$model__xta_obj_option_value = xta_obj_option_value::find()->where([
					"id_obj" =>$model__xta_obj->id,
					"id_obj_option" =>$xta_obj_option_item->id,
					])->one();
				if (!$model__xta_obj_option_value)
				{
					$model__xta_obj_option_value = new xta_obj_option_value();
					$model__xta_obj_option_value->id_obj = $model__xta_obj->id;
					$model__xta_obj_option_value->id_obj_option = $xta_obj_option_item->id;
					
				}
				
				if ($xta_obj_option_item->option_type==1) // Если опция - Text
				{
					$model__xta_obj_option_value->value = strval($value);
					$model__xta_obj_option_value->save();
				}
				elseif($xta_obj_option_item->option_type==0) // Если опция - Select
				{
					$xta_obj_option_available = xta_obj_option_available::find()->where([
						"id_obj_option"=>$xta_obj_option_item->id,
						"id"=>intval($value),
						])->one();
					if ($xta_obj_option_available)
					{
						$model__xta_obj_option_value->value = $xta_obj_option_available->name;
						$model__xta_obj_option_value->save();
					}
				}
				
			}
		}
		//print_r($_POST);
		xta_cache::fn__clear_tag('board_index');
		xta_cache::fn__clear_tag('board_item_'.$id);
		return Yii::$app->getResponse()->redirect('/board/myad');
	}
	else
	{
		echo $this->render('/board/view__add', ['model' => $model_BoardAddForm]);
	}
}
else
{
	$model_BoardAddForm->id_district = $model__xta_obj->id_district;
	$model_BoardAddForm->id_category = $model__xta_obj->id_category;
	$model_BoardAddForm->id_user = $model__xta_obj->id_user;
	$model_BoardAddForm->id_album = $model__xta_obj->id_album;
	$model_BoardAddForm->id_valuta = $model__xta_obj->id_valuta;
	$model_BoardAddForm->name = $model__xta_obj->name;
	$model_BoardAddForm->about = $model__xta_obj->about;
	$model_BoardAddForm->price = $model__xta_obj->price;
	$model_BoardAddForm->phone = $model__xta_obj->phone;
	
	$model_BoardAddForm->images = array();
	$model__xta_album = xta_album::findOne($model__xta_obj->id_album);
	if ($model__xta_album)
	{
		$model__xta_image_many = $model__xta_album->fn__get_xta_image_many()->all();
		foreach ($model__xta_image_many as $model__xta_image_item)
		{
			$model_BoardAddForm->images[]=$model__xta_image_item->fn__get_base64_img();
		}
	}
	
	// Получаем опции объявления
	$model__xta_obj_category = xta_obj_category::find()->where(["id" =>$model__xta_obj->id_category])->one();
	if ($model__xta_obj_category)
	{
		$xta_obj_option_many = $model__xta_obj_category->fn__get_xta_obj_option_many()->all();
		foreach ($xta_obj_option_many as $xta_obj_option_item)
		{
			// Выбираем модель со значением опции объяления
			$model__xta_obj_option_value = xta_obj_option_value::find()->where([
				"id_obj" =>$model__xta_obj->id,
				"id_obj_option" =>$xta_obj_option_item->id,
				])->one();
			if ($model__xta_obj_option_value)
			{
				if ($xta_obj_option_item->option_type==1)  // Если это TEXT
				{
					$_POST['BoardAddForm']['option_'.$xta_obj_option_item->id] = $model__xta_obj_option_value->value;
				}
				elseif($xta_obj_option_item->option_type==0)  // Если это SELECT
				{
					$model__xta_obj_option_available = xta_obj_option_available::find()->where([
						"id_obj_option" =>$xta_obj_option_item->id,
						"name" =>$model__xta_obj_option_value->value,
						])->one();
					if ($model__xta_obj_option_available)
					{
						$_POST['BoardAddForm']['option_'.$xta_obj_option_item->id] = $model__xta_obj_option_available->id;
					}
				}
			}
		}
	}
	
	echo $this->render('/board/view__add', ['model' => $model_BoardAddForm]);
}


?>
