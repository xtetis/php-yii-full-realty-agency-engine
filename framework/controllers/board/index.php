<?
use app\models\gii\xta_obj;
use app\models\gii\xta_obj__search;
use app\models\gii\xta_obj_category;
use app\models\gii\xta_obj_option;
use app\models\gii\xta_obj_option_available;
use app\models\gii\xta_obj_category_description;


use app\models\LibCommon;
use app\models\gii\xta_album;
use app\models\gii\xta_image;
use app\models\gii\xta_site;
use app\models\gii\xta_district;
use app\models\gii\xta_cache;
use app\models\gii\xta_padeg;
use app\models\gii\xta_seo_tdz;


use  yii\db\Query;


// Cache start
//==============================================================================
$id_cache = 'board_index_'.$id.'_'.serialize($_GET);
if (empty($_POST))
{
	$cache = xta_cache::fn__get_cache($id_cache);
}
else
{
	$cache = false;
}
if ($cache===false):
//==============================================================================



$id = intval($id);
if ($id)
{
	$model__cur_xta_obj_category = xta_obj_category::find()->where(["id" => $id])->one();
	if (!$model__cur_xta_obj_category)
	{
		$model__xta_obj = xta_obj::findOne(intval($id));
		if ($model__xta_obj)
		{
			header("HTTP/1.1 301 Moved Permanently");
			header("Location: /board/item/".intval($id));
			exit();
		}
		else
		{
			$id = 0;
		}
		
	}
}
else
{
	$id=0;
}

$data['id'] = $id;





//==============================================================================
$model_xta_site = xta_site::getCurrentSite();

$where = '
xta_obj.id NOT IN (SELECT id_obj FROM xta_obj_abuse)
AND
xta_obj.published=1
AND
xta_obj.pub_date >= (NOW() - INTERVAL '.Yii::$app->params['count_days_obj_active'].' DAY )
AND
xta_obj.id_city = '.$model_xta_site->id_city.'
';
if ($id)
{
	$where.=" AND
				xta_obj.id_category IN (

					SELECT
						t0.id
					FROM
						xta_obj_category as t0
					WHERE
						t0.id = ".$id."

					UNION

					SELECT
						t1.id
					FROM
						xta_obj_category as t1
					WHERE
						t1.id_parent = ".$id."

					UNION

					SELECT
						t2.id
					FROM
						xta_obj_category as t2
					INNER JOIN xta_obj_category as t3
						ON t3.id = t2.id_parent
					WHERE
						t3.id_parent = ".$id."

					UNION

					SELECT
						t4.id
					FROM
						xta_obj_category as t4
					INNER JOIN xta_obj_category as t5
						ON t5.id = t4.id_parent
					INNER JOIN xta_obj_category as t6
						ON t6.id = t5.id_parent
					WHERE
						t6.id_parent = ".$id."
					)
	";
}
//==============================================================================















// Сортировка
//==============================================================================
$orderBy = array('xta_obj.pub_date' => SORT_DESC);
$data['link']['sort_newest'] = '';
$data['link']['sort_lowprice'] = LibCommon::fn__get_board_link($id,array('filter','district'),array('sort'=>'lowprice'));
$data['link']['sort_highprice'] = LibCommon::fn__get_board_link($id,array('filter','district'),array('sort'=>'highprice'));
if ((isset($_GET['sort']))&&(in_array($_GET['sort'],array('lowprice','highprice'))))
{
	if ($_GET['sort']=='lowprice')
	{
		$orderBy = array('xta_obj.price' => SORT_ASC);
		$data['link']['sort_lowprice'] = '';
	}

	if ($_GET['sort']=='highprice')
	{
		$orderBy = array('xta_obj.price' => SORT_DESC);
		$data['link']['sort_highprice'] = '';
	}
	$data['link']['sort_newest'] = LibCommon::fn__get_board_link($id,array('filter','district'));
}
//==============================================================================






// Район
//==============================================================================
$district_list = xta_district::fn__get_data_list(true);
if ((isset($_GET['district']))&&(in_array(intval($_GET['district']),array_keys($district_list))))
{
	$where.=" AND
	          xta_obj.id_district = ".intval($_GET['district'])." ";
}
//==============================================================================














// Фильтрация POST
//==============================================================================
$filters = array();
if ((isset($_POST['filter']))&&(is_array($_POST['filter'])))
{

	if (isset($_POST['filter']['resetfilter']))
	{
		$redirect = LibCommon::fn__get_board_link($id,array('sort','district'),array());
		return $this->redirect($redirect,302);
	}

	$filter_arr = $_POST['filter'];
	if (
	     (isset($filter_arr['price']['min']))&&
	     (strlen($filter_arr['price']['min'])) &&
	     (strval(intval($filter_arr['price']['min'])) == $filter_arr['price']['min'])
	   )
	{
		$filters['price']['min'] = intval($filter_arr['price']['min']);
	}

	if (
	     (isset($filter_arr['price']['max']))&&
	     (strlen($filter_arr['price']['max'])) &&
	     (strval(intval($filter_arr['price']['max'])) == $filter_arr['price']['max'])
	   )
	{
		$filters['price']['max'] = intval($filter_arr['price']['max']);
	}

	if (
	     (isset($filter_arr['photo']['use']))&&
	     (strlen($filter_arr['photo']['use'])) &&
	     (strval(intval($filter_arr['photo']['use'])) == $filter_arr['photo']['use'])
	   )
	{
		$filters['photo']['use'] = intval($filter_arr['photo']['use']);
	}



	if (
	     (isset($filter_arr['opt']))&&
	     (is_array($filter_arr['opt']))
	   )
	{
		foreach ($filter_arr['opt'] as $opt_id => $opt_key)
		{
			if ((is_array($opt_key)) &&(count($opt_key)) && (intval($opt_id)))
			{
				foreach ($opt_key as $opt_key_name => $opt_key_value)
				{
					if (in_array($opt_key_name,array('min','max')))
					{
						if (strval(intval($opt_key_value)) == $opt_key_value)
						{
							$filters['opt'][$opt_id][$opt_key_name] = $opt_key_value;
						}
					}
					elseif($opt_key_name=='set')
					{
						if (strlen($opt_key_value))
						{
							$arr_set_vals = explode(',',$opt_key_value);
							$arr_set_vals_correct = array();
							if (count($arr_set_vals))
							{
								foreach ($arr_set_vals as $arr_set_vals_item)
								{
									if (intval($arr_set_vals_item))
									{
										$arr_set_vals_correct[]=intval($arr_set_vals_item);
									}
								}
							}
							if (count($arr_set_vals_correct))
							{
								$filters['opt'][$opt_id][$opt_key_name] = implode(',',$arr_set_vals_correct);
							}
						}
					}
				}
			}
		}
	}


	$str_filters = LibCommon::fn__get_filter_str($filters);
	if (strlen($str_filters))
	{
		$redirest = LibCommon::fn__get_board_link($id,array('sort','district'),array('filter'=>$str_filters));
	}
	else
	{
		$redirest = LibCommon::fn__get_board_link($id,array('sort','district'),array());
	}
	return $this->redirect($redirest,302);
}

//==============================================================================










// Фильтрация GET
//==============================================================================
if ((isset($_GET['filter']))&&(strlen($_GET['filter'])))
{
	$get_filters = LibCommon::fn__get_filter_arr($_GET['filter']);
	$data['get_filters']= $get_filters;

	if (isset($get_filters['price']['min']))
	{
		$where.=" AND
			        xta_obj.price >= ".intval($get_filters['price']['min'])." ";
	}


	if (isset($get_filters['price']['max']))
	{
		$where.=" AND
			        xta_obj.price <= ".intval($get_filters['price']['max'])." ";
	}

	if (isset($get_filters['photo']['use']))
	{
		$where.=" AND
			        xta_album.id_image > 0 ";
	}

	if ((isset($get_filters['opt']))&&(is_array($get_filters['opt'])))
	{
		foreach ($get_filters['opt'] as $opt_id => $opt_keys)
		{
			$model_xta_obj_option = xta_obj_option::find()->where("
			 id IN (
				SELECT `id_obj_option`
				FROM `xta_obj_option_in_category`
				WHERE `id_obj_category` = ".$id." AND `use_filter` = 1
				)
			 AND id = ".intval($opt_id))->one();
			if ((is_array($opt_keys))&&(count($opt_keys))&&($model_xta_obj_option))
			{
				foreach ($opt_keys as $opt_keys_name => $opt_keys_vals)
				{
					if ($opt_keys_name=='min')
					{
						$where.=" AND
						          xta_obj.id IN (
						            SELECT
						              `id_obj`
						            FROM
						              `xta_obj_option_value`
						            WHERE
						              `id_obj_option` = ".intval($opt_id)."
						              AND
						              `value`>=".intval($opt_keys_vals)."
						          )";
					}
					if ($opt_keys_name=='max')
					{
						$where.=" AND
						          xta_obj.id IN (
						            SELECT
						              `id_obj`
						            FROM
						              `xta_obj_option_value`
						            WHERE
						              `id_obj_option` = ".intval($opt_id)."
						              AND
						              `value`<=".intval($opt_keys_vals)."
						          )";
					}
					if ($opt_keys_name=='set')
					{
						$opt_keys_vals = implode(',',$opt_keys_vals);
						if (strlen($opt_keys_vals))
						{
							$arr_set_vals = explode(',',$opt_keys_vals);
							$arr_set_vals_correct = array();
							if (count($arr_set_vals))
							{
								foreach ($arr_set_vals as $arr_set_vals_item)
								{
									if (intval($arr_set_vals_item))
									{
										$arr_set_vals_correct[]=intval($arr_set_vals_item);
									}
								}
							}
							if (count($arr_set_vals_correct))
							{
								$ids = implode(',',$arr_set_vals_correct);
								$where.=" AND
										      xta_obj.id IN (
										        SELECT
										          `id_obj`
										        FROM
										          `xta_obj_option_value`
										        WHERE
										          `id_obj_option` = ".intval($opt_id)."
										          AND
										          `value` IN (
										            SELECT
										              `name`
										            FROM
										              `xta_obj_option_available`
										            WHERE
										              `id` IN (".$ids.")
										          )
										      )";
							}
						}


					}
				}
			}
		}
	}

	//echo '<pre>';	print_r($get_filters);	echo '</pre>';
}

$data['model_filters'] = xta_obj_option::find()->where("id IN (
SELECT `id_obj_option` FROM `xta_obj_option_in_category` WHERE `id_obj_category` = ".$id." AND `use_filter` = 1)")->all();

$data['model_filters_options'] = xta_obj_option_available::find()->where("id_obj_option IN (
SELECT id FROM xta_obj_option WHERE id IN (
SELECT `id_obj_option` FROM `xta_obj_option_in_category` WHERE `id_obj_category` = ".$id." AND `use_filter` = 1) )")->all();
//==============================================================================









// Пагинация
//==============================================================================
$query = new Query;
$query->select(['count(*) as count']);
$query->from('xta_obj');
$query->join('LEFT JOIN', 'xta_album', 'xta_album.id = xta_obj.id_album');
$query->where($where);
$command = $query->createCommand();
$dataQuery = $command->queryAll();
$count_obj = intval($dataQuery[0]['count']);
$maxpage = ceil(intval($dataQuery[0]['count'])/Yii::$app->params['per_pages']);


$curpage            = LibCommon::fn__get_correct_page($maxpage);
$data['pagination'] = LibCommon::fn__get_html_pagination(LibCommon::fn__get_board_link($id,array('filter','sort','district'),array('page'=>'')), $maxpage, $curpage);



//==============================================================================














// Список объявлений
//==============================================================================
$query = new Query;
$query->select(['xta_obj.*',
                'xta_valuta.shortname as valutaname',
                'xta_city.name as cityname',
                'xta_district.name as districtname',
                'xta_album.id_image',
               ]);
$query->from('xta_obj');
$query->join('LEFT JOIN', 'xta_valuta', 'xta_valuta.id = xta_obj.id_valuta');
$query->join('LEFT JOIN', 'xta_city', 'xta_city.id = xta_obj.id_city');
$query->join('LEFT JOIN', 'xta_district', 'xta_district.id = xta_obj.id_district');
$query->join('LEFT JOIN', 'xta_album', 'xta_album.id = xta_obj.id_album');
$query->where($where);
$query->limit(Yii::$app->params['per_pages']);
$query->offset(Yii::$app->params['per_pages']*($curpage-1));
$query->orderBy($orderBy);
$command = $query->createCommand();
$select = $command->queryAll();
$data['select'] = array();
foreach ($select as $item)
{
	$model__xta_obj_category = xta_obj_category::find()->where(["id" => $item['id_category']])->one();
	$category_array = $model__xta_obj_category->fn__get_categs_array();
	$category_names = array();
	foreach ($category_array as $key1 => $value1)
	{
		$category_names[]='<a href="/board/'.$key1.'">'.$value1.'</a>';
	}
	$category_names = implode(' → ',$category_names);
	$item['category_names']=$category_names;

	if ($item['id_district']==0)
	{
		$item['districtname'] = 'Все районы';
	}
	//print_r($item);

	$item['image'] = xta_image::fn__get_nosrc();
	if ($item['id_image'])
	{
		$model__xta_image = xta_image::find()->where(["id" => $item['id_image']])->one();
		if ($model__xta_image)
		{
			$item['image'] = $model__xta_image->fn__get_thumb_src('small');
		}
	}

	if (intval($item['price']))
	{
		$item['price']='<b>'.$item['price'].'</b> '.$item['valutaname'];
	}
	else
	{
		$item['price']='';
	}

	$data['select'][] = $item;
}
//==============================================================================







$model__xta_city = $model_xta_site->getxta_city()->one();
$data['current_city_paged_5'] =xta_padeg::fn__get_padeg($model__xta_city->name,5);
$data['current_city_paged_1'] =xta_padeg::fn__get_padeg($model__xta_city->name,1);






//==============================================================================
if (
     ($id) &&
     ($model__cur_xta_obj_category->id_parent)
   )
{
	$data['category_id_parent'] = $model__cur_xta_obj_category->id_parent;
	$model__xta_obj_category_parent = xta_obj_category::find()->where(
		[
			"id" => $model__cur_xta_obj_category->id_parent
		])->one();
	if (!$model__xta_obj_category_parent)
	{
		$data['category_parent_name'] = '';
	}
	else
	{
		$data['category_parent_name'] = $model__xta_obj_category_parent->name;
	}
	
}
else
{
	$data['category_id_parent'] = 0;
	$data['category_parent_name'] = '';
}

//==============================================================================






// Верхний блок
//==============================================================================
$data['category_filter']['list'] = array();
$data['category_filter']['name'] = '';
$data['category_filter']['count_obj'] = 0;
$data['category_filter']['img'] = xta_image::fn__get_nosrc();
$data['category_filter']['count_obj'] = $count_obj;

if ($id){
	$model_childs = $model__cur_xta_obj_category->fn__get_children();
	$data['category_filter']['name'] = $model__cur_xta_obj_category->name;
}
else
{
	$model_childs = xta_obj_category::find()->where(["id_parent" => 0])->all();
	$data['category_filter']['name'] = 'Недвижимость';
}





$data['model__xta_city'] = $model__xta_city;
$data['category_filter']['name_city']=$data['category_filter']['name'].' в '.$data['current_city_paged_5'];
if (file_exists($_SERVER['DOCUMENT_ROOT'].'/public/images/categories/'.$id.'.png'))
{
	$data['category_filter']['img'] = '/public/images/categories/'.$id.'.png';
}



foreach ($model_childs as $model_childs_item)
{
	$img = xta_image::fn__get_nosrc();
	if (file_exists($_SERVER['DOCUMENT_ROOT'].'/public/images/categories/'.$model_childs_item->id.'.png'))
	{
		$img = '/public/images/categories/'.$model_childs_item->id.'.png';
	}
	$data['category_filter']['list'][]=array(
		'img'=>$img,
		'name'=>$model_childs_item->name,
		'link'=>'/board/'.$model_childs_item->id,
	);
}




// Добавляем возврат на уровень вверх
if (count($data['category_filter']['list']))
{

	$url = '/board';

	$img = '/public/templates/default/img/ico/home.png';
	if ($id)
	{
		$name = 'Назад';
		$url.='/'.$model__cur_xta_obj_category->id_parent;
		$img = '/public/images/categories/back.png';
	}
	else{
		$name = 'На главную';
		$url = '/';
	}

	if ((!$model__cur_xta_obj_category->id_parent)&&($id))
	{
		$name = 'Все объявления';
		$url = '/board';
	}


	$adds = array(
		'img'=>$img,
		'name'=>$name,
		'link'=>$url,
		);
	array_unshift($data['category_filter']['list'],$adds);
}
else
{
	$data['obj_index_breadcrumbs'] = $model__cur_xta_obj_category->fn__get_categs_array($id);
}



// Районы
$district_list = xta_district::fn__get_data_list(true);
$district_list = [0=>'Все районы'] + $district_list;
$district_list_extended = array();
$district_selected = 0;
if ((isset($_GET['district']))&&(in_array(intval($_GET['district']),array_keys($district_list))))
{
	$district_selected = intval($_GET['district']);
}
foreach ($district_list as $district_list_key => $district_list_value)
{
	$url=LibCommon::fn__get_board_link($id,array('filter','sort'),array());
	if ($district_list_key)
	{
		$url=LibCommon::fn__get_board_link($id,array('filter','sort'),array('district'=>$district_list_key));
	}

	$district_list_extended[$district_list_key] = array(
		'name'=>$district_list_value,
		'url'=>$url,
	);

	if ($district_selected == $district_list_key)
	{
		$district_list_extended[$district_list_key]['selected'] = 1;
	}

}
$data['district_list_extended'] = $district_list_extended;
//==============================================================================






//
//==============================================================================
$data['fn__get_categs_array'] =array();
if($id)
{
	$data['fn__get_categs_array'] =$model__cur_xta_obj_category->fn__get_categs_array();
}
//==============================================================================














$model__xta_obj_category_description = xta_obj_category_description::find()->where([
	"id_site" => $model_xta_site->id,
	"id_obj_category" => $id,
	])->one();
$data['model__xta_obj_category_description'] =$model__xta_obj_category_description;

$data['xta_seo_tdz'] = xta_seo_tdz::fn__get_xta_seo_tdz();



// Cache end
//==============================================================================
	xta_cache::fn__set_cache($id_cache,$data,array('board_index'));
else:
	$data = $cache;
endif;
//==============================================================================







echo $this->render('view__'.$this->action->id, $data);
