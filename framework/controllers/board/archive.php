<?
use app\models\gii\xta_obj;
use app\models\gii\xta_obj__search;
use app\models\gii\xta_obj_category;
use app\models\gii\xta_obj_option;
use app\models\gii\xta_obj_option_available;
use app\models\gii\xta_obj_category_description;


use app\models\LibCommon;
use app\models\gii\xta_album;
use app\models\gii\xta_image;
use app\models\gii\xta_site;
use app\models\gii\xta_district; 
use app\models\gii\xta_cache;
use app\models\gii\xta_padeg;

use  yii\db\Query;


// Cache start
//==============================================================================
$id_cache = 'board_archive_'.serialize($_GET);
if (empty($_POST))
{
	$cache = xta_cache::fn__get_cache($id_cache);
}
else
{
	$cache = false;
}
if ($cache===false):
//==============================================================================



$id = intval($id);
$data['id'] = $id;





//==============================================================================
$model_xta_site = xta_site::getCurrentSite();

$where = '
xta_obj.id NOT IN (SELECT id_obj FROM xta_obj_abuse) 
AND 
xta_obj.published=1
AND 
xta_obj.pub_date < (NOW() - INTERVAL '.Yii::$app->params['count_days_obj_active'].' DAY ) 
AND
xta_obj.id_city = '.$model_xta_site->id_city.'
';

//==============================================================================






















// Пагинация
//==============================================================================
$query = new Query;
$query->select(['count(*) as count']);
$query->from('xta_obj');
$query->join('LEFT JOIN', 'xta_album', 'xta_album.id = xta_obj.id_album');
$query->where($where);
$command = $query->createCommand();
$dataQuery = $command->queryAll();
$count_obj = intval($dataQuery[0]['count']);
$maxpage = ceil(intval($dataQuery[0]['count'])/Yii::$app->params['per_pages']);


$curpage            = LibCommon::fn__get_correct_page($maxpage);
$data['pagination'] = LibCommon::fn__get_html_pagination('/board/archive?page=', $maxpage, $curpage);



//==============================================================================














// Список объявлений
//==============================================================================
$query = new Query;
$query->select(['xta_obj.*', 
                'xta_valuta.shortname as valutaname', 
                'xta_city.name as cityname',
                'xta_district.name as districtname',
                'xta_album.id_image',
               ]);
$query->from('xta_obj');
$query->join('LEFT JOIN', 'xta_valuta', 'xta_valuta.id = xta_obj.id_valuta');
$query->join('LEFT JOIN', 'xta_city', 'xta_city.id = xta_obj.id_city');
$query->join('LEFT JOIN', 'xta_district', 'xta_district.id = xta_obj.id_district');
$query->join('LEFT JOIN', 'xta_album', 'xta_album.id = xta_obj.id_album');
$query->where($where);
$query->limit(Yii::$app->params['per_pages']);
$query->offset(Yii::$app->params['per_pages']*($curpage-1));
$query->orderBy($orderBy);
$command = $query->createCommand();
$select = $command->queryAll();
$data['select'] = array();
foreach ($select as $item)
{
	$model__xta_obj_category = xta_obj_category::find()->where(["id" => $item['id_category']])->one();
	$category_array = $model__xta_obj_category->fn__get_categs_array();
	$category_names = array();
	foreach ($category_array as $key1 => $value1)
	{
		$category_names[]='<a href="/board/'.$key1.'">'.$value1.'</a>';
	}
	$category_names = implode(' → ',$category_names);
	$item['category_names']=$category_names;
	
	if ($item['id_district']==0)
	{
		$item['districtname'] = 'Все районы';
	}
	
	$item['image'] = xta_image::fn__get_nosrc();
	if ($item['id_image'])
	{
		$model__xta_image = xta_image::find()->where(["id" => $item['id_image']])->one();
		if ($model__xta_image)
		{
			$item['image'] = $model__xta_image->fn__get_thumb_src('small');
		}
	}
	
	if (intval($item['price']))
	{
		$item['price']='<b>'.$item['price'].'</b> '.$item['valutaname'];
	}
	else
	{
		$item['price']='';
	}
	
	$data['select'][] = $item;
}
//==============================================================================

















// Верхний блок
//==============================================================================
$data['category_filter']['list'] = array();
$data['category_filter']['name'] = '';
$data['category_filter']['count_obj'] = 0;
$data['category_filter']['img'] = xta_image::fn__get_nosrc();
$data['category_filter']['count_obj'] = $count_obj;
$model__xta_city = $model_xta_site->getxta_city()->one();
//==============================================================================














$data['current_city_paged_5'] =xta_padeg::fn__get_padeg($model__xta_city->name,5);
$data['current_city_paged_1'] =xta_padeg::fn__get_padeg($model__xta_city->name,1);




// Cache end
//==============================================================================
	xta_cache::fn__set_cache($id_cache,$data,array('board_archive'));
else:
	$data = $cache;
endif;
//==============================================================================







echo $this->render('view__'.$this->action->id, $data);

