<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;


class AdminController extends Controller
{
	public $layout = 'admin';

	public function init()
	{
		if (\Yii::$app->user->isGuest) {
			return $this->goHome();
		}

		if (!\Yii::$app->user->identity->is_admin) {
			return $this->goHome();
		}
	}


	public function behaviors()
	{
		return [];
	}

	public function actions()
	{
	
	}


	public function actionIndex()
	{
		require(__DIR__.'/admin/index.php');
	}

	public function actionXta_setting_category($command='select',$id=NULL)
	{
		require(__DIR__.'/admin/xta_setting_category.php');
	}
	
	public function actionXta_country($command='select',$id=NULL)
	{
		require(__DIR__.'/admin/xta_country.php');
	}

	public function actionXta_user_role($command='select',$id=NULL)
	{
		require(__DIR__.'/admin/xta_user_role.php');
	}

	public function actionXta_padeg($command='select',$id=NULL)
	{
		require(__DIR__.'/admin/xta_padeg.php');
	}
	

	public function actionXta_page($command='select',$id=NULL)
	{
		require(__DIR__.'/admin/xta_page.php');
	}

	public function actionXta_valuta($command='select',$id=NULL)
	{
		require(__DIR__.'/admin/xta_valuta.php');
	}

	public function actionXta_region($command='select',$id=NULL)
	{
		require(__DIR__.'/admin/xta_region.php');
	}

	public function actionXta_city($command='select',$id=NULL)
	{
		require(__DIR__.'/admin/xta_city.php');
	}

	public function actionXta_site($command='select',$id=NULL)
	{
		require(__DIR__.'/admin/xta_site.php');
	}

	public function actionXta_district($command='select',$id=NULL)
	{
		require(__DIR__.'/admin/xta_district.php');
	}

	public function actionXta_setting($command='select',$id=NULL)
	{
		require(__DIR__.'/admin/xta_setting.php');
	}

	public function actionXta_user($command='select',$id=NULL)
	{
		require(__DIR__.'/admin/'.$this->action->id.'.php');
	}

	public function actionXta_obj_category($command='select',$id=NULL)
	{
		require(__DIR__.'/admin/'.$this->action->id.'.php');
	}

	public function actionXta_obj_option($command='select',$id=NULL)
	{
		require(__DIR__.'/admin/'.$this->action->id.'.php');
	}

	public function actionXta_obj_abuse($command='select',$id=NULL)
	{
		require(__DIR__.'/admin/'.$this->action->id.'.php');
	}

	public function actionXta_obj_category_description($command='select',$id=NULL)
	{
		require(__DIR__.'/admin/'.$this->action->id.'.php');
	}

	public function actionCache($command='select',$id=NULL)
	{
		require(__DIR__.'/admin/'.$this->action->id.'.php');
	}

	public function actionMod_sitemap($command='',$id=NULL)
	{
		include(__DIR__.'/admin/'.$this->action->id.'.php');
	}

	public function actionParser($command='odnagazeta_com',$id=NULL){
	  include('admin/'.$this->action->id.'/'.$command.'.php');
	}

	public function actionXta_seo_tdz($command='select',$id=NULL)
	{
		include(__DIR__.'/admin/'.$this->action->id.'.php');
	}

	public function actionXta_love_type_account($command='select',$id=NULL)
	{
		include(__DIR__.'/admin/'.$this->action->id.'.php');
	}
	
	public function actionXta_user_album($command='select',$id=NULL)
	{
		include(__DIR__.'/admin/'.$this->action->id.'.php');
	}
	

}
