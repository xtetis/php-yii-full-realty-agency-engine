<?
use app\models\LibCommon;
use app\models\LibSql;

use app\models\gii\xta_city;
use app\models\gii\xta_obj_category;
use app\models\gii\xta_image;
use app\models\gii\xta_site;
use app\models\gii\xta_district; 
use app\models\gii\xta_cache;
use app\models\gii\xta_padeg;
use app\models\gii\xta_seo_tdz;
use  yii\db\Query;


// Cache
//==============================================================================
$id_cache = 'main_page';
$cache = xta_cache::fn__get_cache($id_cache);
if ($cache===false):
//==============================================================================



$model_xta_site = xta_site::getCurrentSite();


// Верхний блок
//==============================================================================
$data['category_filter']['list'] = array();
$data['category_filter']['name'] = '';
$data['category_filter']['count_obj'] = 0;
$data['category_filter']['img'] = xta_image::fn__get_nosrc();
$data['category_filter']['count_obj'] = $count_obj;

if ($id){
	$model_childs = $model__cur_xta_obj_category->fn__get_children();
	$data['category_filter']['name'] = $model__cur_xta_obj_category->name;
}
else
{
	$model_childs = xta_obj_category::find()->where(["id_parent" => 0])->all();
	$data['category_filter']['name'] = 'Бесплатные объявления';
}


$model__xta_city = $model_xta_site->getxta_city()->one();
$data['category_filter']['name_city']=$data['category_filter']['name'].' в '.$model__xta_city->name;
if (file_exists($_SERVER['DOCUMENT_ROOT'].'/public/images/categories/'.$id.'.png'))
{
	$data['category_filter']['img'] = '/public/images/categories/'.$id.'.png';
}



foreach ($model_childs as $model_childs_item)
{
	$img = xta_image::fn__get_nosrc();
	if (file_exists($_SERVER['DOCUMENT_ROOT'].'/public/images/categories/'.$model_childs_item->id.'.png'))
	{
		$img = '/public/images/categories/'.$model_childs_item->id.'.png';
	}
	$data['category_filter']['list'][]=array(
		'img'=>$img,
		'name'=>$model_childs_item->name,
		'link'=>'/board/'.$model_childs_item->id,
	);
}





// Районы
$district_list = xta_district::fn__get_data_list(true);
$district_list = array_merge([0=>'Все районы'],$district_list);
$district_list_extended = array();
$district_selected = 0;
if ((isset($_GET['district']))&&(in_array(intval($_GET['district']),array_keys($district_list))))
{
	$district_selected = intval($_GET['district']);
}
foreach ($district_list as $district_list_key => $district_list_value)
{
	$url=LibCommon::fn__get_board_link($id,array('filter','sort'),array());
	if ($district_list_key)
	{
		$url=LibCommon::fn__get_board_link($id,array('filter','sort'),array('district'=>$district_list_key));
	}

	$district_list_extended[$district_list_key] = array(
		'name'=>$district_list_value,
		'url'=>$url,
	);
	
	if ($district_selected == $district_list_key)
	{
		$district_list_extended[$district_list_key]['selected'] = 1;
	}
	
}
$data['district_list_extended'] = $district_list_extended;
//==============================================================================

















// Список объявлений
//==============================================================================
$where = '
xta_obj.id NOT IN (SELECT id_obj FROM xta_obj_abuse) 
AND 
xta_obj.published=1 
AND 
xta_obj.pub_date >= (NOW() - INTERVAL 60 DAY ) 
AND
xta_obj.id_city = '.$model_xta_site->id_city.'
';
$orderBy = array('xta_obj.pub_date' => SORT_DESC);

$query = new Query;
$query->select(['xta_obj.*', 
                'xta_valuta.shortname as valutaname', 
                'xta_city.name as cityname',
                'xta_district.name as districtname',
                'xta_album.id_image',
               ]);
$query->from('xta_obj');
$query->join('LEFT JOIN', 'xta_valuta', 'xta_valuta.id = xta_obj.id_valuta');
$query->join('LEFT JOIN', 'xta_city', 'xta_city.id = xta_obj.id_city');
$query->join('LEFT JOIN', 'xta_district', 'xta_district.id = xta_obj.id_district');
$query->join('LEFT JOIN', 'xta_album', 'xta_album.id = xta_obj.id_album');
$query->where($where);
$query->limit(20);
$query->offset(0);
$query->orderBy($orderBy);
$command = $query->createCommand();
$select = $command->queryAll();
$data['select'] = array();
foreach ($select as $item)
{
	$model__xta_obj_category = xta_obj_category::find()->where(["id" => $item['id_category']])->one();
	$category_array = $model__xta_obj_category->fn__get_categs_array();
	$category_names = array();
	foreach ($category_array as $key1 => $value1)
	{
		$category_names[]='<a href="/board/'.$key1.'">'.$value1.'</a>';
	}
	$category_names = implode(' → ',$category_names);
	$item['category_names']=$category_names;
	
	if ($item['id_district']==0)
	{
		$item['districtname'] = 'Все районы';
	}
	
	$item['image'] = xta_image::fn__get_nosrc();
	if ($item['id_image'])
	{
		$model__xta_image = xta_image::find()->where(["id" => $item['id_image']])->one();
		if ($model__xta_image)
		{
			$item['image'] = $model__xta_image->fn__get_thumb_src('small');
		}
	}
	
	if (intval($item['price']))
	{
		$item['price']='<b>'.$item['price'].'</b> '.$item['valutaname'];
	}
	else
	{
		$item['price']='';
	}
	
	$data['select'][] = $item;
}
//==============================================================================
$data['districts'] = xta_district::fn__get_data_list(true);
$data['current_city_name'] = xta_city::fn__get_current_city()->name;
$data['current_city_name_padeg_1'] = xta_padeg::fn__get_padeg($data['current_city_name'],1);
$data['current_city_name_padeg_5'] = xta_padeg::fn__get_padeg($data['current_city_name'],5);


$data['xta_seo_tdz'] = xta_seo_tdz::fn__get_xta_seo_tdz();


// Cache
//==============================================================================
	xta_cache::fn__set_cache($id_cache,$data,array('main_page'));
else:
	$data = $cache;
endif;
//==============================================================================

echo $this->render('index',$data);
