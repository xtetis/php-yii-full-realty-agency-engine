<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\RegisterForm;
use app\models\ForgotPassForm;
use app\models\SettingsForm;
use app\models\PasswordForm;
use app\models\User;
use app\models\gii\xta_user;
use app\models\Email;


class AccountController extends Controller
{


    public function behaviors()
    {
    
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['login'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render('index');
    }






	public function actionLogin($command='',$id=0)
	{
		if (!\Yii::$app->user->isGuest)
		{
			return $this->goHome();
		}

		if (
		     (strlen($command)) &&
		     (intval($id))
		   )
		{
			$model__xta_user = xta_user::findOne(intval($id));
			if ($model__xta_user->accesstoken==$command)
			{
				Yii::$app->user->login(User::findByEmail($model__xta_user->email), 3600*24*30);
				$model__xta_user->accesstoken = md5(uniqid());
				$model__xta_user->authkey = md5(uniqid().uniqid());
				$model__xta_user->save();
				if (
				     (isset($_GET['url'])) &&
				     (strlen($_GET['url']))
				   )
				{
					$url = base64_decode($_GET['url']);
					header("Location: ".$url);
					exit();
				}
				else
				{
					return $this->goHome();
				}
			}
		}

		$model = new LoginForm();
		if ($model->load(Yii::$app->request->post()) && $model->login())
		{
			return $this->goBack();
		}
		return $this->render('view__login', ['model' => $model]);
	}

















	public function actionRegister()
	{
		$model_xta_user = new xta_user();
		$model_RegisterForm = new RegisterForm();
		if ($model_RegisterForm->load(Yii::$app->request->post()) && $model_RegisterForm->register()) 
		{
			$data = \Yii::$app->request->post('RegisterForm', []);
			$model_LoginForm = new LoginForm();
			$model_LoginForm->email = $data['email'];
			$model_LoginForm->pass = $data['pass'];
			$model_LoginForm->rememberMe = true;
			$model_LoginForm->login();
			Email::fn__send_register_notification_to_user(Yii::$app->user->id,$data['pass']);
			return $this->render('/blocks/view__register_confirm',$data);
		}
		else 
		{
			return $this->render('view__register', ['model' => $model_RegisterForm]);
		}
	}


	public function actionLogout()
	{
		Yii::$app->user->logout();
		return $this->goHome();
	}


	public function actionForgotpass()
	{
		if (!\Yii::$app->user->isGuest) {
			return $this->goHome();
		}

		$model_ForgotPassForm = new ForgotPassForm();
		if ($model_ForgotPassForm->load(Yii::$app->request->post()) && $model_ForgotPassForm->sendpass()) 
		{
			$data = \Yii::$app->request->post('ForgotPassForm', []);
			return $this->render('/blocks/view__forgot_password_confirm',$data);
		}
		else
		{
			return $this->render('view__forgot_password', ['model' => $model_ForgotPassForm]);
		}
	}
	
	

}
