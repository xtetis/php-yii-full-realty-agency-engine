<?
use app\models\LibCommon;
use app\models\LibSql;
use yii\db\Query;
$connection = \Yii::$app->db;



$data = array();



// Пагинация
//==============================================================================
$sql = "SELECT count(*) as 'count'  FROM 
        (SELECT DISTINCT `id_user_from` FROM 
        (SELECT 
          `id`,
          `createdon`,
          `id_user_from` 
        FROM 
          `xta_user_message`
        WHERE 
          `id_user_to` =".Yii::$app->user->id."
        UNION
        SELECT
          `id`,
          `createdon`, 
          `id_user_to`
        FROM 
          `xta_user_message`
        WHERE 
          `id_user_from` =".Yii::$app->user->id."
        ORDER BY 
          `createdon` DESC ) as t1
        ) as t2";
if ($this->action->id =='unreaded'){
$sql = "SELECT count(*) as 'count'  FROM 
        (
        SELECT DISTINCT
						`id_user_from`
					FROM 
						`xta_user_message`
					WHERE 
						`id_user_to` =".Yii::$app->user->id."
						 AND `readed`=0
        ) as t2";
   }


$model_count = $connection->createCommand($sql);
$count_arr = $model_count->queryAll();
$count = $count_arr[0]['count'];
$maxpage            = ceil($count/Yii::$app->params['per_pages']);
$page               = LibCommon::fn__get_correct_page($maxpage);
$data['pagination'] = LibCommon::fn__get_html_pagination('/account/messages'.(strlen($command)?'/'.$command:'').'?page=', $maxpage, $page);
//==============================================================================






//==============================================================================
$sql = "SELECT DISTINCT `id_user_from`  FROM 
        (SELECT 
          `id`,
          `createdon`,
          `id_user_from` 
        FROM 
          `xta_user_message`
        WHERE 
          `id_user_to` =".Yii::$app->user->id."
        UNION
        SELECT
          `id`,
          `createdon`, 
          `id_user_to`
        FROM 
          `xta_user_message`
        WHERE 
          `id_user_from` =".Yii::$app->user->id."
        ORDER BY 
          `createdon` DESC ) as t1
        LIMIT ".(($page-1)*10)." , 10";


if ( $this->action->id =='unreaded')
{
	$sql = "SELECT DISTINCT `id_user_from` FROM
					(SELECT
						`id_user_from`
					FROM 
						`xta_user_message`
					WHERE 
						`id_user_to` =".Yii::$app->user->id."
						 AND `readed`=0
					ORDER BY 
						`createdon` DESC
					) as t1
					LIMIT ".(($page-1)*10)." , 10";
}

$model = $connection->createCommand($sql);
$user_from = $model->queryAll();

$data['message_list'] = array();

foreach ($user_from as $user_from_item){
	$other_user = $user_from_item['id_user_from'];
	$username = LibSql::fn__get_field_val_by_id('xta_user','username',$other_user);



	$count = LibSql::fn__get_count_by_where('xta_user_message',"
		(`id_user_to` = ".$other_user." AND `id_user_from`=".Yii::$app->user->id.")
		OR
		(`id_user_to` = ".Yii::$app->user->id." AND `id_user_from`=".$other_user.")
		");


	$latest = LibSql::fn__get_fieldval_by_where('xta_user_message','createdon' ,"
		(`id_user_to` = ".$other_user." AND `id_user_from`=".Yii::$app->user->id.")
		OR
		(`id_user_to` = ".Yii::$app->user->id." AND `id_user_from`=".$other_user.")
		ORDER BY `xta_user_message`.`createdon` DESC 
		");

	$unreaded = LibSql::fn__get_count_by_where('xta_user_message',"
	`id_user_to` = ".Yii::$app->user->id." AND `id_user_from`=".$other_user." AND `readed` = 0");

	$messagea=array(
		'id_user'        => Yii::$app->user->id,
		'id_other_user'  => $other_user,
		'unreaded'       => $unreaded,
		'latest'         => $latest,
		'count'          => $count,
		'username'       => $username,
	);

	$data['message_list'][] = $messagea;
}
//==============================================================================
$data['id_user'] = Yii::$app->user->id;
$data['command'] = $this->action->id;
  
  


echo $this->render('view__index', $data);
