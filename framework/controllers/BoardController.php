<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;





class BoardController extends Controller
{

	public function actionIndex($command='select',$id=NULL)
	{
		require(__DIR__.'/board/'.$this->action->id.'.php');
	}


	public function actionArchive($command='select',$id=NULL)
	{
		require(__DIR__.'/board/'.$this->action->id.'.php');
	}


	public function actionAdd()
	{
		if (Yii::$app->user->isGuest)
		{
			return Yii::$app->getResponse()->redirect('/account/login');
		}
		require(__DIR__.'/board/'.$this->action->id.'.php');
	}


	public function actionItem($command='select',$id=NULL)
	{
		require(__DIR__.'/board/'.$this->action->id.'.php');
	}


	public function actionMyad($command='',$id=NULL)
	{
		if (Yii::$app->user->isGuest)
		{
			return Yii::$app->getResponse()->redirect('/account/login');
		}
		require(__DIR__.'/board/'.$this->action->id.'.php');
	}


}
