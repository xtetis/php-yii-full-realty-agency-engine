<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;





class AjaxController extends Controller
{
	public $layout = 'main';
	
	
	
	public function actionIndex()
	{

	}


	public function actionAdd_categories_list($command='all',$id=NULL)
	{
		$this->enableCsrfValidation = false;
		require(__DIR__.'/ajax/'.$this->action->id.'.php');
	}


	public function actionGet_categoryname_by_id($command='name',$id=NULL)
	{
		$this->enableCsrfValidation = false;
		require(__DIR__.'/ajax/'.$this->action->id.'.php');
	}


	public function actionGet_category_option_inputs($command='all',$id=NULL)
	{
		$this->enableCsrfValidation = false;
		require(__DIR__.'/ajax/'.$this->action->id.'.php');
	}


	public function actionGet_dialog_message_list($command='all',$id=NULL)
	{
		if (\Yii::$app->user->isGuest)
		{
			return Yii::$app->getResponse()->redirect('/account/login');
		}
		$this->enableCsrfValidation = false;
		$this->layout = 'empty';
		require(__DIR__.'/ajax/'.$this->action->id.'.php');
	}


	public function actionSend_user_message($command='all',$id=NULL)
	{
		if (\Yii::$app->user->isGuest)
		{
			return Yii::$app->getResponse()->redirect('/account/login');
		}
		
		$this->enableCsrfValidation = false;
		require(__DIR__.'/ajax/'.$this->action->id.'.php');
	}


	public function actionSend_abuse_to_board_item($command='all',$id=NULL)
	{
		$this->enableCsrfValidation = false;
		require(__DIR__.'/ajax/'.$this->action->id.'.php');
	}
	


	
	public function actionLove_album($command='all',$id=NULL)
	{
		if (\Yii::$app->user->isGuest)
		{
			return Yii::$app->getResponse()->redirect('/account/login');
		}
		$this->enableCsrfValidation = false;
		require(__DIR__.'/ajax/'.$this->action->id.'.php');
	}
	

	public function actionCache($command='all',$id=NULL)
	{
		if (\Yii::$app->user->isGuest)
		{
			return Yii::$app->getResponse()->redirect('/account/login');
		}
		require(__DIR__.'/ajax/'.$this->action->id.'.php');
	}

}
