<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Регистрация завершена';
?>
<div class="row top-buffer">
	<div class="blocks___view__register_confirm col-md-6 col-md-offset-3">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h1 class="panel-title">Успешно создана учетная запись:</h1>
			</div>
			<div class="panel-body">
				<ul style="list-style-type: none;">
					<li>
						<b>Логин</b> - <?=$email?>
					</li>
					<li>
						<b>Пароль</b> - <?=$pass?>
					</li>
				</ul>
				<br>
				<p>Вы можете использовать эти данные для 
					<a href="<?=Url::to(['account/login']);?>">
						авторизации на сайте
					</a>
					.
				</p>
				<p>Регистрационные данные отправлены Вам на электронную почту.</p>
			</div>
		</div>
	</div>
</div>
