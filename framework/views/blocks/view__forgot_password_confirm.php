<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;


$this->title = 'Восстановление пароля';
?>
<div class="row top-buffer">
	<div class="blocks___view__forgot_password_confirm col-md-6 col-md-offset-3">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h1 class="panel-title">Восстановление пароля</h1>
			</div>
			<div class="panel-body">
				<p>Инструкции по восслановлению пароля отправлены на ваш email:</p>
				<ul style="list-style-type: none;">
					<li>
						<b>Email</b> - <?=$email?>
					</li>
				</ul>
				<br>
				<p>Проверьте ваш Email и следуйте полученным инструкциям для получения доступа к вашей учетной записи и последующей смены пароля.</p>
			</div>
		</div>
	</div>
</div>
