<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Смена пароля';
?>
<div class="row top-buffer">
	<div class="blocks___view__change_password_confirm col-md-6 col-md-offset-3">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h1 class="panel-title">Пароль изменен</h1>
			</div>
			<div class="panel-body">
				<p>Пароль от вашей учетной записи успешно изменен:</p>
				<ul style="list-style-type: none;">
					<li>
						<b>Новый пароль</b> - <?=$pass?>
					</li>
				</ul>
				<br>
				<p>На ваш Email отправлено письмо с новым паролем.</p>
			</div>
		</div>
	</div>
</div>
