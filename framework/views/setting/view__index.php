<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
$this->title = 'Настройки пользователя';
?>
<div class="row top-buffer">
	<div class="blocks___view__register_confirm col-md-6 col-md-offset-3">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h1 class="panel-title">Настройки пользователя</h1>
			</div>
			<div class="panel-body">
				<?php $form = ActiveForm::begin(); ?>

				<?= $form->field($model, 'username') ?>
				<?= $form->field($model, 'phone') ?>
				<?= $form->field($model, 'skype') ?>

				<div class="form-group text-center">
					<?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
				</div>
				
				<?php ActiveForm::end(); ?>
			</div>
		</div>
	</div>
</div>


