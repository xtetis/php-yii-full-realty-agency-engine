<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
$this->title = 'Смена пароля';
?>

<div class="row top-buffer">
	<div class="account___view__settings_password col-md-6 col-md-offset-3">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h1 class="panel-title">Смена пароля</h1>
			</div>
			<div class="panel-body">
				<?php $form = ActiveForm::begin(); ?>

				    <?= $form->field($model, 'pass')->passwordInput() ?>
				    <?= $form->field($model, 'pass_repeat')->passwordInput() ?>
				
				    <div class="form-group text-center">
				        <?= Html::submitButton('Изменить пароль', ['class' => 'btn btn-primary']) ?>
				    </div>
				<?php ActiveForm::end(); ?>
			</div>
		</div>
	</div>
</div>

