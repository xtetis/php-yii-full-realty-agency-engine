<?
use yii\widgets\ListView;
use yii\widgets\ActiveForm;
use app\models\LibCommon;
?>




<?
// Title
//**************************************************************************************************
$this->title='Архив объявлений';
//**************************************************************************************************











if (isset($_GET['page']) || isset($_GET['filter']))
{
	$this->registerMetaTag([
		'name'    => 'robots',
		'content' => 'noindex,follow'
	]);
}

?>









<div class="panel panel-default" style="margin-top:10px;">
	<div class="panel-heading">
		<div class="row">
			<div class="col-md-1" style="text-align: center;">
				<img src="<?=$category_filter['img']?>" style="width: 50px; height: 50px;">
			</div>
			<div class="col-md-8">
				<div style="font-size: 19px; font-weight: bold;">Архив объявлений</div>
				<div style="font-size: 13px; color: gray;">Архивные объявления: <?=$category_filter['count_obj']?> объявлений</div>
			</div>
			
			
			
		</div>
	</div>
</div>


<?
foreach ($select as $obj)
{
?>

<div class="row" style="margin:0px; border-bottom: 1px solid rgb(215, 215, 215); padding-top: 5px; padding-bottom: 5px; color: #000;">
		<div class="col-md-1 text-left" style="font-size: 13px;"><?=$obj['pub_date']?></div>
		<div class="col-md-1 obj_img">
			<img src="<?=$obj['image']?>">
		</div>
		<div class="col-md-8">
			<div class="obj_name">
				<a href="/board/item/<?=$obj['id']?>">
					<?=$obj['name']?>
				</a>
			</div>
			<div class="category_names"><?=$obj['category_names']?></div>
			<div class="category_names"><?=$obj['cityname']?> → <?=$obj['districtname']?></div>
		</div>
		<div class="col-md-2 obj_price"><?=$obj['price']?></div>
	</div>



<?}?>

<?=$pagination?>




