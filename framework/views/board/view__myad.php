<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\grid\GridView;
use yii\widgets\ListView;
use yii\helpers\Url;
use app\models\gii\xta_album;
use app\models\gii\xta_obj_category;


if ($command=='')
{
	$command_title='активные';
}
elseif ($command=='unpublished')
{
	$command_title='снятые с публикации';
}
elseif ($command=='archive')
{
	$command_title='архивные';
}
elseif ($command=='blocked')
{
	$command_title='заблокированные';
}
$this->title = 'Мои '.$command_title.' объявления';
?>
<div class="row top-buffer">
	<div class="blocks___view__obj col-md-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h1 class="panel-title">Мои <?=$command_title?> объявления</h1>
			</div>
			<div class="panel-body" style="padding: 7px;">


<?

foreach ($objs as $objs_item)
{

	$obj_unpublished_cause='';
	if (!$objs_item['abusecause'])
	{
		$pub_btn = '<li>
									<a href="/board/myad/unpublish/'.$objs_item['id'].'">
										<i class="glyphicon glyphicon-eye-close"></i> Снять с публикации</a></li>
								<li>
									<a href="/board/myad/updatepub/'.$objs_item['id'].'">
										<i class="glyphicon glyphicon-arrow-up"></i>Поднять наверх</a></li>
										';
		$obj_unpublished_cause='
		<div class="obj_unpublished_cause" style="background-color: lime; color: #000;">
			Опубликовано
		</div>
		';
	}
	if	($command=='unpublished')
	{
		$pub_btn = '<li>
									<a href="/board/myad/publish/'.$objs_item['id'].'">
										<i class="glyphicon glyphicon-eye-open"></i> Опубликовать</a></li>';
		$obj_unpublished_cause='
		<div class="obj_unpublished_cause" style="background-color: yellow; color: #000;">
			Снято с публикации
		</div>
		';
	}
	elseif ($command=='archive')
	{
		$pub_btn = '<li>
									<a href="/board/myad/publish/'.$objs_item['id'].'">
										<i class="glyphicon glyphicon-eye-open"></i> Опубликовать</a></li>';
		$obj_unpublished_cause='
		<div class="obj_unpublished_cause" style="background-color: yellow; color: #000;">
			Объявлению больше '.Yii::$app->params['count_days_obj_active'].' дней
		</div>
		';
	}
	elseif ($command=='blocked')
	{
		$obj_unpublished_cause='
			<div class="obj_unpublished_cause">
				Получена жалоба на объявление
			</div>
		';
	}
	
	
	
	$price = '';
	if ($objs_item['price'])
	{
		$price = '<strong style="color:#4e9a06;">'.$objs_item['price'].'</strong> '.$objs_item['valutaname'];
	}


	$model__xta_obj_category = xta_obj_category::findOne($objs_item['id_category']);
	$category_array = $model__xta_obj_category->fn__get_categs_array();
	$category_names = array();
	foreach ($category_array as $key => $value)
	{
		$category_names[]=' <a href="/board/'.$key.'">'.$value.'</a> ';
	}
	$category_names = implode('→',$category_names);

?>

				
				<div class="panel panel-default" style="<?=(($command=='unpublished')?'background: rgb(222, 222, 204) none repeat scroll 0% 0%;':'')?>">
					<?=$obj_unpublished_cause;?>
					<div class="panel-body" style="padding: 7px 5px;">
						<div class="col-md-1 text-left" style="font-size: 13px;"><?=$objs_item['pub_date']?></div>
						<div class="col-md-1"><img src="<?=xta_album::fn__get_main_img_src($objs_item['id_album'])?>" 
										                   style="max-height:50px; max-width:50px;">
						</div>
						<div class="col-md-7">
							<div class="obj_name">
								<a href="/board/item/<?=$objs_item['id']?>" target="_blank">
									<?=$objs_item['name']?>
								</a>
							</div>
							<div class="category_names"><?=$category_names?></div>
						</div>
						<div class="col-md-1" style="padding-top: 15px; white-space:nowrap;"><?=$price?></div>
						<div class="col-md-2 text-right" style="padding-top: 7px;">
						<?if (!$objs_item['abusecause']):?>
							<div class="dropdown">
								<button class="btn btn-default dropdown-toggle bootstrap_hover_dropdown" 
												type="button" id="dropdownMenu1" data-toggle="dropdown" 
												aria-haspopup="true" aria-expanded="true">
									 <i class="glyphicon glyphicon-cog"></i> Управление
									<span class="caret"></span>
								</button>
									<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
										<li>
											<a href="/board/item/<?=$objs_item['id']?>" target="_blank">
												<i class="glyphicon glyphicon-share-alt"></i> Просмотр</a></li>
										<li>
											<a href="/board/myad/edit/<?=$objs_item['id']?>">
												<i class="glyphicon glyphicon-pencil"></i> Редактировать</a></li>
										<?=$pub_btn?>
									</ul>
							</div>
							<?endif;?>
						</div>
					</div>
				</div>
<?
}
?>
<?=$pagination?>
			</div>
		</div>
	</div>
</div>


