<link rel="stylesheet" type="text/css" href="/public/components/html5-uploader/style.css" />
<?php

$this->registerJsFile(Yii::$app->request->baseUrl.'/public/templates/default/js/board_add.js',['depends' => [\yii\web\JqueryAsset::className()]]);

$js = '
    $("[data-toggle=\'tooltip\']").tooltip();
    $("[data-toggle=\'popover\']").popover({ trigger : \'hover focus\',html: true});
';
$this->registerJs($js);

$this->registerJsFile(Yii::$app->request->baseUrl.'/public/components/html5-uploader/javascript.js',['depends' => [\yii\web\JqueryAsset::className()]]);




use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\gii\xta_district; 
use app\models\gii\xta_valuta; 
use app\models\gii\xta_obj_option;




?>





<div class="row top-buffer">
	<div class="view___<?=str_replace('/','__',$this->context->module->requestedRoute)?> col-md-12">


	
		<div class="panel panel-info">
			<div class="panel-heading">
				<h1 class="panel-title">
				
<?
if ($model->id_obj)
{
	echo 'Редактировать объект недвижимости';
}
else
{
	echo 'Добавить объект недвижимости';
}
?>
				</h1>
			</div>
			<div class="panel-body">
				<?php $form = ActiveForm::begin(['id' => 'register-form']); 
				?>




<?=$form->field($model, 'name',[
  'template' => '
<div class="row">
  <div class="col-md-3 text-right">
    {label}
    <span class="red">*</span>
  </div>
  <div class="col-md-6" 
       data-toggle="popover" 
       data-title="Напишите заголовок" 
       data-content="Введите подробный и понятный заголовок.">
     {input} {error}
  </div>
</div>'
,'errorOptions' => ['encode' => false]])->textInput() ?>



				<?=$form->field($model, 'id_category',[
  'template' => '
<div class="row">
  <div class="col-md-3 text-right">
    {label}
    <span class="red">*</span>
  </div>
  <div class="col-md-6" 
       data-toggle="popover" 
       data-title="Выберите рубрику" 
       data-content="Для добавления объекта - обязательнот выберите соответствующую рубрику. 
                     Это поможет клиентам скорее с Вами связаться.">
     <button type="button" class="btn btn-primary" id="button__board_add_id_categiry">
       Выбрать рубрику
     </button>
     <div class="input-group category_selector" id="board_add__category_selector">
       <input type="text" class="form-control" id="id_category_selector" readonly="readonly">
       <div class="input-group-addon" id="change__board_add_id_categiry" style="cursor:pointer;">Изменить</div>
     </div>
     {input} 
     {error}
  </div>
</div>'
])->hiddenInput() ?>

<div id="obj_options_container">
<?
if ($model->id_category)
{
echo xta_obj_option::fn__get_category_option_inputs($model->id_category);
}
?>
</div>



<?
$data_list = xta_district::fn__get_data_list(true);
$data_list = [0=>'Все районы'] + $data_list;
echo $form->field($model, 'id_district',[
  'template' => '
<div class="row">
  <div class="col-md-3 text-right">
    {label}
    <span class="red">*</span>
  </div>
  <div class="col-md-6" 
       data-toggle="popover" 
       data-title="Выберите район" 
       data-content="Выберите район города, который соответствует вашему объекту">
     {input} 
     {error}
  </div>
</div>'
])->dropDownList($data_list) ?>

<?=$form->field($model, 'about',[
  'template' => '
<div class="row">
  <div class="col-md-3 text-right">
    {label}
    <span class="red">*</span>
  </div>
  <div class="col-md-6" 
       data-toggle="popover" 
       data-title="Опишите подробно объект" 
       data-content="<ul class=\'b_add_tooltip_list\'>
  <li>Сделайте ваше описание побуждающим к действию. </li>
  <li>Предоставьте как можно больше подробностей - так вы избежите лишних вопросов и вызовете больше доверия.</li>
  <li>Старайтесь писать грамотно и корректно.</li>
  </ul>">
     {input} 
     {error}
  </div>
</div>','errorOptions' => ['encode' => false]
])->textarea(['rows' => 6]) ?>
<?=$form->field($model, 'price',[
  'template' => '
<div class="row">
  <div class="col-md-3 text-right">
    {label}
  </div>
  <div class="col-md-6" 
       data-toggle="popover" 
       data-title="Укажите цену"
       data-content="Установите корректную цену на ваш товар или услугу. <br> 
                     Оставьте поле пустым, если цена договорная.">
     {input} 
'.
$form->field($model, 'id_valuta',['template' => '{input}'])->dropDownList(xta_valuta::fn__get_data_list())
.'
     {error}
  </div>
</div>
'
])->textInput(['maxlength' => 20, 'class' => 'intval form-control']); ?>










<?
if ((isset($model->images))&&(is_array($model->images))&&(count($model->images)))
{
	foreach ($model->images as $img_item)
	{
		$js = '
			dataArray.push({name : \'file1\', value : \''.$img_item.'\'});
			addImage((dataArray.length-1));
			$(\'#uploaded-holder\').show();
			$(\'#upload-button\').css({\'display\' : \'block\'});
		';
		$this->registerJs($js);
	}
}
?>
	<div class="row" style="margin-bottom: 30px;">
		<div class="col-md-3 text-right">
			<label class="control-label" for="boardaddform-price">Изображения</label>
			<span class="red">*</span>
		</div>
		<div class="col-md-6" 
		     data-toggle="popover" 
		     data-title="Добавьте фотографии" 
		     data-content="Рекомендуем добавить фотографии чтобы потенциальным покупателям/клиентам 
		                   было проще оценить преимущества вашего предложения">
			<!-- Область для перетаскивания -->
			<div id="drop-files" ondragover="return false" style="height:120px;">
				<p>Перетащите сюда или выберите изображение</p>
				<input type="file" name="images" id="uploadbtn"
				       style="text-align: center; font-size: 16px; margin: 0px auto;" multiple />
			</div>
			
			<!-- Область предпросмотра -->
			<div id="uploaded-holder"> 
				<div id="dropped-files">
					<!-- Кнопки загрузить и удалить, а также количество файлов -->
					<div id="upload-button">
						<center>
							<span>0 Файлов</span>
							<div id="loading">
								<div id="loading-bar">
									<div class="loading-color"></div>
								</div>
								<div id="loading-content"></div>
							</div>
						</center>
					</div>
				</div>
			</div>
			<!-- Список загруженных файлов -->
			<div id="file-name-holder">
				<ul id="uploaded-files">
					<h1>Загруженные файлы</h1>
				</ul>
			</div>
		</div>
	</div>




<?=$form->field($model, 'phone',[
  'template' => '
<div class="row">
  <div class="col-md-3 text-right">
    {label}
    <span class="red">*</span>
  </div>
  <div class="col-md-6" 
       data-toggle="popover" 
       data-title="Номер телефона" 
       data-content="Укажите контактный номер телефона">
     {input} {error}
  </div>
</div>'
])->textInput() ?>




<?if (Yii::$app->user->isGuest):?>



<?=$form->field($model, 'username',[
  'template' => '
<div class="row">
  <div class="col-md-3 text-right">
    {label}
    <span class="red">*</span>
  </div>
  <div class="col-md-6" 
       data-toggle="popover" 
       data-title="Контактное лицо" 
       data-content="Как к Вам обращаться?">
     {input} {error}
  </div>
</div>'
])->textInput() ?>



<?=$form->field($model, 'email',[
  'template' => '
<div class="row">
  <div class="col-md-3 text-right">
    {label}
    <span class="red">*</span>
  </div>
  <div class="col-md-6" 
       data-toggle="popover" 
       data-title="Email-адрес" 
       data-content="Укажите реальный email для получения уведомлений и управления объектом">
     {input} {error}
  </div>
</div>'
])->textInput() ?>



<?=$form->field($model, 'skype',[
  'template' => '
<div class="row">
  <div class="col-md-3 text-right">
    {label}
  </div>
  <div class="col-md-6" 
       data-toggle="popover" 
       data-title="Skype" 
       data-content="Укажите skype, как альтернативный способ с вами связаться">
     {input} {error}
  </div>
</div>'
])->textInput() ?>
<?endif;?>











				<?//=$form->field($model, 'pass_repeat')->passwordInput() ?>
				<?//= $form->field($model, 'email')->input('email') ?>

				<div class="form-group text-center" style="margin-top:50px;">
				
						<?= Html::submitButton(((intval($model->id_obj))?'Сохранить объявление':'Добавить объявление'), ['class' => 'btn btn-primary', 'name' => 'register-button']) ?>
				</div>
		
				<?php ActiveForm::end(); ?>
			</div>
		</div>

	</div>
</div>




<div class="modal fade" id="modal__board_add_id_categiry" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Выберите категорию</h4>
      </div>
      <div class="modal-body">
        
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


