<?
use app\components\FrontendRelatedObj;




$this->title=$model__xta_obj->name.' '.$current_city_name.' | '.ucfirst($_SERVER['HTTP_HOST']);






$description = mb_substr($model__xta_obj->about,0,140,'utf-8').'...';
$description = str_replace("\n", "",$description);
$description = str_replace("\r", "",$description);
$description = str_replace('  ',' ',$description);
$curcateg = '';
foreach ($categs_array as $key => $value){
	$curcateg = $value;
}

$description = $curcateg.' в '.$current_city_name_p5.': '.$description;

$this->registerMetaTag([
	'name'    => 'description',
	'content' => $description
]);








$js = '
$(\'.bxslider\').bxSlider({
  pagerCustom: \'#bx-pager\'
});
';
$this->registerJs($js);
?>


<input type="hidden" id="id_board_item" value="<?=$model__xta_obj->id?>">

<div class="row" style="margin-right: 0px;">
	<div class="col-md-9">
		<ol class="breadcrumb">
			<li><a href="/">Главная</a></li>
			<li><a href="/board">Объявления</a></li>
			<?foreach ($categs_array as $key => $value){?>
				<li><a href="/board/<?=$key?>"><?=$value?></a></li>
			<?}?>
		</ol>
		<? ?>
		<h1 class="board_item_h1"><?=$model__xta_obj->name?></h1>
		<div class="board_under_h1 row">
			<div class="col-md-3 board_block_city">
				<img src="/public/templates/default/img/ico/mapicon_32.png" style="width:16px; height:16px;"> 
				<strong><?=$model__xta_city->name?></strong>
			</div>
			<div class="col-md-9">
				Добавлено: <?=$model__xta_obj->createdon?>, номер объявления <?=$model__xta_obj->id?> 
			</div>
		</div>





		<?if ($is_archive):?>
			<div class="alert alert-warning" role="alert" style="margin-top:10px; background: yellow;">
				<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
				<span class="sr-only">Внимание:</span>
				Объявление устарело и перенесено в архив
			</div>
		<?endif;?>







		<?if(count($model__xta_image_many)):?>
		<div class="panel panel-default">
			<div class="panel-body">




<ul class="bxslider">
<?foreach ($model__xta_image_many as $model_image){?>
	<li><img src="<?=$model_image->fn__get_thumb_src('big')?>" /></li>
<?}?>
</ul>


<div id="bx-pager">
<?
$imgcounter=0;
foreach ($model__xta_image_many as $model_image){?>
  <a data-slide-index="<?=$imgcounter++?>" style="cursor:pointer;"><img src="<?=$model_image->fn__get_thumb_src('small')?>" /></a>
<?}?>
</div>


			</div>
		</div>
		<?endif;?>




		
		<div class="panel panel-default" style="margin-top:20px; margin-bottom:10px;">
			<div class="panel-body">
				<div class="col-md-4">
					<strong>Район</strong>:
					<span><?=$district_name?></span>
				</div>
				<?if ((isset($model__xta_obj_option_value_many))&&(count($model__xta_obj_option_value_many))):?>
					<?
					foreach ($model__xta_obj_option_many as $model__xta_obj_option_item)
					{
						foreach ($model__xta_obj_option_value_many as $model__xta_obj_option_value_item)
						{
							if ($model__xta_obj_option_item->id == $model__xta_obj_option_value_item->id_obj_option)
							{
							?>
							<div class="col-md-4">
								<strong><?=$model__xta_obj_option_item->name?></strong>:
								<span><?=$model__xta_obj_option_value_item->value?></span>
							</div>
							<?
							}
						}
					}
					?>
				<?endif;?>
			</div>
		</div>
		


		<div class="panel panel-default" style="margin-top:20px; margin-bottom:10px;">
			<div class="panel-body board_item_about_content">
				<?=$model__xta_obj->about?>
			</div>
		</div>





		
	</div>
	<div class="col-md-3" style="padding:5px;">
		<?if ($model__xta_obj->price):?>
			<div style="text-align: center;
background: rgb(255, 234, 0) none repeat scroll 0% 0%;
padding: 25px;
padding-top:15px;
padding-bottom:15px;
border-radius: 10px;
margin-bottom:10px;
color: #000;
font-weight: bold;
font-size: 25px;">
			<?=$model__xta_obj->price?>
			<?=$model__xta_valuta->shortname?>
			</div>
		<?endif;?>
		
		
		<?if ($is_archive):?>
		
		<?else:?>
			<div style="text-align: center;padding: 10px; padding-bottom:5px; font-size: 18px;" id="">
				Связаться с автором
			</div>
			<div style="padding-bottom:5px;text-align: center; font-weight: bold; font-size: 20px;">
				<?=$model__xta_user->username?>
			</div>
			<div class="panel panel-default">
				<div class="panel-body">
					<div>
						<button type="button" style="width:100%; font-size: 19px;" class="btn btn-primary btn-lg"
							id="btn__message_to_board_item_author_modal_show">
							<i class="glyphicon glyphicon-envelope"></i>
							Написать автору
						</button>
					</div>
					<div style="margin-top:10px;">
						<button type="button" style="width:100%; font-size: 19px;" class="btn btn-primary btn-lg"
						id="board_item_phone_btn">
							<div>
								<i class="glyphicon glyphicon-earphone"></i>
								<? 
								if (strlen($model__xta_obj->phone))
								{
									$phone = $model__xta_obj->phone;
								}
								else
								{
									$phone = $model__xta_user->phone;
								}
								?>
								<span id="board_item_phone_block" idx="<?=base64_encode($phone)?>">
									<?=substr($phone,0,3)?>
									<?=preg_replace('/(\d)/i','*',substr($phone, 3));?>
								</span>
							</div>
							<div style="font-size: 12px; margin-top: 5px;" id="board_item_phone_alert">
								<span style="border-bottom-width: 1px;border-bottom-color: #fff; border-bottom-style: dashed;">
									Показать телефон
								</span>
							</div>
						</button>
					</div>
				</div>
			</div>
		<?endif;?>
		
		
		
	</div>
</div>


















<div class="modal fade" tabindex="-1" role="dialog" id="modal__message_to_board_item_author">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Отправить сообщение автору</h4>
      </div>
      <div class="modal-body">
      	<div id="modal_alert_message" class="alert alert-danger"></div>
	      <input type="hidden" id="message_to_board_item__id_user" value="<?=$model__xta_user->id?>">
	      <?if (!Yii::$app->user->id):?>
				<div class="form-group group__message_to_board_item__username">
					<label>Имя</label>
					<input type="text" class="form-control" id="message_to_board_item__username" placeholder="Имя">
				</div>
				<div class="form-group group__message_to_board_item__email">
					<label>Email адрес</label>
					<input type="email" class="form-control" id="message_to_board_item__email" placeholder="Email">
				</div>
				<div class="form-group group__message_to_board_item__phone">
					<label>Номер телефона</label>
					<input type="text" class="form-control" id="message_to_board_item__phone" placeholder="Телефон">
				</div>
				<?endif?>
				<div class="form-group">
					<label>Сообщение</label>
					<textarea class="form-control" id="message_to_board_item__message" rows="3"></textarea>
				</div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
        <button type="button" class="btn btn-primary" 
                id="btn__message_to_board_item_author_modal_send">Отправить сообщение</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->































<div class="modal fade" tabindex="-1" role="dialog" id="modal__abuse_to_board_item">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Пожаловаться на объявление</h4>
      </div>
      <div class="modal-body">
      	<div id="modal_alert_message__abuse" class="alert alert-danger"></div>
	      <?if (!Yii::$app->user->id):?>
				<div class="form-group group__abuse_to_board_item__username">
					<label>Имя</label>
					<input type="text" class="form-control" id="abuse_to_board_item__username" placeholder="Имя">
				</div>
				<div class="form-group group__abuse_to_board_item__email">
					<label>Email адрес</label>
					<input type="email" class="form-control" id="abuse_to_board_item__email" placeholder="Email">
				</div>
				<div class="form-group group__abuse_to_board_item__phone">
					<label>Номер телефона</label>
					<input type="text" class="form-control" id="abuse_to_board_item__phone" placeholder="Телефон">
				</div>
				<?endif?>
				<div class="form-group">
					<label>Текст жалобы</label>
					<textarea class="form-control" id="abuse_to_board_item__message" rows="3"></textarea>
				</div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
        <button type="button" class="btn btn-primary" 
                id="btn__abuse_to_board_item_modal_send">Отправить сообщение</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<?

?>
