<?
use yii\widgets\ListView;
use yii\widgets\ActiveForm;
use app\models\LibCommon;
use app\components\FrontendCategoryListTree;
?>




<?

// Title
//**************************************************************************************************
$title ='Недвижимость в '.$current_city_paged_5;

if ($id)
{
	$_fn__get_categs_array = $fn__get_categs_array;
	$title=array_pop($_fn__get_categs_array).' в '.$current_city_paged_5;
	if (count($_fn__get_categs_array))
	{
		$title.=' - '.array_pop($_fn__get_categs_array).' ';
	}
	$title.=' - недвижмость '.$current_city_paged_1;
}

if (
       ($model__xta_obj_category_description) &&
       (strlen(trim($model__xta_obj_category_description->title))) &&
       (!isset($_GET['page'])) &&
       (!isset($_GET['filter'])) &&
       (!isset($_GET['district']))
     ):
	$title = $model__xta_obj_category_description->title;
endif;


if (($xta_seo_tdz) && (strlen($xta_seo_tdz->title)))
{
	$title = $xta_seo_tdz->title;
}

$this->title=$title;
//**************************************************************************************************











// Description
//**************************************************************************************************
$description = 'Вся недвижимость '.$current_city_paged_1.' на сайте "'.ucfirst($_SERVER['HTTP_HOST']).'"';

if ($id)
{
	$_fn__get_categs_array = $fn__get_categs_array;
	$cur_categ_name = array_pop($_fn__get_categs_array);
	$description = 'Вся недвижимость "'.$cur_categ_name.'" в '.$current_city_paged_5.', '.$cur_categ_name;
	if (count($_fn__get_categs_array))
	{
		$parent_categ_name = array_pop($_fn__get_categs_array);
		$description = 'Вся недвижимость "'.$cur_categ_name.'" - "'.$parent_categ_name.'" в Одессе';
	}
}


if (
       ($model__xta_obj_category_description) &&
       (strlen(trim($model__xta_obj_category_description->description))) &&
       (!isset($_GET['page'])) &&
       (!isset($_GET['filter'])) &&
       (!isset($_GET['district']))
     ):
	$description = $model__xta_obj_category_description->description;
endif;


if (($xta_seo_tdz) && (strlen($xta_seo_tdz->description)))
{
	$description = $xta_seo_tdz->description;
}

$this->registerMetaTag([
	'name'    => 'description',
	'content' => $description
]);
//**************************************************************************************************



if (isset($_GET['page']) || isset($_GET['filter']))
{
	$this->registerMetaTag([
		'name'    => 'robots',
		'content' => 'noindex,follow'
	]);
}

?>









<div class="panel panel-default" style="margin-top:10px;">
	<div class="panel-heading">
		<div class="row">
			<div class="col-md-1" style="text-align: center; float: left;">
				<img src="<?=$category_filter['img']?>" style="width: 50px; height: 50px;" alt="<?=$category_filter['name_city']?>">
			</div>
			<div class="col-md-8">
				<div style="font-size: 19px; font-weight: bold;"><?=$category_filter['name']?></div>
				<div style="font-size: 13px; color: gray;">
					<h1 style="display:inline; font-size: 13px; color: gray;"><?=$category_filter['name_city']?></h1>
					: <?=$category_filter['count_obj']?> объектов
				</div>
			</div>







		</div>
	</div>
	<div class="panel-body" style="padding: 0px 0px;">














		<?if (count($category_filter['list'])):?>
			<div style="display: table;">
				<?
				/*
				foreach ($category_filter['list'] as $item)
				{?>
					<a href="<?=$item['link']?>">
						<div class="board_obj_list_cat_item">
							<div>
								<img src="<?=$item['img']?>" style="width:100px; height:100px;">
							</div>
							<div class="board_obj_list_cat_item_text">
								<?=$item['name']?>
							</div>
						</div>
					</a>
				<?
				}
				*/
				?>
			</div>
		<?else:?>
			<ol class="breadcrumb" style="margin-bottom: 0px; border-bottom: 1px solid rgb(216, 216, 216);">
				<li><a href="/">Главная</a></li>
				<li><a href="/board">Недвижимость</a></li>
				<?foreach ($obj_index_breadcrumbs as $key => $value){?>
					<li><a href="/board/<?=$key?>"><?=$value?></a></li>
				<?}?>
			</ol>
		<?endif;?>
























	</div>
	<div class="panel-footer">
		<div class="row">
			<div class="col-md-3">

			</div>
			<div class="sortbar">
				<ul class="nav nav-pills ">


				<?
				if (strlen($link['sort_newest']))
				{
					echo '<li role="presentation">
					        <a href="'.$link['sort_newest'].'">Самые новые</a>
					      </li>';
				}
				else
				{
					echo '<li role="presentation" class="active">
					        <a href="javascript:void(0)">Самые новые</a>
					      </li>';
				}
				?>
				<?
				if (strlen($link['sort_lowprice']))
				{
					echo '<li role="presentation"><a href="'.$link['sort_lowprice'].'">Самые дешевые</a></li>';
				}
				else
				{
					echo '<li role="presentation" class="active">
					        <a href="javascript:void(0)">Самые дешевые</a>
					      </li>';
				}
				?>
				<?
				if (strlen($link['sort_highprice']))
				{
					echo '<li role="presentation"><a href="'.$link['sort_highprice'].'">Самые дорогие</a></li>';
				}
				else
				{
					echo '<li role="presentation" class="active">
					        <a href="javascript:void(0)">Самые дорогие</a>
					      </li>';
				}
				?>
				</ul>
			</div>
		</div>
	</div>
</div>


<div class="row">
	<div class="col-md-3"  style=" padding-right: 0px;">



























				
			

			<div class="panel panel-default">
				<div class="panel-body" style="padding: 10px;">
					
					<div class="form-group">
						<label>Тип объекта</label>
						<?=FrontendCategoryListTree::widget() ?>
					</div>
				</div>
			</div>



			<div class="panel panel-default" style="margin-bottom:5px;">
				<div class="panel-body" style="padding: 10px; ">
					<div class="form-group">
						<label>Район</label>
						<select class="form-control" onchange="window.location = $(this).val()">
							<?
							foreach ($district_list_extended as $district_list_extended_value)
							{
								echo '<option value="'.$district_list_extended_value['url'].'"
														  '.((isset($district_list_extended_value['selected']))?'selected="selected"':'').'
														  >'.
													$district_list_extended_value['name'].
											'</option>';
							}
							?>
						</select>
					</div>
				</div>
			</div>



				<?php $form = ActiveForm::begin(); ?>
					<div style="display: table; width: 100%;">
						<div class="col-md-12" style="padding-left: 0px; padding-right: 0px;">
							<div class="panel panel-default" style="margin-bottom:5px;">
								<div class="panel-body" style="padding: 10px;">
									<div class="form-group">
										<label>Цена</label>
										<div class="input-group">
											<div class="input-group-addon">от</div>
											<input type="text" class="form-control intval"
											       name="filter[price][min]"
											       placeholder="Цена от (грн.)"
											       value="<?=((isset($get_filters['price']['min']))?$get_filters['price']['min']:'')?>">
											<div class="input-group-addon">грн.</div>
										</div>
									</div>
									<div class="form-group" style="margin-bottom:0px;">
										<div class="input-group">
											<div class="input-group-addon">до</div>
											<input type="text" class="form-control intval"
											       name="filter[price][max]"
											       placeholder="Цена до (грн.)"
											       value="<?=((isset($get_filters['price']['max']))?$get_filters['price']['max']:'')?>">
											<div class="input-group-addon">грн.</div>
										</div>
									</div>
								</div>
							</div>
						</div>

<?
foreach ($model_filters as $model_filters_item)
{
	if (!$model_filters_item->filter_type)
	{
?>

	<div class="col-md-12" style="padding-left: 0px; margin-bottom: 4px; margin-top: 3px; padding-right: 0px;">
		<div class="panel panel-default" style="margin-bottom:5px; ">
			<div class="panel-body" style="padding: 7px;">
				<div class="form-group">
					<label><?=$model_filters_item->name?></label>
					<div class="input-group">
						<div class="input-group-addon">от</div>
						<input type="text" class="form-control intval"
						       name="filter[opt][<?=$model_filters_item->id?>][min]"
						       placeholder="<?=htmlspecialchars($model_filters_item->name,ENT_QUOTES)?> от"
						       value="<?=((isset($get_filters['opt'][$model_filters_item->id]['min']))?$get_filters['opt'][$model_filters_item->id]['min']:'')?>">
					</div>
				</div>
				<div class="form-group" style="margin-bottom:0px;">
					<div class="input-group">
						<div class="input-group-addon">до</div>
						<input type="text" class="form-control intval"
						       name="filter[opt][<?=$model_filters_item->id?>][max]"
						       placeholder="<?=htmlspecialchars($model_filters_item->name,ENT_QUOTES)?> до"
						       value="<?=((isset($get_filters['opt'][$model_filters_item->id]['max']))?$get_filters['opt'][$model_filters_item->id]['max']:'')?>">
					</div>
				</div>
			</div>
		</div>
	</div>

<?
	}
}
?>




<?
foreach ($model_filters as $model_filters_item)
{
	if ($model_filters_item->filter_type==1)
	{
?>

	<div class="col-md-12" style="padding-left: 0px; margin-bottom: 4px; padding-right: 0px;">
		<div class="panel panel-default" style="margin-bottom:5px;">
			<div class="panel-body" style="padding: 10px;">
				<label><?=$model_filters_item->name?></label>
				<div class="form-group">
					<select class="form-control"
					        multiple="multiple"
					        id="filter_opt<?=$model_filters_item->id?>_set">
<?
if (($model_filters_options) && (is_array($model_filters_options)))
{
	foreach ($model_filters_options as $model_filters_options_item)
	{
		if ($model_filters_options_item->id_obj_option == $model_filters_item->id)
		{
			$selected = "";
			if (
			     (isset($get_filters['opt'][$model_filters_item->id]['set'])) &&
			     (is_array($get_filters['opt'][$model_filters_item->id]['set'])) &&
			     (in_array($model_filters_options_item->id,$get_filters['opt'][$model_filters_item->id]['set']))
			   )
			{
				$selected = ' selected="selected" ';
			}
		?>
			<option value="<?=$model_filters_options_item->id?>" <?=$selected?>>
				<?=$model_filters_options_item->name?>
			</option>
		<?
		}

	}
}

$filter_val_txt = '';
$filter_val_txt_arr = array();
if (
     (isset($get_filters['opt'][$model_filters_item->id]['set'])) &&
     (is_array($get_filters['opt'][$model_filters_item->id]['set'])) &&
     (count($get_filters['opt'][$model_filters_item->id]['set']))
   )
{
	foreach ($get_filters['opt'][$model_filters_item->id]['set'] as $setitem)
	{
		if (intval($setitem))
		{
			$filter_val_txt_arr[]=intval($setitem);
		}
	}

	if (count($filter_val_txt_arr))
	{
		$filter_val_txt = implode(',',$filter_val_txt_arr);
	}
}
?>
					</select>
					<input type="hidden" name="filter[opt][<?=$model_filters_item->id?>][set]"
					       id="filter_opt<?=$model_filters_item->id?>_set_input"
					       value="<?=$filter_val_txt?>">
				</div>
			</div>
		</div>
	</div>
<?




$js = '
$(\'#filter_opt'.$model_filters_item->id.'_set\').multipleSelect({
	width: \'100%\',
	name: \'filter[opt]['.$model_filters_item->id.'][set][]\',
	selectAllText: \'Выделить все\',
	allSelected: \'Все выделены\',
	countSelected: \'# из % выделено\',
	placeholder: "'.htmlspecialchars($model_filters_item->name,ENT_QUOTES).'",
	onClick: function(view)
	{
		var values = $(\'#filter_opt'.$model_filters_item->id.'_set\').multipleSelect("getSelects");
		$("#filter_opt'.$model_filters_item->id.'_set_input").val(values);
	},
	onCheckAll: function(view)
	{
		var values = $(\'#filter_opt'.$model_filters_item->id.'_set\').multipleSelect("getSelects");
		$("#filter_opt'.$model_filters_item->id.'_set_input").val(values);
	},
	onUncheckAll: function(view)
	{
		var values = $(\'#filter_opt'.$model_filters_item->id.'_set\').multipleSelect("getSelects");
		$("#filter_opt'.$model_filters_item->id.'_set_input").val(values);
	},
});
';
$this->registerJs($js);
?>
<?
	}
}
?>



					</div>

					<div style="display: table; padding-top: 3px; width: 100%;">
						<div class="checkbox" style="margin-bottom: 10px;">
							<label>
								<input type="checkbox"
										   name="filter[photo][use]"
										   value="1"
										   <?=((isset($get_filters['photo']['use']))?'checked="checked"':'')?>>
								Только с фото
							</label>
						</div>

						<div>
							<button type="submit" class="btn btn-default" name="filter[resetfilter]" style="display: block;
margin: 4px;">
								Сбросить фильтры <i class="glyphicon glyphicon-remove"></i>
							</button>
							<button type="submit" class="btn btn-primary"  style="display: block;
margin: 4px;">
								Фильтровать <i class="glyphicon glyphicon-search"></i>
							</button>
						</div>
					</div>

				<?php ActiveForm::end(); ?>
























	</div>
	<div class="col-md-9">
	
	

		<?
		$i=0;
		foreach ($select as $obj)
		{
		$i++;

		?>



		<div class="row" style="margin:0px; border-bottom: 1px solid rgb(215, 215, 215); padding-top: 5px; padding-bottom: 5px; color: #000;">
				<div class="col-md-2 obj_img col-xs-2">
					<img src="<?=$obj['image']?>">
				</div>
				<div class="col-md-8 col-xs-6">
					<div class="obj_name">
						<a href="/board/item/<?=$obj['id']?>">
							<?=$obj['name']?>
						</a>
					</div>
					<div class="category_names"><?=$obj['category_names']?></div>
					<div class="category_names"><?=$obj['cityname']?> → <?=$obj['districtname']?></div>
				</div>
				<div class="col-md-2 obj_price col-xs-2"><?=$obj['price']?></div>
			</div>



		<?}?>

		<?=$pagination?>
	</div>
</div>




