<?php
use app\components\FrontendLoveSearchForm;


$this->title = 'Знакомства в Кривом Роге: поиск анкет по параметрам';
$this->registerMetaTag([
    'name' => 'description',
    'content' => 'Список анкет жителей Кривого Рога для знакомств'
]);

?>



<div class="row" style="margin-top:10px;">
	<!--div class="col-md-3">.col-md-4</div -->
	<!--div class="col-md-9" -->
	<div class="col-md-12">
		<?=FrontendLoveSearchForm::widget() ?>
		<div class="row">
			<? foreach($model__xta_love_anketa__all as $model__xta_love_anketa):?>
				<div class="col-sm-6 col-md-4">
					<a href="/love/anketa/<?=$model__xta_love_anketa->id?>" target="_blank">
						<div class="thumbnail">
							<div class="thumbnail_container" style="height: 260px; overflow-y: hidden;">
								<img src="<?=$model__xta_love_anketa->fn__get_anketa_main_photo_src()?>" alt="..." style="width: 100%;">
							</div>
							<div class="caption">
								<h3><?=$model__xta_love_anketa->fn__get_name()?></h3>
								<p><?=$model__xta_love_anketa->fn__get_age(true)?></p>
							</div>
						</div>
					</a>
				</div>
			<? endforeach; ?>
		</div>
		
	</div>
</div>
