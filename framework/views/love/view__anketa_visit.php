<link rel="stylesheet" type="text/css" href="/public/components/html5-uploader/style.css" />
<?php
use yii\helpers\Url;
//use app\models\mgii\mgii_xta_love_anketa;


$js = '
    $("[data-toggle=\'tooltip\']").tooltip();
';
$this->registerJs($js);
?>

<div class="row" style="margin-top:10px;">
	<!--div class="col-md-3">.col-md-4</div -->
	<!--div class="col-md-9" -->
	<div class="col-md-12">

		
		

		<div class="panel panel-default">
			<div class="panel-heading">
				<b>
					Посетители моей анкеты
				</b>
			</div>
			<div class="panel-body">
				<div class="row">
					<? foreach ($model__xta_love_anketa_visit__many as $model__xta_love_anketa_visit):?>
						<div class="col-md-8">
							<a href="/love/anketa/<?=$model__xta_love_anketa_visit->id_anketa_visitor?>">
							<?=$model__xta_user_visitor__array[$model__xta_love_anketa_visit->id_anketa_visitor]->username?>
							</a>
						</div>
						<div class="col-md-4"><?=$model__xta_love_anketa_visit->timestamp?></div>

					<? endforeach;?>
				</div>
			</div>
		</div>
		
		
	</div>
</div>



































<div class="modal fade" id="modal__add_album" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Добавить альбом</h4>
      </div>
      <div class="modal-body">
      	
      	<div id="modal_alert_message__add_love_album" class="alert alert-danger" style="display:none;"></div>
      	
      	<input type="hidden" id="input__id_album" value="0">
      	
				<div class="form-group">
					<label>Название альбома</label>
					<input type="text" class="form-control" id="input__name_album" placeholder="Имя">
				</div>
				
				<div class="checkbox">
					<label>
						<input type="checkbox" id="input__hidden_album" value="1"> Скрытый альбом
					</label>
				</div>
				
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
        <button type="button" class="btn btn-primary"  id="btn__submit_add_love_album">Создать альбом</button>
      </div>
    </div>
  </div>
</div>












<div class="modal fade" id="modal__add_photo_to_album" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Загрузить фото в альбом</h4>
      </div>
      <div class="modal-body">
			
			<div id="modal_alert_message__upload_photo_to_album" class="alert alert-danger" style="display:none;"></div>
			
			<input type="hidden" id="input__upload_id_album" value="0">
			<!-- Область для перетаскивания -->
			<div id="drop-files" ondragover="return false" style="height:120px;">
				<p>Перетащите сюда или выберите изображение</p>
				<input type="file" name="images" id="uploadbtn"
				       style="text-align: center; font-size: 16px; margin: 0px auto;" multiple />
			</div>
			
			<!-- Область предпросмотра -->
			<div id="uploaded-holder"> 
				<div id="dropped-files" style="height: auto; display: table; width: 100%;">
					<!-- Кнопки загрузить и удалить, а также количество файлов -->
					<div id="upload-button">
						<center>
							<span>0 Файлов</span>
							<div id="loading">
								<div id="loading-bar">
									<div class="loading-color"></div>
								</div>
								<div id="loading-content"></div>
							</div>
						</center>
					</div>
				</div>
			</div>
			<!-- Список загруженных файлов -->
			<div id="file-name-holder">
				<ul id="uploaded-files">
					<h1>Загруженные файлы</h1>
				</ul>
			</div>
      	
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
        <button type="button" class="btn btn-primary"  id="btn__upload_photo_to_album">Загрузить фото</button>
      </div>
    </div>
  </div>
</div>
















<div class="modal fade" tabindex="-1" role="dialog" id="modal__message_to_board_item_author">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Отправить сообщение пользователю</h4>
      </div>
      <div class="modal-body">
      	<div id="modal_alert_message" class="alert alert-danger"></div>
	      <input type="hidden" id="message_to_board_item__id_user" value="<?=$model__xta_love_anketa->id_user?>">
	      <?if (!Yii::$app->user->id):?>
				<div class="form-group group__message_to_board_item__username">
					<label>Имя</label>
					<input type="text" class="form-control" id="message_to_board_item__username" placeholder="Имя">
				</div>
				<div class="form-group group__message_to_board_item__email">
					<label>Email адрес</label>
					<input type="email" class="form-control" id="message_to_board_item__email" placeholder="Email">
				</div>
				<div class="form-group group__message_to_board_item__phone">
					<label>Номер телефона</label>
					<input type="text" class="form-control" id="message_to_board_item__phone" placeholder="Телефон">
				</div>
				<?endif?>
				<div class="form-group">
					<label>Сообщение</label>
					<textarea class="form-control" id="message_to_board_item__message" rows="3"></textarea>
				</div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
        <button type="button" class="btn btn-primary" 
                id="btn__message_to_board_item_author_modal_send">Отправить сообщение</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->



