<link rel="stylesheet" type="text/css" href="/public/components/html5-uploader/style.css" />
<link rel="stylesheet" type="text/css" href="/public/components/lightbox2/src/css/lightbox.css" />

<?php
use yii\helpers\Url;
use app\models\mgii\mgii_xta_love_anketa;



$this->registerJsFile(Yii::$app->request->baseUrl.'/public/components/html5-uploader/javascript.js',['depends' => [\yii\web\JqueryAsset::className()]]);


$this->registerJsFile(Yii::$app->request->baseUrl.'/public/components/lightbox2/src/js/lightbox.js',['depends' => [\yii\web\JqueryAsset::className()]]);

$js = '
    $("[data-toggle=\'tooltip\']").tooltip();
';
$this->registerJs($js);



$this->title = 'Фотоальбомы: '.$model__xta_love_anketa->fn__get_name().' из Кривого Рога';
$this->registerMetaTag([
    'name' => 'description',
    'content' => 'Фотоальбомы и фотографии пользователя '.$model__xta_love_anketa->fn__get_name().' из Кривого Рога.'
]);

?>

<div class="row" style="margin-top:10px;">
	<!--div class="col-md-3">.col-md-4</div -->
	<!--div class="col-md-9" -->
	<div class="col-md-12">

		
		
		<?if(!$anketa_owner):?>
		<div class="row">
			<div class="col-md-4">
				<a href="/love/anketa/albums" class="thumbnail">
					<img src="<?=$img?>" alt="...">
				</a>
			</div>
			<div class="col-md-8">
				<h1 style="margin: 0px;"><?=$model__xta_love_anketa->fn__get_name()?></h1>
				<div style="color: gray; font-size: 12px;">
					Была сегодня в 22:28 
				</div>

				<div style="">
					<?=$age?> <?=$years_name?>, <?=$zodiac?>. Украина, Кривой Рог
				</div>
				
				<?if(!$anketa_owner):?>
					<div style="margin: 20px; margin-bottom: 10px;">
						<div class="btn-group" role="group" aria-label="...">
							<button type="button" class="btn btn-primary"  id="btn__send_message_modal_show">Написать сообщение</button>
						</div>
					</div>
				<?endif;?>
				
				<div class="alert alert-warning" role="alert">
					<?=$model__xta_love_anketa->status?>
				</div>
				
				<div style="">
					<div class="btn-group" role="group" aria-label="...">
						<a type="button" class="btn btn-default" href="/love/anketa/<?=$model__xta_love_anketa->id?>">Анкета знакомств</a>
						<a type="button" class="btn btn-default active">Альбомы</a>
					</div>
				</div>
				
			</div>
		</div>
		<br>
		<?endif;?>
		
		

		<div class="panel panel-default">
			<div class="panel-heading">
				<b>
					<?if($anketa_owner):?>
						Мои альбомы
					<?else:?>
						Альбомы пользователя <?=$model__xta_love_anketa->fn__get_name()?>
					<?endif;?>
				</b>
				<?if($anketa_owner):?>
					<a type="button" class="btn btn-success btn-xs" id="btn__add_love_album" style="float: right;" href="javascript:void(0);">
						<span class="glyphicon glyphicon-plus"></span> альбом
					</a>
				<?endif;?>
			</div>
			<div class="panel-body">
				
<?
if ($model__xta_love_album_many)
{
	foreach ($model__xta_love_album_many as $model__xta_love_album_item)
	{
?>

				<div class="panel panel-info">
					<div class="panel-heading">
						<b id="album_<?=$model__xta_love_album_item->id?>_name"><?=$model__xta_love_album_item->name?></b>
						<?if($anketa_owner):?>


							&nbsp;<?=(($model__xta_love_album_item->hidden)?'
							<span class="label label-default" id="album_'.$model__xta_love_album_item->id.'_hidden" val="1">скрыт</span>':'
							<span class="label label-success" id="album_'.$model__xta_love_album_item->id.'_hidden" val="0">опубликован</span>')?>
						

						
							<a style="float: right;" class="btn btn-danger btn-xs" 
								href="/love/anketa/albums?do=delalbum&id=<?=$model__xta_love_album_item->id?>" 
								onclick="return confirm('Вы уверены, что хотите удалить альбом со всеми фотографиями ?');"
								data-placement="top"
								title="Удалить альбом">
								<span class="glyphicon glyphicon-trash"></span>
							</a>
						
							<a style="float: right; margin-right:10px;"
								class="btn btn-primary btn-xs btn__edit_love_album" 
								href="javascript:void(0);" 
								idx="<?=$model__xta_love_album_item->id?>"
								data-placement="top"
								title="Редактировать имя альбома">
								<span class="glyphicon glyphicon-pencil"></span>
							</a>
						
							<a style="float: right; margin-right:10px;"
								class="btn btn-success btn-xs btn__modal_upload_photo_to_love_album"
								href="javascript:void(0);"
								idx="<?=$model__xta_love_album_item->id?>"
								data-placement="top"
								title="Загрузить фото в альбом">
								<span class="glyphicon glyphicon-plus"></span> фото
							</a>
							
						<?endif;?>
						
					</div>
					<div class="panel-body">

						<?
						foreach ($images[$model__xta_love_album_item->id_album] as $model_xta_image):
						?>
						<div class="col-sm-6 col-md-4">
							<div class="thumbnail">
								<a href="<?=$model_xta_image->fn__get_src()?>" data-lightbox="example-set">
									<img class="example-image" src="<?=$model_xta_image->fn__get_src()?>" alt="">
								</a>
								
								<?if($anketa_owner):?>
									<div class="caption btn-group" style="margin-left: auto; margin-right: auto; display: table;">
									
										<?
										if ($model__xta_love_anketa->id_image != $model_xta_image->id):
										?>
										<a href="/love/anketa/albums?do=setmain&id_album=<?=$model__xta_love_album_item->id?>&id_photo=<?=$model_xta_image->id?>" 
											class="btn btn-primary" 
											data-toggle="tooltip" 
											data-placement="top" 
											title="Сделать изображение заглавным"><i class="glyphicon glyphicon-ok"></i></a> 
										<?
										endif;
										?>
										<a href="/love/anketa/albums?do=delphoto&id_album=<?=$model__xta_love_album_item->id?>&id_photo=<?=$model_xta_image->id?>"
											class="btn btn-danger"
											onclick="return confirm('Удалить изображение из альбома ?');"
											data-toggle="tooltip"
											data-placement="top" title="Удалить изображение"><i class="glyphicon glyphicon-remove"></i></a>
									</div>
								<?endif;?>
							</div>
						</div>
						<?
						endforeach;
						?>

					</div>
				</div>

<?
	}
}
?>
			</div>
		</div>
		
		
	</div>
</div>



































<div class="modal fade" id="modal__add_album" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Добавить альбом</h4>
      </div>
      <div class="modal-body">
      	
      	<div id="modal_alert_message__add_love_album" class="alert alert-danger" style="display:none;"></div>
      	
      	<input type="hidden" id="input__id_album" value="0">
      	
				<div class="form-group">
					<label>Название альбома</label>
					<input type="text" class="form-control" id="input__name_album" placeholder="Имя">
				</div>
				
				<div class="checkbox">
					<label>
						<input type="checkbox" id="input__hidden_album" value="1"> Скрытый альбом
					</label>
				</div>
				
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
        <button type="button" class="btn btn-primary"  id="btn__submit_add_love_album">Создать альбом</button>
      </div>
    </div>
  </div>
</div>












<div class="modal fade" id="modal__add_photo_to_album" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Загрузить фото в альбом</h4>
      </div>
      <div class="modal-body">
			
			<div id="modal_alert_message__upload_photo_to_album" class="alert alert-danger" style="display:none;"></div>
			
			<input type="hidden" id="input__upload_id_album" value="0">
			<!-- Область для перетаскивания -->
			<div id="drop-files" ondragover="return false" style="height:120px;">
				<p>Перетащите сюда или выберите изображение</p>
				<input type="file" name="images" id="uploadbtn"
				       style="text-align: center; font-size: 16px; margin: 0px auto;" multiple />
			</div>
			
			<!-- Область предпросмотра -->
			<div id="uploaded-holder"> 
				<div id="dropped-files" style="height: auto; display: table; width: 100%;">
					<!-- Кнопки загрузить и удалить, а также количество файлов -->
					<div id="upload-button">
						<center>
							<span>0 Файлов</span>
							<div id="loading">
								<div id="loading-bar">
									<div class="loading-color"></div>
								</div>
								<div id="loading-content"></div>
							</div>
						</center>
					</div>
				</div>
			</div>
			<!-- Список загруженных файлов -->
			<div id="file-name-holder">
				<ul id="uploaded-files">
					<h1>Загруженные файлы</h1>
				</ul>
			</div>
      	
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
        <button type="button" class="btn btn-primary"  id="btn__upload_photo_to_album">Загрузить фото</button>
      </div>
    </div>
  </div>
</div>
















<div class="modal fade" tabindex="-1" role="dialog" id="modal__message_to_board_item_author">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Отправить сообщение пользователю</h4>
      </div>
      <div class="modal-body">
      	<div id="modal_alert_message" class="alert alert-danger"></div>
	      <input type="hidden" id="message_to_board_item__id_user" value="<?=$model__xta_love_anketa->id_user?>">
	      <?if (!Yii::$app->user->id):?>
				<div class="form-group group__message_to_board_item__username">
					<label>Имя</label>
					<input type="text" class="form-control" id="message_to_board_item__username" placeholder="Имя">
				</div>
				<div class="form-group group__message_to_board_item__email">
					<label>Email адрес</label>
					<input type="email" class="form-control" id="message_to_board_item__email" placeholder="Email">
				</div>
				<div class="form-group group__message_to_board_item__phone">
					<label>Номер телефона</label>
					<input type="text" class="form-control" id="message_to_board_item__phone" placeholder="Телефон">
				</div>
				<?endif?>
				<div class="form-group">
					<label>Сообщение</label>
					<textarea class="form-control" id="message_to_board_item__message" rows="3"></textarea>
				</div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
        <button type="button" class="btn btn-primary" 
                id="btn__message_to_board_item_author_modal_send">Отправить сообщение</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->



