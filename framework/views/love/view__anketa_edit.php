<?php
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use app\models\mgii\mgii_xta_love_type_account;
use app\models\mgii\mgii_xta_love_anketa;


$this->title = 'Редактирование анкеты знакомств';
$this->registerMetaTag([
    'name' => 'description',
    'content' => 'Измените вышу анкету на сайте знакомств'
]);



$js = '
    $("[data-toggle=\'tooltip\']").tooltip();
    $("[data-toggle=\'popover\']").popover({ trigger : \'hover focus\',html: true,placement:\'top\'});
';
$this->registerJs($js);



$this->registerJsFile(Yii::$app->request->baseUrl.'/public/components/jqueryui/jquery-ui.min.js',['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl.'/public/components/jqueryui/datepicker-ru.js',['depends' => [\yii\web\JqueryAsset::className()]]);

?>
<link rel="stylesheet" href="/public/components/jqueryui/jquery-ui.min.css">




<div class="row" style="margin-top:20px;">
	<!--div class="col-md-3">.col-md-4</div -->
	<!--div class="col-md-9" -->
	<div class="col-md-12">

		<div class="panel panel-info">
			<div class="panel-heading">
				<h1 class="panel-title">Редактирование анкеты</h1>
			</div>
			<div class="panel-body">
<?php $form = ActiveForm::begin(['id' => 'anketa-form']); ?>

				<div class="panel panel-info">
					<div class="panel-heading">
						Обо мне
					</div>
					<div class="panel-body">

						<div class="row">
							<div class="col-md-6">
								<?=$form->field($model, 'id_love_type_account',['template' => '
									{label}
									<div data-toggle="popover" 
											 data-title="Ваш тип анкеты" 
											 data-content="Выберите ваш тип анкеты">
										 {input} {error}
									</div>
								'])->dropDownList(mgii_xta_love_type_account::fn__get_data_list());?>
							</div>
							<div class="col-md-6">
								<?=$form->field($model, 'birthdate',[
									'template' => '
										{label}
									<div data-toggle="popover" 
											 data-title="Дата рождения" 
											 data-content="Укажите дату рождения для того чтобы другие пользователи могли вас найти">
										 {input} {error}
									</div>'
								])->textInput(
										[
											'maxlenght' => 255,
											'readonly' => 'readonly',
											'value' => date('d/m/Y', strtotime($model->birthdate)),
										]
										); ?>
							</div>
						</div>
							


						<?=$form->field($model, 'about_me',[
							'template' => '
								{label}
							<div data-toggle="popover" 
									 data-title="Опишите кратко себя" 
									 data-content="Опишите себя, свои особенные предпочтения.">
								 {input} {error}
							</div>'
						])->textarea(['rows' => 2,'maxlength'=>200]) ?>
						

						<?=$form->field($model, 'status',[
							'template' => '
								{label}
							<div data-toggle="popover" 
									 data-title="Укажите ваш статус" 
									 data-content="Введите подробный и понятный заголовок. Избегайте слов с ЗАГЛАВНЫМИ БУКВАМИ. 
												         Чем больше тематических слов будет в заголовке, тем вероятнее смогут 
												         найти объявление те, кто в нем заинтересован.">
								 {input} {error}
							</div>'
						])->textarea(['rows' => 2,'maxlength'=>99]) ?>


					</div>
				</div>




				<div class="panel panel-info">
					<div class="panel-heading">
						Кого ищу
					</div>
					<div class="panel-body">
						
						<?=$form->field($model, 'id_search_love_type_account',['template' => '
							{label}
							<div data-toggle="popover" 
									 data-title="Укажите кого вы ищете" 
									 data-content="Выберите тип анкеты пользователя, с которым вы бы хотели познакомиться">
								 {input} {error}
							</div>
						'])->dropDownList(mgii_xta_love_type_account::fn__get_data_list());?>
						
						<?=$form->field($model, 'search_about',[
							'template' => '
							{label}
							<div data-toggle="popover" 
									 data-title="Опишите того кого ищете" 
									 data-content="Пару слов о том, кого ищете на сайте">
								 {input} {error}
							</div>'
						])->textarea(['rows' => 2,'maxlength'=>200])  ?>
						
						<div class="row">
							<div class="col-md-12">
								<label class="control-label">Возраст</label>
							</div>
							<div class="col-md-1">
								<label class="control-label">от</label>
							</div>
							<div class="col-md-5">
							<?
							$arr = array();
							for ($i = 1; $i < 99; $i++) {
							$arr[$i] = $i;
							}
							echo $form->field($model, 'search_age_start',['template' => '{input}{error}'])
							        ->dropDownList($arr);
							?>
							</div>
							<div class="col-md-1">
								<label class="control-label">до</label>
							</div>
							<div class="col-md-5">
							<?
							$arr = array();
							for ($i = 1; $i < 99; $i++) {
							$arr[$i] = $i;
							}
							echo $form->field($model, 'search_age_end',['template' => '{input}{error}'])
							        ->dropDownList($arr);
							?>
							
							</div>
						</div>
						
					</div>
				</div>

















				<div class="panel panel-info">
					<div class="panel-heading">
						Мои данные
					</div>
					<div class="panel-body">

						<div class="row">
							<div class="col-md-6">
								<?=$form->field($model, 'family_status',['template' => '
									{label}
									<div data-toggle="popover" 
											 data-title="Семейное положение" 
											 data-content="Укажите ваше семейное положение">
										 {input} {error}
									</div>
								'])->dropDownList(mgii_xta_love_anketa::fn__get_family_status_list());?>
							</div>
							<div class="col-md-6">
								<?=$form->field($model, 'child_status',['template' => '
									{label}
									<div data-toggle="popover" 
											 data-title="Дети" 
											 data-content="Есть ли дети">
										 {input} {error}
									</div>
								'])->dropDownList(mgii_xta_love_anketa::fn__get_child_status_list());?>
							</div>
							
							
							<div class="col-md-6">
								<?=$form->field($model, 'weight',[
									'template' => '
										{label}
									<div data-toggle="popover" 
											 data-title="Ваш вес" 
											 data-content="Укажите ваш вес">
										 {input} {error}
									</div>'
								])->textInput()
								?>
							</div>


							<div class="col-md-6">
								<?=$form->field($model, 'height',[
									'template' => '
										{label}
									<div data-toggle="popover" 
											 data-title="Ваш рост" 
											 data-content="Укажите ваш рост">
										 {input} {error}
									</div>'
								])->textInput()
								?>
							</div>


							<div class="col-md-6">
								<?=$form->field($model, 'physique',['template' => '
									{label}
									<div data-toggle="popover" 
											 data-title="Телосложение" 
											 data-content="Укажите ваше телосложение">
										 {input} {error}
									</div>
								'])->dropDownList(mgii_xta_love_anketa::fn__get_physique_list());?>
							</div>


							<div class="col-md-6">
								<?=$form->field($model, 'smoking',['template' => '
									{label}
									<div data-toggle="popover" 
											 data-title="Отношение к курению" 
											 data-content="Укажите ваше отношение к курению">
										 {input} {error}
									</div>
								'])->dropDownList(mgii_xta_love_anketa::fn__get_smoking_list());?>
							</div>


							<div class="col-md-6">
								<?=$form->field($model, 'alcohol',['template' => '
									{label}
									<div data-toggle="popover" 
											 data-title="Отношение к алкоголю" 
											 data-content="Укажите ваше отношение к алкоголю">
										 {input} {error}
									</div>
								'])->dropDownList(mgii_xta_love_anketa::fn__get_alcohol_list());?>
							</div>


							<div class="col-md-6">
								<?=$form->field($model, 'material_support',['template' => '
									{label}
									<div data-toggle="popover" 
											 data-title="Спонсорство" 
											 data-content="Укажите ваше отношение к спонсорству">
										 {input} {error}
									</div>
								'])->dropDownList(mgii_xta_love_anketa::fn__get_material_support_list());?>
							</div>

						</div>

					</div>
				</div>















				<div class="form-group text-center" style="margin-top:50px;">
						<?= Html::submitButton(((intval($model->id_user))?'Сохранить анкету':'Создать анкету'), ['class' => 'btn btn-primary', 'name' => 'register-button']) ?>
				</div>
				

<?php ActiveForm::end(); ?>
			</div>
		</div>
	</div>
</div>
