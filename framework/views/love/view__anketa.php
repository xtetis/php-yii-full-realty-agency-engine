<?php
use yii\helpers\Url;
use app\models\mgii\mgii_xta_love_anketa;



$this->title = 'Анкета пользователя '.$model__xta_love_anketa->fn__get_name().' из Кривого Рога';
$this->registerMetaTag([
    'name' => 'description',
    'content' => 'Анкета пользователя '.$model__xta_love_anketa->fn__get_name().' из Кривого Рога'
]);

?>

<div class="row" style="margin-top:10px;">
	<!--div class="col-md-3">.col-md-4</div -->
	<!--div class="col-md-9" -->
	<div class="col-md-12">

	
		<div class="row">
			<div class="col-md-4">
				<a href="/love/anketa/albums<?if(!$anketa_owner):?>/<?=$model__xta_love_anketa->id?><?endif;?>" class="thumbnail">
					<img src="<?=$img?>" alt="...">
				</a>
			</div>
			<div class="col-md-8">
				<h1 style="margin: 0px;"><?=$model__xta_love_anketa->fn__get_name()?></h1>
				<div style="color: gray; font-size: 12px;">
					<!--Была сегодня в 22:28  -->
				</div>

				<div style="">
					<?=$age?> <?=$years_name?>, <?=$zodiac?>. Украина, Кривой Рог
				</div>
				
				<?if(!$anketa_owner):?>
					<div style="margin: 20px; margin-bottom: 10px;">
						<div class="btn-group" role="group" aria-label="...">
							<button type="button" class="btn btn-primary" id="btn__send_message_modal_show">Написать сообщение</button>
						</div>
					</div>
				<?endif;?>
				
				<div class="alert alert-warning" role="alert">
					<?=$model__xta_love_anketa->status?>
				</div>
				
				<div style="">
					<div class="btn-group" role="group">
						<button type="button" class="btn btn-default active">Анкета знакомств</button>
						<a type="button" class="btn btn-default" href="/love/anketa/albums<?if(!$anketa_owner):?>/<?=$model__xta_love_anketa->id?><?endif;?>">Альбомы</a>
					</div>
				</div>
				
			</div>
		</div>

		<br>

		<div class="panel panel-info">
			<div class="panel-heading">
				<b>Я ищу</b>
				<?if($anketa_owner):?>
					<div style="float:right;">
						<a href="/love/anketa/edit"><span class="glyphicon glyphicon-pencil"></span> Редактировать</a>
					</div>
				<?endif;?>
			</div>
			<div class="panel-body row">
				
				<div class="col-md-5">
					<div>
						<b>Познакомлюсь:</b>
					</div>
					<div>
						с <?=mb_strtolower($xta_love_type_account_alternate_p4,'utf-8')?> в возрасте <?=$model__xta_love_anketa->search_age_start?> - <?=$model__xta_love_anketa->search_age_end?> лет
					</div>
					
					

					<div>
						<b>Кого я хочу найти:</b>
					</div>
					<div>
						<?=$model__xta_love_anketa->search_about?>
					</div>
				</div>
				<div class="col-md-1">

				</div>
				<div class="col-md-6">
					
					<div>
						<b>Материальная поддержка:</b>
					</div>
					<div>
						<?=mgii_xta_love_anketa::fn__get_material_support_list()[$model__xta_love_anketa->material_support];?>
					</div>
				</div>
				
			</div>
		</div>









<div class="panel panel-info">
			<div class="panel-heading">
				<b>Обо мне</b>
				<?if($anketa_owner):?>
					<div style="float:right;">
						<a href="/love/anketa/edit"><span class="glyphicon glyphicon-pencil"></span> Редактировать</a>
					</div>
				<?endif;?>
			</div>
			<div class="panel-body row">
				<div class="col-md-12">
					<?=$model__xta_love_anketa->about_me?>
				</div>
				<div class="col-md-5">
					<div>
						<b>Рост:</b> <?=$model__xta_love_anketa-> 	height;?> см 
					</div>
					<div>
						<b>Вес:</b> <?=$model__xta_love_anketa->weight;?> кг 
					</div>

					<div>
						<b>Телосложение: </b>
					</div>
					<div>
						<?=mgii_xta_love_anketa::fn__get_physique_list()[$model__xta_love_anketa->physique];?>
					</div>
					
					<div>
						<b>Семейное положение: </b>
					</div>
					<div>
						<?=mgii_xta_love_anketa::fn__get_family_status_list()[$model__xta_love_anketa->family_status];?>
					</div>
					
					<div>
						<b>Дети: </b>
					</div>
					<div>
						<?=mgii_xta_love_anketa::fn__get_child_status_list()[$model__xta_love_anketa->child_status];?>
					</div>

				</div>
				<div class="col-md-1">

				</div>
				<div class="col-md-6">
					<div>
						<b>Отношение к курению: </b>
					</div>
					<div>
						<?=mgii_xta_love_anketa::fn__get_smoking_list()[$model__xta_love_anketa->smoking];?>
					</div>
					
					<div>
						<b>Отношение к алкоголю: </b>
					</div>
					<div>
						<?=mgii_xta_love_anketa::fn__get_alcohol_list()[$model__xta_love_anketa->alcohol];?>
					</div>
				</div>
				
			</div>
		</div>
		
		
		
		
		
		
	</div>
</div>




























<div class="modal fade" tabindex="-1" role="dialog" id="modal__message_to_board_item_author">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Отправить сообщение пользователю</h4>
      </div>
      <div class="modal-body">
      	<div id="modal_alert_message" class="alert alert-danger"></div>
	      <input type="hidden" id="message_to_board_item__id_user" value="<?=$model__xta_love_anketa->id_user?>">
	      <?if (!Yii::$app->user->id):?>
				<div class="form-group group__message_to_board_item__username">
					<label>Имя</label>
					<input type="text" class="form-control" id="message_to_board_item__username" placeholder="Имя">
				</div>
				<div class="form-group group__message_to_board_item__email">
					<label>Email адрес</label>
					<input type="email" class="form-control" id="message_to_board_item__email" placeholder="Email">
				</div>
				<div class="form-group group__message_to_board_item__phone">
					<label>Номер телефона</label>
					<input type="text" class="form-control" id="message_to_board_item__phone" placeholder="Телефон">
				</div>
				<?endif?>
				<div class="form-group">
					<label>Сообщение</label>
					<textarea class="form-control" id="message_to_board_item__message" rows="3"></textarea>
				</div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
        <button type="button" class="btn btn-primary" 
                id="btn__message_to_board_item_author_modal_send">Отправить сообщение</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->





