<?
use app\components\AdminDirectoryTop;
use yii\bootstrap\ActiveForm;
?>


<?
echo AdminDirectoryTop::widget([
    'params' => [
        'action_name' => $action_name,
        'title' => 'Парсер объявлений ODNAGAZETA.COM',
        'show_new'=> false,
        'show_all'=>false,
        'other_buttons'=>array(

'<a class="btn btn-default" href="/admin/parser/'.$command.'"><span class="glyphicon glyphicon-home"></span>Начало</a>',
'<a class="btn btn-default  dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-arrow-down"></span>Парсить</a>
  <ul class="dropdown-menu" style="margin-left: 72px; margin-top: -10px;">
    <li><a href="/admin/parser/'.$command.'/1">1. Генерируем страницы навигации</a></li>
    <li><a href="/admin/parser/'.$command.'/2">2. Скачиваем страницы навигации</a></li>
    <li><a href="/admin/parser/'.$command.'/3">3. Парсим страницы навигации</a></li>
    <li><a href="/admin/parser/'.$command.'/4">4. Скачиваем страницы объявлений</a></li>
    <li><a href="/admin/parser/'.$command.'/5">5. Парсим страницы объявлений</a></li>
    <li><a href="/admin/parser/'.$command.'/6">6. Скачиваем изображения</a></li>
    <li><a href="/admin/parser/'.$command.'/7">7. Генерируем SQL</a></li>
  </ul>

',
'<a class="btn btn-default" href="/admin/parser/'.$command.'?do=clearparse" onclick="return confirm(\'Вы уверены что хотите очистить данные парсинга?\')"><span class="glyphicon glyphicon-trash"></span>Стереть данные парсинга</a>',
'<a class="btn btn-default" data-toggle="modal" data-target="#modalsettings">
  <span class="glyphicon glyphicon-wrench"></span> Настройки
 </a>',

        ),
    ],
]);
?>



<?=$repeat_parse?>







<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">Консоль</h3>
  </div>
  <div class="panel-body">
    <div style="height:250px; overflow:scroll;'">
      <?=$console?>
    </div>
  </div>
</div>









<div class="modal fade" id="modalsettings">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Настройки парсера</h4>
      </div>
      <div class="modal-body">


        <?php $form = ActiveForm::begin([
            'id' => 'settings_form',
         ]); ?>
          <div class="form-group">
            <label>Категория</label>
            <input type="text" class="form-control" name="settings_path" value="<?=$settings_path?>" required>
            <div style="color: gray;">Например <b>/znakomstva/ona-ishchet-ego</b></div>
          </div>
          <div class="form-group">
            <label>Количество страниц для парсинга</label>
            <input type="number" class="form-control" name="settings_pagecount" value="<?=$settings_pagecount?>" required>
          </div>
          <div class="form-group">
            <label>ID категории для импорта</label>
            <input type="number" class="form-control" name="settings_id_category" 
                   value="<?=$settings_id_category?>" required>
          </div>
          <div class="form-group">
            <label>ID юзера</label>
            <input type="number" class="form-control" name="settings_id_user" 
                   value="<?=$settings_id_user?>" required>
          </div>
        <?php ActiveForm::end(); ?>


      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
        <button type="button" class="btn btn-primary" 
                onclick="$('#settings_form').submit();">Сохранить настройки</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
