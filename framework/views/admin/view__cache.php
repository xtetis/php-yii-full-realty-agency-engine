<?
use app\components\AdminDirectoryTop;
?>


<?
echo AdminDirectoryTop::widget([
    'params' => [
        'action_name' => $action_name,
        'title' => 'Кеш',
        'show_new'=> false,
        'other_buttons'=>array(
'<a class="btn btn-default" href="/admin/cache/clearfile">
	<span class="glyphicon glyphicon-trash"></span>
	Очистить файловый кеш
</a>',

'<a class="btn btn-default" href="/admin/cache/clearall">
	<span class="glyphicon glyphicon-trash"></span>
	Очистить весь кеш
</a>',
        ),
    ],
]);
?>



<table class="table table-striped table-bordered">
	<tr>
		<th style="width:10px;">
			#
		</th>
		<th>
			Тег
		</th>
		<td>
			Количество единиц кеша
		</td>
		<td style="width:10px;">
			
		</td>
	</tr>
<?foreach ($model__xta_cache_tag_all as $item):
	if (strpos($item->name,'board_item_')!==false)
	{
		continue;
	}
?>
	<tr>
		<td>
			<?=$item->id?>
		</td>
		<td>
			<?=$item->name?>
		</td>
		<td>
		<?=$counts_cache[$item->id]?>
		</td>
		<td>
			<a class="btn btn-default" href="/admin/cache/cleartag/<?=$item->id?>">
				<span class="glyphicon glyphicon-trash"></span>
			</a>
		</td>
	</tr>
<?endforeach;?>
</table>


