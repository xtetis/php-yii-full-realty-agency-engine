<?php


$this->registerJsFile(Yii::$app->request->baseUrl.'/public/templates/default/js/admin.js',
                      ['depends' => [\yii\web\JqueryAsset::className()]]);


use yii\helpers\Html;
use yii\grid\GridView;
use leandrogehlen\treegrid\TreeGrid;
use app\components\AdminDirectoryTop;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\gii\xta_obj_category;

global $action_name_grid;
$action_name_grid = $action_name;
?>

<?
echo AdminDirectoryTop::widget([
    'params' => [
        'action_name' => $action_name,
        'title' => 'Категории объявлений',
    ],
]);
?>





<?if ($command=='select'):?>
<table class="table table-bordered table-striped">
<tr>
	<th style="width:30px;">ID</th>
	<th>Название</th>
	<th style="width:60px;"></th>
</tr>
<?
foreach ($categories_all as $item)
{?>
<tr class="xta_obj_category__parent__<?=$item->id_parent?>" 
    style="<?=(intval($item->id_parent)?'display:none;':'')?>"
    idx="<?=$item->id?>">
	<td><?=$item->id?></td>
	<td style="padding-left:<?=(8+26*($item->getParentDepth()))?>px;">
		<?
		if ($item->hasChildren())
		{
			echo '<a style="cursor:pointer;" 
							 class="'.$action_name.'__parent_toggle idx_'.$item->id.'"
							 idx="'.$item->id.'"">
							<span class="glyphicon glyphicon-plus"></span>
						</a>';
		}
		else
		{
			echo '<a><span class="glyphicon glyphicon-asterisk"></span></a>';
		}
		?>
		<?=$item->name?>
		<?//print_r($item)?>
	</td>
	<td>
	<?=Html::a('<span class="glyphicon glyphicon-pencil"></span>','/admin/'.$action_name.'/update/'.$item->id);?>
	<?=Html::a('<span class="glyphicon glyphicon-trash"></span>',
					               '/admin/'.$action_name.'/delete/'.$item->id,
					               [
                           'data-confirm'=>'Вы уверены, что хотите удалить эту запись?',
                         ]);?>
	</td>
</tr>
<?}?>
</table>
<?endif?>






<?if (($command=='create')||($command=='update')):?>
    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'id_parent')->dropDownList(xta_obj_category::getObjCategoryDataList()) ?>
    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'hone')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>
    <?
    $input_allsite = $form->field($model, 'hideobjmain');
    $input_allsite->options = ['id' => 'admin_input_settings'];
    echo $input_allsite->checkbox();
    ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>
<?endif?>
