<?php 

$this->registerJsFile(Yii::$app->request->baseUrl.'/public/templates/default/js/admin.js',
                      ['depends' => [\yii\web\JqueryAsset::className()]]);

use yii\helpers\Html;
use yii\grid\GridView;
use app\components\AdminDirectoryTop;
use yii\widgets\ActiveForm;
use app\models\gii\xta_setting_category; 
use app\models\gii\xta_site;
use yii\helpers\ArrayHelper;

global $action_name_grid;
$action_name_grid = $action_name;
?>

<?
echo AdminDirectoryTop::widget([
    'params' => [
        'action_name' => $action_name,
        'title' => 'Настройки',
    ],
]);
?>





<?if ($command=='select'):?>
<?= GridView::widget([
	'dataProvider' => $dataProvider,
	'filterModel' => $searchModel,
	'layout' => '{summary}{items}{pager}',
	'summary' => 'Показано {begin} - {end} из {totalCount} записей
',
	'columns' => [
		[
		'attribute'=>'id',
		'headerOptions' => ['width' => '40'],
		],
		[
			'attribute' => 'setting_category',
			'value' => 'xta_setting_category.name',
			'label'=>'Категория',
		],
		'name',
		'title',
		[
			'value' => 'value',
			'label'=>'Мульт',
			'content'=>function($data)
			{
				$value = unserialize($data->value);
				if (isset($value[0])&&(strlen($value[0])))
				{
					return "Да";
				}
				else
				{
					return "Нет";
				}
				
			}
		],
		[
			'value' => 'value',
			'label'=>'Заполнено',
			'content'=>function($data)
			{
				$value = unserialize($data->value);
				if (isset($value[0])&&(strlen($value[0])))
				{
					return "Да";
				}
				elseif (isset($value[xta_site::getCurrentSiteId()])&&(strlen($value[xta_site::getCurrentSiteId()])))
				{
					return "Да";
				}
				else
				{
					return "Нет";
				}
				
			}
		],
		[
			'class' => 'yii\grid\ActionColumn',
			'headerOptions' => ['width' => '50'],
			'template' => '{update} {delete}',
			'buttons' => [
				'update' => function ($url,$model,$key) {
					global $action_name_grid;
					return Html::a('<span class="glyphicon glyphicon-pencil"></span>',
					               '/admin/'.$action_name_grid.'/update/'.$key);
				},
				'delete' => function ($url,$model,$key) {
					global $action_name_grid;
					return Html::a('<span class="glyphicon glyphicon-trash"></span>',
					               '/admin/'.$action_name_grid.'/delete/'.$key,
					               [
                           'data-confirm'=>'Вы уверены, что хотите удалить эту запись?',
                         ]);
				},
			],
		],
	],
]); ?>
<?endif?>








<?if (($command=='create')||($command=='update')):?>

	<?php $form = ActiveForm::begin(); ?>

		<?= $form->field($model, 'id_setting_category')->dropDownList(xta_setting_category::getSettingCategoryList()) ?>
    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
    <?
    $input_allsite = $form->field($model, 'allsite');
    $input_allsite->options = ['id' => 'admin_input_settings'];
    echo $input_allsite->checkbox();
    ?>
    
    <?= $form->field($model, 'value')->textarea(['rows' => 6]) ?>
    <?= $form->field($model, 'default')->textarea(['rows' => 6]) ?>
    
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>
<?endif?>
