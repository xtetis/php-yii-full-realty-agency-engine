<script src="/public/components/chart_js/Chart.bundle.js "></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>




<div>

	<!-- Nav tabs -->
	<ul class="nav nav-tabs" role="tablist">
		<li class="active">
			<a href="#container_canvas_obj" role="tab" data-toggle="tab">
				Новые объявления
			</a>
		</li>
		<li>
			<a href="#container_canvas_obj_pub" role="tab" data-toggle="tab">
				Опубликованные объявления
			</a>
		</li>
		<li>
			<a href="#container_canvas_message" role="tab" data-toggle="tab">
				Новые сообщения
			</a>
		</li>
	</ul>

	<!-- Tab panes -->
	<div class="tab-content">
		<div class="tab-pane active" id="container_canvas_obj">
			<canvas id="canvas_obj"></canvas>
		</div>
		<div class="tab-pane" id="container_canvas_obj_pub">
			<canvas id="canvas_obj_pub"></canvas>
		</div>
		<div class="tab-pane" id="container_canvas_message">
			<canvas id="canvas_message"></canvas>
		</div>
	</div>

</div>







<script>
  
  var randomColorFactor = function() {
      return Math.round(Math.random() * 255);
  };
  var randomColor = function(opacity) {
      return 'rgba(63,191,127,' + (opacity || '.3') + ')';
  };

  var config_obj = {
      type: 'line',
      data: {
          labels: [<?=implode(',',$count_obj['info'][0])?>],
          datasets: [{
              label: "Новых объявлений",
              data: [<?=implode(',',$count_obj['info'][1])?>],
          }]
      },
      options: {
          responsive: true,
          title:{
              display:true,
              text:'Новые объявления'
          },
          tooltips: {
              mode: 'label',
              callbacks: {
              }
          },
          hover: {
              mode: 'dataset'
          },
          scales: {
              xAxes: [{
                  display: true,
                  scaleLabel: {
                      display: true,
                      labelString: 'Дни'
                  }
              }],
              yAxes: [{
                  display: true,
                  scaleLabel: {
                      display: true,
                      labelString: 'Объявления'
                  }
              }]
          }
      }
  };

  $.each(config_obj.data.datasets, function(i, dataset) {
      dataset.borderColor = randomColor(0.4);
      dataset.backgroundColor = randomColor(0.5);
      dataset.pointBorderColor = randomColor(0.7);
      dataset.pointBackgroundColor = randomColor(0.5);
      dataset.pointBorderWidth = 1;
  });




















  var config_obj_pub = {
      type: 'line',
      data: {
          labels: [<?=implode(',',$count_obj_pub['info'][0])?>],
          datasets: [{
              label: "Опубликовано объявлений",
              data: [<?=implode(',',$count_obj_pub['info'][1])?>],
          }]
      },
      options: {
          responsive: true,
          title:{
              display:true,
              text:'Опубликованные объявления'
          },
          tooltips: {
              mode: 'label',
              callbacks: {
              }
          },
          hover: {
              mode: 'dataset'
          },
          scales: {
              xAxes: [{
                  display: true,
                  scaleLabel: {
                      display: true,
                      labelString: 'Дни'
                  }
              }],
              yAxes: [{
                  display: true,
                  scaleLabel: {
                      display: true,
                      labelString: 'Объявления'
                  }
              }]
          }
      }
  };

  $.each(config_obj_pub.data.datasets, function(i, dataset) {
      dataset.borderColor = randomColor(0.4);
      dataset.backgroundColor = randomColor(0.5);
      dataset.pointBorderColor = randomColor(0.7);
      dataset.pointBackgroundColor = randomColor(0.5);
      dataset.pointBorderWidth = 1;
  });
















  var config_message = {
      type: 'line',
      data: {
          labels: [<?=implode(',',$count_message['info'][0])?>],
          datasets: [{
              label: "Новых сообщений",
              data: [<?=implode(',',$count_message['info'][1])?>],
          }]
      },
      options: {
          responsive: true,
          title:{
              display:true,
              text:'Новые сообщения'
          },
          tooltips: {
              mode: 'label',
              callbacks: {
              }
          },
          hover: {
              mode: 'dataset'
          },
          scales: {
              xAxes: [{
                  display: true,
                  scaleLabel: {
                      display: true,
                      labelString: 'Дни'
                  }
              }],
              yAxes: [{
                  display: true,
                  scaleLabel: {
                      display: true,
                      labelString: 'Сообщения'
                  }
              }]
          }
      }
  };

  $.each(config_message.data.datasets, function(i, dataset) {
      dataset.borderColor = randomColor(0.4);
      dataset.backgroundColor = randomColor(0.5);
      dataset.pointBorderColor = randomColor(0.7);
      dataset.pointBackgroundColor = randomColor(0.5);
      dataset.pointBorderWidth = 1;
  });






  window.onload = function() {
      var ctx = document.getElementById("canvas_obj").getContext("2d");
      window.myLine = new Chart(ctx, config_obj);
      
      var ctx = document.getElementById("canvas_obj_pub").getContext("2d");
      window.myLine = new Chart(ctx, config_obj_pub);
      
      var ctx = document.getElementById("canvas_message").getContext("2d");
      window.myLine = new Chart(ctx, config_message);
  };


</script>
  
  
