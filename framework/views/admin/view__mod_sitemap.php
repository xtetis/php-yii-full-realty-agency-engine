<h4 class="text-center" style="margin-top: 0px;">
  <strong>
    Генератор карты сайта
  </strong>
</h4>




<div style="padding-top:20px;">

<div class="btn-toolbar" role="toolbar">

<div class="btn-group btn-group-sm">
  <a class="btn btn-primary" href="/admin/mod_sitemap">Начало</a>
</div>

<div class="btn-group btn-group-sm">
  <a href="/admin/mod_sitemap/delmap" 
     data-toggle="tooltip"
     class="btn btn-primary" 
     data-placement="top" 
     style="color:#fff;"
     onclick="return confirm('Вы уверены, что хотите удалить текущую карту сайта?');"
     title="Удалить текущую карту сайта">
     Удалить карту
  </a>
</div>

<div class="btn-group btn-group-sm">
  <a class="btn btn-primary" 
     href="/admin/mod_sitemap/step1"
     title="Выполняем запрос и сохраняем результат в файл"
     data-toggle="tooltip"
     class="btn btn-primary" 
     data-placement="top" 
     style="color:#fff;">
     Шаг 1
  </a>
  <a class="btn btn-primary" 
     href="/admin/mod_sitemap/step2"
     title="Формируем SEF урлы"
     data-toggle="tooltip"
     class="btn btn-primary" 
     data-placement="top" 
     style="color:#fff;">
     Шаг 2
  </a>
  <a class="btn btn-primary" 
     href="/admin/mod_sitemap/step3"
     title="Формируем куски XML"
     data-toggle="tooltip"
     class="btn btn-primary" 
     data-placement="top" 
     style="color:#fff;">
     Шаг 3
  </a>
  <a class="btn btn-primary" 
     href="/admin/mod_sitemap/step4"
     title="Делаем одну карту сайта или несколько и разбиваем на части"
     data-toggle="tooltip"
     class="btn btn-primary" 
     data-placement="top" 
     style="color:#fff;">
     Шаг 4
  </a>
</div> 





<div class="btn-group btn-group-sm" style="float:right;">
  <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown">
    Помощь <span class="caret"></span>
  </button>
  <ul class="dropdown-menu dropdown-menu-right dropdown-menu-sm" role="menu">
    <li><a target="_blank" href="https://support.google.com/webmasters/answer/71453?hl=ru">
        Файл индекса Sitemap</a></li>
    <li><a target="_blank" href="http://webmaster.yandex.ua/sitemaptest.xml">
        Проверка карты сайта</a></li>
    <li><a target="_blank" href="http://www.sitemaps.org/ru/faq.html">
        Ответы на вопросы по Sitemap</a></li>
        
  </ul>
</div>




    
</div>





                        
                

                   




</div>


<?=$info['need_to_parse'];?>



<div style="padding-top:30px; <?=(strlen($info['protocol'])?'':'display:none;')?>">
<div class="panel panel-default" style="height: 190px;">
  <div class="panel-heading">
    <h3 class="panel-title">Протокол генерации карты</h3>
  </div>
  <div class="panel-body" style="height: 150px; overflow-y:scroll;">
     <?=$info['protocol'];?>
     <br>
  </div>
</div>
</div>


<div style="margin-top:15px;">
  <blockquote>
    <p style="font-size:13px;">
      Для генерации карты сайта - удалите старую карту сайта, после этого пройдите от 1 шага до 4. 
      В результате, в "Протоколе генерации карты" получите сообщение об успешной генерации карты сайта.
    </p>
  </blockquote>
</div>
