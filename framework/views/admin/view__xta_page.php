<?php
use yii\helpers\Html;
use yii\grid\GridView;
use app\components\AdminDirectoryTop;
use yii\widgets\ActiveForm;
use app\models\gii\xta_site;
use yii\helpers\ArrayHelper;

global $action_name_grid;
$action_name_grid = $action_name;
?>

<?
echo AdminDirectoryTop::widget([
    'params' => [
        'action_name' => $action_name,
        'title' => 'Статические страницы',
    ],
]);
?>







<?if ($command=='select'):?>
<?= GridView::widget([
	'dataProvider' => $dataProvider,
	'filterModel' => $searchModel,
	'layout' => '{summary}{items}{pager}',
	'summary' => 'Показано {begin} - {end} из {totalCount} записей
',
	'columns' => [
		[
		'attribute'=>'id',
		'headerOptions' => ['width' => '40'],
		],
		'name',
		[
			'class' => 'yii\grid\ActionColumn',
			'headerOptions' => ['width' => '50'],
			'template' => '{update} {delete}',
			'buttons' => [
				'update' => function ($url,$model,$key) {
					global $action_name_grid;
					return Html::a('<span class="glyphicon glyphicon-pencil"></span>',
					               '/admin/'.$action_name_grid.'/update/'.$key);
				},
				'delete' => function ($url,$model,$key) {
					global $action_name_grid;
					return Html::a('<span class="glyphicon glyphicon-trash"></span>',
					               '/admin/'.$action_name_grid.'/delete/'.$key,
					               [
                           'data-confirm'=>'Вы уверены, что хотите удалить эту запись?',
                         ]);
				},
			],
		],
	],
]); ?>
<?endif?>





<?if (($command=='create')||($command=='update')):?>
    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'id_site')->dropDownList(xta_site::fn__get_xta_site_DataList()) ?>
    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'about')->textarea(['rows' => 6]) ?>
	  <?= $form->field($model, 'published')->dropDownList(array(
				'1'=>'Публиковать',
				'0'=>'Не публиковать',
				));?>
    <?= $form->field($model, 'editedon')->textInput(['maxlength' => true]) ?>
	  <?= $form->field($model, 'eval')->dropDownList(array(
				'0'=>'Не выполнять',
				'1'=>'Выполнять',
				));?>
	  <?= $form->field($model, 'richedit')->dropDownList(array(
				'0'=>'Не использовать визуальный редактор',
				'1'=>'Использовать визуальные радатор',
				));?>
    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>
	  <?= $form->field($model, 'index')->dropDownList(array(
				'1'=>'Индексировать',
				'0'=>'Не индексировать',
				));?>
		<?if ($model->richedit):?>
		<script>CKEDITOR.replace( 'xta_page-about' );</script>
		<?endif;?>

    
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>
<?endif?>
