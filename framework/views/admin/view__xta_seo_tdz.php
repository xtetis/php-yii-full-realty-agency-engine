<?php
use yii\helpers\Html;
use yii\grid\GridView;
use app\components\AdminDirectoryTop;
use yii\widgets\ActiveForm;
use app\models\gii\xta_site;
use yii\helpers\ArrayHelper;

global $action_name_grid;
$action_name_grid = $action_name;
?>

<?
echo AdminDirectoryTop::widget([
    'params' => [
        'action_name' => $action_name,
        'title' => 'SEO для страниц',
    ],
]);
?>





<?if ($command=='select'):?>
<?= GridView::widget([
	'dataProvider' => $dataProvider,
	'filterModel' => $searchModel,
	'layout' => '{summary}{items}{pager}',
	'summary' => 'Показано {begin} - {end} из {totalCount} записей
',
	'columns' => [
		[
		'attribute'=>'id',
		'headerOptions' => ['width' => '40'],
		],
		[
			'attribute' => 'id_site',
			'value' => 'xta_site.name',
			'label'=>'Сайт',
		],
		'rurl',
		[
			'class' => 'yii\grid\ActionColumn',
			'headerOptions' => ['width' => '50'],
			'template' => '{update} {delete}',
			'buttons' => [
				'update' => function ($url,$model,$key) {
					global $action_name_grid;
					return Html::a('<span class="glyphicon glyphicon-pencil"></span>',
					               '/admin/'.$action_name_grid.'/update/'.$key);
				},
				'delete' => function ($url,$model,$key) {
					global $action_name_grid;
					return Html::a('<span class="glyphicon glyphicon-trash"></span>',
					               '/admin/'.$action_name_grid.'/delete/'.$key,
					               [
                           'data-confirm'=>'Вы уверены, что хотите удалить эту запись?',
                         ]);
				},
			],
		],
	],
]); ?>
<?endif?>








<?if (($command=='create')||($command=='update')):?>
    <?php $form = ActiveForm::begin(); ?>
		  <?= $form->field($model, 'id_site')->dropDownList(xta_site::fn__get_xta_site_DataList()) ?>
		  <?= $form->field($model, 'rurl')->textInput(['maxlength' => true]) ?>
		  <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
		  <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>
		  <?= $form->field($model, 'hone')->textInput(['maxlength' => true]) ?>
		  <?= $form->field($model, 'index')->dropDownList(array(
					'1'=>'Индексировать',
					'0'=>'Не индексировать',
					));?>
		  <?= $form->field($model, 'canonical')->textInput(['maxlength' => true]) ?>
		  <?= $form->field($model, 'seotext')->textarea(['rows' => 6]) ?>
      <script>CKEDITOR.replace( 'xta_seo_tdz-seotext' );</script>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>
<?endif?>
