<?php
use yii\helpers\Html;
use yii\grid\GridView;
use app\components\AdminDirectoryTop;
use yii\widgets\ActiveForm;
use yii\bootstrap\Tabs;
use yii\helpers\ArrayHelper;
use app\models\gii\xta_city;


global $action_name_grid;
$action_name_grid = $action_name;
?>

<?
echo AdminDirectoryTop::widget([
    'params' => [
        'action_name' => $action_name,
        'title' => 'Опции объявлений',
    ],
]);
?>





<?if ($command=='select'):?>
<?= GridView::widget([
	'dataProvider' => $dataProvider,
	'filterModel' => $searchModel,
	'layout' => '{summary}{items}{pager}',
	'summary' => 'Показано {begin} - {end} из {totalCount} записей
',
	'columns' => [
		[
		'attribute'=>'id',
		'headerOptions' => ['width' => '40'],
		],
//		[
//			'attribute' => 'city',
//			'value' => 'xta_city.name',
//			'label'=>'Город',
//		],
		'name',
		[
		'attribute'=>'filter_type',
		'value' => function ($model, $index, $widget) 
			{
				$retarr =array('0'=>'Range (int)','1'=>'Select (multi)');
				return $retarr[$model->filter_type].' ('.$model->filter_type.')';
			},
		],
		
		[
			'class' => 'yii\grid\ActionColumn',
			'headerOptions' => ['width' => '50'],
			'template' => '{update} {delete}',
			'buttons' => [
				'update' => function ($url,$model,$key) {
					global $action_name_grid;
					return Html::a('<span class="glyphicon glyphicon-pencil"></span>',
					               '/admin/'.$action_name_grid.'/update/'.$key);
				},
				'delete' => function ($url,$model,$key) {
					global $action_name_grid;
					return Html::a('<span class="glyphicon glyphicon-trash"></span>',
					               '/admin/'.$action_name_grid.'/delete/'.$key,
					               [
                           'data-confirm'=>'Вы уверены, что хотите удалить эту запись?',
                         ]);
				},
			],
		],
	],
]); ?>
<?endif?>








<?if (($command=='create')||($command=='update')):?>

	<ul class="nav nav-tabs" role="tablist">
		<li class="<?=(($active_tab=='option')?'active':'')?>">
			<a href="#option" aria-controls="option" role="tab" data-toggle="tab">Опция</a>
		</li>
		<?if ($command=='update'):?>
		<li class=" <?=(($active_tab=='categs')?'active':'')?>"> 
			<a href="#categs" aria-controls="categs" role="tab" data-toggle="tab">Категории</a>
		</li>
		<li class="<?=(($active_tab=='values')?'active':'')?>">
			<a href="#values" aria-controls="values" role="tab" data-toggle="tab">Возможные значения</a>
		</li>
		<?endif?>
	</ul>

	<div class="tab-content">
		<div role="tabpanel" class="tab-pane <?=(($active_tab=='option')?'active':'')?>" id="option">
			<?php $form = ActiveForm::begin(); ?>
			<?= $form->field($model, 'option_type')->dropDownList([0=>'Select',1=>'TextInput']) ?>
			<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
			<?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>
			<?= $form->field($model, 'introtext')->textInput(['maxlength' => true]) ?>
			<?= $form->field($model, 'filter_type')->dropDownList(array(
					'0'=>'Range (int)',
					'1'=>'Select (multi)'
					));?>
			
			<div class="form-group">
					<?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', 
					['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
			</div>
			<?php ActiveForm::end(); ?>
		</div>

<?if ($command=='update'):?>
	<div role="tabpanel" class="tab-pane <?=(($active_tab=='categs')?'active':'')?>" id="categs">

<!--pre><?//print_r($in_category);?></pre -->

		<?php $form = ActiveForm::begin();  ?>
			<table class="table table-bordered table-striped">
				<tr>
					<th style="width:30px;">ID</th>
					<th>Название</th>
					<th style="width:60px;">Фильтр</th>
					<th style="width:60px;">Св-во</th>
				</tr>
				<?foreach ($categories_all as $item){?>
				<tr class="xta_obj_category__parent__<?=$item['id_parent']?>" idx="<?=$item['id']?>">
					<td><?=$item['id']?></td>
					<td style="padding-left:<?=(8+26*($item['parent_depth']))?>px;">
						<?
						if ($item['has_children'])
						{
							echo '<a style="cursor:pointer;" idx="'.$item['id'].'"">
											<span class="glyphicon glyphicon-minus"></span>
										</a>';
						}
						else
						{
							echo '<a><span class="glyphicon glyphicon-asterisk"></span></a>';
						}
						?>
						<?=$item['name']?>
					</td>
					<td style="text-align:center;">
						<input type="checkbox" 
						       name="in_category[use_filter][]" 
						       value="<?=$item['id']?>" 
						       <?=((isset($in_category[$item['id']]['use_filter']))&&(intval($in_category[$item['id']]['use_filter'])))?'checked="checked"':''?>>
					</td>
					<td style="text-align:center;">
						<?if (!$item['has_children']):?>
							<input type="checkbox" 
								     name="in_category[use_setting][]" 
								     value="<?=$item['id']?>" 
								     <?=( 
								          (isset($in_category[$item['id']]['use_setting']))&&
								          (intval($in_category[$item['id']]['use_setting']))
								        )?'checked="checked"':''?>>
						<?endif;?>
					</td>
				</tr>
				<?}?>
			</table>
			<div class="form-group">
				<?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', 
				['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
			</div>
		<?php ActiveForm::end();  ?>
	</div>















	<div role="tabpanel" class="tab-pane <?=(($active_tab=='values')?'active':'')?>" id="values">
		<div style="padding:10px; text-align:right;">
			<button type="button" class="btn btn-primary" id="new_av">
				Добавить запись
			</button>
		</div>

		<div class="modal fade" id="modal_edit_available" tabindex="-1" role="dialog">
			<?php $form = ActiveForm::begin(); ?>
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">Изменение доступного значения</h4>
					</div>
					<div class="modal-body">
						<input type="hidden" id="av_id" name="opt_available_id" value="0">
						<div class="form-group">
							<label>Родительский элемент</label>
							<select class="form-control" id="av_parent" name="opt_parent">
								<option value="0">Корень (0)</option>
								<?foreach ($model_obj_option_available as $mdata):?>
									<?if ($mdata->id_parent==0):?>
										<option value="<?=$mdata->id?>"><?=$mdata->name?> (<?=$mdata->id?>)</option>
									<?endif;?>
								<?endforeach;?>
							</select>
						</div>
						<div class="form-group">
							<label>Имя</label>
							<input type="text" class="form-control" id="av_name" name="opt_available">
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
						<button type="submit" class="btn btn-primary">Сохранить</button>
					</div>
				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
			<?php ActiveForm::end(); ?>
		</div><!-- /.modal -->

		<table class="table table-striped table-bordered table-hover">
			<tr>
				<th style="width:30px;">#</th>
				<th style="width:80px;">Родитель</th>
				<th>Имя</th>
				<th style="width:30px;"></th>
			</tr>
		<?foreach ($model_obj_option_available as $mdata):?>
			<?if ($mdata->id_parent==0):?>
			<tr>
				<td><?=$mdata->id?></td>
				<td id="av_id_<?=$mdata->id?>_parent" class="av_id_parent"><?=$mdata->id_parent?></td>
				<td id="av_id_<?=$mdata->id?>_name"><?=$mdata->name?></td>
				<td>
					<a class="edit_av" href="javascript:void(0)" idx="<?=$mdata->id?>">
						<i class="glyphicon glyphicon-pencil"></i>
					</a>
				</td>
			</tr>
			<?foreach ($model_obj_option_available as $mdata_child):?>
				<?if ($mdata_child->id_parent==$mdata->id):?>
				<tr>
					<td><?=$mdata_child->id?></td>
					<td id="av_id_<?=$mdata_child->id?>_parent" class="av_id_parent"><?=$mdata_child->id_parent?></td>
					<td id="av_id_<?=$mdata_child->id?>_name" style="padding-left:40px;">
						 <?=$mdata_child->name?>
					</td>
					<td>
						<a class="edit_av" href="javascript:void(0)" idx="<?=$mdata_child->id?>">
							<i class="glyphicon glyphicon-pencil"></i>
						</a>
					</td>
				</tr>
				<?endif;?>
			<?endforeach;?>
			<?endif;?>
		<?endforeach;?>
		</table>
		
	</div>
	<? endif;	?>
</div>


<?
$js = '
	$(".edit_av").click(function() {
		var idx = $(this).attr("idx");
		$("#av_id").val(idx); 
		// Если у элемента есть дочерние, то нельзя менять родителя
		$(".av_id_parent").each(function() {
			if ($(this).text() == idx)
			{
				$("#av_parent").attr("disabled","disabled")
			}
		});
		$("#av_parent").val($("#av_id_"+idx+"_parent").html());
		$("#av_name").val($("#av_id_"+idx+"_name").html());
		$("#modal_edit_available").modal("show");
	});
	
	$("#new_av").click(function() {
		$("#av_name").val("");
		$("#av_id").val(0);
		$("#av_parent").removeAttr("disabled");
		$("#av_parent").val(0);
		$("#modal_edit_available").modal("show");
	});
';
$this->registerJs($js);
?>


<?endif?>
