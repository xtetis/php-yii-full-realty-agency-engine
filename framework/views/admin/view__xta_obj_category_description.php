<?php
use yii\helpers\Html;
use yii\grid\GridView;
use app\components\AdminDirectoryTop;
use yii\widgets\ActiveForm;
use app\models\gii\xta_obj_category; 
use app\models\gii\xta_site; 

use app\models\gii\xta_obj_category_description; 
use yii\helpers\ArrayHelper;

global $action_name_grid;
$action_name_grid = $action_name;
?>

<?
echo AdminDirectoryTop::widget([
    'params' => [
        'action_name' => $action_name,
        'title' => 'ТДЗ категорий объявлений',
    ],
]);
?>





<?if ($command=='select'):?>
<?= GridView::widget([
	'dataProvider' => $dataProvider,
	'filterModel' => $searchModel,
	'layout' => '{summary}{items}{pager}',
	'summary' => 'Показано {begin} - {end} из {totalCount} записей
',
	'columns' => [
		[
		'attribute'=>'id',
		'headerOptions' => ['width' => '40'],
		],
		[
			'attribute' => 'obj_category',
			'value' => 'xta_obj_category.name',
			'label'=>'Категория',
		],
		[
			'attribute' => 'id_site',
			'value' => 'xta_site.name',
			'label'=>'Сайт',
		],
		//'name',
		[
			'class' => 'yii\grid\ActionColumn',
			'headerOptions' => ['width' => '50'],
			'template' => '{update} {delete}',
			'buttons' => [
				'update' => function ($url,$model,$key) {
					global $action_name_grid;
					return Html::a('<span class="glyphicon glyphicon-pencil"></span>',
					               '/admin/'.$action_name_grid.'/update/'.$key);
				},
				'delete' => function ($url,$model,$key) {
					global $action_name_grid;
					return Html::a('<span class="glyphicon glyphicon-trash"></span>',
					               '/admin/'.$action_name_grid.'/delete/'.$key,
					               [
                           'data-confirm'=>'Вы уверены, что хотите удалить эту запись?',
                         ]);
				},
			],
		],
	],
]); ?>
<?endif?>








<?if (($command=='create')||($command=='update')):?>
<?

$querys = xta_obj_category_description::find()->where(["id_site" => xta_site::getCurrentSiteId()])->all();
$disallow_array = array();
foreach ($querys as $item)
{
	if (($command=='update')&&($model->id_obj_category==$item->id_obj_category))
	{
		continue;
	}
	
	$disallow_array[]=$item->id_obj_category;
}




$category_list = xta_obj_category::getObjCategoryDataList(false,array(),$disallow_array);



//$category_list_keys = array_keys($category_list);
?>
	<?php $form = ActiveForm::begin(); ?>
	<?= $form->field($model, 'id_obj_category')->dropDownList($category_list) ?>
	<?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
	<?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>
	<?= $form->field($model, 'hone')->textInput(['maxlength' => true]) ?>
	<?= $form->field($model, 'seotext')->textarea(['rows' => 6]) ?>
	<script>CKEDITOR.replace( 'xta_obj_category_description-seotext' );</script>
	<div class="form-group">
		  <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	</div>
	<?php ActiveForm::end(); ?>


<?endif?>
