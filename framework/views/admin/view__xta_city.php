<?php
use yii\helpers\Html;
use yii\grid\GridView;
use app\components\AdminDirectoryTop;
use yii\widgets\ActiveForm;
use app\models\gii\xta_country; 
use yii\helpers\ArrayHelper;

global $action_name_grid;
$action_name_grid = $action_name;
?>

<?
echo AdminDirectoryTop::widget([
    'params' => [
        'action_name' => $action_name,
        'title' => 'Города',
    ],
]);
?>





<?if ($command=='select'):?>
<?= GridView::widget([
	'dataProvider' => $dataProvider,
	'filterModel' => $searchModel,
	'layout' => '{summary}{items}{pager}',
	'summary' => 'Показано {begin} - {end} из {totalCount} записей',
	'columns' => [
		[
		'attribute'=>'id',
		'headerOptions' => ['width' => '40'],
		],
		[
			'attribute' => 'region',
			'value' => 'xta_region.name',
			'label'=>'Регион',
		],
		'name',
		[
			'class' => 'yii\grid\ActionColumn',
			'headerOptions' => ['width' => '50'],
			'template' => '{update} {delete}',
			'buttons' => [
				'update' => function ($url,$model,$key) {
					global $action_name_grid;
					return Html::a('<span class="glyphicon glyphicon-pencil"></span>',
					               '/admin/'.$action_name_grid.'/update/'.$key);
				},
				'delete' => function ($url,$model,$key) {
					global $action_name_grid;
					return Html::a('<span class="glyphicon glyphicon-trash"></span>',
					               '/admin/'.$action_name_grid.'/delete/'.$key,
					               [
                           'data-confirm'=>'Вы уверены, что хотите удалить эту запись?',
                         ]);
				},
			],
		],
	],
]); ?>
<?endif?>








<?if (($command=='create')||($command=='update')):?>
    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'id_region')->dropDownList($model->getRegionDataList()) ?>
    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>
<?endif?>
