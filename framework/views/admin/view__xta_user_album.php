<?php
use yii\helpers\Html;
use yii\grid\GridView;
use app\components\AdminDirectoryTop;
use yii\widgets\ActiveForm;
use app\models\gii\xta_site;
use yii\helpers\ArrayHelper;



global $action_name_grid;
$action_name_grid = $action_name;
?>

<?
echo AdminDirectoryTop::widget([
    'params' => [
        'action_name' => $action_name,
        'title' => 'Пользовательские альбомы',
    ],
]);
?>







<?if ($command=='select'):?>
<?= GridView::widget([
	'dataProvider' => $dataProvider,
	'filterModel' => $searchModel,
	'layout' => '{summary}{items}{pager}',
	'summary' => 'Показано {begin} - {end} из {totalCount} записей
',
	'columns' => [
		[
		'attribute'=>'id',
		'headerOptions' => ['width' => '40'],
		],
		'name',
		[
			'class' => 'yii\grid\ActionColumn',
			'headerOptions' => ['width' => '50'],
			'template' => '{update} {delete}',
			'buttons' => [
				'update' => function ($url,$model,$key) {
					global $action_name_grid;
					return Html::a('<span class="glyphicon glyphicon-pencil"></span>',
					               '/admin/'.$action_name_grid.'/update/'.$key);
				},
				'delete' => function ($url,$model,$key) {
					global $action_name_grid;
					return Html::a('<span class="glyphicon glyphicon-trash"></span>',
					               '/admin/'.$action_name_grid.'/delete/'.$key,
					               [
                           'data-confirm'=>'Вы уверены, что хотите удалить эту запись?',
                         ]);
				},
			],
		],
	],
]); ?>
<?endif?>





<?if (($command=='create')||($command=='update')):?>
    <?php $form = ActiveForm::begin(); ?>
    <?//= $form->field($model, 'id_site')->dropDownList(xta_site::fn__get_xta_site_DataList()) ?>
    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'about')->textarea(['rows' => 6]) ?>
		<?//if ($model->richedit):?>
		<script>CKEDITOR.replace( 'xta_user_album-about' );</script>
		<?//endif;?>
		
		


    
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>
    
		<?if ($command=='update'):?>
			<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
				<?= $form->field($model_UploadForm, 'imageFiles[]')->fileInput(['multiple' => true, 'accept' => 'image/*']) ?>
				
				<?= Html::submitButton('Загрузить', ['class' => 'btn btn-primary']) ?>
			<?php ActiveForm::end(); ?>
			
			
			<div class="row">
			<?
			if (
				    (isset($model__xta_image_many)) &&
				    (is_array($model__xta_image_many)) &&
				    (count($model__xta_image_many))
				 )
			{
				foreach ($model__xta_image_many as $model__xta_image)
				{
					?>
					<div class="col-xs-6 col-md-3">
						<a href="#" class="thumbnail">
							<img src="<?=$model__xta_image->fn__get_thumb_src()?>" alt="...">
						</a>
						<a href="/admin/<?=$action_name_grid?>/update/<?=$model->id?>?delimage=<?=$model__xta_image->id?>"><span class="glyphicon glyphicon-remove"></span></a>
					</div>
					<?
				}
			}
			?>
			</div>
			
			
		<?endif?>
		
		

    
<?endif?>
