<?php
use yii\helpers\Url;


$title = 'Агентство недвижимости "Ключ": продажа и аренда недвижимости в '.$current_city_name_padeg_5.' '.' | '.ucfirst($_SERVER['HTTP_HOST']);
$description = 'Агентство недвижимости "Ключ" '.$current_city_name_padeg_1.'.';


if ($xta_seo_tdz)
{
	if (strlen($xta_seo_tdz->title))
	{
		$title = $xta_seo_tdz->title;
	}

	if (strlen($xta_seo_tdz->description))
	{
		$description = $xta_seo_tdz->description;
	}
}

$this->title=$title;
$this->registerMetaTag([
	'name'    => 'description',
	'content' => $description
]);






$this->registerJsFile(Yii::$app->request->baseUrl.'/public/components/bxslider/src/js/jquery.bxslider.js',['depends' => [\yii\web\JqueryAsset::className()]]);





$js = "
$('.bxslider').bxSlider({
  mode: 'fade',
  captions: true
});
";
$this->registerJs($js);

?>



<ul class="bxslider">
	<li>
		<img src="/public/templates/default/img/slider/slider1.jpg" title="Самый большой выбор недвижимости" />
	</li>
	<li>
		<img src="/public/templates/default/img/slider/slider2.jpg" title="Только актуальные цены" />
	</li>
	<li>
		<img src="/public/templates/default/img/slider/slider3.jpg"  />
	</li>
	<li>
		<img src="/public/templates/default/img/slider/slider4.jpg" />
	</li>
	<li>
		<img src="/public/templates/default/img/slider/slider5.jpg"  />
	</li>
</ul>



<div class="main_cat_item_container">
<?
foreach ($category_filter['list'] as $item)
{?>
	<div class="main_cat_item_inner">
		<a href="<?=$item['link']?>">
			<div style="text-align: center;" class="main_cat_item_inner_img">
				<img src="<?=$item['img']?>" style="width:80%;">
			</div>
			<div class="main_cat_item_inner_text">
					<?=$item['name']?>
			</div>
		</a>
	</div>
<?
}
?>
</div>

<div style="text-align: center; margin-top: -15px;">
	<a href="/board"  class="btn btn-primary">Вся недвижимость Одессы <i class="glyphicon glyphicon-th-list"></i></a>
</div>



<div>
<p>
Одессу можно сравнить с прекрасной женщиной, которая не теряет молодость с годами. У Одессы есть своя изюминка, заключающаяся в этих петляющих одесских улочках, в неповторимой архитектуре старых зданий, в этом зеленом море шелковиц и каштанов.
</p><p>
К счастью, горожане-патриоты смогли сохранить сердце города в первозданном виде, и поэтому мы можем погулять по Дерибасовской, посидеть на лавочке в Горсаду, полюбоваться морскими пейзажами на картинах одесских художников на Соборной площади. Для иностранцев эти места являются достопримечательностями, для одесситов это история, это место, где можно вернуться в прошлое и зарядиться там энергией родного города.
</p><p>
Купить квартиру в центре Одессы мечтает каждый одессит. Кто бы не хотел иметь квартиру, где можно, спокойно попивая кофе и глядя в окно, любоваться легендарным Дюком? Или жить рядом со знаменитым Оперным театром? И таких исторических памятников, каждый из которых посвящен важному для города событию или человеку, более восьмисот. Поэтому, купив квартиру в центре, Вам стоит только оглянуться – и Вы обязательно найдете несколько исторических памятников вокруг себя. А сколько театров? А сколько музеев в этом районе? Дендропарк, ботанический сад, дельфинарий – да, живя в этом районе, Вам не пришлось бы скучать. Что еще следует отметить – в последнее время архитекторы и строители взялись «наводить марафет» в центре города. И теперь на центральных улицах нас радуют своим видом помолодевшие фасадом старинные здания, распрямившиеся и повеселевшие.
</p>

</div>



<ul class="main_cursite_district_list nav nav-pills">
<?
foreach ($districts as $key => $value)
{
?>
	<li>
		<a href="/board?district=<?=$key?>">
			<img src="/public/templates/default/img/ico/square.png">
			<?=$value?> район
		</a>
	</li>
<?
}
?>
</ul>





