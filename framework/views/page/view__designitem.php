<?
use yii\widgets\ListView;
use yii\widgets\ActiveForm;
use app\models\LibCommon;
use app\components\FrontendCategoryListTree;


$this->title='Услуги дизайнера';
?>

<!-- Add fancyBox main JS and CSS files -->
	<link rel="stylesheet" type="text/css" href="/public/components/fancybox/source/jquery.fancybox.css?v=2.1.5" media="screen" />

	<!-- Add Button helper (this is optional) -->
	<link rel="stylesheet" type="text/css" href="/public/components/fancybox/source/helpers/jquery.fancybox-buttons.css?v=1.0.5" />

	<!-- Add Thumbnail helper (this is optional) -->
	<link rel="stylesheet" type="text/css" href="/public/components/fancybox/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" />


<h1>Альбом: <?=$model__xta_user_album->name?></h1>
<br>
<div class="row">
<?
if (
     (isset($images)) &&
     (is_array($images))
   )
{
		foreach ($images as $model__xta_image)
		{
			?>
				<div class="col-xs-6 col-md-3">
					<a href="<?=$model__xta_image->fn__get_src()?>" class="thumbnail fancybox-button" rel="fancybox-button">
						<img src="<?=$model__xta_image->fn__get_thumb_src()?>" alt="...">
					</a>
				</div>
			<?
			$i++;
		}
}
?>
</div>
<?=$model__xta_user_album->about?>

<?
$this->registerJsFile('/public/components/fancybox/source/jquery.fancybox.pack.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

$this->registerJsFile('/public/components/fancybox/source/helpers/jquery.fancybox-buttons.js', ['depends' => [\yii\web\JqueryAsset::className()]]);


$this->registerJsFile('/public/components/fancybox/source/helpers/jquery.fancybox-media.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('/public/components/fancybox/source/helpers/jquery.fancybox-thumbs.js', ['depends' => [\yii\web\JqueryAsset::className()]]);



$js = "
	$('.fancybox-button').fancybox({
		prevEffect		: 'none',
		nextEffect		: 'none',
		closeBtn		: false,
		helpers		: {
			title	: { type : 'inside' },
			buttons	: {}
		}
	});
";
$this->registerJs($js);
?>




