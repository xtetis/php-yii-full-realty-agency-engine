<?
use yii\widgets\ListView;
use yii\widgets\ActiveForm;
use app\models\LibCommon;
use app\components\FrontendCategoryListTree;
?>




<?

// ТДЗ
//**************************************************************************************************
if (strlen($model->title))
{
	$this->title=$model->title;
}
else
{
	$this->title=$model->name;
}


if (strlen($model->description))
{
	$this->registerMetaTag([
		'name'    => 'description',
		'content' => $model->description
	]);
}

if (!$model->index)
{
	$this->registerMetaTag([
		'name'    => 'robots',
		'content' => 'noindex,follow'
	]);
}
//**************************************************************************************************

?>





<h1>
<?=$model->name?>
</h1>
<div>
	<?if($model->id!=4):?>
		<?=$model->about?>
	<?endif;?>
</div>

<div>
<?if($model->id=4):?>

<table class="table  table-bordered">
	<tr>
		<td style="width:20px;"><i class="glyphicon glyphicon-earphone"></i></td>
		<td>Телефон:</td>
		<td>+38 (048) 793-00-53</td>
	</tr>
	<tr>
		<td><i class="glyphicon glyphicon-earphone"></i></td>
		<td>Телефон:</td>
		<td>+38 (048) 783-00-53</td>
	</tr>
	<tr>
		<td><i class="glyphicon glyphicon-earphone"></i></td>
		<td>Телефон:</td>
		<td>+38 (048) 736-00-53</td>
	</tr>
	<tr>
		<td><i class="glyphicon glyphicon-envelope"></i></td>
		<td>Email:</td>
		<td>7930043@gmail.com</td>
	</tr>
	<tr>
		<td><i class="glyphicon glyphicon-map-marker"></i></td>
		<td>Адрес:</td>
		<td>65078, г. Одесса, ул. Космонавтов, 32, каб. №8001</td>
	</tr>	
</table>


<div style="margin-left:-6px; margin-right:-6px;">
<script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?sid=3XjuJdvYacHu4fk4_11KvogNedABryx_&amp;width=100%25&amp;height=400&amp;lang=ru_RU&amp;sourceType=constructor&amp;scroll=true"></script>
</div>
<br>
<div class="panel panel-default">
  <div class="panel-heading">Написать нам</div>
  <div class="panel-body">

<?php
//******************************************************************************

if (isset($_POST['frm_sndmail']))
{
	$_name    = $_POST['frm_name'];
	$_email   = $_POST['frm_email'];
	$_phone   = $_POST['frm_phone'];
	$_message = $_POST['frm_message'];
	
	$error_msg='';
	if (strlen($_name)<3)
	{
		$error_msg='Имя не может быть короче 3 символов';
	}
	if (strlen($_email)<5)
	{
		$error_msg='Email не может быть короче 5 символов';
	}

	if (strlen($_phone)<5)
	{
		$error_msg='Телефон не может быть короче 8 символов';
	}
	if (strlen($_message)<10)
	{
		$error_msg='Сообщение не может быть короче 10 символов';
	}

	if (!strlen($error_msg))
	{
		$message = "Вам отправили сообщение с сайта ".$_SERVER['HTTP_HOST']."\r\n".
			          "Имя: ".$_name."\r\n".
			          "Email: ".$_email."\r\n".
			          "Телефон: ".$_phone."\r\n".
			          "Сообщение: ".$_message."\r\n";

		mail('7930043@gmail.com', 'Вам отправили сообщение с сайта '.$_SERVER['HTTP_HOST'], $message);

		$_name    = '';
		$_email   = '';
		$_phone   = '';
		$_message = '';
		$confirm_msg='Ваше сообщение отправлено';

	}
	
}

$form = '
<div style="font-weight:bold; color:#a40000;">
  '.$error_msg.'
</div>
<div style="font-weight:bold; color:#4e9a06;">
  '.$confirm_msg.'
</div>
<form method="post" style="">
  <div>
    <label><b>Ваше имя</b></label>
    <input type="text" class="form-control" name="frm_name" value="'.$_name.'" required>
  </div>
  
  <div style="padding-top:5px;">
    <label><b>Email</b></label>
    <input type="email" name="frm_email" value="'.$_email.'" class="form-control" required>
  </div>
  
  <div style="padding-top:5px;">
    <label><b>Телефон</b></label>
    <input type="text" name="frm_phone" value="'.$_phone.'" class="form-control" required>
  </div>
  
  <div style="padding-top:5px;">
    <label><b>Сообщение</b></label>
    <textarea name="frm_message" class="form-control" required>'.$_message.'</textarea>
  </div>
  
  <div style="padding-top:5px;">
    <input type="submit"  class="btn btn-default" name="frm_sndmail" value="Отправить">
  </div>
</form>
';
echo $form;
?>
  </div>
</div>



<?endif;?>
</div>



