<?
use yii\widgets\ListView;
use yii\widgets\ActiveForm;
use app\models\LibCommon;
use app\components\FrontendCategoryListTree;


$this->title='Услуги дизайнера';
?>


<h1>Услуги дизайнера</h1>

<?
foreach ($model__xta_user_album_all as $model__xta_user_album)
{
	?>
	<a href="/page/designitem/<?=$model__xta_user_album->id?>"><h3><?=$model__xta_user_album->name?></h3></a>
	<p><?=mb_substr(strip_tags($model__xta_user_album->about),0,300,'utf-8')?>...</p>
	<?
	if (isset($images[$model__xta_user_album->id_album]))
	{
		$i=0;
		foreach ($images[$model__xta_user_album->id_album] as $model__xta_image)
		{
			?>
				<div class="col-xs-6 col-md-3">
					<a href="/page/designitem/<?=$model__xta_user_album->id?>" class="thumbnail">
						<img src="<?=$model__xta_image->fn__get_thumb_src()?>" alt="...">
					</a>
				</div>
			<?
			$i++;
			if ($i==4)
			{
				break;
			}
		}
	}
	
}
?>






