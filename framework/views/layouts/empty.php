<?php

/* @var $this \yii\web\View */
/* @var $content string */
use yii\bootstrap\BootstrapAsset;
use app\components\FrontendTopMenu;
use app\components\FrontendUserMenu;
use app\components\FrontendFooter;
use yii\helpers\Html;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
	<link href="/public/templates/default/img/favicon/favicon_32_32.png" rel="shortcut icon" type="image/x-icon" />
	<meta charset="<?= Yii::$app->charset ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?= Html::csrfMetaTags() ?>
	<title><?= Html::encode($this->title) ?></title>
	<?php $this->head() ?>
	
	<!-- ************************* Библиотека для работы с шафрованием *************************** -->
		<script type="text/javascript" src="/public/templates/default/js/myencode.js"></script>
		<script type="text/javascript" src="/public/templates/default/js/base64v1_0.js"></script>
	<!-- ************************* /Библиотека для работы с шафрованием ************************** -->


	<!--**************************************** LightBox **************************** -->
	<link type="text/css" rel="stylesheet" href="/public/components/lightslider/src/css/lightslider.css" />
	<link type="text/css" rel="stylesheet" href="/public/components/lightGallery/src/css/lightgallery.css" />
	<!--*************************************** /LightBox **************************** -->
	

	<link rel="stylesheet" type="text/css" href="/public/templates/default/css/content.css" />
</head>
<body style="background: none;">
<?php $this->beginBody() ?>
	<?= $content ?>
<?php $this->endBody() ?>
<script src="/public/templates/default/js/bootstrap_hover_dropdown.js"></script>
<script src="/public/components/lightGallery/src/js/lightgallery.js"></script>
<script src="/public/components/lightslider/src/js/lightslider.js"></script>
<script src="/public/templates/default/js/common.js"></script>

</body>
</html>
<?php $this->endPage() ?>
