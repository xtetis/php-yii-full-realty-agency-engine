<?php

/* @var $this \yii\web\View */
/* @var $content string */
use yii\bootstrap\BootstrapAsset;
use app\components\AdminTopMenu;
use yii\helpers\Html;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
	<link href="/public/templates/default/img/favicon/favicon_32_32.png" rel="shortcut icon" type="image/x-icon" />
	<meta charset="<?= Yii::$app->charset ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?= Html::csrfMetaTags() ?>
	<title>Админка <?=$_SERVER['HTTP_HOST']?></title>
	<?php $this->head() ?>
	
	<!-- ************************* Библиотека для работы с шафрованием *************************** -->
		<script type="text/javascript" src="/public/templates/default/js/myencode.js"></script>
		<script type="text/javascript" src="/public/templates/default/js/base64v1_0.js"></script>
	<!-- ************************* /Библиотека для работы с шафрованием ************************** -->


	<!--**************************************** Постраничная навигация **************************** -->
		<link rel="stylesheet" type="text/css" href="/public/components/paginator3000/paginator3000.css" />
		<script type="text/javascript" src="/public/components/paginator3000/paginator3000.js"></script>
	<!--*************************************** /Постраничная навигация **************************** -->
	
	<link rel="stylesheet" type="text/css" href="/public/templates/default/css/admin_content.css" />
	<script type="text/javascript" src="/public/components/ckeditor/ckeditor.js"></script>

</head>
<body>
<?php $this->beginBody() ?>
	<?=AdminTopMenu::widget() ?>
	<div class="container" id="root_container">
		<div id="top_menu">
			
		</div>
		
		<div class="panel panel-default" style="margin-top:10px;">
			<div class="panel-body">
				<?=$content?>
			</div>
		</div>
	</div>
<?php $this->endBody() ?>
<script src="/public/templates/default/js/bootstrap_hover_dropdown.js"></script>
</body>
</html>
<?php $this->endPage() ?>
