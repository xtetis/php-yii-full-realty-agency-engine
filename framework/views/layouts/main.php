<?php

/* @var $this \yii\web\View */
/* @var $content string */
use yii\bootstrap\BootstrapAsset;
use app\components\FrontendTopMenu;
use app\components\FrontendUserMenu;
use app\components\FrontendFooter;
use app\models\gii\xta_setting; 
use yii\helpers\Html;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
	<link href="/public/templates/default/img/favicon/favicon_32_32.png" rel="shortcut icon" type="image/x-icon" />
	<meta charset="<?= Yii::$app->charset ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?= Html::csrfMetaTags() ?>
	<title><?= Html::encode($this->title) ?></title>
	<?php $this->head() ?>
	
	<!-- ************************* Библиотека для работы с шафрованием *************************** -->
		<script type="text/javascript" src="/public/templates/default/js/myencode.js"></script>
		<script type="text/javascript" src="/public/templates/default/js/base64v1_0.js"></script>
	<!-- ************************* /Библиотека для работы с шафрованием ************************** -->


	<!--**************************************** Постраничная навигация **************************** -->
		<link rel="stylesheet" type="text/css" href="/public/components/paginator3000/paginator3000.css" />
		<script type="text/javascript" src="/public/components/paginator3000/paginator3000.js"></script>
	<!--*************************************** /Постраничная навигация **************************** -->


	<!--**************************************** LightBox **************************** -->
	<!-- link type="text/css" rel="stylesheet" href="/public/components/lightslider/src/css/lightslider.css" /-->
	<!-- link type="text/css" rel="stylesheet" href="/public/components/lightGallery/src/css/lightgallery.css" /-->
	<!-- bxSlider Javascript file -->
<?
$this->registerJsFile('/public/components/bxslider/src/js/jquery.bxslider.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
?>
	<link href="/public/components/bxslider/src/css/jquery.bxslider.css" rel="stylesheet" />
	<!--*************************************** /LightBox **************************** -->
	
	
	<!--*************************************** multiple-select **************************** -->
	<link type="text/css" rel="stylesheet" href="/public/components/multiple-select/multiple-select.css" /> 
	<!--*************************************** /multiple-select **************************** -->


	<link rel="stylesheet" type="text/css" href="/public/templates/default/css/content.css" />
	<meta name="viewport" content="width=device-width; initial-scale=1">

</head>
<body>
<?php $this->beginBody() ?>
	<div class="container" id="root_container">
		<!--div id="undertop" class="row">
			<div class="col-md-4">
				<img src="">
			</div>
			
		</div -->
		<div id="top_menu" style="display:none;">
			<?=FrontendTopMenu::widget() ?>
		</div>
		<div id="main_container_outer">
			<div id="main_container">
				<div style="display: table; width: 100%;">
					<div class="col-md-4 text-left col-sm-4">
						<a href="/">
							<div style="width: 250px;">
								<div id="logodiv">
									<img src="/public/templates/default/img/struct/logo/logo1.png" style="max-height: 110px;">
								</div>
								<div id="logodivbottom">
									 <?//if(fn__get_site_razdel()=='love'){?>
										 <!--<span style="background-color:#a40000;">Знакомства в Кривом Роге</span> -->
									 <?//}else{?>
											<span>Недижимость Одессы</span>
									 <?//}?>
								</div>
							</div>
						</a>
					</div>
					<div class="col-md-4">
						<div style="margin-top: 90px; font-size: 17px; font-weight: bold;">Профессионализм - залог успеха</div>
					</div>

					<div class="col-md-4 text-right col-sm-4">
						<div>
							<!--a href="/"><img src="/public/images/other/vk.png" style="max-height:36px;"></a-->
							<a href="https://www.facebook.com/keyrealtyodessa/"><img src="/public/images/other/fb.png" style="max-height:36px;"></a>
							<!--a href="/"><img src="/public/images/other/tw.png" style="max-height:36px;"></a-->
							<!--a href="/"><img src="/public/images/other/youtube_logo.png" style="max-height:36px;"></a-->
						</div>
						<div style="width: 160px; text-align: left; margin-left: 160px;">
							
							<div>
								<span class="glyphicon glyphicon-envelope" aria-hidden="true"></span>
								<a class="email_link" href="mailto:7930043@gmail.com">7930043@gmail.com</a>
							</div>
							<div>
								<span class="glyphicon glyphicon-earphone" aria-hidden="true""></span>
								+38 048 793 0053
							</div>
							<div>
								<span class="glyphicon glyphicon-earphone" aria-hidden="true"></span>
								+38 048 783 0053
							</div>
							<div>
								<span class="glyphicon glyphicon-earphone" aria-hidden="true"></span>
								+38 048 736 0053
							</div>
							
							
						</div>
						
					</div>
					
					<!--div class="col-md-4 text-right col-sm-4">
						<a class="btn btn-primary btn-lg" 
							 style="font-weight:bold; color:#fff; margin:10px;"
							 href="/board/add">+ Добавить объявление</a>
					</div -->
				
				</div>
				





























				
				<?=FrontendUserMenu::widget() ?>
				<div class="wrap">
					<?= $content ?>
				</div>
			</div>
		</div>
		<div id="bottom_menu">
			<div class="row" style="background: #fff;border-top-left-radius: 9px;margin: 0px; padding:2px;">
				<?//=FrontendFooter::widget() ?>
				<div class="col-md-2">
					<img src="/public/templates/default/img/struct/logo/logo1.png" style="max-width:120px; padding:5px;">
				</div>
				<div class="col-md-7">
					<p>
						Copyright 2016 Агентство Недвижимости "Ключ"<br>
						Продажа и аренда недвижимости в Одессе<br>
						65078, г. Одесса, ул. Космонавтов, 32, каб. №8001
					</p>
				</div>
				<div class="col-md-3">
					<div>
						<span class="glyphicon glyphicon-envelope" aria-hidden="true"></span>
						<a class="email_link" href="mailto:7930043@gmail.com">7930043@gmail.com</a>
					</div>
					<div>
						<span class="glyphicon glyphicon-earphone" aria-hidden="true""></span>
						+38 048 793 0053
					</div>
					<div>
						<span class="glyphicon glyphicon-earphone" aria-hidden="true"></span>
						+38 048 783 0053
					</div>
					<div>
						<span class="glyphicon glyphicon-earphone" aria-hidden="true"></span>
						+38 048 736 0053
					</div>
				</div>
			</div>
		</div>
	</div>

<?
if (isset($_GET['clearcache']))
{
?>
<div id="clearcache_conteiner"></div>
<?
}
?>

<?php $this->endBody() ?>
<script src="/public/templates/default/js/bootstrap_hover_dropdown.js"></script>
<script src="/public/components/lightGallery/src/js/lightgallery.js"></script>
<script src="/public/components/lightslider/src/js/lightslider.js"></script>
<script src="/public/templates/default/js/common.js"></script>
<script src="/public/components/multiple-select/multiple-select.js"></script>

<?=xta_setting::fn__get_setting('yandex_metrica_counter')?>
</body>
</html>
<?php $this->endPage() ?>
