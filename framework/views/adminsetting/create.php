<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\gii\xta_setting */

$this->title = 'Create Xta Setting';
$this->params['breadcrumbs'][] = ['label' => 'Xta Settings', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="xta-setting-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
