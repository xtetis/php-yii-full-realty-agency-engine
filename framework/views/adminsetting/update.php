<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\gii\xta_setting */

$this->title = 'Update Xta Setting: ' . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Xta Settings', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="xta-setting-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
