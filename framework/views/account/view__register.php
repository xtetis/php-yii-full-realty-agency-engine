<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;


$this->title = 'Регистрация пользователя';
$this->registerMetaTag([
    'name' => 'description',
    'content' => 'Зарегистрируйтесь на сайте и размещайте бесплатно ваши объявления.'
]);
?>
<div class="row top-buffer">
	<div class="account___view__register col-md-6 col-md-offset-3">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h1 class="panel-title">Регистрация нового пользователя</h1>
			</div>
			<div class="panel-body">
				<?php $form = ActiveForm::begin(['id' => 'register-form']); ?>

				<?=$form->field($model, 'username') ?>
				<?=$form->field($model, 'email') ?>
				<?=$form->field($model, 'phone') ?>
				<?=$form->field($model, 'skype') ?>
				<?=$form->field($model, 'pass')->passwordInput() ?>
				<?=$form->field($model, 'pass_repeat')->passwordInput() ?>

				<div class="form-group text-center">
						<?= Html::submitButton('Регистрация', ['class' => 'btn btn-primary', 'name' => 'register-button']) ?>
				</div>
		
				<div>
					<a href="<?=Url::to(['account/forgotpass']);?>">Забыли пароль?</a>
					<a class="pull-right" href="<?=Url::to(['account/login']);?>">Авторизация</a>
				</div>

				<?php ActiveForm::end(); ?>
			</div>
		</div>

	</div>
</div>
