<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

$this->title = ' Восстановление пароля ';
$this->registerMetaTag([
    'name' => 'description',
    'content' => 'Если вы забыли пароль от Вашей учетной записи - рекомендуем восстановить пароль.'
]);
?>

<div class="row top-buffer">
	<div class="account___view__forgot_password col-md-6 col-md-offset-3">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h1 class="panel-title">Восстановление пароля </h1>
			</div>
			<div class="panel-body">
				<?php $form = ActiveForm::begin(); ?>

				<?= $form->field($model, 'email') ?>
			
				<div class="form-group text-center">
					<?= Html::submitButton('Восстановить', ['class' => 'btn btn-primary']) ?>
				</div>
				<div>
					<a href="<?=Url::to(['account/login']);?>">Авторизация</a>
					<a class="pull-right" href="<?=Url::to(['account/register']);?>">Регистрация</a>
				</div>

				<?php ActiveForm::end(); ?>
			</div>
		</div>
	</div>
</div>
