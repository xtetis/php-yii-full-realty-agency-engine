<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

$this->title = 'Авторизация на сайте';
$this->registerMetaTag([
    'name' => 'description',
    'content' => 'Авторизируйтесь на сайте - это позволит Вам управлять созданными объявлениями и настройками учетной записи.'
]);
?>
<div class="row top-buffer">
	<div class="account___view__login col-md-6 col-md-offset-3">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h1 class="panel-title">Авторизация</h1>
			</div>
			<div class="panel-body">
				<?php $form = ActiveForm::begin(); ?>

				<?= $form->field($model, 'email') ?>
				<?= $form->field($model, 'pass')->passwordInput() ?>
				<?= $form->field($model, 'rememberMe')->checkbox() ?>
				<div class="form-group text-center">
					<?= Html::submitButton('Вход', ['class' => 'btn btn-primary']) ?>
				</div>
				<div>
					<a href="<?=Url::to(['account/forgotpass']);?>">Забыли пароль?</a>
					<a class="pull-right" href="<?=Url::to(['account/register']);?>">Регистрация</a>
				</div>

				<?php ActiveForm::end(); ?>
			</div>
		</div>
	</div>
</div>
