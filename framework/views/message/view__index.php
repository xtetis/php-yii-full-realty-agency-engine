<?

$this->title = 'Мои сообщения';

?>

<div class="row top-buffer">
	<div class="blocks___view__messages col-md-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h1 class="panel-title">Мои <?=(($command=='unreaded')?'непрочитанные':'');?> сообщения</h1>
			</div>
			<div class="panel-body" style="padding: 7px;">
				<?if ($message_list):?>
					<table class="table table-hover">
						<thead>
							<tr>
								<th>№</th>
								<th>От кого сообщение</th>
								<th>Всего сообщений</th>
								<th>Последнее сообщение</th>
								<th style="width: 100px;"></th>
							</tr>
						</thead>
						<tbody>
							<?
								$i=1;
								foreach ($message_list as $message_list_item):
									$unreaded = $message_list_item['unreaded'];
									$unreaded_class = ($unreaded?'primary':'default');
									$unreaded_btn = '<img src="/public/templates/default/img/ico/email.gif"> Новые сообщения';
									$unreaded_btn = ($unreaded?$unreaded_btn:'<span class="glyphicon glyphicon-envelope">
										</span>	Cообщений <span class="badge">'.$message_list_item['count'].'</span>');
							?>
								<tr>
									<td style="padding-left:10px;"><?=$i++;?></td>
									<td style="padding-left:10px;"><?=$message_list_item['username']?></td>
									<td style="padding-left:10px;"><?=$message_list_item['count']?></td>
									<td style="padding-left:10px;"><?=$message_list_item['latest']?></td>
									<td>
										<a class="btn btn-<?=$unreaded_class?> account_messages_opendialog"
										   title="Непрочитанный сообщений - <?=$unreaded?>"
										   style="white-space:nowrap;" 
										   id_user="<?=$message_list_item['id_other_user']?>" 
										   username="<?=htmlspecialchars($message_list_item['username'])?>">
										   <?=$unreaded_btn?>
										</a>
									</td>
								</tr>
							<?endforeach;?>
						</tbody>
					</table>
				<?else:?>
					У вас еще нет сообщений
				<?endif;?>
				<?=$pagination?>
			</div>
		</div>
	</div>
</div>





<div class="modal fade"  id="modal__dialog_user_messages" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Переписка с пользователем <span id="user_messages_other_username"></span></h4>
      </div>
      <div class="modal-body" style="padding: 5px;">
        <div>
        	<iframe id="user_messages_frame" style="width: 100%; border: 1px solid rgb(206, 206, 206); border-radius: 5px; padding: 5px; height: 300px;"></iframe>
        </div>
        <div>
        	<input type="hidden" 
        				 id="modal__dialog_user_messages__id_user"
        				 value="">
        	<textarea class="form-control" 
        	          id="modal__dialog_user_messages__message_textarea"
        						placeholder="Ваше сообщение..."></textarea>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
        <button type="button" class="btn btn-primary" 
        				id="modal__dialog_user_messages__btn_send">Отправить</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->




