<?php

namespace app\models;

use Yii;
use yii\base\Model;
use app\models\gii\xta_user;
use app\models\gii\xta_setting;

/**
 * LoginForm is the model behind the login form.
 */
class RegisterForm extends Model
{
public $email;
public $pass;
public $pass_repeat;
public $username;
public $phone;
public $skype;

	public function rules()
	{
		return [
			[['username', 'email', 'pass', 'pass_repeat','phone'], 'required','message'=>'Пожалуйста, заполните поле "{attribute}"'],
			[['username', 'email', 'phone'], 'trim'],
			[['username'], 'string', 'encoding'=>'UTF-8', 'min' => 3, 'tooShort'=>'Поле "{attribute}" не может быть короче 3 символов'],
			[['email'], 'string', 'min' => 6, 'tooShort'=>'Поле "{attribute}" не может быть короче 6 символов'],
			[['pass'], 'string', 'min' => 6, 'tooShort'=>'Поле "{attribute}" не может быть короче 6 символов'],
			['pass', 'compare', 'message'=>'Пароли должны совпадать'],
			['email', 'email', 'message'=>'Поле "{attribute}" не являетс корректным email адресом'],
			['email', 'email', 'message'=>'Поле "{attribute}" не являетс корректным email адресом'],
			['email', 'unique', 'targetClass' => 'app\models\gii\xta_user', 'targetAttribute'=>'email' , 'message'=>'Этот Email уже зарегистрирован'],
			['phone', 'string', 'min' => 6, 'tooShort'=>'Поле "{attribute}" не может быть короче 6 символов'],
			['phone', 'match', 'pattern' => '/^[0-9]{6,12}$/i','message'=>'Поле "Телефон" может содержать только цифры'],
			['skype', 'match', 'pattern' => '/^[a-z][a-z0-9\.,\-_]{5,31}$/i','message'=>'Поле "{attribute}" не являетс корректным логином Skype'],
			
		];
	}
    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            //'verifyCode' => 'Verification Code',
            'username'=>'Ваше имя',
            'email'=>'Email',
            'phone'=>'Телефон',
            'skype'=>'Skype',
            'pass'=>'Пароль',
            'pass_repeat'=>'Повтор пароля',
        ];
    }
    

	public function register()
	{
		if ($this->validate())
		{
			$model_xta_user = new xta_user();
			$accesstoken = md5(md5($this->email).md5($this->pass));
			$model_xta_user->email = $this->email;
			$model_xta_user->pass = md5(md5($this->pass));
			$model_xta_user->username = $this->username;
			$model_xta_user->phone = $this->phone;
			$model_xta_user->id_user_role = 2;
			$model_xta_user->skype = $this->skype;
			$model_xta_user->accesstoken = $accesstoken;
			$model_xta_user->authkey = '';
			
			if ($model_xta_user->validate())
			{
				$model_xta_user->save();
				$this->fn__send_email_create_user();
        return true;
			}
		}
	return false;
	}
	
	public function fn__send_email_create_user()
	{
		$tpl = "Здравствуйте +username+.
						Вы успешно зарегистрированы на сайте http://+http_host+/

						Для авторизации на сайте используйте следующие данные:
						Email: +email+
						Пароль: +password+";
		$tpl = str_replace('+http_host+',$_SERVER['HTTP_HOST'],$tpl);
		$tpl = str_replace('+email+',$this->email,$tpl);
		$tpl = str_replace('+pass+',$this->pass,$tpl);
		$tpl = str_replace('+username+',$this->username,$tpl);
		
		\Yii::$app->mailer->compose()->setTo($this->email)
		->setFrom(['noreply@'.$_SERVER['HTTP_HOST'] => 'Сайт '.$_SERVER['HTTP_HOST']])
		->setSubject('Благодарим за регистрацию на сайте '.$_SERVER['HTTP_HOST'])
		->setTextBody($tpl)
		->send();




		$tpl = "<div>На сайте +http_host+ создан пользователь.</div> 
						<br><br>
						<div>Пользователь: +username+,</div> 
						<div>Пароль:  +pass+,</div> 
						<div>email: +email+</div>";
						
		$tpl = str_replace('+http_host+',$_SERVER['HTTP_HOST'],$tpl);
		$tpl = str_replace('+email+',$this->email,$tpl);
		$tpl = str_replace('+pass+',$this->pass,$tpl);
		$tpl = str_replace('+username+',$this->username,$tpl);
		
		\Yii::$app->mailer->compose()->setTo(\Yii::$app->params['adminEmail'])
		->setFrom(['noreply@'.$_SERVER['HTTP_HOST'] => 'Сайт '.$_SERVER['HTTP_HOST']])
		->setSubject('На сайте '.$_SERVER['HTTP_HOST'].' создан пользователь')
		->setTextBody($tpl)
		->send();
		
	}
	
}
