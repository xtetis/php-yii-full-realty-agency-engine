<?php

namespace app\models\mgii;

use Yii;
use app\models\gii\xta_love_anketa;
use app\models\mgii\mgii_xta_love_type_account;
use app\models\mgii\mgii_xta_love_anketa_visit;
use app\models\mgii\mgii_xta_user;
use app\models\LibCommon;



class mgii_xta_love_anketa extends xta_love_anketa
{
  public function rules()
  {
    return [
        [['id_user',
          'status',
          'about_me',
          'search_about',
          'search_age_start',
          'search_age_end',
          'family_status',
          'child_status',
          'birthdate',
          'weight',
          'height',
          'physique',
          'smoking',
          'alcohol',
          'material_support',
          ], 'required','message'=>'Пожалуйста, заполните поле "{attribute}"'],
          
        [['status',
          'about_me',
          'search_about',
          'search_age_start',
          'search_age_end',
          'family_status',
          'child_status',
          'birthdate'], 'trim'],
          
        [['id_user',
          'id_love_type_account',
          'id_search_love_type_account',
          'id_image',
          'search_age_start',
          'search_age_end',
          'family_status',
          'child_status'], 'integer'],
          
        [['about_me', 'search_about'], 'string', 'encoding'=>'UTF-8', 'min' => 10, 'tooShort'=>'Поле "{attribute}" не может быть короче 10 символа'],
        
        [['birthdate','createdon','publushedon'], 'safe'],
        
        [['birthdate'], 'date','message'=>'Неверный формат даты в поле "{attribute}" (должен быть 1999-10-30)', 'format' => 'php:Y-m-d'],
//        [['birthdate'], 'date','message'=>'Неверный формат даты в поле "{attribute}" (должен быть 30/10/1999)', 'format' => 'php:d/m/Y'],
        
        [['status'], 'string', 'max' => 100],
        
        [['search_age_start','search_age_end'],'in', 'range' => range(1,99)],
        
        ['search_age_start', 'compare', 'compareAttribute' => 'search_age_end', 'operator' => '<=', 'enableClientValidation' => false,'message'=>'"От" должно быть больше чем "До"'],
    ];
  }
  

  public function attributeLabels()
  {
    return [
      'id' => 'ID',
      'id_user' => 'Ссылка на пользователя',
      'id_love_type_account' => 'Тип анкеты',
      'id_search_love_type_account' => 'Кого ищете',
      'id_image' => 'Ссылка на заглавное изображение',
      'status' => 'Мой статус',
      'about_me' => 'Немного о себе',
      'search_about' => 'О том, кого ищу',
      'search_age_start' => 'Какого возраста ищете',
      'search_age_end' => 'Какого возраста ищете',
      'family_status' => 'Семейное положение',
      'child_status' => 'Дети',
      'birthdate' => 'Дата рождения',
      'weight' => 'Вес, кг',
      'height' => 'Рост, см',
      'physique' => 'Телосложение',
      'smoking' => 'Отношение к курению',
      'alcohol' => 'Отношение к алкоголю',
      'material_support' => 'Материальная поддержка',
    ];
  }
  
  
  
  public function fn__get_family_status_list($search = false)
  {
		if ($search)
		{
		  return [
		    0 => 'Любое',
		    1 => 'Не женат (не замужем)',
		    2 => 'Женат (замужем)',
		    3 => 'Разведен (разведена)',
		    4 => 'Вдовец (вдова)',
		  ];
		}
		else
		{
		  return [
		    0 => 'Не указано',
		    1 => 'Не женат (не замужем)',
		    2 => 'Женат (замужем)',
		    3 => 'Разведен (разведена)',
		    4 => 'Вдовец (вдова)',
		  ];
		}

  }
  
  
  public function fn__get_child_status_list()
  {
    return [
      0 => 'Не указано',
      1 => 'Есть',
      2 => 'Нет',
    ];
  }
  

  public function fn__get_physique_list()
  {
    return [
      0 => 'Не указано',
      1 => 'Спортивное',
      2 => 'Плотное',
    ];
  }
  

  public function fn__get_smoking_list()
  {
    return [
      0 => 'Не указано',
      1 => 'Курю',
      2 => 'Не курю',
      3 => 'От случая к случаю',
    ];
  }
  
  
  public function fn__get_alcohol_list()
  {
    return [
      0 => 'Не указано',
      1 => 'Выпиваю',
      2 => 'Не употребляю',
      3 => 'От случая к случаю',
    ];
  }


  public function fn__get_material_support_list()
  {
    return [
      0 => 'Не указано',
      1 => 'Ищу материальную поддержку',
      2 => 'Готов стать спонсором',
      3 => 'Не нуждаюсь в спонсоре и не хочу им быть',
    ];
  }



    public function fn__get_love_type_account()
    {
        return $this->hasOne(mgii_xta_love_type_account::className(), ['id' => 'id_love_type_account']);
    }




	public function fn__get_anketa_main_photo_src()
	{
		$model__xta_image = mgii_xta_image::findOne($this->id_image);
		if ($model__xta_image)
		{
			return $model__xta_image->fn__get_src();
		}
		else
		{
			return '/public/templates/default/img/ico/user-photo.png';
		}
	}
	
	
	public function fn__get_age($getname = true)
	{
		$date = $this->birthdate;
		$date = explode('-',$date);
		$age = LibCommon::fn__getAge($date[0], $date[1], $date[2]);
		
		if ($getname)
		{
			$age = $age.' '.LibCommon::fn__num2word($age, array('год', 'года', 'лет'));
		}
		return $age;
	}


	
	public function fn__get_zodiac()
	{
		$date = $this->birthdate;
		$date = explode('-',$date);
		$zodiac = LibCommon::fn__get_zodiac($date[1], $date[2]);
		return $zodiac;
	}



	public function fn__is_owner()
	{
		$owner = false;
		if (
					(!Yii::$app->user->isGuest) &&
					(Yii::$app->user->id == $this->id_user)
			 )
		{
			$owner = true;
		}
		return $owner;
	}










	public function fn__get_name()
	{
		$model__xta_user = mgii_xta_user::findOne($this->id_user);
		if ($model__xta_user)
		{
			return $model__xta_user->username;
		}
		else
		{
			return false;
		}
		
	}
















	public function fn__set_visit()
	{
		if (!\Yii::$app->user->isGuest)
		{
			$model__xta_love_anketa__visitor = mgii_xta_love_anketa::find()->where(["id_user" =>Yii::$app->user->id])->one();
			if ($model__xta_love_anketa__visitor->id !=$this->id)
			{
				$model__xta_love_anketa_visit = mgii_xta_love_anketa_visit::find()->where([
						"id_anketa_owner"   => $this->id,
						"id_anketa_visitor" => $model__xta_love_anketa__visitor->id,
					])->one();
				if (!$model__xta_love_anketa_visit)
				{
					$model__xta_love_anketa_visit = new mgii_xta_love_anketa_visit();
					$model__xta_love_anketa_visit->id_anketa_owner   = $this->id;
					$model__xta_love_anketa_visit->id_anketa_visitor = $model__xta_love_anketa__visitor->id;
				}
				$model__xta_love_anketa_visit->timestamp = new \yii\db\Expression('NOW()');
				$model__xta_love_anketa_visit->readed = 0;
				$model__xta_love_anketa_visit->save();
			}
		}
	}

}
