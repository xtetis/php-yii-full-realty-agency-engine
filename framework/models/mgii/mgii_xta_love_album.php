<?php

namespace app\models\mgii;

use Yii;
use app\models\gii\xta_love_album;
use app\models\mgii\mgii_xta_user;



class mgii_xta_love_album extends xta_love_album
{
  public function rules()
  {
        return [
            [['id_user', 'id_album', 'hidden'], 'integer'],
            [['hidden'],'in', 'range' => [0,1]],
            [['name'], 'string', 'max' => 50],
            [['name'], 'string', 'encoding'=>'UTF-8', 'min' => 3, 'tooShort'=>'Поле "{attribute}" не может быть короче 10 символа'],
            [['id_user', 'id_album','name'], 'required','message'=>'Поле "{attribute}" обязательное'],
        ];
  }
  

  public function attributeLabels()
  {
        return [
            'id' => 'ID',
            'id_user' => 'Ссылка на пользователя',
            'id_album' => 'Ссылка на альбом',
            'hidden' => 'Скрытый ли альбом',
            'name' => 'Название альбома',
        ];
  }
  
  

	public function fn__get_xta_user()
	{
		return $this->hasOne(mgii_xta_user::className(), ['id' => 'id_user']);
	}

	public function fn__get_xta_album()
	{
		return $this->hasOne(mgii_xta_album::className(), ['id' => 'id_album']);
	}

}
