<?php

namespace app\models\mgii;


use Yii;
use app\models\gii\xta_love_type_account;
use yii\helpers\ArrayHelper;
use app\models\gii\xta_padeg;
/**
 * This is the model class for table "{{%love_type_account}}".
 *
 * @property integer $id
 * @property string $name
 */
class mgii_xta_love_type_account extends xta_love_type_account
{

	public function fn__get_data_list($alternate=false,$padeg=0)
	{
		if (!$alternate)
		{
			$models = mgii_xta_love_type_account::find()->asArray()->all();
			return ArrayHelper::map($models, 'id', 'name');
		}
		else
		{
			$models = mgii_xta_love_type_account::find()->asArray()->all();
			$arrmap = ArrayHelper::map($models, 'id', 'alternate');
			if ($padeg)
			{
				foreach ($arrmap as &$value)
				{
					$value = xta_padeg::fn__get_padeg($value,$padeg);
				}

			}
			return $arrmap;
		}

	}

}
