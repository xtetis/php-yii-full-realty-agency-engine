<?php

namespace app\models;

use Yii;
use yii\base\Model;
use app\models\gii\xta_user;
use app\models\gii\xta_setting;

/**
 * LoginForm is the model behind the login form.
 */
class SettingsForm extends Model
{
public $username;
public $phone;
public $skype;

	public function rules()
	{
		return [
			[['username', 'phone'], 'required','message'=>'Пожалуйста, заполните поле "{attribute}"'],
			[['username', 'phone'], 'trim'],
			[['username'], 'string', 'encoding'=>'UTF-8', 'min' => 3, 'tooShort'=>'Поле "{attribute}" не может быть короче 3 символов'],
			['phone', 'string', 'min' => 6, 'tooShort'=>'Поле "{attribute}" не может быть короче 6 символов'],
			['phone', 'match', 'pattern' => '/^[0-9]{6,12}$/i','message'=>'Поле "Телефон" может содержать только цифры'],
			['skype', 'match', 'pattern' => '/^[a-z][a-z0-9\.,\-_]{5,31}$/i','message'=>'Поле "{attribute}" не являетс корректным логином Skype'],
			
		];
	}

	public function attributeLabels()
	{
		return [
			'username'=>'Ваше имя',
			'phone'=>'Телефон',
			'skype'=>'Skype',
		];
	}


	public function saveSettings()
	{
		if ($this->validate())
		{
			if (($model__xta_user = xta_user::findOne(Yii::$app->user->id)) !== null) 
			{
				$model__xta_user->username = $this->username;
				$model__xta_user->phone = $this->phone;
				$model__xta_user->skype = $this->skype;
				$model__xta_user->save();
				return true;
			}
		}
		return false;
	}
}
