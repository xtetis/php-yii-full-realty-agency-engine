<?php

namespace app\models\gii;

use Yii;

/**
 * This is the model class for table "{{%cache_tag}}".
 *
 * @property integer $id
 * @property string $name
 */
class xta_cache_tag extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%cache_tag}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя тега кеша',
        ];
    }
}
