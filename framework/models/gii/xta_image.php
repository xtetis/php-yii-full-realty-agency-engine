<?php

namespace app\models\gii;

use Yii;
use app\models\gii\xta_album;
use yii\imagine\Image;
use Imagine\Image\Box;
/**
 * This is the model class for table "{{%image}}".
 *
 * @property integer $id
 * @property integer $id_album
 * @property string $md5
 *
 * @property Album $idAlbum
 */
class xta_image extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%image}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_album'], 'required'],
            [['id_album'], 'integer'],
            [['md5'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_album' => 'Ссылка на альбом, в котором находится изображение',
            'md5' => 'Путь к картинке',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getxta_album()
    {
        return $this->hasOne(xta_album::className(), ['id' => 'id_album']);
    }










//******************************************************************************
function fn__save_base64_to_img($encodedData=''){

	$evil_parts = array('eval(','<?','?>','REQUEST','_GET','_POST','Photoshop','WEBPVP8','RIFF');
	$evil_parts_decode = array('Photoshop','photoshop','Adobe','WEBPVP8','RIFF');
	
	foreach ($evil_parts as $item)
	{
		if (strpos($encodedData,$item)!==false)
		{
			return false;
		}
	}

	$data = explode(',', $encodedData);
	if (count($data)<2)
	{
		return false;
	}

	$encodedData = str_replace(' ','+',$data[1]);
	$decodedData = base64_decode($encodedData);
	
	foreach ($evil_parts as $item)
	{
		if (strpos($decodedData_decode,$item)!==false)
		{
			return false;
		}
	}

	$this->md5 = md5(md5($encodedData).uniqid());
	$id_albim_path = floor($this->id_album/1000).'/'.$this->id_album;
	$uploaddir=$_SERVER['DOCUMENT_ROOT'].'/public/images/album/'.$id_albim_path.'/';
	if ((!file_exists($uploaddir))&&(!mkdir($uploaddir,0777,true)))
	{
		return false;
	}

	$filename = $uploaddir.$this->md5.'.jpg';

	if (!file_exists($filename))
	{
		file_put_contents($filename, $decodedData);
	}
}
//******************************************************************************












//******************************************************************************
function fn__get_base64_img()
{
	$id_albim_path = floor($this->id_album/1000).'/'.$this->id_album;
	$uploaddir=$_SERVER['DOCUMENT_ROOT'].'/public/images/album/'.$id_albim_path.'/';
	$filename = $uploaddir.$this->md5.'.jpg';

	if (file_exists($filename))
	{
		$filedata = file_get_contents($filename);
		return 'data:image/png;base64,'.base64_encode($filedata);
	}
	
	$filename = $uploaddir.$this->md5.'.png';
	if (file_exists($filename))
	{
		$filedata = file_get_contents($filename);
		return 'data:image/png;base64,'.base64_encode($filedata);
	}

	return false;
}
//******************************************************************************




















//******************************************************************************
	function fn__get_src()
	{
		$id_albim_path = floor($this->id_album/1000).'/'.$this->id_album;
		$full_path=$_SERVER['DOCUMENT_ROOT'].'/public/images/album/'.$id_albim_path.'/'.$this->md5.'.png';
		$full_path_jpg=$_SERVER['DOCUMENT_ROOT'].'/public/images/album/'.$id_albim_path.'/'.$this->md5.'.jpg';
		
		if (file_exists($full_path_jpg))
		{
			if (filesize($full_path_jpg)>400000)
			{
				xta_image::fn__correct_img_size($full_path_jpg);
			}
			if (file_exists($full_path))
			{
				unlink($full_path);
			}
			return '/public/images/album/'.$id_albim_path.'/'.$this->md5.'.jpg';
		}
		elseif (file_exists($full_path))
		{
			Image::getImagine()->open($full_path)->save($full_path_jpg, ['quality' => 80]);;
			return '/public/images/album/'.$id_albim_path.'/'.$this->md5.'.png';
		}
		else
		{
			return '/public/images/other/nophoto.png';
		}
	}
//******************************************************************************







//******************************************************************************
	function fn__get_image_full_path()
	{
		$id_albim_path = floor($this->id_album/1000).'/'.$this->id_album;
		$full_path=$_SERVER['DOCUMENT_ROOT'].'/public/images/album/'.$id_albim_path.'/'.$this->md5.'.png';
		$full_path_jpg=$_SERVER['DOCUMENT_ROOT'].'/public/images/album/'.$id_albim_path.'/'.$this->md5.'.jpg';
		if (file_exists($full_path))
		{
			return $full_path;
		}
		elseif(file_exists($full_path_jpg))
		{
			return $full_path_jpg;
		}
		else
		{
			return false;
		}
	}
//******************************************************************************









//******************************************************************************
	function fn__get_thumb_src($size='medium')
	{
		if (!in_array($size,array('big','medium','small')))
		{
			return $this->fn__get_src();
		}




		$original_src = $this->fn__get_src();
		if ($original_src=='/public/images/other/nophoto.png')
		{
			return $original_src;
		}


		$full_path= $_SERVER['DOCUMENT_ROOT'].$original_src;
		


		$thumb_full_path = $_SERVER['DOCUMENT_ROOT'].'/public/images/thumbs/'.$this->md5.'_'.$size.'.jpg';
		$thumb_rel_path = '/public/images/thumbs/'.$this->md5.'_'.$size.'.jpg';

		if (file_exists($thumb_full_path))
		{
			return $thumb_rel_path;
		}
		

		if (filesize($full_path)>500000)
		{
			return $original_src;
		}
		$watermark_full_path = $_SERVER['DOCUMENT_ROOT'].'/public/images/other/krgazeta.png';

		if ($size=='big')
		{
			//@Image::thumbnail($full_path, 700, 400)->save($thumb_full_path, ['quality' => 50]);
			@xta_image::fn__makeThumbnail($full_path, $thumb_full_path, 700, 700, 70);
			
			$img = Image::getImagine()->open($thumb_full_path);
			$size = $img->getSize();
			$width = $size->getWidth();
			$height = $size->getHeight();

			if (($width>=180) && ($height>=70))
			{
				$cw = ceil($width/2)-90;
				$ch = ceil($height/2)-35;
				$start = [$cw,$ch];
				//@Image::watermark($thumb_full_path, $watermark_full_path,$start)->save($thumb_full_path);
			}
			return $thumb_rel_path;
		}
		elseif ($size=='medium')
		{
			//@Image::thumbnail($full_path, 400, 300)->save($thumb_full_path, ['quality' => 50]);
			@xta_image::fn__makeThumbnail($full_path, $thumb_full_path, 400, 400, 50);

			if (($width>=180) && ($height>=70))
			{
				$cw = ceil($width/2)-90;
				$ch = ceil($height/2)-35;
				$start = [$cw,$ch];
				@Image::watermark($thumb_full_path, $watermark_full_path,$start)->save($thumb_full_path);
			}
			return $thumb_rel_path;
			//Image::watermark($thumb_full_path, $watermark_full_path)->save($thumb_full_path, ['quality' => 50]);
		}
		else
		{
			//@Image::thumbnail($full_path, 60, 60)->save($thumb_full_path, ['quality' => 50]);
			@xta_image::fn__makeThumbnail($full_path, $thumb_full_path, 60, 60, 50);
      return $thumb_rel_path;
			//Image::watermark($thumb_full_path, $watermark_full_path)->save($thumb_full_path, ['quality' => 50]);
		}



		return $original_src;
	}
//******************************************************************************















//******************************************************************************
	function fn__get_nosrc()
	{
		return '/public/images/other/nophoto.png';
	}
//******************************************************************************

















//******************************************************************************
	function fn__correct_img_size($filename)
	{
		if (file_exists($filename))
		{
			xta_image::fn__makeThumbnail($filename, $filename, 800, 800, 75);
			/*
			$original = Image::getImagine()->open($filename);
			$originalSize = $original->getSize();
			$width = $originalSize->getWidth();
			$height = $originalSize->getHeight();

			if (($width>800) || ($height>800))
			{
				$new_height = $height;
				$new_width = $width;
				if ($new_height>800)
				{
			
					$koeff = 800/$height;
					$new_width = $new_width*$koeff;
					$new_height = 800;
				}
		
				if ($new_width>800)
				{
					$koeff = 800/$new_width;
					$new_height = $new_height*$koeff;
					$new_width = 800;
				}
				$newSizeThumb = new Box($new_width, $new_height);
				$original->resize($newSizeThumb)->save($filename, ['quality' => 80]);
			}
			*/
		}
	}
//******************************************************************************






//******************************************************************************
function fn__makeThumbnail($sourcefile, $endfile, $thumbwidth, $thumbheight, $quality)
{
	// Takes the sourcefile (path/to/image.jpg) and makes a thumbnail from it
	// and places it at endfile (path/to/thumb.jpg).



	if(exif_imagetype($sourcefile) != IMAGETYPE_JPEG){
		Image::getImagine()->open($sourcefile)->save($sourcefile, ['quality' => 70]);;
	}
	
	
	
	// Load image and get image size.
	$img = imagecreatefromjpeg($sourcefile);
	$width = imagesx( $img );
	$height = imagesy( $img );

	if ($width > $height) {
		  $newwidth = $thumbwidth;
		  $divisor = $width / $thumbwidth;
		  $newheight = floor( $height / $divisor);
	}
	else {
		  $newheight = $thumbheight;
		  $divisor = $height / $thumbheight;
		  $newwidth = floor( $width / $divisor );
	}

	// Create a new temporary image.
	$tmpimg = imagecreatetruecolor( $newwidth, $newheight );

	// Copy and resize old image into new image.
	imagecopyresampled( $tmpimg, $img, 0, 0, 0, 0, $newwidth, $newheight, $width, $height );

	// Save thumbnail into a file.
	imagejpeg( $tmpimg, $endfile, $quality);

	// release the memory
	imagedestroy($tmpimg);
	imagedestroy($img);
}
//******************************************************************************





//******************************************************************************
	function fn__delete_image()
	{
		if (file_exists($this->fn__get_image_full_path()))
		{
			unlink($this->fn__get_image_full_path());
		}

		$this->delete();
	}
//******************************************************************************

















//******************************************************************************
	function fn__move_file_to_album($filename,$id_album,$id_in_base=0)
	{

		if (!file_exists($filename))
		{
			return false;
		}
		//echo $id_album;

		$model__xta_album = xta_album::findOne($id_album);
		if (!$model__xta_album)
		{
			return false;
		}

		$md5 = md5($filename);

		$model_xta_image = new xta_image();
		$model_xta_image->id_album = $id_album;
		if ($id_in_base)
		{
			$model_xta_image->id = $id_in_base;
		}
		$model_xta_image->md5 = $md5;

		$id_albim_path = floor($id_album/1000).'/'.$id_album;
		$uploaddir=$_SERVER['DOCUMENT_ROOT'].'/public/images/album/'.$id_albim_path.'/';
		$new_filename = $uploaddir.$md5.'.jpg';
		if (!file_exists($new_filename))
		{
			if ((!file_exists($uploaddir))&&(!mkdir($uploaddir,0777,true)))
			{
				return false;
			}
			chmod($uploaddir,0777);
			chmod($_SERVER['DOCUMENT_ROOT'].'/public/images/album/'.floor($id_album/1000),0777);
			@file_put_contents($new_filename, @file_get_contents($filename));
		}

		$model_xta_image->save();

		//if ($model__xta_album->id_image==0)
		//{
			$model__xta_album->id_image = $model_xta_image->id;
			$model__xta_album->save();
		//}
		unset($model_xta_image);
		unset($model__xta_album);
	}
//******************************************************************************


}
