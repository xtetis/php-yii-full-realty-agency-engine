<?php

namespace app\models\gii;

use Yii;

/**
 * This is the model class for table "{{%obj_option_value}}".
 *
 * @property integer $id
 * @property integer $id_obj
 * @property integer $id_obj_option
 * @property string $value
 *
 * @property Obj $idObj
 * @property ObjOption $idObjOption
 */
class xta_obj_option_value extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%obj_option_value}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_obj', 'id_obj_option', 'value'], 'required'],
            [['id_obj', 'id_obj_option'], 'integer'],
            [['value'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_obj' => 'Ссылка на объявление',
            'id_obj_option' => 'Ссылка на опцию объявления',
            'value' => 'Значение',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdObj()
    {
        return $this->hasOne(Obj::className(), ['id' => 'id_obj']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdObjOption()
    {
        return $this->hasOne(ObjOption::className(), ['id' => 'id_obj_option']);
    }
}
