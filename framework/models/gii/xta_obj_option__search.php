<?php

namespace app\models\gii;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\gii\xta_obj_option;

/**
 * xta_obj_option__search represents the model behind the search form about `app\models\gii\xta_obj_option`.
 */
class xta_obj_option__search extends xta_obj_option
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'option_type','filter_type'], 'integer'],
            [['name', 'description', 'introtext'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = xta_obj_option::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'option_type' => $this->option_type,
            'filter_type' => $this->filter_type,
            
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'introtext', $this->introtext]);

        return $dataProvider;
    }
}
