<?php

namespace app\models\gii;

use Yii;
use app\models\gii\xta_obj_category;
use app\models\gii\xta_obj_option_available;

/**
 * This is the model class for table "{{%obj_option}}".
 *
 * @property integer $id
 * @property integer $option_type
 * @property string $name
 * @property string $description
 * @property string $introtext
 *
 * @property ObjOptionAvailable[] $objOptionAvailables
 * @property ObjOptionInCategory[] $objOptionInCategories
 * @property ObjOptionValue[] $objOptionValues
 */
class xta_obj_option extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%obj_option}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['option_type','filter_type'], 'integer'],
            [['name', 'description'], 'string', 'max' => 200],
            [['introtext'], 'string', 'max' => 300],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'          => 'ID',
            'option_type' => 'Тип элемента ввода',
            'name'        => 'Название опции',
            'description' => 'Краткое описание опции',
            'introtext'   => 'Подробное описание',
            'introtext'   => 'Подробное описание',
            'filter_type' => 'Тип фильтра',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getObjOptionAvailables()
    {
        return $this->hasMany(ObjOptionAvailable::className(), ['id_obj_option' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getObjOptionInCategories()
    {
        return $this->hasMany(ObjOptionInCategory::className(), ['id_obj_option' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getObjOptionValues()
    {
        return $this->hasMany(ObjOptionValue::className(), ['id_obj_option' => 'id']);
    }
















	public function fn__get_category_option_inputs($id)
	{
		$id = intval($id);
		if ($id)
		{
			$model__xta_obj_category = xta_obj_category::find()->where(["id" => $id])->one();
			if (!$model__xta_obj_category)
			{
				return '';
			}
		}
		else
		{
			return '';
		}

		$ret='';
		$xta_obj_option_many = xta_obj_option::find()->where("
			 id IN (
				SELECT `id_obj_option` 
				FROM `xta_obj_option_in_category` 
				WHERE `id_obj_category` = ".$id." AND `use_filter` = 1
				)
			 ")->all();
		
		foreach ($xta_obj_option_many as $item)
		{
			if ($item->option_type==1)
			{
				$value='';
				if (
				    (isset($_POST['BoardAddForm']['option_'.$item->id]))&&
				    (intval($_POST['BoardAddForm']['option_'.$item->id]))
				   )
				{
					$value = $_POST['BoardAddForm']['option_'.$item->id];
					$value = intval($value);
				}
				$ret.='
					<div class="form-group field-boardaddform-option_'.$item->id.' required">
						<div class="row">
							<div class="col-md-3 text-right">
								<label class="control-label" for="boardaddform-name">'.$item->name.'</label>
							</div>
							<div
								class="col-md-6" 
								data-toggle="popover" 
								data-title="'.$item->description.'" 
								data-content="'.$item->introtext.'">
								<input 
								type="text" 
								id="option_'.$item->id.'" 
								value="'.$value.'"
								class="form-control intval" 
								name="BoardAddForm[option_'.$item->id.']"> 
								<div class="help-block"></div>
							</div>
						</div>
					</div>';

			}
			elseif($item->option_type==0)
			{
				$values = array();
				$opts='';
				$values = xta_obj_option_available::find()->where("id_obj_option = ".$item->id)->all();
				if (($values)&&(is_array($values)))
				{
					$has_hroups = false;
					foreach ($values as $values_item)
					{
						if ($values_item->id_parent)
						{
							$has_hroups = true;
						}
					}
					
					$opts='<option value="0">Не указано</option>';
					if (!$has_hroups)
					{
						foreach ($values as $values_item)
						{
							$selected = -1;
							
							if (
									(isset($_POST['BoardAddForm']['option_'.$item->id]))&&
									(intval($_POST['BoardAddForm']['option_'.$item->id]))
								 )
							{
								$selected = intval($_POST['BoardAddForm']['option_'.$item->id]);
								$selected_str='';
								if ($selected == $values_item->id)
								{
									$selected_str='selected="selected"';
								}
							}
							$opts.='<option value="'.$values_item->id.'" '.$selected_str.'>'.$values_item->name.'</option>';
						}
					}
					else
					{
						foreach ($values as $values_item)
						{
							if (!$values_item->id_parent)
							{
								$opts.='<optgroup label="'.$values_item->name.'">';
								foreach ($values as $values_item_sub)
								{
									if ($values_item->id==$values_item_sub->id_parent)
									{
										$selected = -1;
										if (
												(isset($_POST['BoardAddForm']['option_'.$item->id]))&&
												(intval($_POST['BoardAddForm']['option_'.$item->id]))
											 )
										{
											$selected = intval($_POST['BoardAddForm']['option_'.$item->id]);
											$selected_str='';
											if ($selected == $values_item->id)
											{
												$selected_str='selected="selected"';
											}
										}
										$opts.='<option value="'.$values_item_sub->id.'" '.$selected_str.'>'.$values_item_sub->name.'</option>';
									}
								}
								$opts.='</optgroup>';
							}
						}
					}
				}

					
					$ret.='
		<div class="form-group field-boardaddform-option_'.$item->id.' required">
			<div class="row">
				<div class="col-md-3 text-right">
					<label class="control-label" for="boardaddform-id_district">'.$item->name.'</label>
				</div>
				<div 
					class="col-md-6" 
					data-toggle="popover" 
					data-title="'.$item->description.'" 
					data-content="'.$item->introtext.'">
					<select 
						id="boardaddform-option_'.$item->id.'" 
						class="form-control" 
						name="BoardAddForm[option_'.$item->id.']">
						'.$opts.'
					</select> 
					<div class="help-block"></div>
				</div>
			</div>
		</div>';
			}
		}
		return $ret;
	}















}
