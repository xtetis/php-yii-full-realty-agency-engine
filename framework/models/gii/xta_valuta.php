<?php

namespace app\models\gii;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%valuta}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $code
 * @property string $shortname
 * @property double $value
 *
 * @property Obj[] $objs
 */
class xta_valuta extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%valuta}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['value'], 'number'],
            [['name'], 'string', 'max' => 100],
            [['code', 'shortname'], 'string', 'max' => 10]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название валюты',
            'code' => 'Код Должна соответствовать стандарту ISO code',
            'shortname' => 'Символ слева',
            'value' => 'Значение: Установите на 1.00000 если это валюта по умолчанию.',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getObjs()
    {
        return $this->hasMany(Obj::className(), ['id_valuta' => 'id']);
    }


	public function fn__get_data_list()
	{
		$models = xta_valuta::find()->asArray()->all();
		return ArrayHelper::map($models, 'id', 'name');
	}
}
