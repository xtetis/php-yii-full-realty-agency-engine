<?php

namespace app\models\gii;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%setting_category}}".
 *
 * @property integer $id
 * @property string $name
 *
 * @property Setting[] $settings
 */
class xta_setting_category extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%setting_category}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required','message'=>'Пожалуйста, заполните поле "{attribute}"'],
            [['name'], 'string', 'max' => 100],
            [['name'], 'string', 'min' => 3],

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя категории',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSettings()
    {
        return $this->hasMany(Setting::className(), ['id_setting_category' => 'id']);
    }

	public function getSettingCategoryList() 
	{
		$models = xta_setting_category::find()->asArray()->all();
		return ArrayHelper::map($models, 'id', 'name');
	}
}
