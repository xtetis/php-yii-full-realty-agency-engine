<?php

namespace app\models\gii;

use Yii;

/**
 * This is the model class for table "{{%love_anketa}}".
 *
 * @property integer $id
 * @property integer $id_user
 * @property integer $id_love_type_account
 * @property integer $id_search_love_type_account
 * @property integer $id_image
 * @property string $status
 * @property string $about_me
 * @property string $search_about
 * @property integer $search_age_start
 * @property integer $search_age_end
 * @property integer $family_status
 * @property integer $child_status
 * @property string $birthdate
 * @property integer $weight
 * @property integer $height
 * @property integer $physique
 * @property integer $smoking
 * @property integer $alcohol
 * @property integer $material_support
 * @property string $createdon
 * @property string $publushedon
 *
 * @property User $idUser
 * @property LoveTypeAccount $idLoveTypeAccount
 */
class xta_love_anketa extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%love_anketa}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_user', 'about_me', 'search_about', 'child_status'], 'required'],
            [['id_user', 'id_love_type_account', 'id_search_love_type_account', 'id_image', 'search_age_start', 'search_age_end', 'family_status', 'child_status', 'weight', 'height', 'physique', 'smoking', 'alcohol', 'material_support'], 'integer'],
            [['about_me', 'search_about'], 'string'],
            [['birthdate', 'createdon', 'publushedon'], 'safe'],
            [['status'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_user' => 'Ссылка на пользователя',
            'id_love_type_account' => 'Ссылка на тип аккаунта',
            'id_search_love_type_account' => 'Тип анкеты, кторую ищешь',
            'id_image' => 'Ссылка на заглавное изображение',
            'status' => 'Статус',
            'about_me' => 'Обо мне',
            'search_about' => 'О том, кого ищу',
            'search_age_start' => 'Какого возраста ищете',
            'search_age_end' => 'Какого возраста ищете',
            'family_status' => 'Семейное положение',
            'child_status' => 'Child Status',
            'birthdate' => 'Дата рождения',
            'weight' => 'Вес',
            'height' => 'Рост',
            'physique' => 'Телосложение',
            'smoking' => 'Отношение к курению',
            'alcohol' => 'Отношение к алкоголю',
            'material_support' => 'Материальная поддержка',
            'createdon' => 'Дата создания анкеты',
            'publushedon' => 'Дата публикации анкеты',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdUser()
    {
        return $this->hasOne(User::className(), ['id' => 'id_user']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdLoveTypeAccount()
    {
        return $this->hasOne(LoveTypeAccount::className(), ['id' => 'id_love_type_account']);
    }
}
