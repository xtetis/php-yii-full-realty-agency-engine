<?php

namespace app\models\gii;
use yii\helpers\ArrayHelper;

use Yii;
use app\models\gii\xta_user_role;


/**
 * This is the model class for table "{{%user}}".
 *
 * @property integer $id
 * @property integer $id_district
 * @property integer $id_user_role
 * @property string $email
 * @property string $pass
 * @property string $username
 * @property string $phone
 * @property string $skype
 * @property string $authkey
 * @property string $accesstoken
 *
 * @property Obj[] $objs
 */
class xta_user extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_district', 'id_user_role'], 'integer'],
            [['email', 'pass', 'username', 'phone', 'skype', 'authkey', 'accesstoken'], 'string', 'max' => 200]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_district' => 'Район',
            'id_user_role' => 'Роль',
            'email' => 'Email',
            'pass' => 'Пароль',
            'username' => ' Контактное лицо ',
            'phone' => ' Номер телефона ',
            'skype' => 'Skype',
            'authkey' => 'Хеш, который устанавлявается при авторизации',
            'accesstoken' => 'Accesstoken',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getObjs()
    {
        return $this->hasMany(Obj::className(), ['id_user' => 'id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getObjAbuses()
    {
        return $this->hasMany(ObjAbuse::className(), ['id_user' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getxta_user_role()
    {
        return $this->hasOne(xta_user_role::className(), ['id' => 'id_user_role']);
    }


	public function fn__get_xta_user_list()
	{
		$models = xta_user::find()->asArray()->all();
		return ArrayHelper::map($models, 'id', 'email');
	}
}
