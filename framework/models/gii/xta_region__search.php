<?php

namespace app\models\gii;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\gii\xta_region;
use app\models\gii\xta_country;

/**
 * xta_region__search represents the model behind the search form about `app\models\gii\xta_region`.
 */
class xta_region__search extends xta_region
{
	public $country;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'id_country'], 'integer'],
            [['name'], 'safe'],
            [['country'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = xta_region::find();
				$query->joinWith('xta_country');
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);


        $dataProvider->sort->attributes['country'] = [
            'asc' => ['xta_country.name' => SORT_ASC],
            'desc' => ['xta_country.name' => SORT_DESC],
        ];
    

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }


        $query->andFilterWhere([
            'id' => $this->id,
            'id_country' => $this->id_country,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name]);
        $query->andFilterWhere(['like', xta_country::tableName().'.name', $this->country]);
        

        return $dataProvider;
    }
}
