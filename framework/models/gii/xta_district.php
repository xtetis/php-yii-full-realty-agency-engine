<?php

namespace app\models\gii;
use app\models\gii\xta_city; 
use yii\helpers\ArrayHelper;
use app\models\gii\xta_cache;
use Yii;

/**
 * This is the model class for table "{{%district}}".
 *
 * @property integer $id
 * @property integer $id_city
 * @property string $name
 *
 * @property City $idCity
 */
class xta_district extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%district}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_city'], 'required'],
            [['id_city'], 'integer'],
            [['name'], 'string', 'max' => 200]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_city' => 'Ссылка на город',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getxta_city()
    {
        return $this->hasOne(xta_city::className(), ['id' => 'id_city']);
    }


	public function fn__get_data_list($current_site=false)
	{
		// Cache start
		//==============================================================================
		//$id_cache = 'xta_district::fn__get_data_list_'.serialize($current_site);
		//$cache = xta_cache::fn__get_cache($id_cache);
		//if ($cache===false):
		//==============================================================================
		
			if (!$current_site)
			{
				$models = xta_district::find()->asArray()->all();
				$ret = ArrayHelper::map($models, 'id', 'name');
			}
			else
			{
				$site = xta_site::getCurrentSite();
				$models = xta_district::find()->where(["id_city" => $site->id_city])->asArray()->all();
				$ret = ArrayHelper::map($models, 'id', 'name');
			}
			
		// Cache end
		//==============================================================================
		//	xta_cache::fn__set_cache($id_cache,$ret,array('xta_site','xta_district','xta_site::fn__get_data_list'));
		//else:
		//	$ret = $cache;
		//endif;
		//==============================================================================

		return $ret;
	}

}
