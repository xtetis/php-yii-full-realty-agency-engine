<?php

namespace app\models\gii;

use Yii;

/**
 * This is the model class for table "{{%cache_in_tag}}".
 *
 * @property integer $id
 * @property integer $id_tag
 * @property integer $id_cache
 */
class xta_cache_in_tag extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%cache_in_tag}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_tag', 'id_cache'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_tag' => 'Ссылка на тег',
            'id_cache' => 'Ссылка на кеш',
        ];
    }
}
