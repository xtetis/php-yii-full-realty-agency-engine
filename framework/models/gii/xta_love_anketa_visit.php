<?php

namespace app\models\gii;

use Yii;

/**
 * This is the model class for table "{{%love_anketa_visit}}".
 *
 * @property integer $id
 * @property integer $id_anketa_owner
 * @property integer $id_anketa_visitor
 * @property string $timestamp
 * @property integer $readed
 */
class xta_love_anketa_visit extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%love_anketa_visit}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_anketa_owner', 'id_anketa_visitor'], 'required'],
            [['id_anketa_owner', 'id_anketa_visitor', 'readed'], 'integer'],
            [['timestamp'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_anketa_owner' => 'Хозяин анкеты',
            'id_anketa_visitor' => 'Посетительт анкеты',
            'timestamp' => 'Время посещения анкеты',
            'readed' => 'Просмотрена информация о посещении',
        ];
    }
}
