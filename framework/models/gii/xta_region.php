<?php

namespace app\models\gii;

use Yii;
use app\models\gii\xta_country; 
use yii\helpers\ArrayHelper;
/**
 * This is the model class for table "{{%region}}".
 *
 * @property integer $id
 * @property integer $id_country
 * @property string $name
 *
 * @property City[] $cities
 * @property Country $idCountry
 */
class xta_region extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%region}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_country'], 'required'],
            [['id_country'], 'integer'],
            [['name'], 'string', 'max' => 200]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_country' => 'Страна',
            'name' => 'Регион',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCities()
    {
        return $this->hasMany(City::className(), ['id_region' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getxta_country()
    {
        return $this->hasOne(xta_country::className(), ['id' => 'id_country']);
    }
    
    
    
    public function getCountryDataList() { // could be a static func as well
        $models = xta_country::find()->asArray()->all();
        return ArrayHelper::map($models, 'id', 'name');
    }

}
