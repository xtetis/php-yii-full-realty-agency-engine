<?php

namespace app\models\gii;

use Yii;
use app\models\Email;
use  yii\db\Query;

/**
 * This is the model class for table "{{%user_message}}".
 *
 * @property integer $id
 * @property integer $id_user_from
 * @property integer $id_user_to
 * @property string $message
 * @property string $createdon
 * @property integer $readed
 * @property string $options
 *
 * @property User $idUserFrom
 * @property User $idUserTo
 */
class xta_user_message extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user_message}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_user_from', 'id_user_to', 'readed'], 'integer'],
            [['message', 'id_user_from', 'id_user_to'], 'required'],
            [['message', 'options'], 'string'],
            [['createdon'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_user_from' => 'Пользователь отправитель',
            'id_user_to' => 'Пользователь получатель',
            'message' => 'Message',
            'createdon' => 'Createdon',
            'readed' => 'Readed',
            'options' => 'Опции сообщения',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdUserFrom()
    {
        return $this->hasOne(User::className(), ['id' => 'id_user_from']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdUserTo()
    {
        return $this->hasOne(User::className(), ['id' => 'id_user_to']);
    }



	public function fn__send_message($id_user,$message)
	{
		$model = new xta_user_message();
		$model->id_user_from = Yii::$app->user->id;
		$model->id_user_to = $id_user;
		$model->message = $message;
		$model->readed = 0;
		$model->options = '';
		$model->save();
		
		$subject='Вам отправлено личное <a href="+link_autologin_my_messages_unreaded+">сообщение</a> на сайте 
		<a href="+link_autologin_my_messages_unreaded+">+http_host+</a>';
		$emailmessage='
		<div><b>Отправитель:</b> '.Yii::$app->user->getIdentity()->username.'</div>
		<div><b>Сообщение:</b> '.$message.'</div>
		<div><b>Время отправления:</b> '.date("d-m-Y H:i:s").'</div>
		';
		Email::sendNotification($id_user,$subject,$emailmessage);
		return $model;
	}
	
	
	public function fn__is_new_message()
	{
		if (Yii::$app->user->isGuest)
		{
			return false;
		}
		
		$connection = \Yii::$app->db;
		$sql = "SELECT count(*) as 'count'  FROM 
					  (
							SELECT DISTINCT
								`id_user_from`
							FROM 
								`xta_user_message`
							WHERE 
								`id_user_to` =".Yii::$app->user->id."
								 AND `readed`=0
					  ) as t2";
		$model_count = $connection->createCommand($sql);
		$count_arr = $model_count->queryAll();
		$count = $count_arr[0]['count'];
		return intval($count);
	}
}
