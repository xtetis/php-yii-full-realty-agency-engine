<?php

namespace app\models\gii;

use Yii;
use app\models\gii\xta_city; 
use yii\helpers\ArrayHelper;
/**
 * This is the model class for table "{{%site}}".
 *
 * @property integer $id
 * @property integer $id_city
 * @property string $name
 *
 * @property City $idCity
 */
class xta_site extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%site}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_city'], 'required'],
            [['id_city'], 'integer'],
            [['name'], 'string', 'max' => 200]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_city' => 'Ссылка на город',
            'name' => 'Хост ',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getxta_city()
    {
        return $this->hasOne(xta_city::className(), ['id' => 'id_city']);
    }



	public function fn__get_xta_site_DataList() { // could be a static func as well
		$models = xta_site::find()->asArray()->all();
		return ArrayHelper::map($models, 'id', 'name');
	}


	public function getCurrentSiteId()
	{
		$name = $_SERVER['HTTP_HOST'];
		$site = xta_site::find()->where(["name" => $name])->one();
		if (!count($site))
		{
			die('Нет такого сайта');
		}
		return $site->id;
	}
	
	
	public function getCurrentSite()
	{
		// Cache start
		//==============================================================================
		$id_cache = 'xta_site::getCurrentSite';
		$cache = xta_cache::fn__get_cache($id_cache);
		if ($cache===false):
		//==============================================================================
		
			$name = $_SERVER['HTTP_HOST'];
			$site = xta_site::find()->where(["name" => $name])->one();
			if (!count($site))
			{
				die('Нет такого сайта');
			}
			
		// Cache end
		//==============================================================================
			xta_cache::fn__set_cache($id_cache,$site,array('xta_site','xta_site::getCurrentSite'));
		else:
			$site = $cache;
		endif;
		//==============================================================================

		return $site;
	}
}
