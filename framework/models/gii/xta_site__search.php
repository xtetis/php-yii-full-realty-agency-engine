<?php

namespace app\models\gii;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\gii\xta_site;
use app\models\gii\xta_city;

/**
 * xta_site_search represents the model behind the search form about `app\models\gii\xta_site`.
 */
class xta_site__search extends xta_site
{
	public $city;
	
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'id_city'], 'integer'],
            [['name'], 'safe'],
            [['city'], 'safe'],
            
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = xta_site::find();
				$query->joinWith('xta_city');
				
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);


        $dataProvider->sort->attributes['city'] = [
            'asc' => ['xta_city.name' => SORT_ASC],
            'desc' => ['xta_city.name' => SORT_DESC],
        ];
    

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }


        $query->andFilterWhere([
            'id' => $this->id,
            'id_city' => $this->id_city,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name]);
        $query->andFilterWhere(['like', xta_city::tableName().'.name', $this->city]);
        

        return $dataProvider;
    }
}
