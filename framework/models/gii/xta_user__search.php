<?php

namespace app\models\gii;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\gii\xta_user;
use app\models\gii\xta_user_role;


/**
 * xta_user__search represents the model behind the search form about `app\models\gii\xta_user`.
 */
class xta_user__search extends xta_user
{
	public $user_role;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'id_user_role', 'id_district'], 'integer'],
            [['email', 'pass', 'username', 'phone', 'skype', 'authkey', 'accesstoken','user_role'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
	public function search($params)
	{
	$query = xta_user::find();
	$query->joinWith('xta_user_role');

	$dataProvider = new ActiveDataProvider([
		  'query' => $query,
	]);
	
	$dataProvider->sort->attributes['user_role'] = [
		'asc' => ['xta_user_role.name' => SORT_ASC],
		'desc' => ['xta_user_role.name' => SORT_DESC],
	];

	$this->load($params);

	if (!$this->validate()) 
	{
		return $dataProvider;
	}

	$query->andFilterWhere([
		  'id' => $this->id,
		  'id_user_role' => $this->id_user_role,
		  'id_district' => $this->id_district,
	]);

	$query->andFilterWhere(['like', 'email', $this->email])
		  ->andFilterWhere(['like', 'pass', $this->pass])
		  ->andFilterWhere(['like', 'username', $this->username])
		  ->andFilterWhere(['like', 'phone', $this->phone])
		  ->andFilterWhere(['like', 'skype', $this->skype])
		  ->andFilterWhere(['like', 'authkey', $this->authkey])
		  ->andFilterWhere(['like', 'accesstoken', $this->accesstoken]);
	$query->andFilterWhere(['like', xta_user_role::tableName().'.name', $this->user_role]);

	return $dataProvider;
	}
}
