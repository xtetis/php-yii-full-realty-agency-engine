<?php

namespace app\models\gii;

use Yii;

/**
 * This is the model class for table "{{%love_type_account}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $alternate
 *
 * @property LoveAnketa[] $loveAnketas
 */
class xta_love_type_account extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%love_type_account}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'alternate'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Тип аккаунта',
            'alternate' => 'Альтернативное название ',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLoveAnketas()
    {
        return $this->hasMany(LoveAnketa::className(), ['id_love_type_account' => 'id']);
    }
}
