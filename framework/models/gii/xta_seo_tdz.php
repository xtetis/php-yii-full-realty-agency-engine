<?php

namespace app\models\gii;

use Yii;
use app\models\gii\xta_site;



/**
 * This is the model class for table "{{%seo_tdz}}".
 *
 * @property integer $id
 * @property integer $id_site
 * @property string $rurl
 * @property string $title
 * @property string $description
 * @property string $hone
 * @property integer $index
 * @property string $canonical
 * @property string $seotext
 *
 * @property Site $idSite
 */
 
 
 
class xta_seo_tdz extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%seo_tdz}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_site', 'seotext'], 'required'],
            [['id_site', 'index'], 'integer'],
            [['seotext'], 'string'],
            [['rurl', 'title', 'description', 'hone', 'canonical'], 'string', 'max' => 200],
            [['rurl'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_site' => 'Ссылка на сайт',
            'rurl' => 'Страница',
            'title' => 'Title',
            'description' => 'Description',
            'hone' => 'H1',
            'index' => 'Индексировать страницу',
            'canonical' => 'Каноническая ссылка на страницу',
            'seotext' => 'Сеотекст',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getxta_site()
    {
        return $this->hasOne(xta_site::className(), ['id' => 'id_site']);
    }
    
	public function fn__get_xta_seo_tdz()
	{
		// Cache start
		//==============================================================================
		$id_cache = 'xta_seo_tdz::fn__get_xta_seo_tdz_'.$_SERVER['REQUEST_URI'];
		$cache = xta_cache::fn__get_cache($id_cache);
		if ($cache===false):
		//==============================================================================
		
		$ret = xta_seo_tdz::find()->where([
				"rurl" => $_SERVER['REQUEST_URI'],
				"id_site" => xta_site::getCurrentSite()->id,
			])->one();
			
		if (!$ret)
		{
			$ret = 0;
		}

		// Cache end
		//==============================================================================
			xta_cache::fn__set_cache($id_cache,$ret,array('xta_site','xta_site::getCurrentSite'));
		else:
			$ret = $cache;
		endif;
		//==============================================================================

		return $ret;
	}
}
