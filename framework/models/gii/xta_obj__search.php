<?php

namespace app\models\gii;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\gii\xta_obj;

/**
 * xta_obj__search represents the model behind the search form about `app\models\gii\xta_obj`.
 */
class xta_obj__search extends xta_obj
{
	public $valutaname;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'id_district', 'id_city', 'id_category', 'id_user', 'id_album', 'id_valuta', 'price', 'published'], 'integer'],
            [['name', 'about', 'createdon', 'pub_date', 'source'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = xta_obj::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['pub_date'=>SORT_DESC]]
						//'pagination' => ['pagesize' => 3,],
        ]);
        
        if (
             (isset($params['xta_obj__search']['datepub'])) &&
             ($params['xta_obj__search']['datepub']=='actual')
           )
        {
	        $query->andFilterWhere(['>=', 'xta_obj.pub_date', new \yii\db\Expression('( NOW() - INTERVAL 60 DAY )')]);
        }
        
        if (
             (isset($params['xta_obj__search']['datepub'])) &&
             ($params['xta_obj__search']['datepub']=='noactual')
           )
        {
	        $query->andFilterWhere("( (published=0) OR (pub_date < NOW() - INTERVAL 60 DAY ) )");
        }

        
        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'xta_obj.id' => $this->id,
            'xta_obj.id_district' => $this->id_district,
            'xta_obj.id_city' => $this->id_city,
            'xta_obj.id_category' => $this->id_category,
            'xta_obj.id_user' => $this->id_user,
            'xta_obj.id_album' => $this->id_album,
            'xta_obj.id_valuta' => $this->id_valuta,
            'xta_obj.price' => $this->price,
            'xta_obj.createdon' => $this->createdon,
            'xta_obj.pub_date' => $this->pub_date,
            'xta_obj.published' => $this->published,
        ]);

        $query->andFilterWhere(['like', 'xta_obj.name', $this->name])
            ->andFilterWhere(['like', 'xta_obj.about', $this->about])
            ->andFilterWhere(['like', 'xta_obj.source', $this->source]);

        return $dataProvider;
    }
}
