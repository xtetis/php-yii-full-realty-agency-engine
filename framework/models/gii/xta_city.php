<?php

namespace app\models\gii;

use Yii;
use app\models\gii\xta_region; 
use app\models\gii\xta_site; 
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%city}}".
 *
 * @property integer $id
 * @property integer $id_region
 * @property string $name
 *
 * @property Region $idRegion
 * @property District[] $districts
 * @property Obj[] $objs
 * @property Site[] $sites
 */
class xta_city extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%city}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_region'], 'required'],
            [['id_region'], 'integer'],
            [['name'], 'string', 'max' => 200]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_region' => 'Ссылка на область',
            'name' => 'Город',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getxta_region()
    {
        return $this->hasOne(xta_region::className(), ['id' => 'id_region']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDistricts()
    {
        return $this->hasMany(District::className(), ['id_city' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getObjs()
    {
        return $this->hasMany(Obj::className(), ['id_city' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSites()
    {
        return $this->hasMany(Site::className(), ['id_city' => 'id']);
    }


    public function getRegionDataList() { // could be a static func as well
        $models = xta_region::find()->asArray()->all();
        return ArrayHelper::map($models, 'id', 'name');
    }
    
    public function getCityDataList() { // could be a static func as well
        $models = xta_city::find()->asArray()->all();
        return ArrayHelper::map($models, 'id', 'name');
    }





	public function fn__get_current_city()
	{
		// Cache start
		//==============================================================================
		$id_cache = 'xta_city::fn__get_current_city';
		$cache = xta_cache::fn__get_cache($id_cache);
		if ($cache===false):
		//==============================================================================
		
			$model_current_site = xta_site::getCurrentSite();
			$model_xta_city = xta_city::findOne($model_current_site->id_city);
			if (!$model_xta_city)
			{
				die('Нет такого города');
			}
			
		// Cache end
		//==============================================================================
			xta_cache::fn__set_cache($id_cache,$model_xta_city,array('xta_city','xta_city::fn__get_current_city'));
		else:
			$model_xta_city = $cache;
		endif;
		//==============================================================================

		
		return $model_xta_city;
	}




}
