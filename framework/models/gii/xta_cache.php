<?php

namespace app\models\gii;

use Yii;
use app\models\gii\xta_cache_in_tag;
use app\models\gii\xta_cache_tag;

/**
 * This is the model class for table "{{%cache}}".
 *
 * @property integer $id
 * @property string $md5
 * @property string $cache
 */
class xta_cache extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%cache}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cache'], 'required'],
            [['cache'], 'string'],
            [['md5'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'md5' => 'md5 кеша',
            'cache' => 'Кеш',
        ];
    }


	public function fn__set_cache($md5,$cache,$tags=array())
	{
		$md5 = md5($md5.'_'.$_SERVER['HTTP_HOST']);
		/*
		$model_xta_cache = xta_cache::find()->where(["md5" => $md5])->one();
		if (!$model_xta_cache)
		{
			$model_xta_cache = new xta_cache();
			$model_xta_cache->md5 = $md5;
		}
		$model_xta_cache->cache = serialize($cache);
		$ret = $model_xta_cache->save(false);
		foreach ($tags as $tag)
		{
			$model_xta_cache_tag = xta_cache_tag::find()->where(["name" => $tag])->one();
			if (!$model_xta_cache_tag)
			{
				$model_xta_cache_tag = new xta_cache_tag();
				$model_xta_cache_tag->name = $tag;
			}
			$model_xta_cache_tag->save(false);
			
			$model_xta_cache_in_tag = xta_cache_in_tag::find()->where([
				"id_tag"   => $model_xta_cache_tag->id,
				"id_cache" => $model_xta_cache->id,
				])->one();
			if (!$model_xta_cache_in_tag)
			{
				$model_xta_cache_in_tag = new xta_cache_in_tag();
				$model_xta_cache_in_tag->id_tag   = $model_xta_cache_tag->id;
				$model_xta_cache_in_tag->id_cache = $model_xta_cache->id;
				$model_xta_cache_in_tag->save(false);
			}
		}
		*/
		Yii::$app->cache->set($md5, $cache,0);
	}




	public function fn__get_cache($md5)
	{
		return false;
		$md5 = md5($md5.'_'.$_SERVER['HTTP_HOST']);
		
		$file_cache_value=Yii::$app->cache->get($md5);
		if($file_cache_value===false)
		{
			//$model_xta_cache = xta_cache::find()->where(["md5" => $md5])->one();
			//if (!$model_xta_cache)
			//{
				return false;
			//}
			//$base_cache_value = unserialize($model_xta_cache->cache);
			//Yii::$app->cache->set($md5, $base_cache_value,0);
			//return $base_cache_value;
		}
		else
		{
			return $file_cache_value;
		}
	}



	public function fn__clear_tag($tag)
	{
		/*
		$model_xta_cache_tag = xta_cache_tag::find()->where(["name" => $tag])->one();
		if ($model_xta_cache_tag)
		{
			$connection = Yii::$app->getDb();
			$connection->createCommand('DELETE FROM `xta_cache` WHERE `id` IN (SELECT `id_cache` FROM `xta_cache_in_tag` WHERE `id_tag` ='.$model_xta_cache_tag->id.')')->execute();
			$connection->createCommand('DELETE  FROM `xta_cache_in_tag` WHERE `id_tag` ='.$model_xta_cache_tag->id.'')->execute();
		}
		*/
		Yii::$app->cache->flush();
	}
	
	
	public function fn__clear_all()
	{
		//$connection = Yii::$app->getDb();
		//$connection->createCommand('DELETE FROM `xta_cache`')->execute();
		//$connection->createCommand('DELETE  FROM `xta_cache_in_tag`')->execute();
		Yii::$app->cache->flush();
	}


}
