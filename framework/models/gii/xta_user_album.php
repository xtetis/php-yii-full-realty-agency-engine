<?php

namespace app\models\gii;

use Yii;

/**
 * This is the model class for table "{{%user_album}}".
 *
 * @property integer $id
 * @property integer $id_album
 * @property string $name
 * @property string $about
 */
class xta_user_album extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user_album}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_album'], 'integer'],
            [['about'], 'required'],
            [['about'], 'string'],
            [['name'], 'string', 'max' => 200]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_album' => 'Ссылка на альбом',
            'name' => 'Имя',
            'about' => 'Описание',
        ];
    }
}
