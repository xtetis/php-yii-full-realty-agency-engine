<?php

namespace app\models\gii;

use Yii;

/**
 * This is the model class for table "{{%love_album}}".
 *
 * @property integer $id
 * @property integer $id_user
 * @property integer $id_album
 * @property integer $hidden
 * @property string $name
 *
 * @property User $idUser
 * @property Album $idAlbum
 */
class xta_love_album extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%love_album}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_user', 'id_album'], 'required'],
            [['id_user', 'id_album', 'hidden'], 'integer'],
            [['name'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_user' => 'Ссылка на пользователя',
            'id_album' => 'Ссылка на альбом',
            'hidden' => 'Скрытый ли альбом',
            'name' => 'Название альбома',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdUser()
    {
        return $this->hasOne(User::className(), ['id' => 'id_user']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdAlbum()
    {
        return $this->hasOne(Album::className(), ['id' => 'id_album']);
    }
}
