<?php

namespace app\models\gii;

use Yii;

/**
 * This is the model class for table "{{%obj_option_available}}".
 *
 * @property integer $id
 * @property integer $id_obj_option
 * @property integer $id_parent
 * @property string $name
 *
 * @property ObjOption $idObjOption
 */
class xta_obj_option_available extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%obj_option_available}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_obj_option', 'id_parent'], 'integer'],
            [['name'], 'required'],
            [['name'], 'string', 'max' => 200]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_obj_option' => 'Ссылка на опцию',
            'id_parent' => 'Родительский элемент',
            'name' => 'Имя',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdObjOption()
    {
        return $this->hasOne(ObjOption::className(), ['id' => 'id_obj_option']);
    }
}
