<?php

namespace app\models\gii;

use Yii;

/**
 * This is the model class for table "{{%obj_option_in_category}}".
 *
 * @property integer $id
 * @property integer $id_obj_option
 * @property integer $id_obj_category
 *
 * @property ObjOption $idObjOption
 * @property ObjCategory $idObjCategory
 */
class xta_obj_option_in_category extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%obj_option_in_category}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_obj_option', 'id_obj_category','use_setting','use_filter'], 'required'],
            [['id_obj_option', 'id_obj_category','use_setting','use_filter'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'              => 'ID',
            'id_obj_option'   => 'Ссылка на опцию',
            'id_obj_category' => 'Ссылка на категорию',
            'use_setting'     => 'Использовать свойство в категории',
            'use_filter'      => 'Использовать фильтр в категории',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdObjOption()
    {
        return $this->hasOne(ObjOption::className(), ['id' => 'id_obj_option']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdObjCategory()
    {
        return $this->hasOne(ObjCategory::className(), ['id' => 'id_obj_category']);
    }
}
