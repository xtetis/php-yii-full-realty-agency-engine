<?php

namespace app\models\gii;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%user_role}}".
 *
 * @property integer $id
 * @property string $name
 *
 * @property User[] $users
 */
class xta_user_role extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user_role}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 200]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название роли',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['id_user_role' => 'id']);
    }

	public function fn__get_data_list()
	{
		$models = xta_user_role::find()->asArray()->all();
		return ArrayHelper::map($models, 'id', 'name');
	}

}
