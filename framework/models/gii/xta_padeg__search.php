<?php

namespace app\models\gii;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\gii\xta_padeg;

/**
 * xta_padeg__search represents the model behind the search form about `app\models\gii\xta_padeg`.
 */
class xta_padeg__search extends xta_padeg
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['name', 'padeg1', 'padeg2', 'padeg3', 'padeg4', 'padeg5'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = xta_padeg::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'padeg1', $this->padeg1])
            ->andFilterWhere(['like', 'padeg2', $this->padeg2])
            ->andFilterWhere(['like', 'padeg3', $this->padeg3])
            ->andFilterWhere(['like', 'padeg4', $this->padeg4])
            ->andFilterWhere(['like', 'padeg5', $this->padeg5]);

        return $dataProvider;
    }
}
