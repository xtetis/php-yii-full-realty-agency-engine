<?php

namespace app\models\gii;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\gii\xta_obj_abuse;

/**
 * xta_obj_abuse__search represents the model behind the search form about `app\models\gii\xta_obj_abuse`.
 */
class xta_obj_abuse__search extends xta_obj_abuse
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'id_obj', 'id_user'], 'integer'],
            [['message', 'createdon'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = xta_obj_abuse::find();
        $query->joinWith('xta_obj');
        $query->joinWith('xta_user');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'id_obj' => $this->id_obj,
            'id_user' => $this->id_user,
            'createdon' => $this->createdon,
        ]);

        $query->andFilterWhere(['like', 'message', $this->message]);

        return $dataProvider;
    }
}
