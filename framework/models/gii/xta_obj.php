<?php

namespace app\models\gii;
use app\models\gii\xta_valuta;
use app\models\gii\xta_district;
use app\models\gii\xta_city;
use app\models\gii\xta_obj_category;
use app\models\gii\xta_user;
use app\models\gii\xta_album;
use app\models\gii\xta_obj_abuse;



use Yii;

/**
 * This is the model class for table "{{%obj}}".
 *
 * @property integer $id
 * @property integer $id_district
 * @property integer $id_city
 * @property integer $id_category
 * @property integer $id_user
 * @property integer $id_album
 * @property integer $id_valuta
 * @property string $name
 * @property string $about
 * @property integer $price
 * @property string $createdon
 * @property string $unpub_date
 * @property integer $published
 * @property string $source
 *
 * @property ObjCategory $idCategory
 * @property User $idUser
 * @property Album $idAlbum
 * @property City $idCity
 * @property Valuta $idValuta
 * @property ObjAbuse[] $objAbuses
 * @property ObjOptionValue[] $objOptionValues
 */
class xta_obj extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%obj}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_district', 'id_category', 'id_user', 'id_valuta'], 'required'],
            [['id_district', 'id_city', 'id_category', 'id_user', 'id_album', 'id_valuta', 'price', 'published'], 'integer'],
            [['about'], 'string'],
            [['createdon', 'pub_date'], 'safe'],
            [['name', 'source'], 'string', 'max' => 200],
            [['phone'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_district' => 'Ссылка на район города',
            'id_city' => 'Ссылка на город',
            'id_category' => 'Ссылка на категорию',
            'id_user' => 'Ссылка на пользователя',
            'id_album' => 'Ссылка на альбом',
            'id_valuta' => 'Ссылка на валюту',
            'name' => 'Заголовок объявления',
            'about' => 'Текст объявления',
            'price' => 'Price',
            'phone' => 'Телефон',
            'createdon' => 'Дата создания',
            'pub_date' => 'Дата окончания публикации',
            'published' => 'Опубликовано',
            'source' => 'Источник',
        ];
    }


	public function fn__get_xta_obj_category()
	{
		return $this->hasOne(xta_obj_category::className(), ['id' => 'id_category']);
	}


	public function fn__get_xta_user()
	{
		return $this->hasOne(xta_user::className(), ['id' => 'id_user']);
	}


	public function fn__get_xta_album()
	{
		return $this->hasOne(xta_album::className(), ['id' => 'id_album']);
	}


	public function fn__get_xta_city()
	{
		return $this->hasOne(xta_city::className(), ['id' => 'id_city']);
	}


	public function fn__get_xta_valuta()
	{
		return $this->hasOne(xta_valuta::className(), ['id' => 'id_valuta']);
	}
	
	
	public function fn__get_xta_district()
	{
		return $this->hasOne(xta_district::className(), ['id' => 'id_district']);
	}


	public function fn__get_xta_obj_abuse_many()
	{
		return $this->hasMany(xta_obj_abuse::className(), ['id_obj' => 'id']);
	}

    /**
     * @return \yii\db\ActiveQuery
     */
    public function fn__get_xta_obj_option_value_many()
    {
        return $this->hasMany(xta_obj_option_value::className(), ['id_obj' => 'id']);
    }

	public function fn__get_obj_list()
	{
		
	}



}
