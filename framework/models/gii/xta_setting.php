<?php

namespace app\models\gii;

use Yii;
use app\models\gii\xta_setting_category; 
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%setting}}".
 *
 * @property integer $id
 * @property integer $id_setting_category
 * @property string $name
 * @property string $title
 * @property string $value
 * @property string $default
 *
 * @property SettingCategory $idSettingCategory
 */
class xta_setting extends \yii\db\ActiveRecord
{
	public $allsite;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%setting}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //[['id_setting_category', 'value', 'default'], 'required'],
            [['id_setting_category', 'value','allsite'], 'required'],
            [['id_setting_category'], 'integer'],
            [['value', 'default'], 'string'],
            [['name'], 'string', 'max' => 100],
            [['title'], 'string', 'max' => 250]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_setting_category' => 'Ссылка на категорию настроек',
            'name' => 'Имя',
            'title' => 'Заголовок',
            'value' => 'Значение настройки',
            'default' => 'Значение по-умолчанию',
            'allsite' => 'Использовать для всех сайтов',
        ];
    }

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getxta_setting_category()
	{
		  return $this->hasOne(xta_setting_category::className(), ['id' => 'id_setting_category']);
	}


	/*
	Функция возвращает настройку для текущего сайта
	Параметры:
		$name - имя настройки (ключ)
	*/
	public function fn__get_setting($name)
	{
		$setting = xta_setting::find()->where(["name" => $name])->one();
		if (!count($setting))
		{
			return '';
		}
		$setting = unserialize($setting->value);
		
		if (isset($setting[xta_site::getCurrentSiteId()]))
		{
			return $setting[xta_site::getCurrentSiteId()];
		}
		elseif(isset($setting[0]))
		{
			return $setting[0];
		}
		else
		{
			return '';
		}
	}



}
