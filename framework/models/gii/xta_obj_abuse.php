<?php

namespace app\models\gii;
use app\models\gii\xta_obj; 
use app\models\gii\xta_user; 
use Yii;

/**
 * This is the model class for table "{{%obj_abuse}}".
 *
 * @property integer $id
 * @property integer $id_obj
 * @property integer $id_user
 * @property string $message
 * @property string $createdon
 *
 * @property Obj $idObj
 * @property User $idUser
 */
class xta_obj_abuse extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%obj_abuse}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_obj', 'id_user', 'message'], 'required'],
            [['id_obj', 'id_user'], 'integer'],
            [['message'], 'string'],
            [['createdon'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_obj' => 'Ссылка на объявление',
            'id_user' => 'Ссылка на пользователя',
            'message' => 'Текст жалобы',
            'createdon' => 'Дата создания жалобы',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getxta_obj()
    {
        return $this->hasOne(xta_obj::className(), ['id' => 'id_obj']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getxta_user()
    {
        return $this->hasOne(xta_user::className(), ['id' => 'id_user']);
    }
}
