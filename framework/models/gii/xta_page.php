<?php

namespace app\models\gii;

use Yii;

/**
 * This is the model class for table "{{%page}}".
 *
 * @property integer $id
 * @property integer $id_site
 * @property string $name
 * @property string $about
 * @property integer $published
 * @property string $editedon
 * @property integer $eval
 * @property integer $richedit
 * @property string $title
 * @property string $description
 * @property integer $index
 */
class xta_page extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%page}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_site', 'published', 'eval', 'richedit', 'index'], 'integer'],
            [['name', 'about'], 'required'],
            [['about'], 'string'],
            [['editedon'], 'safe'],
            [['name', 'title', 'description'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_site' => 'Сайт',
            'name' => 'H1',
            'about' => 'Текст страницы',
            'published' => 'Опубликовано',
            'editedon' => 'Дата редактирования',
            'eval' => 'Выполнить',
            'richedit' => 'Визуальный редактор',
            'title' => 'Title',
            'description' => 'Description',
            'index' => 'Индексировать страницу',
        ];
    }
}
