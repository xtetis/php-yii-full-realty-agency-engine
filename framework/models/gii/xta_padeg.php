<?php

namespace app\models\gii;

use Yii;

/**
 * This is the model class for table "{{%padeg}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $padeg1
 * @property string $padeg2
 * @property string $padeg3
 * @property string $padeg4
 * @property string $padeg5
 */
class xta_padeg extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%padeg}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'padeg1', 'padeg2', 'padeg3', 'padeg4', 'padeg5'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Именительнй',
            'padeg1' => 'Родительный',
            'padeg2' => 'Дательный',
            'padeg3' => 'Винительнй',
            'padeg4' => 'Творительный',
            'padeg5' => 'Предложный (Чём? Ком?)',
        ];
    }



	public function fn__get_padeg($text,$padeg)
	{
		$model_xta_padeg = xta_padeg::find()->where(["name" => $text])->one();
		if ($model_xta_padeg)
		{
			$padegnum = 'padeg'.$padeg;
			if (isset($model_xta_padeg->$padegnum))
			{
				$ret = $model_xta_padeg->$padegnum;
				if (!strlen($ret))
				{
					$ret = $model_xta_padeg->name;
				}
			}
			else
			{
				$ret = $model_xta_padeg->name;
			}
			return $ret;
		}
		else
		{
			$url = "http://export.yandex.ru/inflect.xml?name=".$text;
			$xml = simplexml_load_file($url);
			
			$model_xta_padeg = new xta_padeg();
			$model_xta_padeg->name = $text;
			for ($i=1;$i<=5;$i++) 
			{
				$pname = 'padeg'.$i;
				$model_xta_padeg->$pname = trim($xml->inflection[$i]);
			}
			$model_xta_padeg->save();
			return $this->fn__get_padeg($text,$padeg);
		}
		
		return $ret;
/*
// $id - id of resource
// $field - field name to write or print "padezh"
// $padezh - number of padezh
//http://nano.yandex.ru/project/inflect/
//http://pastie.org/1101890


$res = $modx->getObject('modResource', array('id' => $id));
$res_fieldval = $res->get($field);
$res_pagetitle = $res->get('pagetitle');

if (strlen($res_fieldval)>1) 
  {
    
    $arr = explode(',', $res_fieldval);
    return $arr[intval($padezh)];
    
  }
  else{
          $url = "http://export.yandex.ru/inflect.xml?name=".$res_pagetitle;
          $xml = simplexml_load_file($url);
          $original = $xml->original;
        $padezh_value = trim($xml->inflection[intval($padezh)]);
        for ($i=0;$i<=5;$i++) {
                $field_text[$i]=trim($xml->inflection[$i]);
            }
        $field_text_implode = implode(',', $field_text);
        $resource = $modx->getObject('modResource', $id);
        $resource->set($field, $field_text_implode);
        $resource->save();                     
          if (strlen($padezh_value)>1) 
           {
               return $padezh_value;     
           }else{
                     return 'Не определен падеж. Определите вручную';
                }
      }
*/
	}
}
