<?php

namespace app\models\gii;

use Yii;
use app\models\gii\xta_image;

/**
 * This is the model class for table "{{%album}}".
 *
 * @property integer $id
 * @property integer $id_image
 * @property string $name
 *
 * @property Image[] $images
 * @property Obj[] $objs
 */
class xta_album extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%album}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_image'], 'integer'],
            //[['name'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_image' => 'Ссылка на главную картинку',
        ];
    }


	public function fn__get_xta_image_many()
	{
		return $this->hasMany(xta_image::className(), ['id_album' => 'id']);
	}

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getObjs()
    {
        return $this->hasMany(Obj::className(), ['id_album' => 'id']);
    }


















//==============================================================================
	public function fn__exists_main_img()
	{
		if ($this->id_image)
		{
			$model_image = xta_image::find()->where(["id" => $this->id_image])->one();
			if ($model_image)
			{
				$filename = $_SERVER['DOCUMENT_ROOT'].'/public/images/album/'.
				            floor($this->id/1000).'/'.$this->id.'/'.$model_image->md5.'.png';
				if (file_exists($filename))
				{
					return true;
				}
				else
				{
					return false;
				}
				
			}
			else
			{
				return false;
			}
		}
		else
		{
			$model_image = xta_image::find()->where(["id_album" => $this->id])->one();
			if ($model_image)
			{
				$this->id_image = $model_image->id;
				$this->save();
				return true;
			}
			else
			{
				return false;
			}
			
		}
	}
//==============================================================================















//==============================================================================
	public function fn__get_main_img_src($id_album=0)
	{
		if ($id_album)
		{
			$model_album = xta_album::find()->where(["id" => $id_album])->one();
		}
		else
		{
			$model_album = $this;
		}
		
		if ($model_album)
		{
			if ($model_album->fn__exists_main_img())
			{
				$model_image = xta_image::find()->where(["id" => $model_album->id_image])->one();
				$filename = '/public/images/album/'.floor($model_album->id/1000).'/'.
				            $model_album->id.'/'.$model_image->md5.'.png';
				return $filename;
			}
			else
			{
				return '/public/images/other/nophoto.png';
			}
		}
		else
		{
			return '/public/images/other/nophoto.png';
		}
	}
//==============================================================================







//==============================================================================
	public function fn__delete_images_from_album($id_album=0)
	{
		if ($id_album)
		{
			$model_album = xta_album::find()->where(["id" => $id_album])->one();
		}
		else
		{
			$model_album = $this;
		}
		
		if ($model_album)
		{
			
			$model__xta_image_many = $model_album->fn__get_xta_image_many()->all();
			foreach ($model__xta_image_many as $model__xta_image_item)
			{
				$model__xta_image_item->fn__delete_image();
			}
		}
		else
		{
			return false;
		}
	}
//==============================================================================
}
