<?php

namespace app\models\gii;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\gii\xta_obj_category_description;

/**
 * xta_obj_category_description__search represents the model behind the search form about `app\models\gii\xta_obj_category_description`.
 */
class xta_obj_category_description__search extends xta_obj_category_description
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'id_site', 'id_obj_category'], 'integer'],
            [['title', 'description', 'hone', 'seotext'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = xta_obj_category_description::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'id_site' => $this->id_site,
            'id_obj_category' => $this->id_obj_category,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'hone', $this->hone])
            ->andFilterWhere(['like', 'seotext', $this->seotext]);

        return $dataProvider;
    }
}
