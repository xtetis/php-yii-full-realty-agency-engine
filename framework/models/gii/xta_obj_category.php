<?php

namespace app\models\gii;
use yii\helpers\ArrayHelper;
use Yii;

/**
 * This is the model class for table "{{%obj_category}}".
 *
 * @property integer $id
 * @property integer $id_parent
 * @property string $name
 * @property string $img
 * @property string $hone
 * @property string $title
 * @property string $description
 * @property integer $hideobjmain
 *
 * @property Obj[] $objs
 * @property ObjOptionInCategory[] $objOptionInCategories
 */
class xta_obj_category extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%obj_category}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_parent'], 'required'],
            [['id_parent', 'hideobjmain'], 'integer'],
            [['name', 'hone', 'title', 'description'], 'string', 'max' => 200]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_parent' => 'Родитель',
            'name' => 'Название категории',
            'hone' => 'H1 страницы',
            'title' => 'Тайтл страницы',
            'description' => 'Description страницы',
            'hideobjmain' => 'Нужно ли скривать объявления из этой категории на главной странице',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getObjs()
    {
        return $this->hasMany(Obj::className(), ['id_category' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function fn__get_xta_obj_option_many()
    {
        return xta_obj_option::find()->where("id in (
        SELECT 
        	`id_obj_option` 
        FROM 
        	`xta_obj_option_in_category` 
        WHERE 
        	`id_obj_category` = ".$this->id."
        	AND
        	`use_setting`=1
        )");
    }

	public function getParent()
	{
		return xta_obj_category::findOne($this->id_parent);
	}


	/*
		Возвращает количество дочерних категорий
	*/
	public function hasChildren()
	{
		return xta_obj_category::find()->where(['id_parent' => $this->id])->count();
	}
	
	
	/*
		Возвращает количество дочерних категорий
	*/
	public function fn__get_children()
	{
		// Cache start
		//==============================================================================
		$id_cache = 'xta_obj_category::fn__get_children_'.$this->id;
		$cache = xta_cache::fn__get_cache($id_cache);
		if ($cache===false):
		//==============================================================================
		
			$ret = xta_obj_category::find()->where(['id_parent' => $this->id])->all();
			
		// Cache end
		//==============================================================================
			xta_cache::fn__set_cache($id_cache,$ret,array('xta_obj_category','xta_obj_category::fn__get_children'));
		else:
			$ret = $cache;
		endif;
		//==============================================================================
		
		return $ret;
	}





	public function hasChildrenArray($data_xta_obj_category,$item)
	{
		foreach ($data_xta_obj_category as $item1)
		{
			if ($item1['id_parent']==$item['id'])
			{
				return true;
			}
		}
		return false;
	}
	






	/*
		Получаем уровень вложенности текущей директории
	*/
	public function getParentDepth()
	{
		if (!$this->id_parent)
		{
			return 0;
		}
		elseif (!$this->getParent()->id_parent)
		{
			return 1;
		}
		elseif (!$this->getParent()->getParent()->id_parent)
		{
			return 2;
		}
	}
	
	






	/*
		Получаем ассоциативный массив из текущей категории и всех родительских
	*/
	public function fn__get_categs_array($id_category = 0)
	{
		// Cache start
		//==============================================================================
		$id_cache = 'xta_obj_category::fn__get_categs_array_'.$id_category.'_'.$this->id;
		$cache = xta_cache::fn__get_cache($id_cache);
		if ($cache===false):
		//==============================================================================
		
			$ret = array();
			$ret[$this->id]=$this->name;
			if ($this->id_parent)
			{
				$parent = $this->getParent();
				$ret[$parent->id]=$parent->name;
				if ($parent->id_parent)
				{
					$parent = $parent->getParent();
					$ret[$parent->id]=$parent->name;
				}
			}
			$ret = array_reverse($ret,true);
		// Cache end
		//==============================================================================
			xta_cache::fn__set_cache($id_cache,$ret,array('xta_obj_category','xta_obj_category::fn__get_categs_array'));
		else:
			$ret = $cache;
		endif;
		//==============================================================================

		return $ret;
	}
	








	/*
		Получаем количество уровней дочерних элементов
	*/
	public function getChildrenDepth()
	{
		if (!$this->hasChildren())
		{
			return 0;
		}

		$childrens = xta_obj_category::find()->where(['id_parent' => $this->id])->all();
		foreach ($childrens as $item)
		{
			
			if ($item->hasChildren())
			{
				$childrens_childrens = xta_obj_category::find()->where(['id_parent' => $item->id])->all();
				foreach ($childrens_childrens as $item_item)
				{
					if ($item_item->hasChildren())
					{
						return 3;
					}
				}
				return 2;
			}
		}
		return 1;
	}















	public function getObjCategoryDataList($allow_root=true,$allow_categs = array(),$disallow_categs = array()) {
		$categories_all = array();
		$ret = array();
		
		$categories_list = xta_obj_category::find()->all();
		foreach ($categories_list as $item)
		{
			if ($item->id_parent==0)
			{
				if (
				     ((!count($allow_categs)) ||
				     (in_array($item->id,$allow_categs))) &&
				     (!in_array($item->id,$disallow_categs))
				   )
				{
					$ret[$item->id]='→ '.$item->name.' ('.$item->id.')';
				}
				foreach ($categories_list as $sitem)
				{
					if ($sitem->id_parent==$item->id)
					{
						if (
								 ((!count($allow_categs)) ||
								 (in_array($sitem->id,$allow_categs))) &&
								 (!in_array($sitem->id,$disallow_categs))
							 )
						{
							$ret[$sitem->id]='→ → '.$sitem->name.' ('.$sitem->id.')';
						}
						foreach ($categories_list as $ssitem)
						{
							if ($ssitem->id_parent==$sitem->id)
							{
								if (
										 ((!count($allow_categs)) ||
										 (in_array($ssitem->id,$allow_categs))) &&
										 (!in_array($ssitem->id,$disallow_categs))
									 )
								{
									$ret[$ssitem->id]='→ → → '.$ssitem->name.' ('.$ssitem->id.')';
								}
								
							}
						}
					}
				}
			}
		}
		
		if ($allow_root)
		{
			$ret = array_merge(array(0=>'Корень'),$ret);
		}
		
		return $ret;
	}









	/* 
		Получаем количество объявлений в категории
	*/
	public function fn__get_count_obj($id_category,$recursive = false)
	{
		if (!$id_category)
		{
			$ret = xta_obj::find()->where("id NOT IN (SELECT id_obj FROM xta_obj_abuse) AND published=1")->count();
		}
		else
		{
			if (!$recursive)
			{
				$ret = xta_obj::find()->where("id_category = ".$id_category." AND id NOT IN (SELECT id_obj FROM xta_obj_abuse) AND published=1")->count();
			}
			else
			{
				$ret = xta_obj::find()->where("
				id_category IN (

					SELECT 
						t1.id
					FROM 
						xta_obj_category as t1
					WHERE 
						t1.id_parent = ".$id_category."

					UNION

					SELECT 
						t2.id
					FROM 
						xta_obj_category as t2 
					INNER JOIN xta_obj_category as t3
						ON t3.id = t2.id_parent
					WHERE 
						t3.id_parent = ".$id_category."

					UNION

					SELECT 
						t4.id
					FROM 
						xta_obj_category as t4 
					INNER JOIN xta_obj_category as t5
						ON t5.id = t4.id_parent
					INNER JOIN xta_obj_category as t6
						ON t6.id = t5.id_parent
					WHERE 
						t6.id_parent = ".$id_category."
					) 
					AND 
				id NOT IN (SELECT id_obj FROM xta_obj_abuse) 
					AND 
				published=1")->count();
			}
		}
		return $ret;
	}





}
