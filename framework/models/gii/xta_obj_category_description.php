<?php

namespace app\models\gii;

use Yii;

/**
 * This is the model class for table "{{%obj_category_description}}".
 *
 * @property integer $id
 * @property integer $id_site
 * @property integer $id_obj_category
 * @property string $title
 * @property string $description
 * @property string $hone
 * @property string $seotext
 *
 * @property ObjCategory $idObjCategory
 * @property Site $idSite
 */
class xta_obj_category_description extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%obj_category_description}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_site', 'id_obj_category', 'seotext'], 'required'],
            [['id_site', 'id_obj_category'], 'integer'],
            [['seotext'], 'string'],
            [['title', 'description', 'hone'], 'string', 'max' => 500]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_site' => 'Ссылка на сайт',
            'id_obj_category' => 'Ссылка на категорию объявлений',
            'title' => 'Title',
            'description' => 'Description',
            'hone' => 'Hone',
            'seotext' => 'Seotext',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getxta_obj_category()
    {
        return $this->hasOne(xta_obj_category::className(), ['id' => 'id_obj_category']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getxta_site()
    {
        return $this->hasOne(xta_site::className(), ['id' => 'id_site']);
    }
}
