<?php

namespace app\models\gii;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\gii\xta_city;
use app\models\gii\xta_country;


/**
 * xta_city__search represents the model behind the search form about `app\models\gii\xta_city`.
 */
class xta_city__search extends xta_city
{
	public $region;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'id_region'], 'integer'],
            [['name'], 'safe'],
            [['region'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = xta_city::find();
				$query->joinWith('xta_region');
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);


        $dataProvider->sort->attributes['region'] = [
            'asc' => ['xta_region.name' => SORT_ASC],
            'desc' => ['xta_region.name' => SORT_DESC],
        ];
    

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }


        $query->andFilterWhere([
            'id' => $this->id,
            'id_region' => $this->id_region,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name]);
        $query->andFilterWhere(['like', xta_region::tableName().'.name', $this->region]);
        

        return $dataProvider;
    }
}
