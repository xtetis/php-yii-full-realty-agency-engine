<?php

namespace app\models\gii;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\gii\xta_seo_tdz;

/**
 * xta_seo_tdz__search represents the model behind the search form about `app\models\gii\xta_seo_tdz`.
 */
class xta_seo_tdz__search extends xta_seo_tdz
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'id_site', 'index'], 'integer'],
            [['rurl', 'title', 'description', 'hone', 'canonical', 'seotext'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = xta_seo_tdz::find();
				$query->joinWith('xta_site');
				
				
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'id_site' => $this->id_site,
            'index' => $this->index,
        ]);

        $query->andFilterWhere(['like', 'rurl', $this->rurl])
            ->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'hone', $this->hone])
            ->andFilterWhere(['like', 'canonical', $this->canonical])
            ->andFilterWhere(['like', 'seotext', $this->seotext]);

        return $dataProvider;
    }
}
