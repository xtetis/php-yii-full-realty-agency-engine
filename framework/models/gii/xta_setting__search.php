<?php

namespace app\models\gii;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\gii\xta_setting;

/**
 * xta_setting__search represents the model behind the search form about `app\models\gii\xta_setting`.
 */
class xta_setting__search extends xta_setting
{
	public $setting_category;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'id_setting_category'], 'integer'],
            [['name', 'title', 'value', 'default'], 'safe'],
            [['setting_category'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = xta_setting::find();
				$query->joinWith('xta_setting_category');
				
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);


        $dataProvider->sort->attributes['setting_category'] = [
            'asc' => ['xta_setting_category.name' => SORT_ASC],
            'desc' => ['xta_setting_category.name' => SORT_DESC],
        ];
    

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }


        $query->andFilterWhere([
            'id' => $this->id,
            'id_setting_category' => $this->id_setting_category,
        ]);

        $query->andFilterWhere(['like', 'xta_setting.name', $this->name]);
        $query->andFilterWhere(['like', xta_setting_category::tableName().'.name', $this->setting_category]);
        

        return $dataProvider;
    }
}
