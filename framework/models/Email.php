<?php

namespace app\models;

use Yii;
use yii\base\Model;
use app\models\gii\xta_user;
use app\models\User;
use yii\helpers\Url;

/**
 * LoginForm is the model behind the login form.
 */
class Email extends Model
{
	

	public function fn__mail_utf8($from_name,$from_email,$to_name,$to_email,$subject,$message)
	{
		$subject   = strip_tags($subject);
		$from_name = "=?UTF-8?B?".base64_encode($from_name)."?=";
		$to_name   = "=?UTF-8?B?".base64_encode($to_name)."?=";
		$subject   = "=?UTF-8?B?".base64_encode($subject)."?=";

		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";

		$headers .= 'To: '.$to_name.' <'.$to_email.'>' . "\r\n";
		$headers .= 'From: '.$from_name.' <krgazeta@krgazeta.com>' . "\r\n";

		return mail($to_email, $subject, $message, $headers);
	}


	public function sendNotification($id_user=0,$subject='',$message='')
	{

		$from_name  = 'KrGazeta';
		$from_email  = 'noreply@krgazeta.com';


		
		
		$xta_user = xta_user::findOne(intval($id_user));
		if (!$xta_user)
		{
			return false;
		}
		
		
		$to_name  = $xta_user->username;
		$to_email  = $xta_user->email;

		$message = '
		<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
		<html xmlns="http://www.w3.org/1999/xhtml">
		<head>
		<meta name="viewport" content="width=device-width" />
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<title>'.$subject.'</title>
		<style>
		* { 
			margin:0;
			padding:0;
		}
		* { font-family: "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif; }
		img { 
			max-width: 100%; 
		}
		.collapse {
			margin:0;
			padding:0;
		}
		body {
			-webkit-font-smoothing:antialiased; 
			-webkit-text-size-adjust:none; 
			width: 100%!important; 
			height: 100%;
		}
		a { color: #2BA6CB;}

		p.callout {
			padding:15px;
			background-color:#ECF8FF;
			margin-bottom: 15px;
		}
		table.body-wrap { width: 100%;}


		h1,h2,h3,h4,h5,h6 {
		font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif; line-height: 1.1; margin-bottom:15px; color:#000;
		}
		h1 small, h2 small, h3 small, h4 small, h5 small, h6 small { font-size: 60%; color: #6f6f6f; line-height: 0; text-transform: none; }

		h1 { font-weight:200; font-size: 44px;}
		h2 { font-weight:200; font-size: 37px;}
		h3 { font-weight:500; font-size: 27px;}
		h4 { font-weight:500; font-size: 23px;}
		h5 { font-weight:900; font-size: 17px;}
		h6 { font-weight:900; font-size: 14px; text-transform: uppercase; color:#444;}

		.collapse { margin:0!important;}
		p, ul { 
			margin-bottom: 10px; 
			font-weight: normal; 
			font-size:14px; 
			line-height:1.6;
		}
		p.lead { font-size:17px; }
		p.last { margin-bottom:0px;}

		ul li {
			margin-left:5px;
			list-style-position: inside;
		}
		.container {
			display:block!important;
			max-width:600px!important;
			margin:0 auto!important; /* makes it centered */
			clear:both!important;
		}
		.content {
			padding:15px;
			max-width:600px;
			margin:0 auto;
			display:block; 
		}
		.content table { width: 100%; }
		.clear { display: block; clear: both; }
		</style>

		</head>
		 
		<body bgcolor="#FFFFFF">


		<!-- BODY -->
		<table class="body-wrap">
			<tr>
				<td></td>
				<td class="container" bgcolor="#FFFFFF">

					<div class="content">
					<table>
						<tr>
							<td>
								<h3>Здравствуйте, '.$to_name.'</h3>
								<p class="lead">'.$subject.'</p>
								<p>
								'.$message.'
								</p>
								<!-- Callout Panel -->
								<p class="callout">
									Вы получили это письмо, потому как являетесь зарегистрированным пользователем на сайте <a href="+link_autologin+">KrGazeta.com</a>
								</p><!-- /Callout Panel -->					
												
							</td>
						</tr>
					</table>
					</div><!-- /content -->
									
				</td>
				<td></td>
			</tr>
		</table><!-- /BODY -->

		</body>
		</html>
		';
		$message = Email::prepareMessage($message,$to_email);
		$subject = Email::prepareMessage($subject,$to_email);
		$ret = Email::fn__mail_utf8($from_name,$from_email,$to_name,$to_email,$subject,$message);
	}
	

	public function prepareMessage($message,$to_email)
	{
		
		$replacedata = array(
		'+http_host+'=>$_SERVER['HTTP_HOST'],
		'+link_autologin+'=>Email::fn__get_autologin_link($to_email),
		'+link_autologin_my_obj+'=>Email::fn__get_autologin_link($to_email,Url::to(['/board/myad'])),
		'+link_autologin_my_messages+'=>Email::fn__get_autologin_link($to_email,Url::to(['/message'])),
		'+link_autologin_my_messages_unreaded+'=>Email::fn__get_autologin_link($to_email,Url::to(['/message/unreaded'])),
		'+link_autologin_my_settings+'=>Email::fn__get_autologin_link($to_email,Url::to(['/setting'])),
		'+link_autologin_my_settings_password+'=>Email::fn__get_autologin_link($to_email,Url::to(['/setting/password'])),
		);
		
		
		if (is_array($replacedata))
		{
			foreach ($replacedata as $key => $value)
			{
				$message = str_replace($key,$value,$message);
			}
		}
		
		return $message;
	}


	public function fn__get_autologin_link($email,$link='')
	{
		$linkb64 = base64_encode($link);
		$user_info = User::findByEmail($email);
		if (($model__xta_user = xta_user::findOne($user_info->id)) !== null) 
		{
			$accesstoken = md5($user_info->authkey.$user_info->email.$user_info->pass);
			$model__xta_user->accesstoken = $accesstoken;
			$model__xta_user->save();
			$autologin_link = 'http://'.$_SERVER['HTTP_HOST'].'/account/login/'.$accesstoken.'/'.$model__xta_user->id;
			if (strlen($link))
			{
				$autologin_link.='?url='.$linkb64;
			}
		}
		else
		{
			$autologin_link = $link;
		}
		
		return $autologin_link;
	}


	public function fn__get_autologin_link_for_user($id_user,$link='')
	{
		$linkb64 = base64_encode($link);
		$model__xta_user = xta_user::findOne($id_user);
		if ($model__xta_user) 
		{
			$accesstoken = md5($model__xta_user->authkey.$model__xta_user->email.$model__xta_user->pass);
			$model__xta_user->accesstoken = $accesstoken;
			$model__xta_user->save();
			$autologin_link = 'http://'.$_SERVER['HTTP_HOST'].'/account/login/'.$accesstoken.'/'.$model__xta_user->id;
			if (strlen($link))
			{
				$autologin_link.='?url='.$linkb64;
			}
		}
		else
		{
			$autologin_link = $link;
		}
		
		return $autologin_link;
	}




	// Уведомление пользователю о регистрации
	public function fn__send_register_notification_to_user($id_user,$pass)
	{
		$model__xta_user = xta_user::findOne($id_user);
		if ($model__xta_user) 
		{
			$subject='Благодарим за регистрацию на сайте <a href="+link_autologin+">+http_host+</a>';
			$emailmessage='
			<div><b>Регистрационный email:</b> '.$model__xta_user->email.'</div>
			<div><b>Имя:</b> '.$model__xta_user->username.'</div>
			<div><b>Контактный телефон:</b> '.$model__xta_user->phone.'</div>
			<div><b>Пароль:</b> '.$pass.'</div>
			<div><b>Дата регистрации:</b> '.date("d-m-Y H:i:s").'</div>
			<div>Вы можете сменить пароль перейдя по <a href="+link_autologin_my_settings_password+">ссылке</a></div>
			';
			Email::sendNotification($id_user,$subject,$emailmessage);
		}
	}
	





}
