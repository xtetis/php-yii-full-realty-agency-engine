<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\db\Query;


/**
 * LoginForm is the model behind the login form.
 */
class LibSql extends Model
{

	// Возвращает поле по ID
	//****************************************************************************** 
	public function sql_valid($value){ 
		$value = Yii::$app->db->quoteValue($value);
		return $value;
	}
	//******************************************************************************





	// Возвращает поле по ID
	//****************************************************************************** 
	public function fn__get_field_val_by_id($table,$field,$id){ 
		$connection = \Yii::$app->db;
		$sql = "SELECT `".$field."` FROM `".$table."` WHERE `id`=".intval($id);
		$model = $connection->createCommand($sql);
		$record = $model->queryOne();
		return $record[$field];
	}
	//******************************************************************************







	// Функция возвращает количество запосей в таблице
	//****************************************************************************
	public function fn__get_count_by_where($table='',$where='',$cache=false)
	{
		$connection = \Yii::$app->db;
		if (strlen($where))
		{
			$where = ' WHERE '.$where;
		}
		$from = "`".$table."`";
		$from = str_replace('``','`',$from);
		$sql = "SELECT count(`id`) as 'count' FROM ".$from." ".$where;
		$model = $connection->createCommand($sql);
		$record = $model->queryOne();
		return $record['count'];
	}
	//****************************************************************************





	// Функция возвращает значение поля
	//****************************************************************************
	public function fn__get_fieldval_by_where($table='', $field='' ,$where='', $cache=false)
	{
		$connection = \Yii::$app->db;
		if (strlen($where))
		{
			$where = ' WHERE '.$where;
		}
		$sql = "SELECT `".$field."` as 'field' FROM `".$table."` ".$where;
		$model = $connection->createCommand($sql);
		$record = $model->queryOne();
		return $record['field'];
	}
	//****************************************************************************

}
