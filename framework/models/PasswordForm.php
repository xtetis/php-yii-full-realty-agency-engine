<?php

namespace app\models;

use Yii;
use yii\base\Model;
use app\models\gii\xta_user;

/**
 * LoginForm is the model behind the login form.
 */
class PasswordForm extends Model
{
public $pass;
public $pass_repeat;

	public function rules()
	{
		return [
			[['pass', 'pass_repeat'], 'required','message'=>'Пожалуйста, заполните поле "{attribute}"'],
			[['pass'], 'string', 'min' => 6, 'tooShort'=>'Поле "{attribute}" не может быть короче 6 символов'],
			['pass', 'compare', 'message'=>'Пароли должны совпадать'],
			
		];
	}

	public function attributeLabels()
	{
		return [
			'pass'=>'Новый пароль',
			'pass_repeat'=>'Подтверждение нового пароля',
		];
	}


	public function changePassword()
	{
		if ($this->validate())
		{
			if (($model__xta_user = xta_user::findOne(Yii::$app->user->id)) !== null) 
			{
				$data = \Yii::$app->request->post('PasswordForm', []);
				$model__xta_user->pass = md5(md5($data['pass']));
				$model__xta_user->save();
				return true;
			}
		}
		return false;
	}
}
