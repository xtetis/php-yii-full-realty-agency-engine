<?php

namespace app\models;

use Yii;
use yii\base\Model;


/**
 * LoginForm is the model behind the login form.
 */
class LibCommon extends Model
{









# Функция для генерации  md5 хеша случайной строки
//****************************************************************************** 
public function fn__get_random_hash($len = 32) {
  $length = 15;
  $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPRQSTUVWXYZ0123456789";
  $code = "";
  $clen = strlen($chars) - 1;  
  while (strlen($code) < $length) {
    $code .= $chars[mt_rand(0,$clen)];  
  }
  $ret = md5($code);
  if ($len!=32){
     $ret = substr($ret,0,$len);
     }
  
  return $ret;
}
//****************************************************************************** 





	// Функция возвращает корректную страницу ()
	// Входные параметры
	// maxpage - Максимально возможная страница
	// getparamname - Имя GET параметра для получения номера текущей страницы
	//****************************************************************************** 
	public function fn__get_correct_page($maxpage=1, $getparamname='page'){ 
		$maxpage = intval($maxpage);
		if ($maxpage<1){$maxpage=1;}
		
		$page=intval($_GET[$getparamname]);
		if ($page>$maxpage){$page=1;}
		if ($page<1){$page=1;}

		return $page;
	}
	//******************************************************************************




	// Функция возвращает страницы пагинации
	// Входные параметры
	// url - префикс урла
	// totalpages - Всего страниц
	// curpage - текущая страница
	//****************************************************************************** 
	public function fn__get_html_pagination($url='/news?page=', $totalpages=1, $curpage=1){ 
		$curpage = intval($curpage);
		$totalpages = intval($totalpages);
		
		
		for ($i = 1; $i <=$totalpages ; $i++)
		{
		  if (($i<($curpage+5))&&($i>($curpage-5))){
		     $page_class="";
		     if ($i==$curpage)
		     {
		       $page_class="current_page";
		     }
		     $ret.='<a href="'.$url.$i.'" class="'.$page_class.'">'.$i.'</a>';
		     }
		}
		
		
		if ($totalpages)
		{
		$ret='
		<div class="paginationdiv">
			<div class="pages" style="display:inline; ">
				<a href="'.$url.'1" class="first_page"><i class="glyphicon glyphicon-fast-backward"></i></a>
				'.$ret.'
				<a href="'.$url.$totalpages.'" class="last_page"><i class="glyphicon glyphicon-fast-forward"></i></a>
			</div>
			<div style="display: none; float: right; width: 150px;">
				<div class="input-group">
					<input type="text" class="form-control" 
								 placeholder="На страницу"
								 id="input_gotopage">
					<span class="input-group-addon" 
								id="pagination_gotopage"
								totalpages="'.$totalpages.'"
								style="cursor:pointer;">
						<i class="glyphicon glyphicon-share-alt"></i>
					</span>
				</div>
			</div>
		</div>
	<script>
	$(function() {
		$("#pagination_gotopage").click(function() {
			var totalpages = $(this).attr("totalpages");
			var inputpage =  $("#input_gotopage").val();
			inputpage = parseInt(inputpage);
			totalpages = parseInt(totalpages);
			if ((inputpage>=1)&&(inputpage<=totalpages))
			{
				window.location.href=\''.$url.'\'+inputpage;
			}
		});
	});
	</script>
		';
		}
		else
		{
			$ret='';
		}

		return $ret;
	}
	//******************************************************************************





















	/*
		Получам строку фильтра по полученным параметрам
		price_min:4;price_max:4;opt12_set:2;opt22_set:2,4,5
	*/
	//****************************************************************************** 
	public function fn__get_filter_str($filters=array()){ 
		
		$filter_simple=array();
		if ((is_array($filters))&&(count($filters)))
		{
			foreach ($filters as $filters_key => $filters_value)
			{
				if ($filters_key=='price')
				{
					foreach ($filters_value as $key => $value)
					{
						$filter_simple[$filters_key.'_'.$key]=$value;
					}
				}
				if ($filters_key=='photo')
				{
					foreach ($filters_value as $key => $value)
					{
						$filter_simple[$filters_key.'_'.$key]=$value;
					}
				}
				elseif($filters_key=='opt')
				{
					foreach ($filters_value as $opt_id => $opt_vals)
					{
						foreach ($opt_vals as $opt_key => $opt_val_offset)
						{
							if (in_array($opt_key,array('min','max')))
							{
								$filter_simple[$filters_key.$opt_id.'_'.$opt_key]=intval($opt_val_offset);
							}
							elseif ($opt_key=='set')
							{
								if (strlen($opt_val_offset))
								{
									$arr_set_vals = explode(',',$opt_val_offset);
									$arr_set_vals_correct = array();
									if (count($arr_set_vals))
									{
										foreach ($arr_set_vals as $arr_set_vals_item)
										{
											if (intval($arr_set_vals_item))
											{
												$arr_set_vals_correct[]=intval($arr_set_vals_item);
											}
										}
									}
									if (count($arr_set_vals_correct))
									{
										$filter_simple[$filters_key.$opt_id.'_'.$opt_key]=implode(',',$arr_set_vals_correct);
									}
								}
							}
						}
					}
				}
			}
		}
		
		$filter_str_array = array();
		foreach ($filter_simple as $key => $value)
		{
			$filter_str_array[]=$key.':'.$value;
		}
		
		$ret = '';
		if (count($filter_str_array))
		{
			$ret = implode(';',$filter_str_array);
		}
		
		return $ret;
		
	}
	//******************************************************************************




















	/*
		Функция возвращает массив значений согласно полученной строке фильтра
		price_min:4;price_max:4;opt12_set:2;opt22_set:2,4,5
	*/
	//******************************************************************************
	public function fn__get_filter_arr($filters_str){ 
		$filters = array();
		$filters_arr = explode(';',$filters_str);
		foreach ($filters_arr as $filters_arr_item)
		{
			if (strlen($filters_arr_item))
			{
				$filter_val = explode(':',$filters_arr_item);
				if (
				     (count($filter_val)==2) &&
				     (strlen($filter_val[0])) &&
				     (strlen($filter_val[1]))
				   )
				{
					$filter_val_arr = explode(',',$filter_val[1]);
					$filter_val_arr_correct = array();
					foreach ($filter_val_arr as $filter_val_arr_item)
					{
						if (strval(intval($filter_val_arr_item)) == $filter_val_arr_item)
						{
							$filter_val_arr_correct[]=intval($filter_val_arr_item);
						}
					}
					
					$filter_key_arr = explode('_',$filter_val[0]);
					if (
					     (count($filter_key_arr)==2) &&
					     (strlen($filter_key_arr[0])) &&
					     (strlen($filter_key_arr[1]))
					   )
					{
						if (
						    ($filter_key_arr[0]=='price') &&
						    (count($filter_val_arr_correct))
						   )
						{
							$filters[$filter_key_arr[0]][$filter_key_arr[1]]=intval($filter_val_arr_correct[0]);
						}
						elseif (
						    ($filter_key_arr[0]=='photo') &&
						    (count($filter_val_arr_correct))
						   )
						{
							$filters[$filter_key_arr[0]][$filter_key_arr[1]]=intval($filter_val_arr_correct[0]);
						}
						else
						{
							$id_opt =$filter_key_arr[0];
							$id_opt = str_replace('opt','',$id_opt);
							
							if ((strval(intval($id_opt)) == $id_opt) && (intval($id_opt)))
							{
								if (
								    (in_array($filter_key_arr[1],array('min','max'))) &&
								    (count($filter_val_arr_correct))
								   )
								{
									$filters['opt'][$id_opt][$filter_key_arr[1]]=intval($filter_val_arr_correct[0]);
								}
								elseif(($filter_key_arr[1]=='set')&&(count($filter_val_arr_correct)))
								{
									$filters['opt'][$id_opt][$filter_key_arr[1]]=$filter_val_arr_correct;
								}
								
							}
						}
					}
				}
			}
		}
		
		return $filters;
	}
	//******************************************************************************




	/*
		Получам ссылку с учетом фильтров
	*/
	//****************************************************************************** 
	public function fn__get_board_link($id_category=0,$use_params=array(),$add_params=array()){ 
		$ret = '/board';

		if ($id_category)
		{
			$ret.= '/'.$id_category;
		}
		
		$params = array();
		if (in_array('sort',$use_params))
		{
			if ((isset($_GET['sort']))&&(in_array($_GET['sort'],array('lowprice','highprice'))))
			{
				$params['sort']=$_GET['sort'];
			}
		}

		if (in_array('district',$use_params))
		{
			if (isset($_GET['district']))
			{
				$params['district']=$_GET['district'];
			}
		}
		
		
		if ((in_array('filter',$use_params)) && (isset($_GET['filter'])) && (strlen($_GET['filter'])))
		{
			$params['filter']=$_GET['filter'];
		}


		$params = array_merge($params,$add_params);
		if (count($params))
		{
			$params_arr = array();
			foreach ($params as $key => $value)
			{
				$params_arr[] = $key.'='.$value;
				
			}
			$params_arr_str = implode('&',$params_arr);
			$ret.='?'.$params_arr_str;
		}
		
		return $ret;
	}
	//******************************************************************************




// Функция удаляет из строки все кроме цифр
//****************************************************************************** 
function fn__get_only_numbers($string=''){
  return preg_replace("/\D/","",$string);
}
//****************************************************************************** 



//******************************************************************************
function fn__getAge($y, $m, $d)
{
	if($m > date('m') || $m == date('m') && $d > date('d'))
		return (date('Y') - $y - 1);
	else
		return (date('Y') - $y);
}
//******************************************************************************




//******************************************************************************
  function fn__get_zodiac($month, $day) {
    $signs = array("Козерог", "Водолей", "Рыбы", "Овен", "Телец", "Близнецы", "Рак", "Лев", "Девы", "Весы", "Скорпион", "Стрелец");
    $signsstart = array(1=>21, 2=>20, 3=>20, 4=>20, 5=>20, 6=>20, 7=>21, 8=>22, 9=>23, 10=>23, 11=>23, 12=>23);
    return $day < $signsstart[$month + 1] ? $signs[$month - 1] : $signs[$month % 12];
  }
//******************************************************************************



//******************************************************************************
function fn__num2word($num, $words)
{
    $num = $num % 100;
    if ($num > 19) {
        $num = $num % 10;
    }
    switch ($num) {
        case 1: {
            return($words[0]);
        }
        case 2: case 3: case 4: {
            return($words[1]);
        }
        default: {
            return($words[2]);
        }
    }
}
//******************************************************************************





// Функция врзвращает текст с привязкой к ID
//****************************************************************************** 
function fn__get_generated_text_by_uid($text,$id){
  $num = LibCommon::fn__get_only_numbers(md5($id));
  $j=1;
	while ( preg_match( '#\{([^\{\}]+)\}#i', $text, $m ) ) {
		$v = explode( '|', $m[1] );
		$i = $num%count( $v );
		$num.=$num%$j;
		$text = preg_replace( '#'.preg_quote($m[0]).'#i', $v[$i], $text, 1 );
		$j++;
	}
 return $text;
}
//****************************************************************************** 




}
