<?php

namespace app\models;

use Yii;
use yii\base\Model;
use app\models\gii\xta_user;
use app\models\gii\xta_setting;
use app\models\gii\xta_site;
use app\models\gii\xta_district;
use app\models\LibSql;


/**
 * LoginForm is the model behind the login form.
 */
class BoardAddForm extends Model
{

public $id_obj;

public $id_district;
public $id_category;
public $id_user;
public $id_album;
public $id_valuta;
public $name;
public $about;
public $images;
public $phone;
public $skype;
public $price;
public $username;
public $email;


	public function rules()
	{
		$rules=[
			[['phone','name', 'id_district', 'id_category', 'id_valuta','about'], 'required','message'=>'Пожалуйста, заполните поле "{attribute}"'],
			[['name', 'id_district', 'id_category', 'id_valuta', 'about', 'price'], 'trim'],
			[['name'], 'string', 'encoding'=>'UTF-8', 'min' => 15, 'tooShort'=>'Поле "{attribute}" не может быть короче 15 символов'],
			[['about'], 'string', 'encoding'=>'UTF-8', 'min' => 50, 'tooShort'=>'Поле "{attribute}" не может быть короче 50 символов'],
			[['phone', 'price'], 'trim'],
			[['images'], 'safe'],
			['phone', 'match', 'pattern' => '/^[0-9]{6,12}$/i','message'=>'Поле "Телефон" может содержать только цифры'],
			[['phone'], 'string', 'encoding'=>'UTF-8', 'min' => 6, 'tooShort'=>'Поле "{attribute}" не может быть короче 6 символов'],
			['price', 'match', 'pattern' => '/^[0-9]{0,12}$/i','message'=>'Поле "{attribute}" может содержать только цифры'],
			['id_category', 'exist', 'targetClass' => 'app\models\gii\xta_obj_category', 'targetAttribute'=>'id' , 'message'=>'Неверная категория'],
			['about', 'fn__content_stop_words'],
			['name', 'fn__content_stop_words'],
			['about', 'fn__validate_duplicate'],
			['name', 'fn__validate_duplicate'],
		];



		$rules_guest = [
			[['username', 'email'], 'required','message'=>'Пожалуйста, заполните поле "{attribute}"'],
			[['skype', 'username', 'email'], 'trim'],
			[['username'], 'string', 'encoding'=>'UTF-8', 'min' => 3, 'tooShort'=>'Поле "{attribute}" не может быть короче 3 символов'],
			[['email'], 'string', 'min' => 6, 'tooShort'=>'Поле "{attribute}" не может быть короче 6 символов'],
			[['skype'], 'string', 'encoding'=>'UTF-8', 'min' => 3, 'tooShort'=>'Поле "{attribute}" не может быть короче 3 символов'],
			['email', 'email', 'message'=>'Поле "{attribute}" не являетс корректным email адресом'],
			['email', 'unique', 'targetClass' => 'app\models\gii\xta_user', 'targetAttribute'=>'email' , 'message'=>'Этот Email уже зарегистрирован'],

			['skype', 'match', 'pattern' => '/^[a-z][a-z0-9\.,\-_]{5,31}$/i','message'=>'Поле "{attribute}" не являетс корректным логином Skype'],
		];
		
		if (Yii::$app->user->isGuest)
		{
			$rules = array_merge($rules,$rules_guest);
		}
		return $rules;
	}
	
	
	public function fn__content_stop_words($attribute,$params)
	{
		$stop_words = xta_setting::fn__get_setting('obj_stop_words');
		$stop_words = explode(',',$stop_words);
		foreach ($stop_words as $stop_word) {
			$stop_word = trim($stop_word);
			if (
			     (strlen($stop_word)) &&
			     (strpos($this->$attribute,$stop_word))
			   )
			{
				//return false
				$this->addError($attribute, 'Поле содержит стоп слово "'.$stop_word.'"');
				break;
			}
		}
	}


	public function fn__validate_duplicate($attribute,$params)
	{
		if (\Yii::$app->user->isGuest)
		{
			return true;
		}
		$count = LibSql::fn__get_count_by_where('xta_obj',"`id_user`=".Yii::$app->user->id." AND `".$attribute."` = ".LibSql::sql_valid($this->$attribute)." AND id NOT IN (SELECT id_obj FROM xta_obj_abuse)");
		if (
		    ($count) &&
		    (strpos($_SERVER['REQUEST_URI'],'/board/myad/edit/')===false)
		   )
		{
			$this->addError($attribute, 'У вас уже есть такое объявление. Обновите его публикацию <a href="http://krgazeta.lc/account/obj">в разделе "Мои объявления"</a>. Одинаковые объявления удаляются.');
		}
	}
	
	
	
    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'id_district' => 'Район города',
            'name'        =>'Заголовок объявления',
            'about'       =>'Текст объявления',
            'price'       =>'Цена',
            'phone'       =>'Номер телефона',
            'id_valuta'   =>'Валюта',
            'id_category' =>'Рубрика',
            'username'    =>'Контактное лицо',
            'email'=>'Email',
            'skype'=>'Skype',
            'images'=>'Изоражения',
        ];
    }



	public function fn__validate_options()
	{
		return true;
		//$id_category
		//print_r($_POST);
		//return false;
	}
	

	
}
