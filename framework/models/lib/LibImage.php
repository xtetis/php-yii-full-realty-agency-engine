<?php

namespace app\models;

use Yii;
use yii\base\Model;
use app\models\gii\xta_album;


/**
 * LoginForm is the model behind the login form.
 */
class LibImage extends Model
{


	public function prepareText()
	{
		$this->textbody = str_replace('+http_host+',$_SERVER['HTTP_HOST'],$this->textbody);
		$this->subject = str_replace('+http_host+',$_SERVER['HTTP_HOST'],$this->subject);
		$this->textbody = str_replace('+link_autologin+',Url::to(['account/login']).'?goto=',$this->textbody);
		
		
		$autologin_link_array = array(
		'+link_autologin+'=>$this->fn__get_autologin_link($this->to),
		'+link_autologin_my_obj+'=>$this->fn__get_autologin_link($this->to,Url::to(['account/obj'])),
		'+link_autologin_my_messages+'=>$this->fn__get_autologin_link($this->to,Url::to(['account/messages'])),
		'+link_autologin_my_settings+'=>$this->fn__get_autologin_link($this->to,Url::to(['account/settings'])),
		);
		
		if (is_array($replacedata))
		{
			$replacedata = array_merge($replacedata,$autologin_link_array);
		}
		else
		{
			$replacedata = $autologin_link_array;
		}
		
		if (is_array($replacedata))
		{
			foreach ($replacedata as $key => $value)
			{
				$this->textbody = str_replace('+'.$key.'+',$value,$this->textbody);
			}
		}
	}


	public function sendEmail()
	{
		if (!$this->from)
		{
			$this->from = array('noreply@'.$_SERVER['HTTP_HOST'] => 'Сайт '.$_SERVER['HTTP_HOST']);
		}
		\Yii::$app->mailer->compose()->setTo($this->to)
		->setFrom($this->from)->setSubject($this->subject)->setTextBody($this->bodytpl)->send();
	}


	public function fn__get_autologin_link($email,$link='')
	{
		$linkb64 = base64_encode($link);
		$user_info = User::findByEmail($email);
		if (($model__xta_user = xta_user::findOne($user_info->id)) !== null) 
		{
			$accesstoken = md5($user_info->authkey.$user_info->email.$user_info->pass);
			$model__xta_user->accesstoken = $accesstoken;
			$model__xta_user->save();
			$autologin_link = Url::to(['account/login/'.$accesstoken]);
			if (strlen($link))
			{
				$autologin_link.='?url='.$linkb64;
			}
		}
		else
		{
			$autologin_link = $link;
		}
		
		return $autologin_link;
	}
}
