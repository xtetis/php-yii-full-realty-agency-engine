<?php

namespace app\models;

use Yii;
use yii\base\Model;
use app\models\gii\xta_user;
use app\models\User;
use app\models\PrepareEmail;


/**
 * LoginForm is the model behind the login form.
 */
class ForgotPassForm extends Model
{
	public $email;

	public function rules()
	{
		return [
			[['email'], 'required','message'=>'Пожалуйста, заполните поле "{attribute}"'],
			['email', 'email','message'=>'Поле "{attribute}" не являетс корректным email адресом'],
			[['email'], 'exist', 'targetClass' => 'app\models\gii\xta_user', 'targetAttribute'=>'email', 'message'=>'Этот Email не зарегистрирован'],
		];
	}


	public function attributeLabels()
	{
		return [
			'email' => 'Email пользователя',
		];
	}

	public function sendpass()
	{
		$user = User::findByEmail($this->email);
		if (!$user)
		{
			$this->addError('email', 'Такой Email не зарегистрирован');
		}
		else
		{
			$tpl='
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="viewport" content="width=device-width" />

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Восстановление пароля на сайте +http_host+</title>
</head>
<body>
<h3>Здравствуйте, +fullname+</h3>
<p class="lead">На сайте +http_host+ Вы воспользовались формой восстановления пароля:</p>
<table style="width:100%;">
  <tr>
    <td>Время отправления:</td>
    <td>+date_now+</td>
  </tr>
  <tr>
    <td>Для изменения пароля воспользуйтесь ccылкой:</td>
    <td><a href="+link_autologin_my_settings+">Изменить пароль</a></td>
  </tr>
</table>
<br>
<br>
<table style="width:100%;">
  <tr>
    <td><a href="+link_autologin+">+http_host+</a></td>
    <td><a href="+link_autologin_my_obj+">Мои объявления</a></td>
    <td><a href="+link_autologin_my_messages+">Мои сообщения</a></td>
  </tr>
</table>
</body>
</html>
			';
			$model_PrepareEmail = new PrepareEmail();
			$model_PrepareEmail->to = $user->email;
			$model_PrepareEmail->textbody = $$tpl;
			$model_PrepareEmail->subject = 'Восстановление пароля на сайте +http_host+';
			$model_PrepareEmail->prepareText();
			$model_PrepareEmail->sendEmail();
			return true;
		}
		return false;
	}
}
