<?php

namespace app\models;

use Yii;
use yii\base\Model;
use app\models\gii\xta_user;
use app\models\gii\xta_setting;
use app\models\gii\xta_site;
use app\models\gii\xta_district;


/**
 * LoginForm is the model behind the login form.
 */
class LoveAnketaForm extends Model
{


public $id_anketa;
public $name;
public $status;
public $about_me;
public $search_about;
public $search_age_start;
public $search_age_end;
public $family_status;
public $child_status;
public $birthdate;




	public function rules()
	{
		$rules=[
			[['name',
			  'status',
			  'about_me',
			  'search_about',
			  'search_age_start',
			  'search_age_end',
			  'family_status',
			  'child_status',
			  'birthdate'], 'required','message'=>'Пожалуйста, заполните поле "{attribute}"'],
			[['name',
			  'status',
			  'about_me',
			  'search_about',
			  'search_age_start',
			  'search_age_end',
			  'family_status',
			  'child_status',
			  'birthdate'], 'trim'],
			[['name'], 'string', 'encoding'=>'UTF-8', 'min' => 3, 'tooShort'=>'Поле "{attribute}" не может быть короче 3 символа'],
			[['about_me','search_about'], 'string', 'encoding'=>'UTF-8', 'min' => 15, 'tooShort'=>'Поле "{attribute}" не может быть короче 15 символов'],
			[['name'], 'fn__content_stop_words'],
		];


		return $rules;
	}
	
	
	public function fn__content_stop_words($attribute,$params)
	{
		$stop_words = xta_setting::fn__get_setting('obj_stop_words');
		$stop_words = explode(',',$stop_words);
		foreach ($stop_words as $stop_word) {
			$stop_word = trim($stop_word);
			if (
			     (strlen($stop_word)) &&
			     (strpos($this->$attribute,$stop_word))
			   )
			{
				//return false
				$this->addError($attribute, 'Поле содержит стоп слово "'.$stop_word.'"');
				break;
			}
		}
	}
	
	
	
	
	public function attributeLabels()
	{
		return [
			'name' => 'Отображаемое имя',
			'status' => 'Ваш статус',
			
		];
	}




	
}
