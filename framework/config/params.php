<?php

return [
	'adminEmail' => 'tihonenkovaleriy@gmail.com', // Email администратора
	'per_pages' => 10,  // Количество элементов на страницу
	'count_days_obj_active' => 120,  // Количество дней, когда объявление активно
];
