<?php
namespace app\components;

use yii\base\Widget;
use app\models\gii\xta_obj;
use app\models\gii\xta_site;
use app\models\gii\xta_obj_category;
use app\models\gii\xta_image;
use app\models\gii\xta_cache;
use yii\db\Query;


class FrontendLastViewObj extends Widget{
	
	public function init(){
		parent::init();
	}
	
	public function run(){
		$data = array();
/*
		// Cache start
		//==============================================================================
		$id_cache = 'FrontendLastViewObj';
		$cache = xta_cache::fn__get_cache($id_cache);
		if ($cache===false):
		//==============================================================================
		
			$where = '
			xta_obj.id NOT IN (SELECT id_obj FROM xta_obj_abuse) 
			AND 
			xta_obj.published=1 
			AND 
			xta_obj.pub_date >= (NOW() - INTERVAL 60 DAY ) 
			AND
			xta_obj.id_city = '.xta_site::getCurrentSite()->id_city.'
			AND
			xta_obj.id <> '.$this->model__xta_obj->id.'
			';
			$orderBy = array('RAND()' => SORT_DESC);
			$query = new Query;
			$query->select(['xta_obj.id',
						          'xta_obj.name',
						          'xta_obj.id_category',
						          'xta_obj.pub_date',
						          'xta_obj.price',
						          'xta_valuta.shortname as valutaname', 
						          'xta_city.name as cityname',
						          'xta_album.id_image',
						         ]);
			$query->from('xta_obj');
			$query->join('LEFT JOIN', 'xta_valuta', 'xta_valuta.id = xta_obj.id_valuta');
			$query->join('LEFT JOIN', 'xta_city', 'xta_city.id = xta_obj.id_city');
			$query->join('LEFT JOIN', 'xta_album', 'xta_album.id = xta_obj.id_album');
			$query->where($where);
			$query->limit(4);
			$query->offset(0);
			$query->orderBy($orderBy);
			$command = $query->createCommand();
			$select = $command->queryAll();

			$data['select']=$select;
		
			foreach ($select as $item)
			{
				$model__xta_obj_category = xta_obj_category::find()->where(["id" => $item['id_category']])->one();
				$category_array = $model__xta_obj_category->fn__get_categs_array();
				$category_names = array();
				foreach ($category_array as $key1 => $value1)
				{
					$category_names[]='<a href="/board/'.$key1.'">'.$value1.'</a>';
				}
				$category_names = implode(' → ',$category_names);
				$data['category_names'][$item['id']]=$category_names;
			
			
				$data['image'][$item['id']] = xta_image::fn__get_nosrc();
				if ($item['id_image'])
				{
					$model__xta_image = xta_image::find()->where(["id" => $item['id_image']])->one();
					if ($model__xta_image)
					{
						$data['image'][$item['id']] = $model__xta_image->fn__get_thumb_src('small');
					}
				}
			
			}
		// Cache end
		//==============================================================================
			xta_cache::fn__set_cache($id_cache,$data,array('xta_obj','FrontendRelatedObj'));
		else:
			$data = $cache;
		endif;
		//==============================================================================
		*/
		return $this->render('view__FrontendLastViewObj',$data);
	}
}
?>

