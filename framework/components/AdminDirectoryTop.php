<?php
namespace app\components;

use yii\base\Widget;

class AdminDirectoryTop extends Widget{
    public $params = array(
        'action_name'=>'',
        'show_new'=>true,
        'show_all'=>true,
        'show_view'=>false,
        'show_link'=>'/',
        'title' => 'Статьи',
        'other_buttons' => array(),
    );

	public function init(){
		parent::init();
		if($this->params===null){
			$this->params = array(
        'action_name'=>'',
        'show_new'=>true,
        'show_all'=>true,
        'show_view'=>false,
        'show_link'=>'/',
        'title' => 'Статьи',
        'other_buttons' => array(),
   												 );
		}else{
			//print_r($this->params['other_buttons']); exit;
      $this->params['action_name'] = (isset($this->params['action_name'])?$this->params['action_name']:'');
      $this->params['show_new'] = (isset($this->params['show_new'])?$this->params['show_new']:true);
      $this->params['show_all'] = (isset($this->params['show_all'])?$this->params['show_all']:true);
      $this->params['show_view'] = (isset($this->params['show_view'])?$this->params['show_view']:false);
      $this->params['show_link'] = (isset($this->params['show_link'])?$this->params['show_link']:'/');
      $this->params['title'] = (isset($this->params['title'])?$this->params['title']:'Статьи');
      $this->params['other_buttons'] = (isset($this->params['other_buttons'])?$this->params['other_buttons']:array());
      if (!is_array($this->params['other_buttons']))
      {
      	$this->params['other_buttons'] = array();
      }
		}
	}

	public function run(){
	//print_r($this->params); exit;
		$data = array();
		$data['params'] = $this->params;
		return $this->render('view__AdminDirectoryTop',$data);
	}
}
?>

