<?php
namespace app\components;

use yii\base\Widget;
use app\models\mgii\mgii_xta_love_anketa;
use app\models\mgii\mgii_xta_love_type_account;
use yii\db\Query;


class FrontendLoveSearchForm extends Widget{
	
	public function init(){
		parent::init();
	}
	
	public function run(){
		$data = array();
		
		$data['type'] = intval($_GET['type']);
		$data['age_start']            = intval($_GET['age_start']);
		$data['age_end']              = intval($_GET['age_end']);
		if (!$data['age_end'])
		{
			$data['age_end'] = 98;
		}
		$data['family']               = intval($_GET['family']);
		$data['photo']                = intval($_GET['photo']);

		
		return $this->render('view__FrontendLoveSearchForm',$data);
	}
}
?>

