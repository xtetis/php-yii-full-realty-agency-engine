<?php
use yii\helpers\Url;
?>
    
    
<nav class="navbar navbar-default navbar-static-top">
  <div class="container" style="display: ;">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" target="frontend_window" href="/"><?=ucfirst($_SERVER['HTTP_HOST'])?></a>
    </div>
    
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class="dropdown">
          <a href="<?=Url::toRoute('/board')?>" class="dropdown-toggle bootstrap_hover_dropdown" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-info-sign"></span> Справочники<span class="caret"></span></a>
          <ul class="dropdown-menu">

            <li><a href="<?=Url::toRoute('/admin/xta_country')?>">Страны</a></li>
            
            <li><a href="<?=Url::toRoute('/admin/xta_padeg')?>">Падежи</a></li>
            <li><a href="<?=Url::toRoute('/admin/xta_valuta')?>">Валюта</a></li>
            <li><a href="<?=Url::toRoute('/admin/xta_region')?>">Регионы</a></li>
            <li><a href="<?=Url::toRoute('/admin/xta_city')?>">Города</a></li>
            <li><a href="<?=Url::toRoute('/admin/xta_district')?>">Районы</a></li>
            
            <li><a href="<?=Url::toRoute('/admin/xta_obj_category')?>">Категории объявлений</a></li>
            <li><a href="<?=Url::toRoute('/admin/xta_obj_option')?>">Опции объявлений</a></li>
            <li><a href="<?=Url::toRoute('/admin/xta_obj_abuse')?>">Жалобы на объявления</a></li>
            <li><a href="<?=Url::toRoute('/admin/xta_page')?>">Текстовые страницы</a></li>
            <li><a href="<?=Url::toRoute('/admin/xta_user_album')?>">Альбомы</a></li>
            
            
            
            
            
            
            
          </ul>
        </li>
        


        <li class="dropdown">
          <a href="#" class="dropdown-toggle bootstrap_hover_dropdown" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-user"></span> Пользователи<span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="<?=Url::toRoute('/admin/xta_user')?>">Юзеры</a></li>
            <li><a href="<?=Url::toRoute('/admin/xta_user_role')?>">Роли пользователя</a></li>
          </ul>
        </li>




        <li class="dropdown">
          <a href="#" class="dropdown-toggle bootstrap_hover_dropdown" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-circle-arrow-down"></span> Модули<span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="<?=Url::toRoute('/admin/mod_sitemap')?>">Карта сайта</a></li>
          </ul>
        </li>




        <li class="dropdown">
          <a href="#" class="dropdown-toggle bootstrap_hover_dropdown" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-cog"></span> Система<span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="<?=Url::toRoute('/admin/cache')?>">Кеш</a></li>
            <li><a href="<?=Url::toRoute('/admin/xta_site')?>">Сайты</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="<?=Url::toRoute('/admin/xta_setting')?>">Настройки</a></li>
            <li><a href="<?=Url::toRoute('/admin/xta_setting_category')?>">Категории настроек</a></li>
          </ul>
        </li>
      </ul>


      <ul class="nav navbar-nav navbar-right" style="margin-right:-5px;">
        <li class="dropdown">
          <a href="<?=((Yii::$app->user->isGuest)?Url::toRoute('account/login'):'')?>" 
             class="dropdown-toggle bootstrap_hover_dropdown" aria-haspopup="true" aria-expanded="false">
          <?=((Yii::$app->user->isGuest)?' <span class="glyphicon glyphicon-log-in"></span> Авторизация':Yii::$app->user->identity->email)?> 
          <?=((Yii::$app->user->isGuest)?'':'<span class="caret"></span>')?> 
          </a>

          <ul class="dropdown-menu" style="width: 210px;">
            <li><a href="#">Мои объявления</a></li>
            <li><a href="#">Мои сообщения</a></li>
            <li><a target="frontend_window" href="<?=Url::toRoute('account/settings')?>">Настройки</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="<?=Url::toRoute('account/logout')?>">Выйти</a></li>
          </ul>
        </li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div>
</nav>
