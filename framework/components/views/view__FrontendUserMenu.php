<?
use yii\helpers\Url;
?>






<nav class="navbar navbar-inverse " style="margin-top:10px;">
  <div class="container-fluid" style="padding:0px;">
    <!-- Brand and toggle get grouped for better mobile display -->

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li><a href="/">Главная </a></li>
        
        <li class="dropdown">
          <a href="/" class="dropdown-toggle bootstrap_hover_dropdown" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">О компании <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="/page/1">Миссия</a></li>
            <li><a href="/page/2">Команда</a></li>
            <li><a href="/page/3">Наши партнеры</a></li>
            <li><a href="/page/4">Контакты</a></li>
            <li><a href="/page/5">Вакансии</a></li>
          </ul>
        </li>
        <li class="dropdown">
        	<a href="/page/6" class="dropdown-toggle bootstrap_hover_dropdown" data-toggle="dropdown" role="button" onclick="window.location = '/page/6'">Услуги <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="/page/6">Наши услуги</a></li>
            <li><a href="/page/11">Юридические услуги</a></li>
            <li><a href="/page/design">Услуги дизайнера</a></li>
          </ul>
        </li>
        
        
        <li class="dropdown">
        	<a href="/board" class="dropdown-toggle bootstrap_hover_dropdown" data-toggle="dropdown" role="button" onclick="window.location = '/board'">Каталог недвижимости <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="/board/archive">Архив объектов недвижимости</a></li>
          </ul>
					
        </li>
        <li class="dropdown">
          <a href="/" class="dropdown-toggle bootstrap_hover_dropdown" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Информация <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="/page/7">Гид по аренде</a></li>
            <li><a href="/page/8">Гид по продаже</a></li>
            <li><a href="/page/9">Новости</a></li>
            <li><a href="/page/10">Стоимость услуг</a></li>
          </ul>
        </li>
        <li><a href="/page/4">Контакты</a></li>
        
        
		<?if (!Yii::$app->user->isGuest):?>
		
        <li class="dropdown">
          <a href="/" class="dropdown-toggle bootstrap_hover_dropdown" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Аккаунт <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li class="menu-item dropdown dropdown-submenu">
            	<a href="/board/myad">Мои объекты</a>
            	<ul class="dropdown-menu ">
								<li><a href="/board/myad">Активные (<?=$count_published?>)</a></li>
								<li><a href="/board/myad/archive">Архивные (<?=$count_archive?>)</a></li>
								<li><a href="/board/myad/unpublished">Снятые с публикации (<?=$count_unpublished?>)</a></li>
								<li><a href="/board/myad/blocked">Заблокорованные (<?=$count_blocked?>)</a></li>
	            </ul>
            </li>
            <li class="menu-item dropdown dropdown-submenu">
            	<a href="/message">Мои сообщения</a>
            	<ul class="dropdown-menu ">
								<li>
									<a href="/message">Все сообщения</a></li>
								</li>
								<li>
									<a href="/message/unreaded">Непрочитанные (<?=$unreaded_messages_count?>)</a>
								</li>
	            </ul>
            </li>
            <li><a href="/board/add">Добавить объект</a></li>
            <li class="menu-item dropdown dropdown-submenu">
            	<a href="/setting">Настройки</a>
				      <ul class="dropdown-menu ">
								<li>
									<a href="<?=Url::toRoute('/setting')?>">Общие</a></li>
								</li>
								<li>
									<a href="<?=Url::toRoute('/setting/password')?>">Смена пароля</a></li>
								</li>
				      </ul>
            </li>
            <li role="separator" class="divider"></li>
            <li><a href="<?=Url::toRoute('account/logout')?>">Выйти</a></li>
						
          </ul>
        </li>
		
		<?else:?>
			<li><a href="<?=Url::toRoute('account/login')?>">Авторизация</a></li>
		<? endif; ?>
		
		
		
		
		
      </ul>
     
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>














