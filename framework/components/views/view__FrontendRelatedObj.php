<div class="panel panel-default">
	<div class="panel-heading">Похожие объявления </div>
	<div class="panel-body" style="padding: 8px;">
		<?foreach ($select as $item):?>
			<div class="panel panel-default" style="margin-bottom: 10px;">
				<div class="panel-body row" style="padding: 10px;">
					<div class="col-md-2" style="font-size: 13px; width: 11%;"><?=$item['pub_date']?></div>
					<div class="col-md-1 obj_img" style="width: 11%;">
						<img src="<?=$image[$item['id']]?>">
					</div>
					<div class="col-md-7">
						<div>
							<a href="/board/item/<?=$item['id']?>"><?=$item['name']?></a>
						</div>
						<div class="category_names">
							<?=$category_names[$item['id']]?>
						</div>
					</div>
					<div class="col-md-2" style="text-align: right; font-weight: bold;">
						<?if (intval($item['price'])):?>
							<?=$item['price']?> <?=$item['valutaname']?>
						<?endif;?>
					</div>
				</div>
			</div>
		<?endforeach;
		?>
	</div>
</div>
