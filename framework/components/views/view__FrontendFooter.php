<footer class="footer">
	<p class="pull-left">
		Copyright &copy; 2014 - <?= date('Y') ?> Все права защищены
	</p>

	<p class="pull-right">
		<ul class="list-inline pull-right">
			<li><a href="#">Условия использования</a></li>
			<li><a href="#">Помощь</a></li>
			<li><a href="#">Обратная связь</a></li>
			<li><a href="#">Реклама на сайте</a></li>
			<li><a href="#">О сайте</a></li>
		</ul>
	</p>
</footer>
