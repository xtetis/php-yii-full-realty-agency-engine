    <?php
use yii\helpers\Url;
use app\models\gii\xta_user_message;
/*
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;

    NavBar::begin([
        'brandLabel' => ucfirst($_SERVER['HTTP_HOST']),
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-static-top',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            ['label' => 'Home', 'url' => ['/site/index']],
            ['label' => 'About', 'url' => ['/site/about']],
            ['label' => 'Contact', 'url' => ['/site/contact']],
            Yii::$app->user->isGuest ?
                ['label' => 'Login', 'url' => ['/site/login']] :
                [
                    'label' => 'Logout (' . Yii::$app->user->identity->email . ')',
                    'url' => ['/site/logout'],
                    'linkOptions' => ['data-method' => 'post']
                ],
        ],
    ]);
    NavBar::end();
*/
    ?>
    
    
<nav class="navbar navbar-default navbar-static-top">
  <div class="_container_" style="display: ;">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="/"><?=ucfirst($_SERVER['HTTP_HOST'])?></a>
    </div>
    
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class="dropdown">
          <a href="/board"class=""><span class="glyphicon glyphicon-th-list"></span> Недвижимость</a>
        </li>
        
        
        
      </ul>


      <ul class="nav navbar-nav navbar-right" style="margin-right:0px;">
        <li style=""><a>Одесса</a></li>
<?php
if (xta_user_message::fn__is_new_message())
{
?>
<li title="Новые сообщения"><a href="/message/unreaded"><img src="/public/templates/default/img/ico/i_newmail.gif"></a></li>
<?
}
?>
        <li class="dropdown">
          <a href="<?=((Yii::$app->user->isGuest)?Url::toRoute('account/login'):'')?>" 
             class="dropdown-toggle bootstrap_hover_dropdown" aria-haspopup="true" aria-expanded="false">
          <?=((Yii::$app->user->isGuest)?' <span class="glyphicon glyphicon-log-in"></span> Авторизация':Yii::$app->user->identity->email)?> 
          <?=((Yii::$app->user->isGuest)?'':'<span class="caret"></span>')?> 
          </a>

          <ul class="dropdown-menu" style="width: 210px;<?=((Yii::$app->user->isGuest)?'display:none;"':'')?>">
            <li><a href="/board/myad">Мои объекты</a></li>
            <li><a href="/message">Мои сообщения</a></li>
            <li><a href="/board/add">Добавить объект</a></li>
            <li><a href="/setting">Настройки</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="<?=Url::toRoute('account/logout')?>">Выйти</a></li>
          </ul>
        </li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div>
</nav>
