<?php
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use app\models\mgii\mgii_xta_love_anketa;
use app\models\mgii\mgii_xta_love_type_account;

?>

<?php $form = ActiveForm::begin([
										'id' => 'love-search-form',
										'method' => 'get',
										'action' => '/love/search',
										]); ?>
		<div class="panel panel-default">
			<div class="panel-body" style="background: #f2dede; border-color: #ebccd1;" >
				<div class="row">
				
					<div class="col-md-3 form-group">
						<label>Я ищу</label>
						<?=Html::dropDownList('type', $type, mgii_xta_love_type_account::fn__get_data_list(true,3),array('class'=>'form-control')) ?>
						
					</div>
				
					<div class="col-md-4 form-group">
						<label>Возраст</label>
						<div>
							от 
<?
							$arr = array();
							for ($i = 1; $i < 99; $i++) {
							$arr[$i] = $i;
							}
						echo Html::dropDownList('age_start', $age_start, $arr,array('class'=>'form-control','style'=>'display: inline; width: 70px; margin-left: 10px; margin-right: 10px;')) 
?>
							до

<?
							$arr = array();
							for ($i = 1; $i < 99; $i++) {
							$arr[$i] = $i;
							}
						echo Html::dropDownList('age_end', $age_end, $arr,array('class'=>'form-control','style'=>'display: inline; width: 70px; margin-left: 10px; margin-right: 10px;')) 
?>

							лет
						</div>
					</div>
				
				
					<div class="col-md-3 form-group">
						<label>Семейное положение</label>
						<?=Html::dropDownList(
									'family', 
									$family, 
									mgii_xta_love_anketa::fn__get_family_status_list(true),
									array('class'=>'form-control')); ?>
					</div>
					
					<div class="col-md-2 form-group">
						<label>С фото</label>
						<div style="padding-left: 20px;padding-top: 5px;">
							<?=Html::checkbox('photo',$photo,['value'=>'1']);?>
						</div>
					</div>
				
				</div>
				
				
				<div class="row" style="margin-top: 20px;">
					<div class="col-md-12" style="text-align: center;">
						<button type="submit" class="btn btn-primary">Найти</button>
					</div>
				</div>
				


			</div>
		</div>
<?php ActiveForm::end(); ?>
