<?php
namespace app\components;

use yii\base\Widget;
use yii\helpers\Html;
use Yii;
use yii\db\Query;
use app\models\gii\xta_user_message;
$connection = \Yii::$app->db;

class FrontendUserMenu extends Widget{
	public $message;
	
	public function init(){

	/*
		parent::init();
		if($this->message===null){
			$this->message= 'Welcome User';
		}else{
			$this->message= 'Welcome '.$this->message;
		}
	*/
	}
	
	public function run(){
		$connection = \Yii::$app->db;
		$data = array();
		if (Yii::$app->user->isGuest)
		{
			//return '';
		}
		
		
		
		
		if (!Yii::$app->user->isGuest):
			//========================================================================
			$sql = "
			SELECT count(*) as 'count'  FROM 
			(
				SELECT DISTINCT
				`id_user_from`
				FROM 
				`xta_user_message`
				WHERE 
				`id_user_to` =".Yii::$app->user->id."
				AND `readed`=0
			) as t2";


			$model_count = $connection->createCommand($sql);
			$count_arr = $model_count->queryAll();
			$data['unreaded_messages_count'] = $count_arr[0]['count'];
			//========================================================================
			
			
			
		
			//========================================================================
			$query = new Query;
			$query->select(['count(*) as count']);
			$query->from('xta_obj');
			$query->where("id_user = ".Yii::$app->user->id." AND published=1 AND pub_date >= (NOW() - INTERVAL ".Yii::$app->params['count_days_obj_active']." DAY )  AND xta_obj.id NOT IN (SELECT id_obj FROM xta_obj_abuse)");
			$dataQuery = $query->createCommand()->queryOne();
			$data['count_published']  = $dataQuery['count'];
		
			$query = new Query;
			$query->select(['count(*) as count']);
			$query->from('xta_obj');
			$query->where("id_user = ".Yii::$app->user->id." AND published=0 AND xta_obj.id NOT IN (SELECT id_obj FROM xta_obj_abuse)");
			$dataQuery = $query->createCommand()->queryOne();
			$data['count_unpublished']  = $dataQuery['count'];
		
			$query = new Query;
			$query->select(['count(*) as count']);
			$query->from('xta_obj');
			$query->where("id_user = ".Yii::$app->user->id." AND published=1 AND (pub_date < NOW() - INTERVAL ".Yii::$app->params['count_days_obj_active']." DAY ) AND xta_obj.id NOT IN (SELECT id_obj FROM xta_obj_abuse) ");
			$dataQuery = $query->createCommand()->queryOne();
			$data['count_archive']  = $dataQuery['count'];
		
			$query = new Query;
			$query->select(['count(*) as count']);
			$query->from('xta_obj');
			$query->where("id_user = ".Yii::$app->user->id." AND ( xta_obj.id IN (SELECT id_obj FROM xta_obj_abuse) )");
			$dataQuery = $query->createCommand()->queryOne();
			$data['count_blocked']  = $dataQuery['count'];
			//========================================================================
			
		endif;
		

		return $this->render('view__FrontendUserMenu',$data);
	}
}
?>

