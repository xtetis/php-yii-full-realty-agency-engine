<?php
namespace app\components;

use yii\base\Widget;
use app\models\gii\xta_obj_category;
use app\models\gii\xta_cache;

class FrontendCategoryListTree extends Widget{
	public $message;
	
	public function init(){
	}
	
	public function run(){

		// Cache start
		//==============================================================================
		//$id_cache = 'FrontendCategoryListTree';
		//$cache = xta_cache::fn__get_cache($id_cache);
		//if ($cache===false):
		//==============================================================================

		$data['category_data_list'] = xta_obj_category::find()->all();
		$ret = $this->render('view__FrontendCategoryListTree',$data);

		$ret = '
					<button class="btn btn-default dropdown-toggle" type="button" 
					        id="dropdownMenu1" 
					        data-toggle="dropdown" aria-haspopup="true" 
					        aria-expanded="true" style="width: 100%;">
						Вся недвижимость
						<span class="caret"></span>
					</button>
					'.$ret.'
				';
		//file_put_contents($_SERVER['DOCUMENT_ROOT'].'/public/ajax/frontendcategorylisttree/tree.txt',$ret);
		$ret = '
		<div class="dropdown categoryy_tree_box">
		'.$ret.'
		</div>
		';
		// Cache end
		//==============================================================================
		//	xta_cache::fn__set_cache($id_cache,$ret,array('FrontendCategoryListTree','xta_obj_category'));
		//else:
		//	$ret = $cache;
		//endif;
		//==============================================================================



		return $ret;
	}
}
?>

