$( document ).ready(function() {



	// Проверка на ввод целочисленных данных
	//================================================
	$('body').on('keydown', '.intval', function (event){
		  // Allow: backspace, delete, tab, escape, enter and .
		  // "-" has a charCode of 45.
		  if ( $.inArray(event.keyCode,[46,8,9,27,13]) !== -1 ||
		       // Allow: Ctrl+A
		      (event.keyCode == 65 && event.ctrlKey === true) || 
		       // Allow: Ctrl+V
		      (event.keyCode == 86 && event.ctrlKey === true) || 
		       // Allow: home, end, left, right
		      (event.keyCode >= 35 && event.keyCode <= 39)) {
		           // let it happen, don't do anything
		           return;
		  }
		  else {
		      // Ensure that it is a number and stop the keypress
		      if (
		           event.shiftKey || 
		           (event.keyCode < 48 || event.keyCode > 57) && 
		           (event.keyCode < 96 || event.keyCode > 105 ) //&& (event.keyCode != 173 )
		         ) {
		             event.preventDefault(); 
		           }
		  }
	});
	//================================================

	$('[data-toggle="tooltip"]').tooltip()



	$(".account_messages_opendialog").click(function() {
		$('#user_messages_frame').attr('src','');
		var username = $(this).attr('username');
		var id_user = $(this).attr('id_user');
		$('#modal__dialog_user_messages__id_user').val(id_user);
		$('#user_messages_other_username').html(username);
		$('#user_messages_frame').attr('src','/ajax/get_dialog_message_list/'+id_user);
		$('#modal__dialog_user_messages').modal('show');
	});
















	// Отправляем сообщение пользовтелю
	//============================================================================
	$("#modal__dialog_user_messages__btn_send").click(function() {
		var _message = $('#modal__dialog_user_messages__message_textarea').val();
		_message = _message.trim();
		if (_message.length)
		{
			var id_user = $('#modal__dialog_user_messages__id_user').val();
			$('#user_messages_frame').attr('src','');
			$.post("/ajax/send_user_message/all/"+id_user, { mes: _message }).done(function( data ) {
				$('#modal__dialog_user_messages__message_textarea').val('');
				$('#user_messages_frame').attr('src','/ajax/get_dialog_message_list/'+id_user+'?d='+Date.now());
			});
		}
	});
	//============================================================================










	// Показываем телефон
	//============================================================================
	$("#board_item_phone_btn").click(function() {
		var idx = $('#board_item_phone_block').attr('idx');
		$('#board_item_phone_block').html(B64.decode(idx));
		$('#board_item_phone_alert').hide();
	});
	//============================================================================














	// Показываем модальное окно для отправки сообщения
	//============================================================================
	$("#btn__message_to_board_item_author_modal_show").click(function() {
		$('#message_to_board_item__username').val('');
		$('#message_to_board_item__email').val('');
		$('#message_to_board_item__phone').val('');
		$('#message_to_board_item__message').val('');
		$('.modal_alert_message').html('');
		$('#modal_alert_message').hide();
		$('#modal__message_to_board_item_author').modal('show');
	});
	//============================================================================





	// Показываем модальное окно для отправки сообщения
	//============================================================================
	$("#btn__send_message_modal_show").click(function() {
		$('#message_to_board_item__username').val('');
		$('#message_to_board_item__email').val('');
		$('#message_to_board_item__phone').val('');
		$('#message_to_board_item__message').val('');
		$('.modal_alert_message').html('');
		$('#modal_alert_message').hide();
		$('#modal__message_to_board_item_author').modal('show');
	});
	//============================================================================








	// Нажата кнопка отправить сообщение автору объявления
	//============================================================================
	$("#btn__message_to_board_item_author_modal_send").click(function() {
		$('#modal_alert_message').hide();
		var _username = $('#message_to_board_item__username').val();
		var _email    = $('#message_to_board_item__email').val();
		var _phone    = $('#message_to_board_item__phone').val();
		var _message  = $('#message_to_board_item__message').val();
		var id_user  = $('#message_to_board_item__id_user').val();
		
		
		$.post("/ajax/send_user_message/all/"+id_user, {mes:_message,email:_email,username:_username,phone:_phone}).done(function( data ) 
		{
			//alert(data);
			var obj = jQuery.parseJSON(data);
			var result = obj.result;
			var message = obj.message;
			if (result=='ok')
			{
				$('#modal_alert_message').removeClass("alert-danger");
				$('#modal_alert_message').addClass("alert-success");
				$('#message_to_board_item__message').val('');
				$('#message_to_board_item__username').val('');
				$('#message_to_board_item__email').val('');
				$('#message_to_board_item__phone').val('');
				$('.group__message_to_board_item__username').hide();
				$('.group__message_to_board_item__phone').hide();
				$('.group__message_to_board_item__email').hide();
				
			}
			else
			{
				$('#modal_alert_message').removeClass("alert-success");
				$('#modal_alert_message').addClass("alert-danger");
			}
			$('#modal_alert_message').html(message);
			
			$('#modal_alert_message').show();
		});
	});
	//============================================================================













	// Показываем модальное окно для отправки сообщения
	//============================================================================
	$(".btn__abuse_to_board_item_modal_show").click(function() {
		var message = $(this).attr('message')
		$('#abuse_to_board_item__username').val('');
		$('#abuse_to_board_item__email').val('');
		$('#abuse_to_board_item__phone').val('');
		$('#abuse_to_board_item__message').val(message);
		$('#modal_alert_message__abuse').html('');
		$('#modal_alert_message__abuse').hide();
		$('#modal__abuse_to_board_item').modal('show');
	});
	//============================================================================



















	// Нажата кнопка отправить жалобу на объявление
	//============================================================================
	$("#btn__abuse_to_board_item_modal_send").click(function() {
		$('#modal_alert_message__abuse').hide();
		var _username = $('#abuse_to_board_item__username').val();
		var _email    = $('#abuse_to_board_item__email').val();
		var _phone    = $('#abuse_to_board_item__phone').val();
		var _message  = $('#abuse_to_board_item__message').val();
		var _id_board_item  = $('#id_board_item').val();
		
		$.post("/ajax/send_abuse_to_board_item", {mes:_message,email:_email,username:_username,phone:_phone,id_board_item:_id_board_item}).done(function( data ) 
		{
			console.log(data);
			var obj = jQuery.parseJSON(data);
			var result = obj.result;
			var message = obj.message;
			if (result=='ok')
			{
				$('#modal_alert_message__abuse').removeClass("alert-danger");
				$('#modal_alert_message__abuse').addClass("alert-success");
				$('#abuse_to_board_item__message').val('');
				$('#abuse_to_board_item__username').val('');
				$('#abuse_to_board_item__email').val('');
				$('#abuse_to_board_item__phone').val('');
				$('.group__abuse_to_board_item__username').hide();
				$('.group__abuse_to_board_item__phone').hide();
				$('.group__abuse_to_board_item__email').hide();
				$('#clearcache_conteiner').load('/ajax/cache/clearall');
				$.get("/ajax/cache/clearall");
			}
			else
			{
				$('#modal_alert_message__abuse').removeClass("alert-success");
				$('#modal_alert_message__abuse').addClass("alert-danger");
			}
			$('#modal_alert_message__abuse').html(message);
			$('#modal_alert_message__abuse').show();
		});
		//$('#modal__message_to_board_item_author').modal('hide');
	});
	//============================================================================



	// Нажата кнопка добавить альбом в разделе знакомства
	//============================================================================
	$("#btn__add_love_album").click(function() {
		$('#modal__add_album .modal-title').html('Добавить альбом');
		$('#btn__submit_add_love_album').html('Создать альбом');
		$('#modal__add_album').modal('show');
	});
	//============================================================================




	// 
	//============================================================================
	$(".btn__modal_upload_photo_to_love_album").click(function() {
		var idx = $(this).attr('idx');
		$('#input__upload_id_album').val(idx);
		$('#modal__add_photo_to_album').modal('show');
	});
	//============================================================================





	// Нажата кнопка изменить альбом в разделе знакомства
	//============================================================================
	$(".btn__edit_love_album").click(function() {
		$('#modal__add_album .modal-title').html('Изменить альбом');
		$('#btn__submit_add_love_album').html('Сохранить альбом');
		var idx = $(this).attr('idx');
		$('#input__id_album').val(idx);
		var _name = $('#album_'+idx+'_name').html();
		$('#input__name_album').val(_name);
		var _hidden = $('#album_'+idx+'_hidden').attr('val');
		_hidden = Number(_hidden);
		if (_hidden)
		{
			$('#input__hidden_album').prop('checked', true);
		}
		else
		{
			$('#input__hidden_album').prop('checked', false);
		}
		$('#modal__add_album').modal('show');
	});
	//============================================================================





	// Нажата кнопка SUBMIT добавить альбом в разделе знакомства 
	//============================================================================
	$("#btn__submit_add_love_album").click(function() {
		$('#modal_alert_message__add_love_album').hide();
		var _name = $('#input__name_album').val();
		if($('#input__hidden_album').prop('checked'))
		{
			var _hidden = 1;
		} else {
			var _hidden = 0;
		}
		var _id_album    = $('#input__id_album').val();
		$.post("/ajax/love_album/create", {name:_name,hidden:_hidden,id_album:_id_album}).done(function( data ) 
		{
			console.log(data);
			//alert(data);
			var obj = jQuery.parseJSON(data);
			var result = obj.result;
			var message = obj.message;
			if (result=='ok')
			{
				$('#modal__add_album').modal('hide');
				location.reload();
			}
			else
			{
				$('#modal_alert_message__add_love_album').html(message);
				$('#modal_alert_message__add_love_album').show();
			}

		});
	});
	//============================================================================






	// Нажата кнопка загрузки фото в альбом
	//============================================================================
	$("#btn__upload_photo_to_album").click(function() {
		$("#dropped-files input[type='hidden']").each(function( index ) {
			var _img = $( this ).val();
			var _id_album = $('#input__upload_id_album').val();
			
			$.post("/ajax/love_album/upload", {img:_img,id_album:_id_album}).done(function( data ) 
			{
				console.log(data);
				//alert(data);
				var obj = jQuery.parseJSON(data);
				var result = obj.result;
				var message = obj.message;
				if (result=='ok')
				{
					$('#modal__add_photo_to_album').modal('hide');
					location.reload();
				}
				else
				{
					$('#modal_alert_message__upload_photo_to_album').html(message);
					$('#modal_alert_message__upload_photo_to_album').show();
				}

			});
		});
	});
	//============================================================================







	// Отправляем команду на очистку кеша, если в коде страницы есть такая команда
	//============================================================================
	if ($("#clearcache_conteiner").length)
	{
		$.get("/ajax/cache/clearall", function( data ) {
			//alert(data);
		});
	}
	//============================================================================


	//$(".categoryy_tree_box").load("/public/ajax/frontendcategorylisttree/tree.txt");


	$("#mgii_xta_love_anketa-birthdate").datepicker({
		dateFormat: "dd/mm/yy",
		changeMonth: true,
		changeYear: true,
		yearRange: "-100:+0"
		});
	$("#mgii_xta_love_anketa").datepicker( "option", $.datepicker.regional['ru']);





	

});
