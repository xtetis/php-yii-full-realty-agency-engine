$( document ).ready(function() {
	$( "#button__board_add_id_categiry" ).click(function() {
		$.get("/ajax/add_categories_list", function( data ) {
			$('#modal__board_add_id_categiry .modal-body').html(data);
			$('#modal__board_add_id_categiry').modal('show');
		});
	});
	
	$("#change__board_add_id_categiry").click(function() {
		$.get("/ajax/add_categories_list/select/1", function( data ) {
			$('#modal__board_add_id_categiry .modal-body').html(data);
			
			var id_category=$('#boardaddform-id_category').val();
			$('.add_categories_list[idx='+id_category+']').addClass('active');
			
			var parent = $('.add_categories_list[idx='+id_category+']').attr('parent');
			$('.add_categories_list[idx='+parent+']').addClass('active');
			$('.add_categories_list.parent_'+parent).show();
			
			if (parent!=='0')
			{
				var subparent = $('.add_categories_list[idx='+parent+']').attr('parent');
				$('.add_categories_list[idx='+subparent+']').addClass('active');
				$('.add_categories_list.parent_'+subparent).show();
			}
			
			$('#modal__board_add_id_categiry').modal('show');
		});
	});
	
	
	$('body').on('click', '.add_categories_list.level_0', function (){
		var idx = $(this).attr('idx');
		$('.add_categories_list.level_1').hide();
		$('.add_categories_list.level_2').hide();
		$('.column_head_level_1').html($(this).attr('title'));
		$('.column_head_level_2').html('');
		$('.add_categories_list').removeClass('active');
		$(this).addClass('active');
		$('.add_categories_list.parent_'+idx).show();

	});
	
	$('body').on('click', '.add_categories_list.level_1', function (){
		var idx = $(this).attr('idx');
		$('.add_categories_list.level_2').hide();
		$('.column_head_level_2').html($(this).attr('title'));
		$('.add_categories_list.level_1').removeClass('active');
		$(this).addClass('active');
		$('.add_categories_list.parent_'+idx).show();
	});
	
	$('body').on('click', '.add_categories_list.nochild', function (){
		var idx = $(this).attr('idx');
		$("#button__board_add_id_categiry").hide();
		$('#board_add__category_selector').css('display','table');
		$('#id_category_selector').val($(this).attr('title'))
		$('#boardaddform-id_category').val(idx);
		
		$('#obj_options_container').load('/ajax/get_category_option_inputs/all/'+idx, function() {
			$('#modal__board_add_id_categiry').modal('hide');
			$("[data-toggle='tooltip']").tooltip();
			$("[data-toggle='popover']").popover({ trigger : 'hover focus',html: true});
		});
	});
	
	
	var id_category=$('#boardaddform-id_category').val();
	if (parseInt(id_category))
	{
		$.get("/ajax/get_categoryname_by_id/name/"+id_category, function( data ) {
			$("#button__board_add_id_categiry").hide();
			$('#board_add__category_selector').css('display','table');
			$('#id_category_selector').val(data)
		});
	}
	
	
	
	
	
	
	
	
	
	
	
	
	

	


});
