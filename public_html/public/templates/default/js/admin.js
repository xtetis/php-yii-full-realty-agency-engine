function fn__toggle_child_xta_obj_category_items(idx,action)
{
	if (action=='toggle')
	{
		$('tr.xta_obj_category__parent__'+idx).toggle();
	}
	if (action=='hide')
	{
		$('tr.xta_obj_category__parent__'+idx).hide();
	}
	if (action=='show')
	{
		$('tr.xta_obj_category__parent__'+idx).show();
	}
	
	if ($('tr.xta_obj_category__parent__'+idx).css('display') != 'none')
	{
		$("a.xta_obj_category__parent_toggle.idx_"+idx).find('span').addClass('glyphicon-minus');
		$("a.xta_obj_category__parent_toggle.idx_"+idx).find('span').removeClass('glyphicon-plus');
	}
	else
	{
		$("a.xta_obj_category__parent_toggle.idx_"+idx).find('span').removeClass('glyphicon-minus');
		$("a.xta_obj_category__parent_toggle.idx_"+idx).find('span').addClass('glyphicon-plus');
	}
	
	
	$('tr.xta_obj_category__parent__'+idx).each(function( index ) {
		var idx1 = $(this).attr('idx');
		fn__toggle_child_xta_obj_category_items(idx1,'hide');
	});
	

}

$(function() {
	$( "#admin_input_settings" ).click(function() {
		if ($("#xta_setting-allsite").prop("checked"))
		{
			if (confirm("Установив поле \"Использовать для всех сайтов\" вы удалите настройки всех сайтов. Вы уверены в своем выборе?"))
			{
				$("#xta_setting-allsite").prop("checked",true);
			}
			else
			{
				$("#xta_setting-allsite").prop("checked",false);
			}
		}
	});
	
	
	$( "a.xta_obj_category__parent_toggle").click(function() {
		var idx = $(this).attr('idx');
		fn__toggle_child_xta_obj_category_items(idx,'toggle');

	});
	
	
});
