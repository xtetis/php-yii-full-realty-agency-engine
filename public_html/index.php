<?php

require(__DIR__ . '/seo/redirects.php');

// comment out the following two lines when deployed to production
defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_ENV') or define('YII_ENV', 'dev');

require(__DIR__ . '/../framework/vendor/autoload.php');
require(__DIR__ . '/../framework/vendor/yiisoft/yii2/Yii.php');

$config = require(__DIR__ . '/../framework/config/web.php');
// Включать в отчет простые описания ошибок
error_reporting(E_ERROR | E_WARNING | E_PARSE);

(new yii\web\Application($config))->run();
