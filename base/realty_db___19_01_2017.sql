-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 19, 2017 at 03:41 PM
-- Server version: 5.7.16-0ubuntu0.16.04.1
-- PHP Version: 7.0.13-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `realty_db`
--

DELIMITER $$
--
-- Functions
--
DROP FUNCTION IF EXISTS `_fs_transliterate_2`$$
CREATE DEFINER=`test6_db`@`localhost` FUNCTION `_fs_transliterate_2` (`str` TEXT) RETURNS TEXT CHARSET cp1251 SQL SECURITY INVOKER
BEGIN
declare str2 varchar(2);
declare str3 text;
declare len int(11);
declare i int(11);
set str3 = '';
set i = 1;
set len = length(str);

while i <= len do 
set str2 = elt(
instr(
' ,./АаАБбВвГгДдЕеЁёЖжЗзИиЙйКкЛлМмНнОоПпРрСсТтУуфХхцЦЧчШшЩщЪъыЫЬьЭэЮюЯя[\]_{}',
substr(str,i,1)
),
'-',
'',
'',
'/',
'A',
'a',
'A',
'B',
'b',
'V',
'v',
'G',
'g',
'D',
'd',
'E',
'e',
'JO',
'jo',
'ZH',
'zh',
'Z',
'z',
'I',
'i',
'J',
'j',
'K',
'k',
'L',
'l',
'M',
'm',
'N',
'n',
'O',
'o',
'P',
'p',
'R',
'r',
'S',
's',
'T',
't',
'U',
'u',
'f',
'H',
'h',
'c',
'C',
'CH',
'ch',
'SH',
'sh',
'SH',
'sh',
'\'',
'-',
'y',
'Y',
'-',
'\'',
'E-',
'e-',
'YU',
'yu',
'YA',
'ya',
'',
'',
'',
'-',
'',
''
);
if str2 is null then 
set str2 = substr(str,i,1);
end if;
set str3 = concat(str3,str2);
set i = i + 1;
end while;

set str3 = REPLACE(str3,'&quot;','-');
set str3 = REPLACE(str3,'№','-');
set str3 = REPLACE(str3,"'","-");
set str3 = REPLACE(str3,'--','-');
set str3 = REPLACE(str3,'--','-');
set str3 = REPLACE(str3,'--','-');
set str3 = REPLACE(str3,'--','-');
set str3 = REPLACE(str3,')','');
set str3 = REPLACE(str3,'(','');
set str3 = REPLACE(str3,'+','');


set str3 = REPLACE(str3,'«','');
set str3 = REPLACE(str3,'»','');

set str3 = LEFT(str3,LENGTH(str3)-1);


return lower(str3);



END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `xta_album`
--

DROP TABLE IF EXISTS `xta_album`;
CREATE TABLE `xta_album` (
  `id` int(11) NOT NULL,
  `id_image` int(11) NOT NULL DEFAULT '0' COMMENT 'Ссылка на главную картинку'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Список альбомов';

--
-- Dumping data for table `xta_album`
--

INSERT INTO `xta_album` VALUES(1, 1);
INSERT INTO `xta_album` VALUES(2, 6);
INSERT INTO `xta_album` VALUES(3, 7);

-- --------------------------------------------------------

--
-- Table structure for table `xta_cache`
--

DROP TABLE IF EXISTS `xta_cache`;
CREATE TABLE `xta_cache` (
  `id` int(11) NOT NULL,
  `md5` varchar(50) NOT NULL DEFAULT '' COMMENT 'md5 кеша',
  `cache` longtext NOT NULL COMMENT 'Кеш'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Кеш';

-- --------------------------------------------------------

--
-- Table structure for table `xta_cache_in_tag`
--

DROP TABLE IF EXISTS `xta_cache_in_tag`;
CREATE TABLE `xta_cache_in_tag` (
  `id` int(11) NOT NULL,
  `id_tag` int(11) NOT NULL DEFAULT '0' COMMENT 'Ссылка на тег',
  `id_cache` int(11) NOT NULL DEFAULT '0' COMMENT 'Ссылка на кеш'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Связь кеша и тегов';

-- --------------------------------------------------------

--
-- Table structure for table `xta_cache_tag`
--

DROP TABLE IF EXISTS `xta_cache_tag`;
CREATE TABLE `xta_cache_tag` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL DEFAULT '' COMMENT 'Имя тега кеша'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Теги кеша';

-- --------------------------------------------------------

--
-- Table structure for table `xta_city`
--

DROP TABLE IF EXISTS `xta_city`;
CREATE TABLE `xta_city` (
  `id` int(11) NOT NULL,
  `id_region` int(11) NOT NULL COMMENT 'Ссылка на область',
  `name` varchar(200) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Список городов';

--
-- Dumping data for table `xta_city`
--

INSERT INTO `xta_city` VALUES(1, 1, 'Кривой Рог');
INSERT INTO `xta_city` VALUES(4, 1, 'Апостолово');
INSERT INTO `xta_city` VALUES(5, 1, 'Днепродзержинск');
INSERT INTO `xta_city` VALUES(6, 2, 'Одесса');

-- --------------------------------------------------------

--
-- Table structure for table `xta_country`
--

DROP TABLE IF EXISTS `xta_country`;
CREATE TABLE `xta_country` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Список стран';

--
-- Dumping data for table `xta_country`
--

INSERT INTO `xta_country` VALUES(1, 'Украина');
INSERT INTO `xta_country` VALUES(2, 'Россия');

-- --------------------------------------------------------

--
-- Table structure for table `xta_district`
--

DROP TABLE IF EXISTS `xta_district`;
CREATE TABLE `xta_district` (
  `id` int(11) NOT NULL,
  `id_city` int(11) NOT NULL COMMENT 'Ссылка на город',
  `name` varchar(200) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Список районов города';

--
-- Dumping data for table `xta_district`
--

INSERT INTO `xta_district` VALUES(13, 6, 'Суворовский');
INSERT INTO `xta_district` VALUES(14, 6, 'Приморский');
INSERT INTO `xta_district` VALUES(15, 6, 'Малиновский');
INSERT INTO `xta_district` VALUES(16, 6, 'Киевский');

-- --------------------------------------------------------

--
-- Table structure for table `xta_image`
--

DROP TABLE IF EXISTS `xta_image`;
CREATE TABLE `xta_image` (
  `id` int(11) NOT NULL,
  `id_album` int(11) NOT NULL COMMENT 'Ссылка на альбом, в котором находится изображение',
  `md5` varchar(50) NOT NULL DEFAULT '' COMMENT 'Путь к картинке'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Список изображений';

--
-- Dumping data for table `xta_image`
--

INSERT INTO `xta_image` VALUES(1, 1, 'e7ea82b990eb0ebe10f79a4067cd2b95');
INSERT INTO `xta_image` VALUES(6, 2, 'c6538468b48c3715facad21f4194118f');
INSERT INTO `xta_image` VALUES(7, 3, 'da571bc415a06a34e24683a7288c1777');

-- --------------------------------------------------------

--
-- Table structure for table `xta_obj`
--

DROP TABLE IF EXISTS `xta_obj`;
CREATE TABLE `xta_obj` (
  `id` int(11) NOT NULL,
  `id_district` int(11) NOT NULL COMMENT 'Ссылка на район города',
  `id_city` int(11) NOT NULL DEFAULT '0' COMMENT 'Ссылка на город',
  `id_category` int(11) NOT NULL COMMENT 'Ссылка на категорию',
  `id_user` int(11) NOT NULL COMMENT 'Ссылка на пользователя',
  `id_album` int(11) DEFAULT '0' COMMENT 'Ссылка на альбом',
  `id_valuta` int(11) NOT NULL COMMENT 'Ссылка на валюту',
  `name` varchar(200) NOT NULL DEFAULT '' COMMENT 'Заголовок объявления',
  `about` text COMMENT 'Текст объявления',
  `price` int(11) NOT NULL DEFAULT '0',
  `phone` varchar(50) NOT NULL DEFAULT '' COMMENT 'Телефон объявления',
  `createdon` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Дата создания',
  `pub_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `published` int(11) NOT NULL DEFAULT '1' COMMENT 'Опубликовано',
  `source` varchar(200) NOT NULL DEFAULT '' COMMENT 'Источник'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Список объявлений';

--
-- Dumping data for table `xta_obj`
--

INSERT INTO `xta_obj` VALUES(1, 0, 6, 2, 1, 1, 1, 'Продам 3х ком. кв-ру Улучшенной планировки по ул. Адмирала Головко', 'Текст объявленияТекст объявленияТекст объявленияТекст объявленияТекст объявленияТекст объявленияТекст объявленияТекст объявленияТекст объявленияТекст объявления', 44444, '0977552001', '2016-09-21 20:52:43', '2016-09-21 20:52:43', 1, '');
INSERT INTO `xta_obj` VALUES(2, 15, 6, 3, 1, 2, 1, 'Квартира в 22 жемчужине', 'Двадцать Вторая Жемчужина на улице Болгарской – долгожданный проект для одесситов!\r\n\r\nНовый современный жилой комплекс от компании «KАDORR Group» будет возведен в центре легендарного района — Молдаванки, на улице Болгарской, рядом с Алексеевской площадью.\r\n\r\nКолорит старинных уличек Молдаванки в сочетании с высочайшим европейским качеством строительства и обслуживания дома – это мечта многих одесситов.\r\n\r\nВ этом районе есть масса преимуществ. Кроме романтической атмосферы и уютных улочек микрорайона, жильцы получат и очевидные практические преимущества – близость к центру города, авто- и железнодорожному вокзалу, рынку Привоз.\r\n\r\nКак и на других объектах, господин Аднан Киван дает свои персональные гарантии на качество и сроки строительства.\r\n\r\nВ жилом комплексе будет: ухоженная придомовая территория, детская площадка собственная служба охраны и эксплуатации, места для ожидания и отдыха.', 380000, '0977552001', '2016-11-17 12:40:22', '2016-11-17 13:30:54', 1, '');
INSERT INTO `xta_obj` VALUES(3, 16, 6, 3, 1, 3, 2, '1 комн. в Аркадии на Каманина', 'Продам 1 комнатную с отличной планировкой в престижном районе, возле моря.Отличный вариант как для жизни, так и под аренду.', 46000, '0977552001', '2017-01-19 08:38:26', '2017-01-19 08:38:26', 1, '');

-- --------------------------------------------------------

--
-- Table structure for table `xta_obj_abuse`
--

DROP TABLE IF EXISTS `xta_obj_abuse`;
CREATE TABLE `xta_obj_abuse` (
  `id` int(11) NOT NULL,
  `id_obj` int(11) NOT NULL COMMENT 'Ссылка на объявление',
  `id_user` int(11) NOT NULL COMMENT 'Ссылка на пользователя',
  `message` text NOT NULL COMMENT 'Текст жалобы',
  `createdon` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Дата создания жалобы'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Список жалоб на объявления';

-- --------------------------------------------------------

--
-- Table structure for table `xta_obj_category`
--

DROP TABLE IF EXISTS `xta_obj_category`;
CREATE TABLE `xta_obj_category` (
  `id` int(11) NOT NULL,
  `id_parent` int(11) NOT NULL COMMENT 'Ссылка на рожительскую категорию',
  `name` varchar(200) NOT NULL DEFAULT '' COMMENT 'Название категории',
  `hone` varchar(200) NOT NULL DEFAULT '' COMMENT 'H1 страницы',
  `title` varchar(200) NOT NULL DEFAULT '' COMMENT 'Тайтл страницы',
  `description` varchar(200) NOT NULL DEFAULT '' COMMENT 'Description страницы',
  `hideobjmain` int(11) NOT NULL DEFAULT '0' COMMENT 'Нужно ли скривать объявления из этой категории на главной странице'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Список категорий объявлений';

--
-- Dumping data for table `xta_obj_category`
--

INSERT INTO `xta_obj_category` VALUES(2, 0, 'Продажа квартир', 'Купить квартиру', 'Купить квартиру в Кривом Роге от хозяев и агентств: фотографии, цены', 'Квартиры в Кривом Роге по самым выгодным ценам: широкий выбор предложений от хозяев и агентств  по продаже квартир, бесплатные обхявления', 0);
INSERT INTO `xta_obj_category` VALUES(3, 0, 'Продажа квартир в новостроях и ЖК', '', '', '', 0);
INSERT INTO `xta_obj_category` VALUES(4, 0, 'Продажа домов', '', '', 'Продажа домов и коттеджей в Кривом Роге - обширная и актуальная база объявлений о покупке и продаже. Подробное описание, цены и фото объектов смотрите на сайте.', 0);
INSERT INTO `xta_obj_category` VALUES(16, 4, 'Продажа домов в городе', 'Продажа домов в черте города', '', '', 0);
INSERT INTO `xta_obj_category` VALUES(17, 4, 'За городом ', '', '', '', 0);
INSERT INTO `xta_obj_category` VALUES(18, 4, 'Продажа дач', '', '', '', 0);
INSERT INTO `xta_obj_category` VALUES(34, 0, 'Аренда квартир', '', '', '', 0);
INSERT INTO `xta_obj_category` VALUES(35, 0, 'Аренда комнат', '', '', '', 0);
INSERT INTO `xta_obj_category` VALUES(36, 0, 'Аренда домов', '', '', '', 0);
INSERT INTO `xta_obj_category` VALUES(37, 0, 'Аренда земли', '', '', '', 0);
INSERT INTO `xta_obj_category` VALUES(38, 0, 'Аренда гаражей / стоянок', '', '', '', 0);
INSERT INTO `xta_obj_category` VALUES(39, 0, 'Продажа комнат', '', '', '', 0);
INSERT INTO `xta_obj_category` VALUES(40, 0, 'Продажа земли', '', '', '', 0);
INSERT INTO `xta_obj_category` VALUES(41, 0, 'Продажа гаражей / стоянок', '', '', '', 0);
INSERT INTO `xta_obj_category` VALUES(42, 0, 'Аренда за рубежом', '', '', '', 0);
INSERT INTO `xta_obj_category` VALUES(43, 0, 'Аренда помещений', '', '', '', 0);
INSERT INTO `xta_obj_category` VALUES(44, 0, 'Продажа помещений', '', '', '', 0);
INSERT INTO `xta_obj_category` VALUES(45, 0, 'Продажа за рубежом', '', '', '', 0);
INSERT INTO `xta_obj_category` VALUES(47, 34, 'Квартиры посуточно ', '', '', '', 0);
INSERT INTO `xta_obj_category` VALUES(48, 34, 'Квартиры с почасовой оплатой', '', '', '', 0);
INSERT INTO `xta_obj_category` VALUES(49, 34, 'Долгосрочная аренда квартир', '', '', '', 0);
INSERT INTO `xta_obj_category` VALUES(50, 35, 'Койко-места', '', '', '', 0);
INSERT INTO `xta_obj_category` VALUES(51, 35, 'Комнаты посуточно', '', '', '', 0);
INSERT INTO `xta_obj_category` VALUES(52, 35, 'Долгосрочная аренда комнат', '', '', '', 0);
INSERT INTO `xta_obj_category` VALUES(53, 35, 'Комнаты с частичной оплатой', '', '', '', 0);
INSERT INTO `xta_obj_category` VALUES(54, 36, 'Долгосрочная аренда домов', '', '', '', 0);
INSERT INTO `xta_obj_category` VALUES(55, 36, 'Дома посуточно, почасово', '', '', '', 0);
INSERT INTO `xta_obj_category` VALUES(56, 37, 'Аренда земли промышленного назначения', '', '', '', 0);
INSERT INTO `xta_obj_category` VALUES(57, 37, 'Аренда земли сельскохозяйственного назначения', '', '', '', 0);
INSERT INTO `xta_obj_category` VALUES(58, 40, 'Продажа земли под индивидуальное строительство', '', '', '', 0);
INSERT INTO `xta_obj_category` VALUES(59, 40, 'Продажа земли под сад / огород', '', '', '', 0);
INSERT INTO `xta_obj_category` VALUES(60, 40, 'Продажа земли сельскохозяйственного назначения', '', '', '', 0);
INSERT INTO `xta_obj_category` VALUES(61, 40, 'Продажа земли промышленного назначения', '', '', '', 0);
INSERT INTO `xta_obj_category` VALUES(62, 43, 'Аренда магазинов / салонов', '', '', '', 0);
INSERT INTO `xta_obj_category` VALUES(63, 43, 'Аренда ресторанов / баров', '', '', '', 0);
INSERT INTO `xta_obj_category` VALUES(64, 43, 'Аренда офисов', '', '', '', 0);
INSERT INTO `xta_obj_category` VALUES(65, 43, 'Аренда складов', '', '', '', 0);
INSERT INTO `xta_obj_category` VALUES(66, 43, 'Аренда отдельно стоящих зданий', '', '', '', 0);
INSERT INTO `xta_obj_category` VALUES(67, 43, 'Аренда баз отдыха', '', '', '', 0);
INSERT INTO `xta_obj_category` VALUES(68, 43, 'Аренда помещений промышленного назначения', '', '', '', 0);
INSERT INTO `xta_obj_category` VALUES(69, 43, 'Аренда помещений свободного назначения', '', '', '', 0);
INSERT INTO `xta_obj_category` VALUES(70, 43, 'Продажа права аренды', '', '', '', 0);
INSERT INTO `xta_obj_category` VALUES(71, 44, 'Продажа магазинов / салонов ', '', '', '', 0);
INSERT INTO `xta_obj_category` VALUES(72, 44, 'Продажа ресторанов / баров', '', '', '', 0);
INSERT INTO `xta_obj_category` VALUES(73, 44, 'Продажа офисов', '', '', '', 0);
INSERT INTO `xta_obj_category` VALUES(74, 44, 'Продажа складов', '', '', '', 0);
INSERT INTO `xta_obj_category` VALUES(75, 44, 'Продажа отдельно стоящих зданий', '', '', '', 0);
INSERT INTO `xta_obj_category` VALUES(76, 44, 'Продажа баз отдыха', '', '', '', 0);
INSERT INTO `xta_obj_category` VALUES(77, 44, 'Продажа помещений промышленного назначения', '', '', '', 0);
INSERT INTO `xta_obj_category` VALUES(78, 44, 'Продажа помещений свободного назначения', '', '', '', 0);
INSERT INTO `xta_obj_category` VALUES(79, 45, 'Жилая недвижимость в зарубежье', '', '', '', 0);
INSERT INTO `xta_obj_category` VALUES(80, 45, 'Коммерческая недвижимость в зарубежье', '', '', '', 0);
INSERT INTO `xta_obj_category` VALUES(81, 45, 'Участки в зарубежье', '', '', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `xta_obj_category_description`
--

DROP TABLE IF EXISTS `xta_obj_category_description`;
CREATE TABLE `xta_obj_category_description` (
  `id` int(11) NOT NULL,
  `id_site` int(11) NOT NULL COMMENT 'Ссылка на сайт',
  `id_obj_category` int(11) NOT NULL COMMENT 'Ссылка на категорию объявлений',
  `title` varchar(500) NOT NULL DEFAULT '',
  `description` varchar(500) NOT NULL DEFAULT '',
  `hone` varchar(500) NOT NULL DEFAULT '',
  `seotext` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='ТДЗ для категорий';

-- --------------------------------------------------------

--
-- Table structure for table `xta_obj_option`
--

DROP TABLE IF EXISTS `xta_obj_option`;
CREATE TABLE `xta_obj_option` (
  `id` int(11) NOT NULL,
  `option_type` int(11) NOT NULL DEFAULT '0' COMMENT 'Тип элемента ввода (0 - select, 1 - text input)',
  `name` varchar(200) NOT NULL DEFAULT '' COMMENT 'Название опции',
  `description` varchar(200) NOT NULL DEFAULT '' COMMENT 'Краткое описание опции',
  `introtext` varchar(300) NOT NULL DEFAULT '' COMMENT 'Подробное описание',
  `filter_type` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Список опций объявлений в зависимости от выбранной категории';

--
-- Dumping data for table `xta_obj_option`
--

INSERT INTO `xta_obj_option` VALUES(3, 0, 'Этажность здания', 'Укажите этажность здания', 'Постарайтесь указать этаж строения (от 1 до 25 этажа), чтобы потенциальные покупатели/клиенты скорее нашли ваше объявление.', 0);
INSERT INTO `xta_obj_option` VALUES(4, 0, 'Этаж', 'Выберите этаж помещения', 'Укажите на каком этаже расположено помещение', 0);
INSERT INTO `xta_obj_option` VALUES(5, 0, 'Количество комнат', 'Укажите количество комнат', 'Постарайтесь указать количество комнат, чтобы потенциальные покупатели/клиенты скорее нашли ваше объявление.', 1);
INSERT INTO `xta_obj_option` VALUES(6, 0, 'Вид топлива', 'Укажите тип топлива', 'Укажите топливо, которое используется в транспортном средстве', 1);
INSERT INTO `xta_obj_option` VALUES(7, 0, 'Возраст (лет)', 'Укажите возраст', '', 0);
INSERT INTO `xta_obj_option` VALUES(8, 1, 'Общая площадь, кв. м.', 'Общая площадь, кв. м.', 'Укажите общую площадь помещения в квадратных метрах', 0);
INSERT INTO `xta_obj_option` VALUES(9, 0, 'Марка автомобиля', 'Укажите марку автомобиля', 'Постарайтесь указать марку автомобиля, чтобы потенциальные покупатели/клиенты скорее нашли ваше объявление.', 0);
INSERT INTO `xta_obj_option` VALUES(10, 0, 'Год выпуска', 'Укажите год выпуска', 'Постарайтесь указать год выпуска транспорта, чтобы потенциальные покупатели/клиенты скорее нашли ваше объявление.', 0);

-- --------------------------------------------------------

--
-- Table structure for table `xta_obj_option_available`
--

DROP TABLE IF EXISTS `xta_obj_option_available`;
CREATE TABLE `xta_obj_option_available` (
  `id` int(11) NOT NULL,
  `id_obj_option` int(11) NOT NULL DEFAULT '0' COMMENT 'Ссылка на опцию',
  `id_parent` int(11) NOT NULL DEFAULT '0' COMMENT 'Родительский элемент',
  `name` varchar(200) NOT NULL COMMENT 'Имя'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Список доступных значений для опций объявлений';

--
-- Dumping data for table `xta_obj_option_available`
--

INSERT INTO `xta_obj_option_available` VALUES(1, 9, 0, 'Honda');
INSERT INTO `xta_obj_option_available` VALUES(2, 9, 0, 'Toyota');
INSERT INTO `xta_obj_option_available` VALUES(3, 9, 2, 'Yaris');
INSERT INTO `xta_obj_option_available` VALUES(4, 9, 2, 'Camry');
INSERT INTO `xta_obj_option_available` VALUES(5, 9, 2, 'Corola');
INSERT INTO `xta_obj_option_available` VALUES(6, 9, 1, 'Accord');
INSERT INTO `xta_obj_option_available` VALUES(7, 3, 0, '1');
INSERT INTO `xta_obj_option_available` VALUES(8, 3, 0, '2');
INSERT INTO `xta_obj_option_available` VALUES(9, 3, 0, '3');
INSERT INTO `xta_obj_option_available` VALUES(10, 3, 0, '4');
INSERT INTO `xta_obj_option_available` VALUES(11, 3, 0, '5');
INSERT INTO `xta_obj_option_available` VALUES(12, 3, 0, '6');
INSERT INTO `xta_obj_option_available` VALUES(13, 3, 0, '7');
INSERT INTO `xta_obj_option_available` VALUES(14, 3, 0, '8');
INSERT INTO `xta_obj_option_available` VALUES(15, 3, 0, '9');
INSERT INTO `xta_obj_option_available` VALUES(16, 3, 0, '10');
INSERT INTO `xta_obj_option_available` VALUES(17, 3, 0, '11');
INSERT INTO `xta_obj_option_available` VALUES(18, 3, 0, '12');
INSERT INTO `xta_obj_option_available` VALUES(19, 3, 0, '13');
INSERT INTO `xta_obj_option_available` VALUES(20, 3, 0, '14');
INSERT INTO `xta_obj_option_available` VALUES(21, 3, 0, '15');
INSERT INTO `xta_obj_option_available` VALUES(22, 3, 0, '16');
INSERT INTO `xta_obj_option_available` VALUES(23, 3, 0, '17');
INSERT INTO `xta_obj_option_available` VALUES(24, 3, 0, '18');
INSERT INTO `xta_obj_option_available` VALUES(25, 3, 0, '19');
INSERT INTO `xta_obj_option_available` VALUES(26, 3, 0, '20');
INSERT INTO `xta_obj_option_available` VALUES(27, 3, 0, '21');
INSERT INTO `xta_obj_option_available` VALUES(28, 3, 0, '22');
INSERT INTO `xta_obj_option_available` VALUES(29, 3, 0, '23');
INSERT INTO `xta_obj_option_available` VALUES(30, 3, 0, '24');
INSERT INTO `xta_obj_option_available` VALUES(31, 3, 0, '25');
INSERT INTO `xta_obj_option_available` VALUES(32, 3, 0, '26');
INSERT INTO `xta_obj_option_available` VALUES(33, 3, 0, '27');
INSERT INTO `xta_obj_option_available` VALUES(34, 3, 0, '28');
INSERT INTO `xta_obj_option_available` VALUES(35, 3, 0, '29');
INSERT INTO `xta_obj_option_available` VALUES(36, 3, 0, '30');
INSERT INTO `xta_obj_option_available` VALUES(37, 3, 0, '31');
INSERT INTO `xta_obj_option_available` VALUES(38, 3, 0, '32');
INSERT INTO `xta_obj_option_available` VALUES(39, 4, 0, '1');
INSERT INTO `xta_obj_option_available` VALUES(40, 4, 0, '2');
INSERT INTO `xta_obj_option_available` VALUES(41, 4, 0, '3');
INSERT INTO `xta_obj_option_available` VALUES(42, 4, 0, '4');
INSERT INTO `xta_obj_option_available` VALUES(43, 4, 0, '5');
INSERT INTO `xta_obj_option_available` VALUES(44, 4, 0, '6');
INSERT INTO `xta_obj_option_available` VALUES(45, 4, 0, '7');
INSERT INTO `xta_obj_option_available` VALUES(46, 4, 0, '8');
INSERT INTO `xta_obj_option_available` VALUES(47, 4, 0, '9');
INSERT INTO `xta_obj_option_available` VALUES(48, 4, 0, '10');
INSERT INTO `xta_obj_option_available` VALUES(49, 4, 0, '11');
INSERT INTO `xta_obj_option_available` VALUES(50, 4, 0, '12');
INSERT INTO `xta_obj_option_available` VALUES(51, 4, 0, '14');
INSERT INTO `xta_obj_option_available` VALUES(52, 4, 0, '15');
INSERT INTO `xta_obj_option_available` VALUES(53, 4, 0, '16');
INSERT INTO `xta_obj_option_available` VALUES(54, 4, 0, '17');
INSERT INTO `xta_obj_option_available` VALUES(55, 4, 0, '18');
INSERT INTO `xta_obj_option_available` VALUES(56, 4, 0, '19');
INSERT INTO `xta_obj_option_available` VALUES(57, 4, 0, '20');
INSERT INTO `xta_obj_option_available` VALUES(58, 4, 0, '21');
INSERT INTO `xta_obj_option_available` VALUES(59, 4, 0, '22');
INSERT INTO `xta_obj_option_available` VALUES(60, 4, 0, '23');
INSERT INTO `xta_obj_option_available` VALUES(61, 4, 0, '24');
INSERT INTO `xta_obj_option_available` VALUES(62, 4, 0, '25');
INSERT INTO `xta_obj_option_available` VALUES(63, 4, 0, '26');
INSERT INTO `xta_obj_option_available` VALUES(64, 4, 0, '27');
INSERT INTO `xta_obj_option_available` VALUES(65, 4, 0, '28');
INSERT INTO `xta_obj_option_available` VALUES(66, 4, 0, '29');
INSERT INTO `xta_obj_option_available` VALUES(67, 4, 0, '30');
INSERT INTO `xta_obj_option_available` VALUES(68, 4, 0, '31');
INSERT INTO `xta_obj_option_available` VALUES(69, 4, 0, '32');
INSERT INTO `xta_obj_option_available` VALUES(70, 5, 0, '1');
INSERT INTO `xta_obj_option_available` VALUES(71, 5, 0, '2');
INSERT INTO `xta_obj_option_available` VALUES(72, 5, 0, '3');
INSERT INTO `xta_obj_option_available` VALUES(73, 5, 0, '4');
INSERT INTO `xta_obj_option_available` VALUES(74, 5, 0, '5');
INSERT INTO `xta_obj_option_available` VALUES(75, 5, 0, '6');
INSERT INTO `xta_obj_option_available` VALUES(76, 10, 0, '1930');
INSERT INTO `xta_obj_option_available` VALUES(77, 10, 0, '1931');
INSERT INTO `xta_obj_option_available` VALUES(78, 10, 0, '1932');
INSERT INTO `xta_obj_option_available` VALUES(79, 10, 0, '1933');
INSERT INTO `xta_obj_option_available` VALUES(80, 10, 0, '1934');
INSERT INTO `xta_obj_option_available` VALUES(81, 10, 0, '1935');
INSERT INTO `xta_obj_option_available` VALUES(82, 10, 0, '1936');
INSERT INTO `xta_obj_option_available` VALUES(83, 10, 0, '1937');
INSERT INTO `xta_obj_option_available` VALUES(84, 10, 0, '1938');
INSERT INTO `xta_obj_option_available` VALUES(85, 10, 0, '1939');
INSERT INTO `xta_obj_option_available` VALUES(86, 10, 0, '1940');
INSERT INTO `xta_obj_option_available` VALUES(87, 10, 0, '1941');
INSERT INTO `xta_obj_option_available` VALUES(88, 10, 0, '1942');
INSERT INTO `xta_obj_option_available` VALUES(89, 10, 0, '1943');
INSERT INTO `xta_obj_option_available` VALUES(90, 10, 0, '1944');
INSERT INTO `xta_obj_option_available` VALUES(91, 10, 0, '1945');
INSERT INTO `xta_obj_option_available` VALUES(92, 10, 0, '1946');
INSERT INTO `xta_obj_option_available` VALUES(93, 10, 0, '1947');
INSERT INTO `xta_obj_option_available` VALUES(94, 10, 0, '1948');
INSERT INTO `xta_obj_option_available` VALUES(95, 10, 0, '1949');
INSERT INTO `xta_obj_option_available` VALUES(96, 10, 0, '1950');
INSERT INTO `xta_obj_option_available` VALUES(97, 10, 0, '1951');
INSERT INTO `xta_obj_option_available` VALUES(98, 10, 0, '1952');
INSERT INTO `xta_obj_option_available` VALUES(99, 10, 0, '1953');
INSERT INTO `xta_obj_option_available` VALUES(100, 10, 0, '1954');
INSERT INTO `xta_obj_option_available` VALUES(101, 10, 0, '1955');
INSERT INTO `xta_obj_option_available` VALUES(102, 10, 0, '1956');
INSERT INTO `xta_obj_option_available` VALUES(103, 10, 0, '1957');
INSERT INTO `xta_obj_option_available` VALUES(104, 10, 0, '1958');
INSERT INTO `xta_obj_option_available` VALUES(105, 10, 0, '1959');
INSERT INTO `xta_obj_option_available` VALUES(106, 10, 0, '1960');
INSERT INTO `xta_obj_option_available` VALUES(107, 10, 0, '1961');
INSERT INTO `xta_obj_option_available` VALUES(108, 10, 0, '1962');
INSERT INTO `xta_obj_option_available` VALUES(109, 10, 0, '1963');
INSERT INTO `xta_obj_option_available` VALUES(110, 10, 0, '1964');
INSERT INTO `xta_obj_option_available` VALUES(111, 10, 0, '1965');
INSERT INTO `xta_obj_option_available` VALUES(112, 10, 0, '1966');
INSERT INTO `xta_obj_option_available` VALUES(113, 10, 0, '1967');
INSERT INTO `xta_obj_option_available` VALUES(114, 10, 0, '1968');
INSERT INTO `xta_obj_option_available` VALUES(115, 10, 0, '1969');
INSERT INTO `xta_obj_option_available` VALUES(116, 10, 0, '1970');
INSERT INTO `xta_obj_option_available` VALUES(117, 10, 0, '1971');
INSERT INTO `xta_obj_option_available` VALUES(118, 10, 0, '1972');
INSERT INTO `xta_obj_option_available` VALUES(119, 10, 0, '1973');
INSERT INTO `xta_obj_option_available` VALUES(120, 10, 0, '1974');
INSERT INTO `xta_obj_option_available` VALUES(121, 10, 0, '1975');
INSERT INTO `xta_obj_option_available` VALUES(122, 10, 0, '1976');
INSERT INTO `xta_obj_option_available` VALUES(123, 10, 0, '1977');
INSERT INTO `xta_obj_option_available` VALUES(124, 10, 0, '1978');
INSERT INTO `xta_obj_option_available` VALUES(125, 10, 0, '1979');
INSERT INTO `xta_obj_option_available` VALUES(126, 10, 0, '1980');
INSERT INTO `xta_obj_option_available` VALUES(127, 10, 0, '1981');
INSERT INTO `xta_obj_option_available` VALUES(128, 10, 0, '1982');
INSERT INTO `xta_obj_option_available` VALUES(129, 10, 0, '1983');
INSERT INTO `xta_obj_option_available` VALUES(130, 10, 0, '1984');
INSERT INTO `xta_obj_option_available` VALUES(131, 10, 0, '1985');
INSERT INTO `xta_obj_option_available` VALUES(132, 10, 0, '1986');
INSERT INTO `xta_obj_option_available` VALUES(133, 10, 0, '1987');
INSERT INTO `xta_obj_option_available` VALUES(134, 10, 0, '1988');
INSERT INTO `xta_obj_option_available` VALUES(135, 10, 0, '1989');
INSERT INTO `xta_obj_option_available` VALUES(136, 10, 0, '1990');
INSERT INTO `xta_obj_option_available` VALUES(137, 10, 0, '1991');
INSERT INTO `xta_obj_option_available` VALUES(138, 10, 0, '1992');
INSERT INTO `xta_obj_option_available` VALUES(139, 10, 0, '1993');
INSERT INTO `xta_obj_option_available` VALUES(140, 10, 0, '1994');
INSERT INTO `xta_obj_option_available` VALUES(141, 10, 0, '1995');
INSERT INTO `xta_obj_option_available` VALUES(142, 10, 0, '1996');
INSERT INTO `xta_obj_option_available` VALUES(143, 10, 0, '1997');
INSERT INTO `xta_obj_option_available` VALUES(144, 10, 0, '1998');
INSERT INTO `xta_obj_option_available` VALUES(145, 10, 0, '1999');
INSERT INTO `xta_obj_option_available` VALUES(146, 10, 0, '2000');
INSERT INTO `xta_obj_option_available` VALUES(147, 10, 0, '2001');
INSERT INTO `xta_obj_option_available` VALUES(148, 10, 0, '2002');
INSERT INTO `xta_obj_option_available` VALUES(149, 10, 0, '2003');
INSERT INTO `xta_obj_option_available` VALUES(150, 10, 0, '2004');
INSERT INTO `xta_obj_option_available` VALUES(151, 10, 0, '2005');
INSERT INTO `xta_obj_option_available` VALUES(152, 10, 0, '2006');
INSERT INTO `xta_obj_option_available` VALUES(153, 10, 0, '2007');
INSERT INTO `xta_obj_option_available` VALUES(154, 10, 0, '2008');
INSERT INTO `xta_obj_option_available` VALUES(155, 10, 0, '2009');
INSERT INTO `xta_obj_option_available` VALUES(156, 10, 0, '2010');
INSERT INTO `xta_obj_option_available` VALUES(157, 10, 0, '2011');
INSERT INTO `xta_obj_option_available` VALUES(158, 10, 0, '2012');
INSERT INTO `xta_obj_option_available` VALUES(159, 10, 0, '2013');
INSERT INTO `xta_obj_option_available` VALUES(160, 10, 0, '2014');
INSERT INTO `xta_obj_option_available` VALUES(161, 10, 0, '2015');
INSERT INTO `xta_obj_option_available` VALUES(162, 10, 0, '2016');
INSERT INTO `xta_obj_option_available` VALUES(163, 6, 0, 'Бензин');
INSERT INTO `xta_obj_option_available` VALUES(164, 6, 0, 'Дизель');
INSERT INTO `xta_obj_option_available` VALUES(165, 6, 0, 'Газ');
INSERT INTO `xta_obj_option_available` VALUES(166, 6, 0, 'Другое');
INSERT INTO `xta_obj_option_available` VALUES(167, 7, 0, '1');
INSERT INTO `xta_obj_option_available` VALUES(168, 7, 0, '2');
INSERT INTO `xta_obj_option_available` VALUES(169, 7, 0, '3');
INSERT INTO `xta_obj_option_available` VALUES(170, 7, 0, '4');
INSERT INTO `xta_obj_option_available` VALUES(171, 7, 0, '5');
INSERT INTO `xta_obj_option_available` VALUES(172, 7, 0, '6');
INSERT INTO `xta_obj_option_available` VALUES(173, 7, 0, '7');
INSERT INTO `xta_obj_option_available` VALUES(174, 7, 0, '8');
INSERT INTO `xta_obj_option_available` VALUES(175, 7, 0, '9');
INSERT INTO `xta_obj_option_available` VALUES(176, 7, 0, '10');
INSERT INTO `xta_obj_option_available` VALUES(177, 7, 0, '11');
INSERT INTO `xta_obj_option_available` VALUES(178, 7, 0, '12');
INSERT INTO `xta_obj_option_available` VALUES(179, 7, 0, '13');
INSERT INTO `xta_obj_option_available` VALUES(180, 7, 0, '14');
INSERT INTO `xta_obj_option_available` VALUES(181, 7, 0, '15');
INSERT INTO `xta_obj_option_available` VALUES(182, 7, 0, '16');
INSERT INTO `xta_obj_option_available` VALUES(183, 7, 0, '17');
INSERT INTO `xta_obj_option_available` VALUES(184, 7, 0, '18');
INSERT INTO `xta_obj_option_available` VALUES(185, 7, 0, '19');
INSERT INTO `xta_obj_option_available` VALUES(186, 7, 0, '20');
INSERT INTO `xta_obj_option_available` VALUES(187, 7, 0, '21');
INSERT INTO `xta_obj_option_available` VALUES(188, 7, 0, '22');
INSERT INTO `xta_obj_option_available` VALUES(189, 7, 0, '23');
INSERT INTO `xta_obj_option_available` VALUES(190, 7, 0, '24');
INSERT INTO `xta_obj_option_available` VALUES(191, 7, 0, '25');
INSERT INTO `xta_obj_option_available` VALUES(192, 7, 0, '26');
INSERT INTO `xta_obj_option_available` VALUES(193, 7, 0, '27');
INSERT INTO `xta_obj_option_available` VALUES(194, 7, 0, '28');
INSERT INTO `xta_obj_option_available` VALUES(195, 7, 0, '29');
INSERT INTO `xta_obj_option_available` VALUES(196, 7, 0, '30');
INSERT INTO `xta_obj_option_available` VALUES(197, 7, 0, '31');
INSERT INTO `xta_obj_option_available` VALUES(198, 7, 0, '32');
INSERT INTO `xta_obj_option_available` VALUES(199, 7, 0, '33');
INSERT INTO `xta_obj_option_available` VALUES(200, 7, 0, '34');
INSERT INTO `xta_obj_option_available` VALUES(201, 7, 0, '35');
INSERT INTO `xta_obj_option_available` VALUES(202, 7, 0, '36');
INSERT INTO `xta_obj_option_available` VALUES(203, 7, 0, '37');
INSERT INTO `xta_obj_option_available` VALUES(204, 7, 0, '38');
INSERT INTO `xta_obj_option_available` VALUES(205, 7, 0, '39');
INSERT INTO `xta_obj_option_available` VALUES(206, 7, 0, '40');
INSERT INTO `xta_obj_option_available` VALUES(207, 7, 0, '41');
INSERT INTO `xta_obj_option_available` VALUES(208, 7, 0, '42');
INSERT INTO `xta_obj_option_available` VALUES(209, 7, 0, '43');
INSERT INTO `xta_obj_option_available` VALUES(210, 7, 0, '44');
INSERT INTO `xta_obj_option_available` VALUES(211, 7, 0, '45');
INSERT INTO `xta_obj_option_available` VALUES(212, 7, 0, '46');
INSERT INTO `xta_obj_option_available` VALUES(213, 7, 0, '47');
INSERT INTO `xta_obj_option_available` VALUES(214, 7, 0, '48');
INSERT INTO `xta_obj_option_available` VALUES(215, 7, 0, '49');
INSERT INTO `xta_obj_option_available` VALUES(216, 7, 0, '50');
INSERT INTO `xta_obj_option_available` VALUES(217, 7, 0, '51');
INSERT INTO `xta_obj_option_available` VALUES(218, 7, 0, '52');
INSERT INTO `xta_obj_option_available` VALUES(219, 7, 0, '53');
INSERT INTO `xta_obj_option_available` VALUES(220, 7, 0, '54');
INSERT INTO `xta_obj_option_available` VALUES(221, 7, 0, '55');
INSERT INTO `xta_obj_option_available` VALUES(222, 7, 0, '56');
INSERT INTO `xta_obj_option_available` VALUES(223, 7, 0, '57');
INSERT INTO `xta_obj_option_available` VALUES(224, 7, 0, '58');
INSERT INTO `xta_obj_option_available` VALUES(225, 7, 0, '59');
INSERT INTO `xta_obj_option_available` VALUES(226, 7, 0, '60');
INSERT INTO `xta_obj_option_available` VALUES(227, 7, 0, '61');
INSERT INTO `xta_obj_option_available` VALUES(228, 7, 0, '62');
INSERT INTO `xta_obj_option_available` VALUES(229, 7, 0, '63');
INSERT INTO `xta_obj_option_available` VALUES(230, 7, 0, '64');
INSERT INTO `xta_obj_option_available` VALUES(231, 7, 0, '65');
INSERT INTO `xta_obj_option_available` VALUES(232, 7, 0, '66');
INSERT INTO `xta_obj_option_available` VALUES(233, 7, 0, '67');
INSERT INTO `xta_obj_option_available` VALUES(234, 7, 0, '68');
INSERT INTO `xta_obj_option_available` VALUES(235, 7, 0, '69');
INSERT INTO `xta_obj_option_available` VALUES(236, 7, 0, '70');
INSERT INTO `xta_obj_option_available` VALUES(237, 7, 0, '71');
INSERT INTO `xta_obj_option_available` VALUES(238, 7, 0, '72');
INSERT INTO `xta_obj_option_available` VALUES(239, 7, 0, '73');
INSERT INTO `xta_obj_option_available` VALUES(240, 7, 0, '74');
INSERT INTO `xta_obj_option_available` VALUES(241, 7, 0, '75');
INSERT INTO `xta_obj_option_available` VALUES(242, 7, 0, '76');
INSERT INTO `xta_obj_option_available` VALUES(243, 7, 0, '77');
INSERT INTO `xta_obj_option_available` VALUES(244, 7, 0, '78');
INSERT INTO `xta_obj_option_available` VALUES(245, 7, 0, '79');
INSERT INTO `xta_obj_option_available` VALUES(246, 7, 0, '80');
INSERT INTO `xta_obj_option_available` VALUES(247, 7, 0, '81');
INSERT INTO `xta_obj_option_available` VALUES(248, 7, 0, '82');
INSERT INTO `xta_obj_option_available` VALUES(249, 7, 0, '83');
INSERT INTO `xta_obj_option_available` VALUES(250, 7, 0, '84');
INSERT INTO `xta_obj_option_available` VALUES(251, 7, 0, '85');
INSERT INTO `xta_obj_option_available` VALUES(252, 7, 0, '86');
INSERT INTO `xta_obj_option_available` VALUES(253, 7, 0, '87');
INSERT INTO `xta_obj_option_available` VALUES(254, 7, 0, '88');
INSERT INTO `xta_obj_option_available` VALUES(255, 7, 0, '89');
INSERT INTO `xta_obj_option_available` VALUES(256, 7, 0, '90');
INSERT INTO `xta_obj_option_available` VALUES(257, 7, 0, '91');
INSERT INTO `xta_obj_option_available` VALUES(258, 7, 0, '92');
INSERT INTO `xta_obj_option_available` VALUES(259, 7, 0, '93');
INSERT INTO `xta_obj_option_available` VALUES(260, 7, 0, '94');
INSERT INTO `xta_obj_option_available` VALUES(261, 7, 0, '95');
INSERT INTO `xta_obj_option_available` VALUES(262, 7, 0, '96');
INSERT INTO `xta_obj_option_available` VALUES(263, 7, 0, '97');
INSERT INTO `xta_obj_option_available` VALUES(264, 7, 0, '98');
INSERT INTO `xta_obj_option_available` VALUES(265, 7, 0, '99');

-- --------------------------------------------------------

--
-- Table structure for table `xta_obj_option_in_category`
--

DROP TABLE IF EXISTS `xta_obj_option_in_category`;
CREATE TABLE `xta_obj_option_in_category` (
  `id` int(11) NOT NULL,
  `id_obj_option` int(11) NOT NULL COMMENT 'Ссылка на опцию',
  `id_obj_category` int(11) NOT NULL COMMENT 'Ссылка на категорию',
  `use_setting` int(11) NOT NULL DEFAULT '0' COMMENT 'Использовать свойство в категории',
  `use_filter` int(11) NOT NULL DEFAULT '0' COMMENT 'Использовать фильтрацию в категории'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Связь категорий и опций';

--
-- Dumping data for table `xta_obj_option_in_category`
--

INSERT INTO `xta_obj_option_in_category` VALUES(72, 4, 2, 1, 1);
INSERT INTO `xta_obj_option_in_category` VALUES(74, 4, 3, 1, 1);
INSERT INTO `xta_obj_option_in_category` VALUES(75, 4, 34, 0, 1);
INSERT INTO `xta_obj_option_in_category` VALUES(76, 4, 39, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(77, 4, 42, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(78, 4, 47, 1, 1);
INSERT INTO `xta_obj_option_in_category` VALUES(79, 4, 48, 1, 1);
INSERT INTO `xta_obj_option_in_category` VALUES(80, 4, 49, 1, 1);
INSERT INTO `xta_obj_option_in_category` VALUES(82, 5, 2, 1, 1);
INSERT INTO `xta_obj_option_in_category` VALUES(83, 5, 4, 0, 1);
INSERT INTO `xta_obj_option_in_category` VALUES(84, 5, 16, 1, 1);
INSERT INTO `xta_obj_option_in_category` VALUES(85, 5, 17, 1, 1);
INSERT INTO `xta_obj_option_in_category` VALUES(86, 5, 18, 1, 1);
INSERT INTO `xta_obj_option_in_category` VALUES(88, 5, 3, 1, 1);
INSERT INTO `xta_obj_option_in_category` VALUES(102, 5, 47, 1, 1);
INSERT INTO `xta_obj_option_in_category` VALUES(103, 5, 48, 1, 1);
INSERT INTO `xta_obj_option_in_category` VALUES(104, 5, 49, 1, 1);
INSERT INTO `xta_obj_option_in_category` VALUES(109, 5, 54, 1, 1);
INSERT INTO `xta_obj_option_in_category` VALUES(110, 5, 55, 1, 1);
INSERT INTO `xta_obj_option_in_category` VALUES(119, 5, 64, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(128, 5, 73, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(134, 5, 79, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(580, 8, 2, 1, 1);
INSERT INTO `xta_obj_option_in_category` VALUES(581, 8, 4, 0, 1);
INSERT INTO `xta_obj_option_in_category` VALUES(582, 8, 16, 1, 1);
INSERT INTO `xta_obj_option_in_category` VALUES(583, 8, 17, 1, 1);
INSERT INTO `xta_obj_option_in_category` VALUES(584, 8, 18, 1, 1);
INSERT INTO `xta_obj_option_in_category` VALUES(586, 8, 3, 1, 1);
INSERT INTO `xta_obj_option_in_category` VALUES(645, 3, 3, 1, 1);
INSERT INTO `xta_obj_option_in_category` VALUES(648, 3, 2, 1, 1);
INSERT INTO `xta_obj_option_in_category` VALUES(649, 3, 4, 0, 1);
INSERT INTO `xta_obj_option_in_category` VALUES(650, 3, 16, 1, 1);
INSERT INTO `xta_obj_option_in_category` VALUES(651, 3, 17, 1, 1);
INSERT INTO `xta_obj_option_in_category` VALUES(652, 3, 18, 1, 1);
INSERT INTO `xta_obj_option_in_category` VALUES(653, 3, 34, 0, 1);
INSERT INTO `xta_obj_option_in_category` VALUES(654, 3, 35, 0, 1);
INSERT INTO `xta_obj_option_in_category` VALUES(655, 3, 36, 0, 1);
INSERT INTO `xta_obj_option_in_category` VALUES(656, 3, 39, 1, 1);
INSERT INTO `xta_obj_option_in_category` VALUES(657, 3, 47, 1, 1);
INSERT INTO `xta_obj_option_in_category` VALUES(658, 3, 48, 1, 1);
INSERT INTO `xta_obj_option_in_category` VALUES(659, 3, 49, 1, 1);
INSERT INTO `xta_obj_option_in_category` VALUES(660, 3, 50, 1, 1);
INSERT INTO `xta_obj_option_in_category` VALUES(661, 3, 51, 1, 1);
INSERT INTO `xta_obj_option_in_category` VALUES(662, 3, 52, 1, 1);
INSERT INTO `xta_obj_option_in_category` VALUES(663, 3, 53, 1, 1);
INSERT INTO `xta_obj_option_in_category` VALUES(664, 3, 54, 1, 1);
INSERT INTO `xta_obj_option_in_category` VALUES(665, 3, 55, 1, 1);
INSERT INTO `xta_obj_option_in_category` VALUES(691, 3, 37, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(692, 3, 38, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(693, 3, 40, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(694, 3, 41, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(695, 3, 42, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(696, 3, 43, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(697, 3, 44, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(698, 3, 45, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(700, 3, 56, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(701, 3, 57, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(702, 3, 58, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(703, 3, 59, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(704, 3, 60, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(705, 3, 61, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(706, 3, 62, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(707, 3, 63, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(708, 3, 64, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(709, 3, 65, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(710, 3, 66, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(711, 3, 67, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(712, 3, 68, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(713, 3, 69, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(714, 3, 70, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(715, 3, 71, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(716, 3, 72, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(717, 3, 73, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(718, 3, 74, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(719, 3, 75, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(720, 3, 76, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(721, 3, 77, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(722, 3, 78, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(723, 3, 79, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(724, 3, 80, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(725, 3, 81, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(1249, 4, 4, 0, 1);
INSERT INTO `xta_obj_option_in_category` VALUES(1262, 4, 16, 1, 1);
INSERT INTO `xta_obj_option_in_category` VALUES(1263, 4, 17, 1, 1);
INSERT INTO `xta_obj_option_in_category` VALUES(1264, 4, 18, 1, 1);
INSERT INTO `xta_obj_option_in_category` VALUES(1278, 4, 35, 0, 1);
INSERT INTO `xta_obj_option_in_category` VALUES(1279, 4, 36, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(1280, 4, 37, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(1281, 4, 38, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(1282, 4, 40, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(1283, 4, 41, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(1284, 4, 43, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(1285, 4, 44, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(1286, 4, 45, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(1288, 4, 50, 1, 1);
INSERT INTO `xta_obj_option_in_category` VALUES(1289, 4, 51, 1, 1);
INSERT INTO `xta_obj_option_in_category` VALUES(1290, 4, 52, 1, 1);
INSERT INTO `xta_obj_option_in_category` VALUES(1291, 4, 53, 1, 1);
INSERT INTO `xta_obj_option_in_category` VALUES(1292, 4, 54, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(1293, 4, 55, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(1294, 4, 56, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(1295, 4, 57, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(1296, 4, 58, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(1297, 4, 59, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(1298, 4, 60, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(1299, 4, 61, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(1300, 4, 62, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(1301, 4, 63, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(1302, 4, 64, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(1303, 4, 65, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(1304, 4, 66, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(1305, 4, 67, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(1306, 4, 68, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(1307, 4, 69, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(1308, 4, 70, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(1309, 4, 71, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(1310, 4, 72, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(1311, 4, 73, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(1312, 4, 74, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(1313, 4, 75, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(1314, 4, 76, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(1315, 4, 77, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(1316, 4, 78, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(1317, 4, 79, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(1318, 4, 80, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(1319, 4, 81, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(1868, 5, 34, 0, 1);
INSERT INTO `xta_obj_option_in_category` VALUES(1869, 5, 35, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(1870, 5, 36, 0, 1);
INSERT INTO `xta_obj_option_in_category` VALUES(1871, 5, 37, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(1872, 5, 38, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(1873, 5, 39, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(1874, 5, 40, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(1875, 5, 41, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(1876, 5, 42, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(1877, 5, 43, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(1878, 5, 44, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(1879, 5, 45, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(1881, 5, 50, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(1882, 5, 51, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(1883, 5, 52, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(1884, 5, 53, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(1885, 5, 56, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(1886, 5, 57, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(1887, 5, 58, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(1888, 5, 59, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(1889, 5, 60, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(1890, 5, 61, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(1891, 5, 62, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(1892, 5, 63, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(1893, 5, 65, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(1894, 5, 66, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(1895, 5, 67, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(1896, 5, 68, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(1897, 5, 69, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(1898, 5, 70, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(1899, 5, 71, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(1900, 5, 72, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(1901, 5, 74, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(1902, 5, 75, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(1903, 5, 76, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(1904, 5, 77, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(1905, 5, 78, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(1906, 5, 80, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(1907, 5, 81, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(2431, 6, 2, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(2432, 6, 4, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(2443, 6, 16, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(2444, 6, 17, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(2445, 6, 18, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(2447, 6, 3, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(2461, 6, 34, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(2462, 6, 35, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(2463, 6, 36, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(2464, 6, 37, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(2465, 6, 38, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(2466, 6, 39, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(2467, 6, 40, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(2468, 6, 41, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(2469, 6, 42, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(2470, 6, 43, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(2471, 6, 44, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(2472, 6, 45, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(2474, 6, 47, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(2475, 6, 48, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(2476, 6, 49, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(2477, 6, 50, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(2478, 6, 51, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(2479, 6, 52, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(2480, 6, 53, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(2481, 6, 54, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(2482, 6, 55, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(2483, 6, 56, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(2484, 6, 57, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(2485, 6, 58, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(2486, 6, 59, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(2487, 6, 60, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(2488, 6, 61, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(2489, 6, 62, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(2490, 6, 63, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(2491, 6, 64, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(2492, 6, 65, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(2493, 6, 66, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(2494, 6, 67, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(2495, 6, 68, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(2496, 6, 69, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(2497, 6, 70, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(2498, 6, 71, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(2499, 6, 72, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(2500, 6, 73, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(2501, 6, 74, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(2502, 6, 75, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(2503, 6, 76, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(2504, 6, 77, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(2505, 6, 78, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(2506, 6, 79, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(2507, 6, 80, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(2508, 6, 81, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(2806, 7, 2, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(2807, 7, 4, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(2819, 7, 16, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(2820, 7, 17, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(2821, 7, 18, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(2823, 7, 3, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(2837, 7, 34, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(2838, 7, 35, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(2839, 7, 36, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(2840, 7, 37, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(2841, 7, 38, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(2842, 7, 39, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(2843, 7, 40, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(2844, 7, 41, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(2845, 7, 42, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(2846, 7, 43, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(2847, 7, 44, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(2848, 7, 45, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(2850, 7, 47, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(2851, 7, 48, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(2852, 7, 49, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(2853, 7, 50, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(2854, 7, 51, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(2855, 7, 52, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(2856, 7, 53, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(2857, 7, 54, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(2858, 7, 55, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(2859, 7, 56, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(2860, 7, 57, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(2861, 7, 58, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(2862, 7, 59, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(2863, 7, 60, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(2864, 7, 61, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(2865, 7, 62, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(2866, 7, 63, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(2867, 7, 64, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(2868, 7, 65, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(2869, 7, 66, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(2870, 7, 67, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(2871, 7, 68, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(2872, 7, 69, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(2873, 7, 70, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(2874, 7, 71, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(2875, 7, 72, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(2876, 7, 73, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(2877, 7, 74, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(2878, 7, 75, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(2879, 7, 76, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(2880, 7, 77, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(2881, 7, 78, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(2882, 7, 79, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(2883, 7, 80, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(2884, 7, 81, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(3420, 8, 34, 0, 1);
INSERT INTO `xta_obj_option_in_category` VALUES(3421, 8, 35, 0, 1);
INSERT INTO `xta_obj_option_in_category` VALUES(3422, 8, 36, 0, 1);
INSERT INTO `xta_obj_option_in_category` VALUES(3423, 8, 37, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(3424, 8, 38, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(3425, 8, 39, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(3426, 8, 40, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(3427, 8, 41, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(3428, 8, 42, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(3429, 8, 43, 0, 1);
INSERT INTO `xta_obj_option_in_category` VALUES(3430, 8, 44, 0, 1);
INSERT INTO `xta_obj_option_in_category` VALUES(3431, 8, 45, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(3433, 8, 47, 1, 1);
INSERT INTO `xta_obj_option_in_category` VALUES(3434, 8, 48, 1, 1);
INSERT INTO `xta_obj_option_in_category` VALUES(3435, 8, 49, 1, 1);
INSERT INTO `xta_obj_option_in_category` VALUES(3436, 8, 50, 1, 1);
INSERT INTO `xta_obj_option_in_category` VALUES(3437, 8, 51, 1, 1);
INSERT INTO `xta_obj_option_in_category` VALUES(3438, 8, 52, 1, 1);
INSERT INTO `xta_obj_option_in_category` VALUES(3439, 8, 53, 1, 1);
INSERT INTO `xta_obj_option_in_category` VALUES(3440, 8, 54, 1, 1);
INSERT INTO `xta_obj_option_in_category` VALUES(3441, 8, 55, 1, 1);
INSERT INTO `xta_obj_option_in_category` VALUES(3442, 8, 56, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(3443, 8, 57, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(3444, 8, 58, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(3445, 8, 59, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(3446, 8, 60, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(3447, 8, 61, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(3448, 8, 62, 1, 1);
INSERT INTO `xta_obj_option_in_category` VALUES(3449, 8, 63, 1, 1);
INSERT INTO `xta_obj_option_in_category` VALUES(3450, 8, 64, 1, 1);
INSERT INTO `xta_obj_option_in_category` VALUES(3451, 8, 65, 1, 1);
INSERT INTO `xta_obj_option_in_category` VALUES(3452, 8, 66, 1, 1);
INSERT INTO `xta_obj_option_in_category` VALUES(3453, 8, 67, 1, 1);
INSERT INTO `xta_obj_option_in_category` VALUES(3454, 8, 68, 1, 1);
INSERT INTO `xta_obj_option_in_category` VALUES(3455, 8, 69, 1, 1);
INSERT INTO `xta_obj_option_in_category` VALUES(3456, 8, 70, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(3457, 8, 71, 1, 1);
INSERT INTO `xta_obj_option_in_category` VALUES(3458, 8, 72, 1, 1);
INSERT INTO `xta_obj_option_in_category` VALUES(3459, 8, 73, 1, 1);
INSERT INTO `xta_obj_option_in_category` VALUES(3460, 8, 74, 1, 1);
INSERT INTO `xta_obj_option_in_category` VALUES(3461, 8, 75, 1, 1);
INSERT INTO `xta_obj_option_in_category` VALUES(3462, 8, 76, 1, 1);
INSERT INTO `xta_obj_option_in_category` VALUES(3463, 8, 77, 1, 1);
INSERT INTO `xta_obj_option_in_category` VALUES(3464, 8, 78, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(3465, 8, 79, 1, 1);
INSERT INTO `xta_obj_option_in_category` VALUES(3466, 8, 80, 1, 1);
INSERT INTO `xta_obj_option_in_category` VALUES(3467, 8, 81, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(3991, 9, 2, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(3992, 9, 4, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(4005, 9, 16, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(4006, 9, 17, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(4007, 9, 18, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(4009, 9, 3, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(4023, 9, 34, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(4024, 9, 35, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(4025, 9, 36, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(4026, 9, 37, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(4027, 9, 38, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(4028, 9, 39, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(4029, 9, 40, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(4030, 9, 41, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(4031, 9, 42, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(4032, 9, 43, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(4033, 9, 44, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(4034, 9, 45, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(4036, 9, 47, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(4037, 9, 48, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(4038, 9, 49, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(4039, 9, 50, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(4040, 9, 51, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(4041, 9, 52, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(4042, 9, 53, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(4043, 9, 54, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(4044, 9, 55, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(4045, 9, 56, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(4046, 9, 57, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(4047, 9, 58, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(4048, 9, 59, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(4049, 9, 60, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(4050, 9, 61, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(4051, 9, 62, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(4052, 9, 63, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(4053, 9, 64, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(4054, 9, 65, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(4055, 9, 66, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(4056, 9, 67, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(4057, 9, 68, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(4058, 9, 69, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(4059, 9, 70, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(4060, 9, 71, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(4061, 9, 72, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(4062, 9, 73, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(4063, 9, 74, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(4064, 9, 75, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(4065, 9, 76, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(4066, 9, 77, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(4067, 9, 78, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(4068, 9, 79, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(4069, 9, 80, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(4070, 9, 81, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(4593, 10, 2, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(4594, 10, 4, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(4607, 10, 16, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(4608, 10, 17, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(4609, 10, 18, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(4611, 10, 3, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(4625, 10, 34, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(4626, 10, 35, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(4627, 10, 36, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(4628, 10, 37, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(4629, 10, 38, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(4630, 10, 39, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(4631, 10, 40, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(4632, 10, 41, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(4633, 10, 42, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(4634, 10, 43, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(4635, 10, 44, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(4636, 10, 45, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(4638, 10, 47, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(4639, 10, 48, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(4640, 10, 49, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(4641, 10, 50, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(4642, 10, 51, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(4643, 10, 52, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(4644, 10, 53, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(4645, 10, 54, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(4646, 10, 55, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(4647, 10, 56, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(4648, 10, 57, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(4649, 10, 58, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(4650, 10, 59, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(4651, 10, 60, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(4652, 10, 61, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(4653, 10, 62, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(4654, 10, 63, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(4655, 10, 64, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(4656, 10, 65, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(4657, 10, 66, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(4658, 10, 67, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(4659, 10, 68, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(4660, 10, 69, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(4661, 10, 70, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(4662, 10, 71, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(4663, 10, 72, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(4664, 10, 73, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(4665, 10, 74, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(4666, 10, 75, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(4667, 10, 76, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(4668, 10, 77, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(4669, 10, 78, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(4670, 10, 79, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(4671, 10, 80, 0, 0);
INSERT INTO `xta_obj_option_in_category` VALUES(4672, 10, 81, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `xta_obj_option_value`
--

DROP TABLE IF EXISTS `xta_obj_option_value`;
CREATE TABLE `xta_obj_option_value` (
  `id` int(11) NOT NULL,
  `id_obj` int(11) NOT NULL COMMENT 'Ссылка на объявление',
  `id_obj_option` int(11) NOT NULL COMMENT 'Ссылка на опцию объявления',
  `value` text NOT NULL COMMENT 'Значение'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Список значений опций объявлений';

--
-- Dumping data for table `xta_obj_option_value`
--

INSERT INTO `xta_obj_option_value` VALUES(1, 1, 3, '7');
INSERT INTO `xta_obj_option_value` VALUES(2, 1, 4, '14');
INSERT INTO `xta_obj_option_value` VALUES(3, 1, 5, '3');
INSERT INTO `xta_obj_option_value` VALUES(4, 1, 8, '44');
INSERT INTO `xta_obj_option_value` VALUES(5, 2, 3, '20');
INSERT INTO `xta_obj_option_value` VALUES(6, 2, 4, '2');
INSERT INTO `xta_obj_option_value` VALUES(7, 2, 5, '1');
INSERT INTO `xta_obj_option_value` VALUES(8, 2, 8, '35');
INSERT INTO `xta_obj_option_value` VALUES(9, 3, 4, '6');
INSERT INTO `xta_obj_option_value` VALUES(10, 3, 5, '1');
INSERT INTO `xta_obj_option_value` VALUES(11, 3, 8, '49');
INSERT INTO `xta_obj_option_value` VALUES(12, 3, 3, '20');

-- --------------------------------------------------------

--
-- Table structure for table `xta_padeg`
--

DROP TABLE IF EXISTS `xta_padeg`;
CREATE TABLE `xta_padeg` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT '' COMMENT 'Оригинальный текст',
  `padeg1` varchar(255) NOT NULL DEFAULT '',
  `padeg2` varchar(255) NOT NULL DEFAULT '',
  `padeg3` varchar(255) NOT NULL DEFAULT '',
  `padeg4` varchar(255) NOT NULL DEFAULT '',
  `padeg5` varchar(255) NOT NULL DEFAULT '' COMMENT 'Предложный (Чём? Ком?)'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `xta_padeg`
--

INSERT INTO `xta_padeg` VALUES(2, 'Кривой Рог', 'Кривого Рога', 'Кривому Рогу', 'Кривой Рог', 'Кривым Рогом', 'Кривом Роге');
INSERT INTO `xta_padeg` VALUES(3, 'Одесса', 'Одессы', 'Одессе', 'Одессу', 'Одессы', 'Одессе');

-- --------------------------------------------------------

--
-- Table structure for table `xta_page`
--

DROP TABLE IF EXISTS `xta_page`;
CREATE TABLE `xta_page` (
  `id` int(11) NOT NULL,
  `id_site` int(11) NOT NULL DEFAULT '1',
  `name` varchar(255) NOT NULL,
  `about` text NOT NULL,
  `published` int(1) NOT NULL DEFAULT '1',
  `editedon` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `eval` int(1) NOT NULL DEFAULT '0',
  `richedit` int(1) NOT NULL DEFAULT '1',
  `title` varchar(255) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT '',
  `index` int(1) NOT NULL DEFAULT '1' COMMENT 'Индексировать страницу'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `xta_page`
--

INSERT INTO `xta_page` VALUES(1, 1, 'Миссия', '<div id="lipsum">\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam facilisis vitae dolor sed scelerisque. Proin blandit, dui sed volutpat pretium, justo est eleifend mi, a tincidunt libero lacus ac augue. Proin vitae enim eu ligula lobortis finibus. Fusce eget lacus mi. Donec malesuada porttitor orci at viverra. Nam mollis felis id libero rutrum hendrerit. Vestibulum accumsan porta metus eu hendrerit. Praesent purus elit, scelerisque vitae nunc ut, vestibulum ornare urna. Pellentesque aliquam fringilla purus, id viverra turpis porttitor efficitur. Fusce pharetra purus enim, sit amet ultrices ipsum pulvinar sed. Fusce rhoncus magna in justo ullamcorper ultrices.</p>\r\n\r\n<p>Quisque mattis diam at faucibus tincidunt. Nulla non augue et lectus hendrerit lacinia et eu eros. Nunc dolor sem, vehicula et ante rutrum, luctus semper tellus. Sed vestibulum laoreet efficitur. Mauris semper mi dolor, sit amet imperdiet risus posuere sed. Quisque nec dapibus mi, in gravida augue. Quisque ullamcorper commodo mi, vel faucibus magna tempor eget. Vestibulum iaculis dapibus est. Cras scelerisque quam eget euismod tempor. Mauris pharetra vestibulum vestibulum. Cras convallis massa felis.</p>\r\n\r\n<p>Sed finibus blandit feugiat. Maecenas vel erat blandit, elementum libero non, sodales neque. Suspendisse et mollis neque. Nunc vestibulum condimentum blandit. Nam nec dui sollicitudin, efficitur mauris sed, vulputate ligula. Nullam dapibus, diam egestas dapibus lobortis, lorem justo posuere quam, sit amet vulputate est nulla nec magna. Proin feugiat hendrerit diam et placerat. Nam accumsan hendrerit est in vestibulum. Duis varius placerat purus, eget dictum odio efficitur eget. Suspendisse ac tincidunt leo. Duis ultricies ipsum sit amet dui tristique imperdiet. Praesent molestie magna id risus interdum convallis. Sed sit amet metus felis.</p>\r\n\r\n<p>Fusce placerat iaculis tortor, vel fringilla nunc tempus et. Nam sed libero maximus, rhoncus ex sit amet, convallis nibh. Duis ipsum elit, aliquet vitae ex ac, pellentesque tempus ex. Mauris dui ligula, interdum maximus vestibulum in, auctor et arcu. Vivamus at augue commodo quam sollicitudin pharetra. Duis at enim ut libero malesuada convallis non ac elit. Nunc a commodo purus, in varius ex. Sed viverra vulputate nisl, ut fermentum nisi euismod non. Curabitur neque sapien, pulvinar maximus tristique eu, ullamcorper ac nibh.</p>\r\n\r\n<p>Fusce eros justo, rhoncus ut varius nec, aliquam eu eros. Sed ut tincidunt turpis. Aenean volutpat est ac mattis vehicula. Maecenas nec lacus feugiat, auctor velit non, lacinia massa. Ut mattis, massa sed pellentesque fermentum, nibh libero ultricies neque, vel convallis quam arcu sed dui. Curabitur rutrum sed ante quis pulvinar. Nunc scelerisque nibh vitae tellus efficitur, eget pharetra elit ullamcorper. Vivamus lacinia iaculis orci et pellentesque. Pellentesque pulvinar pellentesque urna ac interdum. Maecenas mollis lorem mauris. Aenean gravida dolor at rhoncus gravida.</p>\r\n\r\n<p>Mauris venenatis fringilla lacus in cursus. Aenean maximus rutrum mauris, at condimentum massa vehicula vitae. Aliquam ut cursus est. Vivamus mattis eleifend ex eget pharetra. Phasellus sit amet egestas mi. Pellentesque ac venenatis nunc. Integer fermentum, nunc id egestas congue, orci erat tempor nibh, id elementum ex lacus vel erat. Ut ac ipsum ac orci vestibulum ultricies. Integer porta, dui quis egestas gravida, orci eros accumsan diam, in mattis libero enim at dui. Pellentesque malesuada eros vitae justo mattis hendrerit. Maecenas pretium felis mi, at molestie ex hendrerit ut.</p>\r\n\r\n<p>Proin cursus dictum vehicula. Curabitur quis sapien et dui pellentesque lobortis. Morbi rutrum mi ut mi vehicula, at maximus sem dapibus. Donec sit amet est eget ex aliquam bibendum. Aenean rhoncus, magna at tincidunt porta, sapien orci rutrum magna, vel mattis felis erat ac nunc. Nulla augue neque, euismod id dictum non, pulvinar et metus. Vestibulum porttitor massa nisi, vitae consequat velit vehicula nec. Phasellus ac facilisis arcu.</p>\r\n</div>\r\n', 1, '2016-11-18 11:57:19', 0, 1, '', '', 1);
INSERT INTO `xta_page` VALUES(2, 1, 'Команда', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam facilisis vitae dolor sed scelerisque. Proin blandit, dui sed volutpat pretium, justo est eleifend mi, a tincidunt libero lacus ac augue. Proin vitae enim eu ligula lobortis finibus. Fusce eget lacus mi. Donec malesuada porttitor orci at viverra. Nam mollis felis id libero rutrum hendrerit. Vestibulum accumsan porta metus eu hendrerit. Praesent purus elit, scelerisque vitae nunc ut, vestibulum ornare urna. Pellentesque aliquam fringilla purus, id viverra turpis porttitor efficitur. Fusce pharetra purus enim, sit amet ultrices ipsum pulvinar sed. Fusce rhoncus magna in justo ullamcorper ultrices.</p>\r\n\r\n<p>Quisque mattis diam at faucibus tincidunt. Nulla non augue et lectus hendrerit lacinia et eu eros. Nunc dolor sem, vehicula et ante rutrum, luctus semper tellus. Sed vestibulum laoreet efficitur. Mauris semper mi dolor, sit amet imperdiet risus posuere sed. Quisque nec dapibus mi, in gravida augue. Quisque ullamcorper commodo mi, vel faucibus magna tempor eget. Vestibulum iaculis dapibus est. Cras scelerisque quam eget euismod tempor. Mauris pharetra vestibulum vestibulum. Cras convallis massa felis.</p>\r\n\r\n<p>Sed finibus blandit feugiat. Maecenas vel erat blandit, elementum libero non, sodales neque. Suspendisse et mollis neque. Nunc vestibulum condimentum blandit. Nam nec dui sollicitudin, efficitur mauris sed, vulputate ligula. Nullam dapibus, diam egestas dapibus lobortis, lorem justo posuere quam, sit amet vulputate est nulla nec magna. Proin feugiat hendrerit diam et placerat. Nam accumsan hendrerit est in vestibulum. Duis varius placerat purus, eget dictum odio efficitur eget. Suspendisse ac tincidunt leo. Duis ultricies ipsum sit amet dui tristique imperdiet. Praesent molestie magna id risus interdum convallis. Sed sit amet metus felis.</p>\r\n\r\n<p>Fusce placerat iaculis tortor, vel fringilla nunc tempus et. Nam sed libero maximus, rhoncus ex sit amet, convallis nibh. Duis ipsum elit, aliquet vitae ex ac, pellentesque tempus ex. Mauris dui ligula, interdum maximus vestibulum in, auctor et arcu. Vivamus at augue commodo quam sollicitudin pharetra. Duis at enim ut libero malesuada convallis non ac elit. Nunc a commodo purus, in varius ex. Sed viverra vulputate nisl, ut fermentum nisi euismod non. Curabitur neque sapien, pulvinar maximus tristique eu, ullamcorper ac nibh.</p>\r\n\r\n<p>Fusce eros justo, rhoncus ut varius nec, aliquam eu eros. Sed ut tincidunt turpis. Aenean volutpat est ac mattis vehicula. Maecenas nec lacus feugiat, auctor velit non, lacinia massa. Ut mattis, massa sed pellentesque fermentum, nibh libero ultricies neque, vel convallis quam arcu sed dui. Curabitur rutrum sed ante quis pulvinar. Nunc scelerisque nibh vitae tellus efficitur, eget pharetra elit ullamcorper. Vivamus lacinia iaculis orci et pellentesque. Pellentesque pulvinar pellentesque urna ac interdum. Maecenas mollis lorem mauris. Aenean gravida dolor at rhoncus gravida.</p>\r\n\r\n<p>Mauris venenatis fringilla lacus in cursus. Aenean maximus rutrum mauris, at condimentum massa vehicula vitae. Aliquam ut cursus est. Vivamus mattis eleifend ex eget pharetra. Phasellus sit amet egestas mi. Pellentesque ac venenatis nunc. Integer fermentum, nunc id egestas congue, orci erat tempor nibh, id elementum ex lacus vel erat. Ut ac ipsum ac orci vestibulum ultricies. Integer porta, dui quis egestas gravida, orci eros accumsan diam, in mattis libero enim at dui. Pellentesque malesuada eros vitae justo mattis hendrerit. Maecenas pretium felis mi, at molestie ex hendrerit ut.</p>\r\n\r\n<p>Proin cursus dictum vehicula. Curabitur quis sapien et dui pellentesque lobortis. Morbi rutrum mi ut mi vehicula, at maximus sem dapibus. Donec sit amet est eget ex aliquam bibendum. Aenean rhoncus, magna at tincidunt porta, sapien orci rutrum magna, vel mattis felis erat ac nunc. Nulla augue neque, euismod id dictum non, pulvinar et metus. Vestibulum porttitor massa nisi, vitae consequat velit vehicula nec. Phasellus ac facilisis arcu</p>\r\n', 1, '2016-11-18 13:19:38', 0, 1, '', '', 1);
INSERT INTO `xta_page` VALUES(3, 1, 'Наши партнеры', '<div>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam facilisis vitae dolor sed scelerisque. Proin blandit, dui sed volutpat pretium, justo est eleifend mi, a tincidunt libero lacus ac augue. Proin vitae enim eu ligula lobortis finibus. Fusce eget lacus mi. Donec malesuada porttitor orci at viverra. Nam mollis felis id libero rutrum hendrerit. Vestibulum accumsan porta metus eu hendrerit. Praesent purus elit, scelerisque vitae nunc ut, vestibulum ornare urna. Pellentesque aliquam fringilla purus, id viverra turpis porttitor efficitur. Fusce pharetra purus enim, sit amet ultrices ipsum pulvinar sed. Fusce rhoncus magna in justo ullamcorper ultrices.</p>\r\n\r\n<p>Quisque mattis diam at faucibus tincidunt. Nulla non augue et lectus hendrerit lacinia et eu eros. Nunc dolor sem, vehicula et ante rutrum, luctus semper tellus. Sed vestibulum laoreet efficitur. Mauris semper mi dolor, sit amet imperdiet risus posuere sed. Quisque nec dapibus mi, in gravida augue. Quisque ullamcorper commodo mi, vel faucibus magna tempor eget. Vestibulum iaculis dapibus est. Cras scelerisque quam eget euismod tempor. Mauris pharetra vestibulum vestibulum. Cras convallis massa felis.</p>\r\n\r\n<p>Sed finibus blandit feugiat. Maecenas vel erat blandit, elementum libero non, sodales neque. Suspendisse et mollis neque. Nunc vestibulum condimentum blandit. Nam nec dui sollicitudin, efficitur mauris sed, vulputate ligula. Nullam dapibus, diam egestas dapibus lobortis, lorem justo posuere quam, sit amet vulputate est nulla nec magna. Proin feugiat hendrerit diam et placerat. Nam accumsan hendrerit est in vestibulum. Duis varius placerat purus, eget dictum odio efficitur eget. Suspendisse ac tincidunt leo. Duis ultricies ipsum sit amet dui tristique imperdiet. Praesent molestie magna id risus interdum convallis. Sed sit amet metus felis.</p>\r\n\r\n<p>Fusce placerat iaculis tortor, vel fringilla nunc tempus et. Nam sed libero maximus, rhoncus ex sit amet, convallis nibh. Duis ipsum elit, aliquet vitae ex ac, pellentesque tempus ex. Mauris dui ligula, interdum maximus vestibulum in, auctor et arcu. Vivamus at augue commodo quam sollicitudin pharetra. Duis at enim ut libero malesuada convallis non ac elit. Nunc a commodo purus, in varius ex. Sed viverra vulputate nisl, ut fermentum nisi euismod non. Curabitur neque sapien, pulvinar maximus tristique eu, ullamcorper ac nibh.</p>\r\n\r\n<p>Fusce eros justo, rhoncus ut varius nec, aliquam eu eros. Sed ut tincidunt turpis. Aenean volutpat est ac mattis vehicula. Maecenas nec lacus feugiat, auctor velit non, lacinia massa. Ut mattis, massa sed pellentesque fermentum, nibh libero ultricies neque, vel convallis quam arcu sed dui. Curabitur rutrum sed ante quis pulvinar. Nunc scelerisque nibh vitae tellus efficitur, eget pharetra elit ullamcorper. Vivamus lacinia iaculis orci et pellentesque. Pellentesque pulvinar pellentesque urna ac interdum. Maecenas mollis lorem mauris. Aenean gravida dolor at rhoncus gravida.</p>\r\n\r\n<p>Mauris venenatis fringilla lacus in cursus. Aenean maximus rutrum mauris, at condimentum massa vehicula vitae. Aliquam ut cursus est. Vivamus mattis eleifend ex eget pharetra. Phasellus sit amet egestas mi. Pellentesque ac venenatis nunc. Integer fermentum, nunc id egestas congue, orci erat tempor nibh, id elementum ex lacus vel erat. Ut ac ipsum ac orci vestibulum ultricies. Integer porta, dui quis egestas gravida, orci eros accumsan diam, in mattis libero enim at dui. Pellentesque malesuada eros vitae justo mattis hendrerit. Maecenas pretium felis mi, at molestie ex hendrerit ut.</p>\r\n\r\n<p>Proin cursus dictum vehicula. Curabitur quis sapien et dui pellentesque lobortis. Morbi rutrum mi ut mi vehicula, at maximus sem dapibus. Donec sit amet est eget ex aliquam bibendum. Aenean rhoncus, magna at tincidunt porta, sapien orci rutrum magna, vel mattis felis erat ac nunc. Nulla augue neque, euismod id dictum non, pulvinar et metus. Vestibulum porttitor massa nisi, vitae consequat velit vehicula nec. Phasellus ac facilisis arcu</p>\r\n</div>\r\n', 1, '2016-11-18 13:21:46', 0, 1, '', '', 1);
INSERT INTO `xta_page` VALUES(4, 1, 'Контакты', '<p>.</p>\r\n', 1, '2016-11-18 13:22:33', 0, 1, '', '', 1);
INSERT INTO `xta_page` VALUES(5, 1, 'Вакансии', '<div>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam facilisis vitae dolor sed scelerisque. Proin blandit, dui sed volutpat pretium, justo est eleifend mi, a tincidunt libero lacus ac augue. Proin vitae enim eu ligula lobortis finibus. Fusce eget lacus mi. Donec malesuada porttitor orci at viverra. Nam mollis felis id libero rutrum hendrerit. Vestibulum accumsan porta metus eu hendrerit. Praesent purus elit, scelerisque vitae nunc ut, vestibulum ornare urna. Pellentesque aliquam fringilla purus, id viverra turpis porttitor efficitur. Fusce pharetra purus enim, sit amet ultrices ipsum pulvinar sed. Fusce rhoncus magna in justo ullamcorper ultrices.</p>\r\n\r\n<p>Quisque mattis diam at faucibus tincidunt. Nulla non augue et lectus hendrerit lacinia et eu eros. Nunc dolor sem, vehicula et ante rutrum, luctus semper tellus. Sed vestibulum laoreet efficitur. Mauris semper mi dolor, sit amet imperdiet risus posuere sed. Quisque nec dapibus mi, in gravida augue. Quisque ullamcorper commodo mi, vel faucibus magna tempor eget. Vestibulum iaculis dapibus est. Cras scelerisque quam eget euismod tempor. Mauris pharetra vestibulum vestibulum. Cras convallis massa felis.</p>\r\n\r\n<p>Sed finibus blandit feugiat. Maecenas vel erat blandit, elementum libero non, sodales neque. Suspendisse et mollis neque. Nunc vestibulum condimentum blandit. Nam nec dui sollicitudin, efficitur mauris sed, vulputate ligula. Nullam dapibus, diam egestas dapibus lobortis, lorem justo posuere quam, sit amet vulputate est nulla nec magna. Proin feugiat hendrerit diam et placerat. Nam accumsan hendrerit est in vestibulum. Duis varius placerat purus, eget dictum odio efficitur eget. Suspendisse ac tincidunt leo. Duis ultricies ipsum sit amet dui tristique imperdiet. Praesent molestie magna id risus interdum convallis. Sed sit amet metus felis.</p>\r\n\r\n<p>Fusce placerat iaculis tortor, vel fringilla nunc tempus et. Nam sed libero maximus, rhoncus ex sit amet, convallis nibh. Duis ipsum elit, aliquet vitae ex ac, pellentesque tempus ex. Mauris dui ligula, interdum maximus vestibulum in, auctor et arcu. Vivamus at augue commodo quam sollicitudin pharetra. Duis at enim ut libero malesuada convallis non ac elit. Nunc a commodo purus, in varius ex. Sed viverra vulputate nisl, ut fermentum nisi euismod non. Curabitur neque sapien, pulvinar maximus tristique eu, ullamcorper ac nibh.</p>\r\n\r\n<p>Fusce eros justo, rhoncus ut varius nec, aliquam eu eros. Sed ut tincidunt turpis. Aenean volutpat est ac mattis vehicula. Maecenas nec lacus feugiat, auctor velit non, lacinia massa. Ut mattis, massa sed pellentesque fermentum, nibh libero ultricies neque, vel convallis quam arcu sed dui. Curabitur rutrum sed ante quis pulvinar. Nunc scelerisque nibh vitae tellus efficitur, eget pharetra elit ullamcorper. Vivamus lacinia iaculis orci et pellentesque. Pellentesque pulvinar pellentesque urna ac interdum. Maecenas mollis lorem mauris. Aenean gravida dolor at rhoncus gravida.</p>\r\n\r\n<p>Mauris venenatis fringilla lacus in cursus. Aenean maximus rutrum mauris, at condimentum massa vehicula vitae. Aliquam ut cursus est. Vivamus mattis eleifend ex eget pharetra. Phasellus sit amet egestas mi. Pellentesque ac venenatis nunc. Integer fermentum, nunc id egestas congue, orci erat tempor nibh, id elementum ex lacus vel erat. Ut ac ipsum ac orci vestibulum ultricies. Integer porta, dui quis egestas gravida, orci eros accumsan diam, in mattis libero enim at dui. Pellentesque malesuada eros vitae justo mattis hendrerit. Maecenas pretium felis mi, at molestie ex hendrerit ut.</p>\r\n\r\n<p>Proin cursus dictum vehicula. Curabitur quis sapien et dui pellentesque lobortis. Morbi rutrum mi ut mi vehicula, at maximus sem dapibus. Donec sit amet est eget ex aliquam bibendum. Aenean rhoncus, magna at tincidunt porta, sapien orci rutrum magna, vel mattis felis erat ac nunc. Nulla augue neque, euismod id dictum non, pulvinar et metus. Vestibulum porttitor massa nisi, vitae consequat velit vehicula nec. Phasellus ac facilisis arcu</p>\r\n</div>\r\n', 1, '2016-11-18 13:24:03', 0, 1, '', '', 1);
INSERT INTO `xta_page` VALUES(6, 1, 'Услуги', '<div>\r\n<div>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam facilisis vitae dolor sed scelerisque. Proin blandit, dui sed volutpat pretium, justo est eleifend mi, a tincidunt libero lacus ac augue. Proin vitae enim eu ligula lobortis finibus. Fusce eget lacus mi. Donec malesuada porttitor orci at viverra. Nam mollis felis id libero rutrum hendrerit. Vestibulum accumsan porta metus eu hendrerit. Praesent purus elit, scelerisque vitae nunc ut, vestibulum ornare urna. Pellentesque aliquam fringilla purus, id viverra turpis porttitor efficitur. Fusce pharetra purus enim, sit amet ultrices ipsum pulvinar sed. Fusce rhoncus magna in justo ullamcorper ultrices.</p>\r\n\r\n<p>Quisque mattis diam at faucibus tincidunt. Nulla non augue et lectus hendrerit lacinia et eu eros. Nunc dolor sem, vehicula et ante rutrum, luctus semper tellus. Sed vestibulum laoreet efficitur. Mauris semper mi dolor, sit amet imperdiet risus posuere sed. Quisque nec dapibus mi, in gravida augue. Quisque ullamcorper commodo mi, vel faucibus magna tempor eget. Vestibulum iaculis dapibus est. Cras scelerisque quam eget euismod tempor. Mauris pharetra vestibulum vestibulum. Cras convallis massa felis.</p>\r\n\r\n<p>Sed finibus blandit feugiat. Maecenas vel erat blandit, elementum libero non, sodales neque. Suspendisse et mollis neque. Nunc vestibulum condimentum blandit. Nam nec dui sollicitudin, efficitur mauris sed, vulputate ligula. Nullam dapibus, diam egestas dapibus lobortis, lorem justo posuere quam, sit amet vulputate est nulla nec magna. Proin feugiat hendrerit diam et placerat. Nam accumsan hendrerit est in vestibulum. Duis varius placerat purus, eget dictum odio efficitur eget. Suspendisse ac tincidunt leo. Duis ultricies ipsum sit amet dui tristique imperdiet. Praesent molestie magna id risus interdum convallis. Sed sit amet metus felis.</p>\r\n\r\n<p>Fusce placerat iaculis tortor, vel fringilla nunc tempus et. Nam sed libero maximus, rhoncus ex sit amet, convallis nibh. Duis ipsum elit, aliquet vitae ex ac, pellentesque tempus ex. Mauris dui ligula, interdum maximus vestibulum in, auctor et arcu. Vivamus at augue commodo quam sollicitudin pharetra. Duis at enim ut libero malesuada convallis non ac elit. Nunc a commodo purus, in varius ex. Sed viverra vulputate nisl, ut fermentum nisi euismod non. Curabitur neque sapien, pulvinar maximus tristique eu, ullamcorper ac nibh.</p>\r\n\r\n<p>Fusce eros justo, rhoncus ut varius nec, aliquam eu eros. Sed ut tincidunt turpis. Aenean volutpat est ac mattis vehicula. Maecenas nec lacus feugiat, auctor velit non, lacinia massa. Ut mattis, massa sed pellentesque fermentum, nibh libero ultricies neque, vel convallis quam arcu sed dui. Curabitur rutrum sed ante quis pulvinar. Nunc scelerisque nibh vitae tellus efficitur, eget pharetra elit ullamcorper. Vivamus lacinia iaculis orci et pellentesque. Pellentesque pulvinar pellentesque urna ac interdum. Maecenas mollis lorem mauris. Aenean gravida dolor at rhoncus gravida.</p>\r\n\r\n<p>Mauris venenatis fringilla lacus in cursus. Aenean maximus rutrum mauris, at condimentum massa vehicula vitae. Aliquam ut cursus est. Vivamus mattis eleifend ex eget pharetra. Phasellus sit amet egestas mi. Pellentesque ac venenatis nunc. Integer fermentum, nunc id egestas congue, orci erat tempor nibh, id elementum ex lacus vel erat. Ut ac ipsum ac orci vestibulum ultricies. Integer porta, dui quis egestas gravida, orci eros accumsan diam, in mattis libero enim at dui. Pellentesque malesuada eros vitae justo mattis hendrerit. Maecenas pretium felis mi, at molestie ex hendrerit ut.</p>\r\n\r\n<p>Proin cursus dictum vehicula. Curabitur quis sapien et dui pellentesque lobortis. Morbi rutrum mi ut mi vehicula, at maximus sem dapibus. Donec sit amet est eget ex aliquam bibendum. Aenean rhoncus, magna at tincidunt porta, sapien orci rutrum magna, vel mattis felis erat ac nunc. Nulla augue neque, euismod id dictum non, pulvinar et metus. Vestibulum porttitor massa nisi, vitae consequat velit vehicula nec. Phasellus ac facilisis arcu</p>\r\n</div>\r\n</div>\r\n', 1, '2016-11-18 13:30:27', 0, 1, '', '', 1);
INSERT INTO `xta_page` VALUES(7, 1, 'Гид по аренде', '<div>\r\n<div>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam facilisis vitae dolor sed scelerisque. Proin blandit, dui sed volutpat pretium, justo est eleifend mi, a tincidunt libero lacus ac augue. Proin vitae enim eu ligula lobortis finibus. Fusce eget lacus mi. Donec malesuada porttitor orci at viverra. Nam mollis felis id libero rutrum hendrerit. Vestibulum accumsan porta metus eu hendrerit. Praesent purus elit, scelerisque vitae nunc ut, vestibulum ornare urna. Pellentesque aliquam fringilla purus, id viverra turpis porttitor efficitur. Fusce pharetra purus enim, sit amet ultrices ipsum pulvinar sed. Fusce rhoncus magna in justo ullamcorper ultrices.</p>\r\n\r\n<p>Quisque mattis diam at faucibus tincidunt. Nulla non augue et lectus hendrerit lacinia et eu eros. Nunc dolor sem, vehicula et ante rutrum, luctus semper tellus. Sed vestibulum laoreet efficitur. Mauris semper mi dolor, sit amet imperdiet risus posuere sed. Quisque nec dapibus mi, in gravida augue. Quisque ullamcorper commodo mi, vel faucibus magna tempor eget. Vestibulum iaculis dapibus est. Cras scelerisque quam eget euismod tempor. Mauris pharetra vestibulum vestibulum. Cras convallis massa felis.</p>\r\n\r\n<p>Sed finibus blandit feugiat. Maecenas vel erat blandit, elementum libero non, sodales neque. Suspendisse et mollis neque. Nunc vestibulum condimentum blandit. Nam nec dui sollicitudin, efficitur mauris sed, vulputate ligula. Nullam dapibus, diam egestas dapibus lobortis, lorem justo posuere quam, sit amet vulputate est nulla nec magna. Proin feugiat hendrerit diam et placerat. Nam accumsan hendrerit est in vestibulum. Duis varius placerat purus, eget dictum odio efficitur eget. Suspendisse ac tincidunt leo. Duis ultricies ipsum sit amet dui tristique imperdiet. Praesent molestie magna id risus interdum convallis. Sed sit amet metus felis.</p>\r\n\r\n<p>Fusce placerat iaculis tortor, vel fringilla nunc tempus et. Nam sed libero maximus, rhoncus ex sit amet, convallis nibh. Duis ipsum elit, aliquet vitae ex ac, pellentesque tempus ex. Mauris dui ligula, interdum maximus vestibulum in, auctor et arcu. Vivamus at augue commodo quam sollicitudin pharetra. Duis at enim ut libero malesuada convallis non ac elit. Nunc a commodo purus, in varius ex. Sed viverra vulputate nisl, ut fermentum nisi euismod non. Curabitur neque sapien, pulvinar maximus tristique eu, ullamcorper ac nibh.</p>\r\n\r\n<p>Fusce eros justo, rhoncus ut varius nec, aliquam eu eros. Sed ut tincidunt turpis. Aenean volutpat est ac mattis vehicula. Maecenas nec lacus feugiat, auctor velit non, lacinia massa. Ut mattis, massa sed pellentesque fermentum, nibh libero ultricies neque, vel convallis quam arcu sed dui. Curabitur rutrum sed ante quis pulvinar. Nunc scelerisque nibh vitae tellus efficitur, eget pharetra elit ullamcorper. Vivamus lacinia iaculis orci et pellentesque. Pellentesque pulvinar pellentesque urna ac interdum. Maecenas mollis lorem mauris. Aenean gravida dolor at rhoncus gravida.</p>\r\n\r\n<p>Mauris venenatis fringilla lacus in cursus. Aenean maximus rutrum mauris, at condimentum massa vehicula vitae. Aliquam ut cursus est. Vivamus mattis eleifend ex eget pharetra. Phasellus sit amet egestas mi. Pellentesque ac venenatis nunc. Integer fermentum, nunc id egestas congue, orci erat tempor nibh, id elementum ex lacus vel erat. Ut ac ipsum ac orci vestibulum ultricies. Integer porta, dui quis egestas gravida, orci eros accumsan diam, in mattis libero enim at dui. Pellentesque malesuada eros vitae justo mattis hendrerit. Maecenas pretium felis mi, at molestie ex hendrerit ut.</p>\r\n\r\n<p>Proin cursus dictum vehicula. Curabitur quis sapien et dui pellentesque lobortis. Morbi rutrum mi ut mi vehicula, at maximus sem dapibus. Donec sit amet est eget ex aliquam bibendum. Aenean rhoncus, magna at tincidunt porta, sapien orci rutrum magna, vel mattis felis erat ac nunc. Nulla augue neque, euismod id dictum non, pulvinar et metus. Vestibulum porttitor massa nisi, vitae consequat velit vehicula nec. Phasellus ac facilisis arcu</p>\r\n</div>\r\n</div>\r\n', 1, '2016-11-18 13:35:36', 0, 1, '', '', 1);
INSERT INTO `xta_page` VALUES(8, 1, 'Гид по продаже', '<div>\r\n<div>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam facilisis vitae dolor sed scelerisque. Proin blandit, dui sed volutpat pretium, justo est eleifend mi, a tincidunt libero lacus ac augue. Proin vitae enim eu ligula lobortis finibus. Fusce eget lacus mi. Donec malesuada porttitor orci at viverra. Nam mollis felis id libero rutrum hendrerit. Vestibulum accumsan porta metus eu hendrerit. Praesent purus elit, scelerisque vitae nunc ut, vestibulum ornare urna. Pellentesque aliquam fringilla purus, id viverra turpis porttitor efficitur. Fusce pharetra purus enim, sit amet ultrices ipsum pulvinar sed. Fusce rhoncus magna in justo ullamcorper ultrices.</p>\r\n\r\n<p>Quisque mattis diam at faucibus tincidunt. Nulla non augue et lectus hendrerit lacinia et eu eros. Nunc dolor sem, vehicula et ante rutrum, luctus semper tellus. Sed vestibulum laoreet efficitur. Mauris semper mi dolor, sit amet imperdiet risus posuere sed. Quisque nec dapibus mi, in gravida augue. Quisque ullamcorper commodo mi, vel faucibus magna tempor eget. Vestibulum iaculis dapibus est. Cras scelerisque quam eget euismod tempor. Mauris pharetra vestibulum vestibulum. Cras convallis massa felis.</p>\r\n\r\n<p>Sed finibus blandit feugiat. Maecenas vel erat blandit, elementum libero non, sodales neque. Suspendisse et mollis neque. Nunc vestibulum condimentum blandit. Nam nec dui sollicitudin, efficitur mauris sed, vulputate ligula. Nullam dapibus, diam egestas dapibus lobortis, lorem justo posuere quam, sit amet vulputate est nulla nec magna. Proin feugiat hendrerit diam et placerat. Nam accumsan hendrerit est in vestibulum. Duis varius placerat purus, eget dictum odio efficitur eget. Suspendisse ac tincidunt leo. Duis ultricies ipsum sit amet dui tristique imperdiet. Praesent molestie magna id risus interdum convallis. Sed sit amet metus felis.</p>\r\n\r\n<p>Fusce placerat iaculis tortor, vel fringilla nunc tempus et. Nam sed libero maximus, rhoncus ex sit amet, convallis nibh. Duis ipsum elit, aliquet vitae ex ac, pellentesque tempus ex. Mauris dui ligula, interdum maximus vestibulum in, auctor et arcu. Vivamus at augue commodo quam sollicitudin pharetra. Duis at enim ut libero malesuada convallis non ac elit. Nunc a commodo purus, in varius ex. Sed viverra vulputate nisl, ut fermentum nisi euismod non. Curabitur neque sapien, pulvinar maximus tristique eu, ullamcorper ac nibh.</p>\r\n\r\n<p>Fusce eros justo, rhoncus ut varius nec, aliquam eu eros. Sed ut tincidunt turpis. Aenean volutpat est ac mattis vehicula. Maecenas nec lacus feugiat, auctor velit non, lacinia massa. Ut mattis, massa sed pellentesque fermentum, nibh libero ultricies neque, vel convallis quam arcu sed dui. Curabitur rutrum sed ante quis pulvinar. Nunc scelerisque nibh vitae tellus efficitur, eget pharetra elit ullamcorper. Vivamus lacinia iaculis orci et pellentesque. Pellentesque pulvinar pellentesque urna ac interdum. Maecenas mollis lorem mauris. Aenean gravida dolor at rhoncus gravida.</p>\r\n\r\n<p>Mauris venenatis fringilla lacus in cursus. Aenean maximus rutrum mauris, at condimentum massa vehicula vitae. Aliquam ut cursus est. Vivamus mattis eleifend ex eget pharetra. Phasellus sit amet egestas mi. Pellentesque ac venenatis nunc. Integer fermentum, nunc id egestas congue, orci erat tempor nibh, id elementum ex lacus vel erat. Ut ac ipsum ac orci vestibulum ultricies. Integer porta, dui quis egestas gravida, orci eros accumsan diam, in mattis libero enim at dui. Pellentesque malesuada eros vitae justo mattis hendrerit. Maecenas pretium felis mi, at molestie ex hendrerit ut.</p>\r\n\r\n<p>Proin cursus dictum vehicula. Curabitur quis sapien et dui pellentesque lobortis. Morbi rutrum mi ut mi vehicula, at maximus sem dapibus. Donec sit amet est eget ex aliquam bibendum. Aenean rhoncus, magna at tincidunt porta, sapien orci rutrum magna, vel mattis felis erat ac nunc. Nulla augue neque, euismod id dictum non, pulvinar et metus. Vestibulum porttitor massa nisi, vitae consequat velit vehicula nec. Phasellus ac facilisis arcu</p>\r\n</div>\r\n</div>\r\n', 1, '2016-11-18 13:39:13', 0, 1, '', '', 1);
INSERT INTO `xta_page` VALUES(9, 1, 'Новости', '<div>\r\n<div>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam facilisis vitae dolor sed scelerisque. Proin blandit, dui sed volutpat pretium, justo est eleifend mi, a tincidunt libero lacus ac augue. Proin vitae enim eu ligula lobortis finibus. Fusce eget lacus mi. Donec malesuada porttitor orci at viverra. Nam mollis felis id libero rutrum hendrerit. Vestibulum accumsan porta metus eu hendrerit. Praesent purus elit, scelerisque vitae nunc ut, vestibulum ornare urna. Pellentesque aliquam fringilla purus, id viverra turpis porttitor efficitur. Fusce pharetra purus enim, sit amet ultrices ipsum pulvinar sed. Fusce rhoncus magna in justo ullamcorper ultrices.</p>\r\n\r\n<p>Quisque mattis diam at faucibus tincidunt. Nulla non augue et lectus hendrerit lacinia et eu eros. Nunc dolor sem, vehicula et ante rutrum, luctus semper tellus. Sed vestibulum laoreet efficitur. Mauris semper mi dolor, sit amet imperdiet risus posuere sed. Quisque nec dapibus mi, in gravida augue. Quisque ullamcorper commodo mi, vel faucibus magna tempor eget. Vestibulum iaculis dapibus est. Cras scelerisque quam eget euismod tempor. Mauris pharetra vestibulum vestibulum. Cras convallis massa felis.</p>\r\n\r\n<p>Sed finibus blandit feugiat. Maecenas vel erat blandit, elementum libero non, sodales neque. Suspendisse et mollis neque. Nunc vestibulum condimentum blandit. Nam nec dui sollicitudin, efficitur mauris sed, vulputate ligula. Nullam dapibus, diam egestas dapibus lobortis, lorem justo posuere quam, sit amet vulputate est nulla nec magna. Proin feugiat hendrerit diam et placerat. Nam accumsan hendrerit est in vestibulum. Duis varius placerat purus, eget dictum odio efficitur eget. Suspendisse ac tincidunt leo. Duis ultricies ipsum sit amet dui tristique imperdiet. Praesent molestie magna id risus interdum convallis. Sed sit amet metus felis.</p>\r\n\r\n<p>Fusce placerat iaculis tortor, vel fringilla nunc tempus et. Nam sed libero maximus, rhoncus ex sit amet, convallis nibh. Duis ipsum elit, aliquet vitae ex ac, pellentesque tempus ex. Mauris dui ligula, interdum maximus vestibulum in, auctor et arcu. Vivamus at augue commodo quam sollicitudin pharetra. Duis at enim ut libero malesuada convallis non ac elit. Nunc a commodo purus, in varius ex. Sed viverra vulputate nisl, ut fermentum nisi euismod non. Curabitur neque sapien, pulvinar maximus tristique eu, ullamcorper ac nibh.</p>\r\n\r\n<p>Fusce eros justo, rhoncus ut varius nec, aliquam eu eros. Sed ut tincidunt turpis. Aenean volutpat est ac mattis vehicula. Maecenas nec lacus feugiat, auctor velit non, lacinia massa. Ut mattis, massa sed pellentesque fermentum, nibh libero ultricies neque, vel convallis quam arcu sed dui. Curabitur rutrum sed ante quis pulvinar. Nunc scelerisque nibh vitae tellus efficitur, eget pharetra elit ullamcorper. Vivamus lacinia iaculis orci et pellentesque. Pellentesque pulvinar pellentesque urna ac interdum. Maecenas mollis lorem mauris. Aenean gravida dolor at rhoncus gravida.</p>\r\n\r\n<p>Mauris venenatis fringilla lacus in cursus. Aenean maximus rutrum mauris, at condimentum massa vehicula vitae. Aliquam ut cursus est. Vivamus mattis eleifend ex eget pharetra. Phasellus sit amet egestas mi. Pellentesque ac venenatis nunc. Integer fermentum, nunc id egestas congue, orci erat tempor nibh, id elementum ex lacus vel erat. Ut ac ipsum ac orci vestibulum ultricies. Integer porta, dui quis egestas gravida, orci eros accumsan diam, in mattis libero enim at dui. Pellentesque malesuada eros vitae justo mattis hendrerit. Maecenas pretium felis mi, at molestie ex hendrerit ut.</p>\r\n\r\n<p>Proin cursus dictum vehicula. Curabitur quis sapien et dui pellentesque lobortis. Morbi rutrum mi ut mi vehicula, at maximus sem dapibus. Donec sit amet est eget ex aliquam bibendum. Aenean rhoncus, magna at tincidunt porta, sapien orci rutrum magna, vel mattis felis erat ac nunc. Nulla augue neque, euismod id dictum non, pulvinar et metus. Vestibulum porttitor massa nisi, vitae consequat velit vehicula nec. Phasellus ac facilisis arcu</p>\r\n</div>\r\n</div>\r\n', 1, '2016-11-18 13:42:27', 0, 1, '', '', 1);
INSERT INTO `xta_page` VALUES(10, 1, 'Стоимость услуг', '<div>\r\n<div>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam facilisis vitae dolor sed scelerisque. Proin blandit, dui sed volutpat pretium, justo est eleifend mi, a tincidunt libero lacus ac augue. Proin vitae enim eu ligula lobortis finibus. Fusce eget lacus mi. Donec malesuada porttitor orci at viverra. Nam mollis felis id libero rutrum hendrerit. Vestibulum accumsan porta metus eu hendrerit. Praesent purus elit, scelerisque vitae nunc ut, vestibulum ornare urna. Pellentesque aliquam fringilla purus, id viverra turpis porttitor efficitur. Fusce pharetra purus enim, sit amet ultrices ipsum pulvinar sed. Fusce rhoncus magna in justo ullamcorper ultrices.</p>\r\n\r\n<p>Quisque mattis diam at faucibus tincidunt. Nulla non augue et lectus hendrerit lacinia et eu eros. Nunc dolor sem, vehicula et ante rutrum, luctus semper tellus. Sed vestibulum laoreet efficitur. Mauris semper mi dolor, sit amet imperdiet risus posuere sed. Quisque nec dapibus mi, in gravida augue. Quisque ullamcorper commodo mi, vel faucibus magna tempor eget. Vestibulum iaculis dapibus est. Cras scelerisque quam eget euismod tempor. Mauris pharetra vestibulum vestibulum. Cras convallis massa felis.</p>\r\n\r\n<p>Sed finibus blandit feugiat. Maecenas vel erat blandit, elementum libero non, sodales neque. Suspendisse et mollis neque. Nunc vestibulum condimentum blandit. Nam nec dui sollicitudin, efficitur mauris sed, vulputate ligula. Nullam dapibus, diam egestas dapibus lobortis, lorem justo posuere quam, sit amet vulputate est nulla nec magna. Proin feugiat hendrerit diam et placerat. Nam accumsan hendrerit est in vestibulum. Duis varius placerat purus, eget dictum odio efficitur eget. Suspendisse ac tincidunt leo. Duis ultricies ipsum sit amet dui tristique imperdiet. Praesent molestie magna id risus interdum convallis. Sed sit amet metus felis.</p>\r\n\r\n<p>Fusce placerat iaculis tortor, vel fringilla nunc tempus et. Nam sed libero maximus, rhoncus ex sit amet, convallis nibh. Duis ipsum elit, aliquet vitae ex ac, pellentesque tempus ex. Mauris dui ligula, interdum maximus vestibulum in, auctor et arcu. Vivamus at augue commodo quam sollicitudin pharetra. Duis at enim ut libero malesuada convallis non ac elit. Nunc a commodo purus, in varius ex. Sed viverra vulputate nisl, ut fermentum nisi euismod non. Curabitur neque sapien, pulvinar maximus tristique eu, ullamcorper ac nibh.</p>\r\n\r\n<p>Fusce eros justo, rhoncus ut varius nec, aliquam eu eros. Sed ut tincidunt turpis. Aenean volutpat est ac mattis vehicula. Maecenas nec lacus feugiat, auctor velit non, lacinia massa. Ut mattis, massa sed pellentesque fermentum, nibh libero ultricies neque, vel convallis quam arcu sed dui. Curabitur rutrum sed ante quis pulvinar. Nunc scelerisque nibh vitae tellus efficitur, eget pharetra elit ullamcorper. Vivamus lacinia iaculis orci et pellentesque. Pellentesque pulvinar pellentesque urna ac interdum. Maecenas mollis lorem mauris. Aenean gravida dolor at rhoncus gravida.</p>\r\n\r\n<p>Mauris venenatis fringilla lacus in cursus. Aenean maximus rutrum mauris, at condimentum massa vehicula vitae. Aliquam ut cursus est. Vivamus mattis eleifend ex eget pharetra. Phasellus sit amet egestas mi. Pellentesque ac venenatis nunc. Integer fermentum, nunc id egestas congue, orci erat tempor nibh, id elementum ex lacus vel erat. Ut ac ipsum ac orci vestibulum ultricies. Integer porta, dui quis egestas gravida, orci eros accumsan diam, in mattis libero enim at dui. Pellentesque malesuada eros vitae justo mattis hendrerit. Maecenas pretium felis mi, at molestie ex hendrerit ut.</p>\r\n\r\n<p>Proin cursus dictum vehicula. Curabitur quis sapien et dui pellentesque lobortis. Morbi rutrum mi ut mi vehicula, at maximus sem dapibus. Donec sit amet est eget ex aliquam bibendum. Aenean rhoncus, magna at tincidunt porta, sapien orci rutrum magna, vel mattis felis erat ac nunc. Nulla augue neque, euismod id dictum non, pulvinar et metus. Vestibulum porttitor massa nisi, vitae consequat velit vehicula nec. Phasellus ac facilisis arcu</p>\r\n</div>\r\n</div>\r\n', 1, '2016-11-18 13:44:28', 0, 1, '', '', 1);
INSERT INTO `xta_page` VALUES(11, 1, 'Юридические услуги', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam facilisis vitae dolor sed scelerisque. Proin blandit, dui sed volutpat pretium, justo est eleifend mi, a tincidunt libero lacus ac augue.</p>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam facilisis vitae dolor sed scelerisque. Proin blandit, dui sed volutpat pretium, justo est eleifend mi, a tincidunt libero lacus ac augue. Proin vitae enim eu ligula lobortis finibus. Fusce eget lacus mi. Donec malesuada porttitor orci at viverra. Nam mollis felis id libero rutrum hendrerit. Vestibulum accumsan porta metus eu hendrerit. Praesent purus elit, scelerisque vitae nunc ut, vestibulum ornare urna. Pellentesque aliquam fringilla purus, id viverra turpis porttitor efficitur. Fusce pharetra purus enim, sit amet ultrices ipsum pulvinar sed. Fusce rhoncus magna in justo ullamcorper ultrices.</p>\r\n\r\n<p>Quisque mattis diam at faucibus tincidunt. Nulla non augue et lectus hendrerit lacinia et eu eros. Nunc dolor sem, vehicula et ante rutrum, luctus semper tellus. Sed vestibulum laoreet efficitur. Mauris semper mi dolor, sit amet imperdiet risus posuere sed. Quisque nec dapibus mi, in gravida augue. Quisque ullamcorper commodo mi, vel faucibus magna tempor eget. Vestibulum iaculis dapibus est. Cras scelerisque quam eget euismod tempor. Mauris pharetra vestibulum vestibulum. Cras convallis massa felis.</p>\r\n\r\n<p>Sed finibus blandit feugiat. Maecenas vel erat blandit, elementum libero non, sodales neque. Suspendisse et mollis neque. Nunc vestibulum condimentum blandit. Nam nec dui sollicitudin, efficitur mauris sed, vulputate ligula. Nullam dapibus, diam egestas dapibus lobortis, lorem justo posuere quam, sit amet vulputate est nulla nec magna. Proin feugiat hendrerit diam et placerat. Nam accumsan hendrerit est in vestibulum. Duis varius placerat purus, eget dictum odio efficitur eget. Suspendisse ac tincidunt leo. Duis ultricies ipsum sit amet dui tristique imperdiet. Praesent molestie magna id risus interdum convallis. Sed sit amet metus felis.</p>\r\n\r\n<p>Fusce placerat iaculis tortor, vel fringilla nunc tempus et. Nam sed libero maximus, rhoncus ex sit amet, convallis nibh. Duis ipsum elit, aliquet vitae ex ac, pellentesque tempus ex. Mauris dui ligula, interdum maximus vestibulum in, auctor et arcu. Vivamus at augue commodo quam sollicitudin pharetra. Duis at enim ut libero malesuada convallis non ac elit. Nunc a commodo purus, in varius ex. Sed viverra vulputate nisl, ut fermentum nisi euismod non. Curabitur neque sapien, pulvinar maximus tristique eu, ullamcorper ac nibh.</p>\r\n\r\n<p>Fusce eros justo, rhoncus ut varius nec, aliquam eu eros. Sed ut tincidunt turpis. Aenean volutpat est ac mattis vehicula. Maecenas nec lacus feugiat, auctor velit non, lacinia massa. Ut mattis, massa sed pellentesque fermentum, nibh libero ultricies neque, vel convallis quam arcu sed dui. Curabitur rutrum sed ante quis pulvinar. Nunc scelerisque nibh vitae tellus efficitur, eget pharetra elit ullamcorper. Vivamus lacinia iaculis orci et pellentesque. Pellentesque pulvinar pellentesque urna ac interdum. Maecenas mollis lorem mauris. Aenean gravida dolor at rhoncus gravida.</p>\r\n\r\n<p>Mauris venenatis fringilla lacus in cursus. Aenean maximus rutrum mauris, at condimentum massa vehicula vitae. Aliquam ut cursus est. Vivamus mattis eleifend ex eget pharetra. Phasellus sit amet egestas mi. Pellentesque ac venenatis nunc. Integer fermentum, nunc id egestas congue, orci erat tempor nibh, id elementum ex lacus vel erat. Ut ac ipsum ac orci vestibulum ultricies. Integer porta, dui quis egestas gravida, orci eros accumsan diam, in mattis libero enim at dui. Pellentesque malesuada eros vitae justo mattis hendrerit. Maecenas pretium felis mi, at molestie ex hendrerit ut.</p>\r\n\r\n<p>Proin cursus dictum vehicula. Curabitur quis sapien et dui pellentesque lobortis. Morbi rutrum mi ut mi vehicula, at maximus sem dapibus. Donec sit amet est eget ex aliquam bibendum. Aenean rhoncus, magna at tincidunt porta, sapien orci rutrum magna, vel mattis felis erat ac nunc. Nulla augue neque, euismod id dictum non, pulvinar et metus. Vestibulum porttitor massa nisi, vitae consequat velit vehicula nec. Phasellus ac facilisis arcu</p>\r\n\r\n<p>Proin vitae enim eu ligula lobortis finibus. Fusce eget lacus mi. Donec malesuada porttitor orci at viverra. Nam mollis felis id libero rutrum hendrerit. Vestibulum accumsan porta metus eu hendrerit. Praesent purus elit, scelerisque vitae nunc ut, vestibulum ornare urna. Pellentesque aliquam fringilla purus, id viverra turpis porttitor efficitur. Fusce pharetra purus enim, sit amet ultrices ipsum pulvinar sed. Fusce rhoncus magna in justo ullamcorper ultrices. Quisque mattis diam at faucibus tincidunt. Nulla non augue et lectus hendrerit lacinia et eu eros. Nunc dolor sem, vehicula et ante rutrum, luctus semper tellus. Sed vestibulum laoreet efficitur. Mauris semper mi dolor, sit amet imperdiet risus posuere sed. Quisque nec dapibus mi, in gravida augue. Quisque ullamcorper commodo mi, vel faucibus magna tempor eget. Vestibulum iaculis dapibus est. Cras scelerisque quam eget euismod tempor. Mauris pharetra vestibulum vestibulum. Cras convallis massa felis. Sed finibus blandit feugiat. Maecenas vel erat blandit, elementum libero non, sodales neque. Suspendisse et mollis neque. Nunc vestibulum condimentum blandit. Nam nec dui sollicitudin, efficitur mauris sed, vulputate ligula. Nullam dapibus, diam egestas dapibus lobortis, lorem justo posuere quam, sit amet vulputate est nulla nec magna. Proin feugiat hendrerit diam et placerat. Nam accumsan hendrerit est in vestibulum. Duis varius placerat purus, eget dictum odio efficitur eget. Suspendisse ac tincidunt leo. Duis ultricies ipsum sit amet dui tristique imperdiet. Praesent molestie magna id risus interdum convallis. Sed sit amet metus felis. Fusce placerat iaculis tortor, vel fringilla nunc tempus et. Nam sed libero maximus, rhoncus ex sit amet, convallis nibh. Duis ipsum elit, aliquet vitae ex ac, pellentesque tempus ex. Mauris dui ligula, interdum maximus vestibulum in, auctor et arcu. Vivamus at augue commodo quam sollicitudin pharetra. Duis at enim ut libero malesuada convallis non ac elit. Nunc a commodo purus, in varius ex. Sed viverra vulputate nisl, ut fermentum nisi euismod non. Curabitur neque sapien, pulvinar maximus tristique eu, ullamcorper ac nibh. Fusce eros justo, rhoncus ut varius nec, aliquam eu eros. Sed ut tincidunt turpis. Aenean volutpat est ac mattis vehicula. Maecenas nec lacus feugiat, auctor velit non, lacinia massa. Ut mattis, massa sed pellentesque fermentum, nibh libero ultricies neque, vel convallis quam arcu sed dui. Curabitur rutrum sed ante quis pulvinar. Nunc scelerisque nibh vitae tellus efficitur, eget pharetra elit ullamcorper. Vivamus lacinia iaculis orci et pellentesque. Pellentesque pulvinar pellentesque urna ac interdum. Maecenas mollis lorem mauris. Aenean gravida dolor at rhoncus gravida. Mauris venenatis fringilla lacus in cursus. Aenean maximus rutrum mauris, at condimentum massa vehicula vitae. Aliquam ut cursus est. Vivamus mattis eleifend ex eget pharetra. Phasellus sit amet egestas mi. Pellentesque ac venenatis nunc. Integer fermentum, nunc id egestas congue, orci erat tempor nibh, id elementum ex lacus vel erat. Ut ac ipsum ac orci vestibulum ultricies. Integer porta, dui quis egestas gravida, orci eros accumsan diam, in mattis libero enim at dui. Pellentesque malesuada eros vitae justo mattis hendrerit. Maecenas pretium felis mi, at molestie ex hendrerit ut. Proin cursus dictum vehicula. Curabitur quis sapien et dui pellentesque lobortis. Morbi rutrum mi ut mi vehicula, at maximus sem dapibus. Donec sit amet est eget ex aliquam bibendum. Aenean rhoncus, magna at tincidunt porta, sapien orci rutrum magna, vel mattis felis erat ac nunc. Nulla augue neque, euismod id dictum non, pulvinar et metus. Vestibulum porttitor massa nisi, vitae consequat velit vehicula nec. Phasellus ac facilisis arcu</p>\r\n', 1, '2017-01-19 13:39:23', 0, 1, '', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `xta_region`
--

DROP TABLE IF EXISTS `xta_region`;
CREATE TABLE `xta_region` (
  `id` int(11) NOT NULL,
  `id_country` int(11) NOT NULL,
  `name` varchar(200) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Список областей';

--
-- Dumping data for table `xta_region`
--

INSERT INTO `xta_region` VALUES(1, 1, 'Днепропетровская');
INSERT INTO `xta_region` VALUES(2, 1, 'Одесская');
INSERT INTO `xta_region` VALUES(4, 2, 'Ростовская');

-- --------------------------------------------------------

--
-- Table structure for table `xta_seo_tdz`
--

DROP TABLE IF EXISTS `xta_seo_tdz`;
CREATE TABLE `xta_seo_tdz` (
  `id` int(11) NOT NULL,
  `id_site` int(11) NOT NULL COMMENT 'Ссылка на сайт',
  `rurl` varchar(200) NOT NULL DEFAULT '' COMMENT 'Страница',
  `title` varchar(200) NOT NULL DEFAULT '',
  `description` varchar(200) NOT NULL DEFAULT '',
  `hone` varchar(200) NOT NULL DEFAULT '' COMMENT 'H1',
  `index` int(11) NOT NULL DEFAULT '1' COMMENT 'Индексировать страницу',
  `canonical` varchar(200) NOT NULL DEFAULT '' COMMENT 'Каноническая ссылка на страницу',
  `seotext` text NOT NULL COMMENT 'Сеотекст'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='ТДЗ для любых страниц';

--
-- Dumping data for table `xta_seo_tdz`
--

INSERT INTO `xta_seo_tdz` VALUES(1, 1, '/board', '', '', '', 1, '', 'rrrrrrrrrrrrrrrr');

-- --------------------------------------------------------

--
-- Table structure for table `xta_setting`
--

DROP TABLE IF EXISTS `xta_setting`;
CREATE TABLE `xta_setting` (
  `id` int(11) NOT NULL,
  `id_setting_category` int(11) NOT NULL COMMENT 'Ссылка на категорию настроек',
  `name` varchar(100) NOT NULL DEFAULT '',
  `title` varchar(250) NOT NULL DEFAULT '',
  `value` text NOT NULL,
  `default` text NOT NULL COMMENT 'Значение по-умолчанию'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `xta_setting`
--

INSERT INTO `xta_setting` VALUES(28, 9, 'cache_live_sec', 'Время жизни кеша в секундах', 'a:1:{i:0;s:7:"9999999";}', '');
INSERT INTO `xta_setting` VALUES(29, 8, 'tpl__email_user_create_to_user', 'Шаблон письма, отправляемое пользоватею о регистрации', 'a:1:{i:0;s:287:"Здравствуйте +username+.<br>\r\nВы успешно зарегистрированы на сайте http://+http_host+/<br><br>\r\nДля авторизации на сайте используйте следующие данные:<br>\r\nEmail: +email+<br>\r\nПароль: +pass+";}', '');
INSERT INTO `xta_setting` VALUES(30, 8, 'admin_email', 'Email администратора', 'a:1:{i:0;s:26:"tihonenkovaleriy@gmail.com";}', '');
INSERT INTO `xta_setting` VALUES(31, 8, 'tpl__email_user_create_to_admin', 'Шаблон письма, отправляемое админу о создании профиля', 'a:1:{i:0;s:202:"<div>На сайте +http_host+ создан пользователь.</div> \r\n<br><br>\r\n<div>Пользователь: +username+,</div> \r\n<div>Пароль:  +pass+,</div> \r\n<div>email: +email+</div>";}', '');
INSERT INTO `xta_setting` VALUES(32, 4, 'obj_stop_words', 'Стоп слова для объявления, комментариев (при их наличии в тексте, сообщение добавляться не будет). Вводить через запятую', 'a:1:{i:0;s:247:"viagra, cialis, levitra, buy, http://, generic, online, without, sex, порно, скачать, лолиты, трах, ютуб, Азарт, Онлайн, онлайн, online, покер, подпольные, Kлиeнтckиe, 791З79Зб4ЗЗ";}', '');
INSERT INTO `xta_setting` VALUES(33, 9, 'watermark_img_path', 'Относительный путь к картинке водяного знака (начинается с /themes/board...)', 'a:1:{i:1;s:40:"/themes/board/img/watermark/krgazeta.png";}', '');
INSERT INTO `xta_setting` VALUES(34, 2, 'yandex_verification_meta', 'Метатег для верификации сайта в Яндекс вебмастере', 'a:1:{i:1;s:62:"<meta name=\'yandex-verification\' content=\'7ed1d9adeec002b8\' />";}', '');
INSERT INTO `xta_setting` VALUES(35, 2, 'yandex_metrica_counter', 'Код яндекс метрики', 'a:1:{i:1;s:1058:"<!-- Yandex.Metrika counter -->\r\n<script type="text/javascript">\r\n(function (d, w, c) {\r\n    (w[c] = w[c] || []).push(function() {\r\n        try {\r\n            w.yaCounter24704774 = new Ya.Metrika({id:24704774,\r\n                    webvisor:true,\r\n                    clickmap:true,\r\n                    trackLinks:true,\r\n                    accurateTrackBounce:true});\r\n        } catch(e) { }\r\n    });\r\n\r\n    var n = d.getElementsByTagName("script")[0],\r\n        s = d.createElement("script"),\r\n        f = function () { n.parentNode.insertBefore(s, n); };\r\n    s.type = "text/javascript";\r\n    s.async = true;\r\n    s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";\r\n\r\n    if (w.opera == "[object Opera]") {\r\n        d.addEventListener("DOMContentLoaded", f, false);\r\n    } else { f(); }\r\n})(document, window, "yandex_metrika_callbacks");\r\n</script>\r\n<noscript><div><img src="//mc.yandex.ru/watch/24704774" style="position:absolute; left:-9999px;" alt="" /></div></noscript>\r\n<!-- /Yandex.Metrika counter -->";}', '');
INSERT INTO `xta_setting` VALUES(36, 8, 'tpl__email_user_send_message', 'Шаблон письма юзеру о том, что ему прислали сообщение с сайта', 'a:1:{i:0;s:1339:"<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">\r\n<html xmlns="http://www.w3.org/1999/xhtml">\r\n<head>\r\n<meta name="viewport" content="width=device-width" />\r\n\r\n<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />\r\n<title>Вам на сайте +http_host+ отправлено личное сообщение</title>\r\n</head>\r\n<body>\r\n<h3>Здравствуйте, +username+</h3>\r\n<p class="lead">На сайте +http_host+ пользователь +username_from+ отправил Вам личное сообщение:</p>\r\n<table style="width:100%;">\r\n  <tr>\r\n    <td>Сообщение:</td>\r\n    <td>+message+</td>\r\n  </tr>\r\n  <tr>\r\n    <td>Время отправления:</td>\r\n    <td>+date_now+</td>\r\n  </tr>\r\n  <tr>\r\n    <td>Для просмотра сообщения или ответа воспользуйтесь ccылкой:</td>\r\n    <td><a href="+link_mymess_autologin+">Ответить</a></td>\r\n  </tr>\r\n</table>\r\n<br>\r\n<br>\r\n<table style="width:100%;">\r\n  <tr>\r\n    <td><a href="+link_main_autologin+">+http_host+</a></td>\r\n    <td><a href="+link_myobj_autologin+">Мои объявления</a></td>\r\n    <td><a href="+link_mymess_autologin+">Мои сообщения</a></td>\r\n  </tr>\r\n</table>\r\n\r\n\r\n</body>\r\n</html>";}', '');
INSERT INTO `xta_setting` VALUES(37, 8, 'tpl__email_user_forgotpassword', 'Шаблон письма, отправляемое пользоватею о восстановлении пароля', 'a:1:{i:0;s:1225:"<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">\r\n<html xmlns="http://www.w3.org/1999/xhtml">\r\n<head>\r\n<meta name="viewport" content="width=device-width" />\r\n\r\n<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />\r\n<title>Восстановление пароля на сайте +http_host+</title>\r\n</head>\r\n<body>\r\n<h3>Здравствуйте, +username+</h3>\r\n<p class="lead">На сайте +http_host+ Вы воспользовались формой восстановления пароля:</p>\r\n<table style="width:100%;">\r\n  <tr>\r\n    <td>Время отправления:</td>\r\n    <td>+date_now+</td>\r\n  </tr>\r\n  <tr>\r\n    <td>Для изменения пароля воспользуйтесь ccылкой:</td>\r\n    <td><a href="+link_settings_autologin+">Изменить пароль</a></td>\r\n  </tr>\r\n</table>\r\n<br>\r\n<br>\r\n<table style="width:100%;">\r\n  <tr>\r\n    <td><a href="+link_main_autologin+">+http_host+</a></td>\r\n    <td><a href="+link_myobj_autologin+">Мои объявления</a></td>\r\n    <td><a href="+link_mymess_autologin+">Мои сообщения</a></td>\r\n  </tr>\r\n</table>\r\n</body>\r\n</html>";}', '');
INSERT INTO `xta_setting` VALUES(38, 8, 'tpl__email_obj_create_to_admin', 'Шаблон письма, отправляемое админу о создании объявления', 'a:1:{i:0;s:262:"<div>На сайте +http_host+ создано объявление.</div> \r\n<br><br>\r\n<div>Пользователь: +username+,</div> \r\n<div>Заголовок:  +name+,</div> \r\n<div>Ссылка на объявление: http://krgazeta.com/board/+id+</div>";}', '');
INSERT INTO `xta_setting` VALUES(41, 8, 'tpl__email_obj_abuse_to_admin', 'Шаблон письма, отправляемое админу о жалобе на объявление', 'a:1:{i:0;s:332:"<div>На сайте +http_host+ отправлена жалоба на объявление.</div> \r\n<br><br>\r\n<div>Пользователь, который отправил жалобу: +username+,</div> \r\n<div>Объявление:  +id+,</div> \r\n<div>Ссылка на объявление: http://krgazeta.com/board/+id+</div>";}', '');
INSERT INTO `xta_setting` VALUES(42, 4, 'hidden_words', 'Список слов, при наличии которых объявление не будет показываться в списке общих объявлений, а лишь в своей категории (в основном для знакомств)', 'a:1:{i:0;s:10:"минет";}', '');

-- --------------------------------------------------------

--
-- Table structure for table `xta_setting_category`
--

DROP TABLE IF EXISTS `xta_setting_category`;
CREATE TABLE `xta_setting_category` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL DEFAULT '' COMMENT 'Имя категории'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Список категорий настроек';

--
-- Dumping data for table `xta_setting_category`
--

INSERT INTO `xta_setting_category` VALUES(1, 'Шаблон');
INSERT INTO `xta_setting_category` VALUES(2, 'SEO');
INSERT INTO `xta_setting_category` VALUES(3, 'Реклама');
INSERT INTO `xta_setting_category` VALUES(4, 'Разное');
INSERT INTO `xta_setting_category` VALUES(5, 'Соцсети');
INSERT INTO `xta_setting_category` VALUES(8, 'Email');
INSERT INTO `xta_setting_category` VALUES(9, 'Система');

-- --------------------------------------------------------

--
-- Table structure for table `xta_site`
--

DROP TABLE IF EXISTS `xta_site`;
CREATE TABLE `xta_site` (
  `id` int(11) NOT NULL,
  `id_city` int(11) NOT NULL COMMENT 'Ссылка на город',
  `name` varchar(200) NOT NULL DEFAULT '' COMMENT 'Хост '
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Список сайтов';

--
-- Dumping data for table `xta_site`
--

INSERT INTO `xta_site` VALUES(1, 6, 'localhost');

-- --------------------------------------------------------

--
-- Table structure for table `xta_user`
--

DROP TABLE IF EXISTS `xta_user`;
CREATE TABLE `xta_user` (
  `id` int(11) NOT NULL,
  `id_user_role` int(11) NOT NULL DEFAULT '2' COMMENT 'Ссылка на роль',
  `id_district` int(11) NOT NULL DEFAULT '0' COMMENT 'Ссылка на район проживания',
  `email` varchar(200) NOT NULL DEFAULT '' COMMENT 'Email',
  `pass` varchar(200) NOT NULL DEFAULT '' COMMENT 'Пароль',
  `username` varchar(200) NOT NULL DEFAULT '' COMMENT ' Контактное лицо ',
  `phone` varchar(200) NOT NULL DEFAULT '' COMMENT ' Номер телефона ',
  `skype` varchar(200) NOT NULL DEFAULT '' COMMENT 'Skype',
  `authkey` varchar(200) NOT NULL COMMENT 'Хеш, который устанавлявается при авторизации',
  `accesstoken` varchar(200) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Список пользователей';

--
-- Dumping data for table `xta_user`
--

INSERT INTO `xta_user` VALUES(1, 1, 1, 'tihonenkovaleriy@gmail.com', 'd4073fcc3f9212b43970b2cfbeeff39f', 'Валерий', '0977552001', 'xtetis', '', '6197cef9980ed9f3655e21baf805d396');
INSERT INTO `xta_user` VALUES(2630, 2, 13, 'test@test.ru', 'cce7a605289166d3a5e2a34dadb320db', 'test', '1234567890', 'test', '', '0d48cc6610d9a5d3d1175ac9be1509e2');
INSERT INTO `xta_user` VALUES(2631, 2, 0, 'afonyabel.goro.d@gmail.com', 'd6b04f339a7adeb5e963900196f01ab7', 'lirboge', '123456', 'lirbogeNM', '', '73804214598e45d2a439de7906d16593');
INSERT INTO `xta_user` VALUES(2632, 2, 0, 'af.onyabel.g.oro.d@gmail.com', 'c7a20d6373c32d534e98d2fe6ae7a875', 'eirboge', '123456', 'eirbogeQW', '', '70b728053fe5c394cfbe74ddd6258b9f');

-- --------------------------------------------------------

--
-- Table structure for table `xta_user_message`
--

DROP TABLE IF EXISTS `xta_user_message`;
CREATE TABLE `xta_user_message` (
  `id` int(11) NOT NULL,
  `id_user_from` int(11) NOT NULL DEFAULT '0' COMMENT 'Пользователь отправитель',
  `id_user_to` int(11) NOT NULL DEFAULT '0' COMMENT 'Пользователь получатель',
  `message` text NOT NULL,
  `createdon` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `readed` int(11) NOT NULL DEFAULT '0',
  `options` text NOT NULL COMMENT 'Опции сообщения'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `xta_user_role`
--

DROP TABLE IF EXISTS `xta_user_role`;
CREATE TABLE `xta_user_role` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL DEFAULT '' COMMENT 'Название роли'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Список ролей пользователей';

--
-- Dumping data for table `xta_user_role`
--

INSERT INTO `xta_user_role` VALUES(1, 'admin');
INSERT INTO `xta_user_role` VALUES(2, 'user');
INSERT INTO `xta_user_role` VALUES(3, 'moderator');

-- --------------------------------------------------------

--
-- Table structure for table `xta_valuta`
--

DROP TABLE IF EXISTS `xta_valuta`;
CREATE TABLE `xta_valuta` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL DEFAULT '' COMMENT 'Название валюты',
  `code` varchar(10) NOT NULL DEFAULT '' COMMENT 'Код Должна соответствовать стандарту ISO code',
  `shortname` varchar(10) NOT NULL DEFAULT '' COMMENT 'Символ слева',
  `value` float NOT NULL DEFAULT '1' COMMENT 'Значение: Установите на 1.00000 если это валюта по умолчанию.'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Список валют';

--
-- Dumping data for table `xta_valuta`
--

INSERT INTO `xta_valuta` VALUES(1, 'Гривны', 'UAH', 'грн.', 1);
INSERT INTO `xta_valuta` VALUES(2, 'Доллары', 'USD', '$', 0.086252);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `xta_album`
--
ALTER TABLE `xta_album`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_image` (`id_image`);

--
-- Indexes for table `xta_cache`
--
ALTER TABLE `xta_cache`
  ADD PRIMARY KEY (`id`),
  ADD KEY `md5` (`md5`);

--
-- Indexes for table `xta_cache_in_tag`
--
ALTER TABLE `xta_cache_in_tag`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `xta_cache_tag`
--
ALTER TABLE `xta_cache_tag`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `xta_city`
--
ALTER TABLE `xta_city`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_region` (`id_region`);

--
-- Indexes for table `xta_country`
--
ALTER TABLE `xta_country`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `xta_district`
--
ALTER TABLE `xta_district`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_city` (`id_city`);

--
-- Indexes for table `xta_image`
--
ALTER TABLE `xta_image`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_album` (`id_album`);

--
-- Indexes for table `xta_obj`
--
ALTER TABLE `xta_obj`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_category` (`id_category`,`id_user`),
  ADD KEY `id_user` (`id_user`),
  ADD KEY `id_album` (`id_album`),
  ADD KEY `id_city` (`id_city`),
  ADD KEY `id_valuta` (`id_valuta`),
  ADD KEY `id_album_2` (`id_album`),
  ADD KEY `id_district` (`id_district`);

--
-- Indexes for table `xta_obj_abuse`
--
ALTER TABLE `xta_obj_abuse`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_obj` (`id_obj`,`id_user`),
  ADD KEY `id_user` (`id_user`);

--
-- Indexes for table `xta_obj_category`
--
ALTER TABLE `xta_obj_category`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_parent` (`id_parent`),
  ADD KEY `id_parent_2` (`id_parent`);

--
-- Indexes for table `xta_obj_category_description`
--
ALTER TABLE `xta_obj_category_description`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_site_2` (`id_site`,`id_obj_category`),
  ADD KEY `id_obj_category` (`id_obj_category`),
  ADD KEY `id_site` (`id_site`);

--
-- Indexes for table `xta_obj_option`
--
ALTER TABLE `xta_obj_option`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_obj_option_type` (`option_type`);

--
-- Indexes for table `xta_obj_option_available`
--
ALTER TABLE `xta_obj_option_available`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_obj_option` (`id_obj_option`);

--
-- Indexes for table `xta_obj_option_in_category`
--
ALTER TABLE `xta_obj_option_in_category`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_obj_option` (`id_obj_option`),
  ADD KEY `id_obj_category` (`id_obj_category`);

--
-- Indexes for table `xta_obj_option_value`
--
ALTER TABLE `xta_obj_option_value`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_obj` (`id_obj`,`id_obj_option`),
  ADD KEY `id_obj_option` (`id_obj_option`);

--
-- Indexes for table `xta_padeg`
--
ALTER TABLE `xta_padeg`
  ADD PRIMARY KEY (`id`),
  ADD KEY `name` (`name`);

--
-- Indexes for table `xta_page`
--
ALTER TABLE `xta_page`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `xta_region`
--
ALTER TABLE `xta_region`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_country` (`id_country`);

--
-- Indexes for table `xta_seo_tdz`
--
ALTER TABLE `xta_seo_tdz`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `rurl` (`rurl`),
  ADD KEY `id_site` (`id_site`);

--
-- Indexes for table `xta_setting`
--
ALTER TABLE `xta_setting`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_setting_category` (`id_setting_category`);

--
-- Indexes for table `xta_setting_category`
--
ALTER TABLE `xta_setting_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `xta_site`
--
ALTER TABLE `xta_site`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_city` (`id_city`),
  ADD KEY `id_city_2` (`id_city`);

--
-- Indexes for table `xta_user`
--
ALTER TABLE `xta_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `name` (`email`,`pass`),
  ADD KEY `id_user_role` (`id_user_role`),
  ADD KEY `id_district` (`id_district`),
  ADD KEY `hash` (`authkey`);

--
-- Indexes for table `xta_user_message`
--
ALTER TABLE `xta_user_message`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_user_from` (`id_user_from`),
  ADD KEY `id_user_to` (`id_user_to`);

--
-- Indexes for table `xta_user_role`
--
ALTER TABLE `xta_user_role`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `xta_valuta`
--
ALTER TABLE `xta_valuta`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `xta_album`
--
ALTER TABLE `xta_album`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `xta_cache`
--
ALTER TABLE `xta_cache`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `xta_cache_in_tag`
--
ALTER TABLE `xta_cache_in_tag`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `xta_cache_tag`
--
ALTER TABLE `xta_cache_tag`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `xta_city`
--
ALTER TABLE `xta_city`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `xta_country`
--
ALTER TABLE `xta_country`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `xta_district`
--
ALTER TABLE `xta_district`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `xta_image`
--
ALTER TABLE `xta_image`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `xta_obj`
--
ALTER TABLE `xta_obj`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `xta_obj_abuse`
--
ALTER TABLE `xta_obj_abuse`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `xta_obj_category`
--
ALTER TABLE `xta_obj_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=82;
--
-- AUTO_INCREMENT for table `xta_obj_category_description`
--
ALTER TABLE `xta_obj_category_description`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `xta_obj_option`
--
ALTER TABLE `xta_obj_option`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `xta_obj_option_available`
--
ALTER TABLE `xta_obj_option_available`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=266;
--
-- AUTO_INCREMENT for table `xta_obj_option_in_category`
--
ALTER TABLE `xta_obj_option_in_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4673;
--
-- AUTO_INCREMENT for table `xta_obj_option_value`
--
ALTER TABLE `xta_obj_option_value`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `xta_padeg`
--
ALTER TABLE `xta_padeg`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `xta_page`
--
ALTER TABLE `xta_page`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `xta_region`
--
ALTER TABLE `xta_region`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `xta_seo_tdz`
--
ALTER TABLE `xta_seo_tdz`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `xta_setting`
--
ALTER TABLE `xta_setting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;
--
-- AUTO_INCREMENT for table `xta_setting_category`
--
ALTER TABLE `xta_setting_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `xta_site`
--
ALTER TABLE `xta_site`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `xta_user`
--
ALTER TABLE `xta_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2633;
--
-- AUTO_INCREMENT for table `xta_user_message`
--
ALTER TABLE `xta_user_message`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `xta_user_role`
--
ALTER TABLE `xta_user_role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `xta_valuta`
--
ALTER TABLE `xta_valuta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `xta_city`
--
ALTER TABLE `xta_city`
  ADD CONSTRAINT `xta_city_ibfk_1` FOREIGN KEY (`id_region`) REFERENCES `xta_region` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `xta_district`
--
ALTER TABLE `xta_district`
  ADD CONSTRAINT `xta_district_ibfk_1` FOREIGN KEY (`id_city`) REFERENCES `xta_city` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `xta_image`
--
ALTER TABLE `xta_image`
  ADD CONSTRAINT `xta_image_ibfk_1` FOREIGN KEY (`id_album`) REFERENCES `xta_album` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `xta_obj`
--
ALTER TABLE `xta_obj`
  ADD CONSTRAINT `xta_obj_ibfk_1` FOREIGN KEY (`id_category`) REFERENCES `xta_obj_category` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `xta_obj_ibfk_2` FOREIGN KEY (`id_user`) REFERENCES `xta_user` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `xta_obj_ibfk_3` FOREIGN KEY (`id_album`) REFERENCES `xta_album` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `xta_obj_ibfk_4` FOREIGN KEY (`id_city`) REFERENCES `xta_city` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `xta_obj_ibfk_5` FOREIGN KEY (`id_valuta`) REFERENCES `xta_valuta` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `xta_obj_category_description`
--
ALTER TABLE `xta_obj_category_description`
  ADD CONSTRAINT `xta_obj_category_description_ibfk_1` FOREIGN KEY (`id_obj_category`) REFERENCES `xta_obj_category` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `xta_obj_category_description_ibfk_2` FOREIGN KEY (`id_site`) REFERENCES `xta_site` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `xta_obj_option_available`
--
ALTER TABLE `xta_obj_option_available`
  ADD CONSTRAINT `xta_obj_option_available_ibfk_1` FOREIGN KEY (`id_obj_option`) REFERENCES `xta_obj_option` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `xta_obj_option_in_category`
--
ALTER TABLE `xta_obj_option_in_category`
  ADD CONSTRAINT `xta_obj_option_in_category_ibfk_1` FOREIGN KEY (`id_obj_option`) REFERENCES `xta_obj_option` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `xta_obj_option_in_category_ibfk_2` FOREIGN KEY (`id_obj_category`) REFERENCES `xta_obj_category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `xta_obj_option_value`
--
ALTER TABLE `xta_obj_option_value`
  ADD CONSTRAINT `xta_obj_option_value_ibfk_1` FOREIGN KEY (`id_obj`) REFERENCES `xta_obj` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `xta_obj_option_value_ibfk_3` FOREIGN KEY (`id_obj_option`) REFERENCES `xta_obj_option` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `xta_region`
--
ALTER TABLE `xta_region`
  ADD CONSTRAINT `xta_region_ibfk_1` FOREIGN KEY (`id_country`) REFERENCES `xta_country` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `xta_seo_tdz`
--
ALTER TABLE `xta_seo_tdz`
  ADD CONSTRAINT `xta_seo_tdz_ibfk_1` FOREIGN KEY (`id_site`) REFERENCES `xta_site` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `xta_setting`
--
ALTER TABLE `xta_setting`
  ADD CONSTRAINT `xta_setting_ibfk_1` FOREIGN KEY (`id_setting_category`) REFERENCES `xta_setting_category` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `xta_site`
--
ALTER TABLE `xta_site`
  ADD CONSTRAINT `xta_site_ibfk_1` FOREIGN KEY (`id_city`) REFERENCES `xta_city` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `xta_user`
--
ALTER TABLE `xta_user`
  ADD CONSTRAINT `xta_user_ibfk_1` FOREIGN KEY (`id_user_role`) REFERENCES `xta_user_role` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `xta_user_message`
--
ALTER TABLE `xta_user_message`
  ADD CONSTRAINT `xta_user_message_ibfk_1` FOREIGN KEY (`id_user_from`) REFERENCES `xta_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `xta_user_message_ibfk_2` FOREIGN KEY (`id_user_to`) REFERENCES `xta_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
